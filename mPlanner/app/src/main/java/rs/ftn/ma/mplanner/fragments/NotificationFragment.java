package rs.ftn.ma.mplanner.fragments;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.RadioButton;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.Arrays;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.adapters.NotificationAdapter;
import rs.ftn.ma.mplanner.interfaces.NotificationCallback;
import rs.ftn.ma.mplanner.model.Notification;
import rs.ftn.ma.mplanner.repo.NotificationRepository;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link NotificationFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NotificationFragment extends Fragment {


    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private ArrayList<Notification> nots;
    private ArrayList<Notification> notsAll;

    private SensorManager sensorManager;
    private Sensor accelerometer;
    private SensorEventListener shakeDetector;
    private final float shakeThreshold = 5.0f;  // Adjust the threshold as needed
    private long lastShakeTime;

    private enum NotificationViewMode {
        ALL, UNREAD, READ
    }

    private NotificationViewMode currentViewMode = NotificationViewMode.ALL;

    public NotificationFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment NotificationFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static NotificationFragment newInstance(String param1, String param2) {
        NotificationFragment fragment = new NotificationFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        nots = new ArrayList<>();
        notsAll = new ArrayList<>();
        View v = inflater.inflate(R.layout.fragment_notification_list, container, false);

        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        FirebaseUser cUser = mAuth.getCurrentUser();

        NotificationRepository nrepo = new NotificationRepository();
        ListView lv = v.findViewById(R.id.notifications_list);

        RadioButton all = v.findViewById(R.id.notif_all_button);
        RadioButton read = v.findViewById(R.id.notif_read_button);
        RadioButton unread = v.findViewById(R.id.notif_unread_button);
        //private NotificationViewMode currentViewMode = NotificationViewMode.ALL;


        // Inicijalizacija SensorManager-a i postavljanje shake detektora
        sensorManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        shakeDetector = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                    float x = event.values[0];
                    float y = event.values[1];
                    float z = event.values[2];

                    float gX = x / SensorManager.GRAVITY_EARTH;
                    float gY = y / SensorManager.GRAVITY_EARTH;
                    float gZ = z / SensorManager.GRAVITY_EARTH;

                    // gForce will be close to 1 when there is no movement
                    float gForce = (float) Math.sqrt(gX * gX + gY * gY + gZ * gZ);

                    if (gForce > shakeThreshold) {
                        final long now = System.currentTimeMillis();
                        if (lastShakeTime + 1000 > now) {
                            return;
                        }
                        lastShakeTime = now;
                        onShake(v);
                    }
                }
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {
                // No need to implement for this use case
            }
        };

        nrepo.selectByFirebaseID(new NotificationCallback() {
            @Override
            public void onNotificationReceived(Notification[] notifications) {
                nots = new ArrayList<>(Arrays.asList(notifications));
                notsAll = new ArrayList<>(Arrays.asList(notifications));
                NotificationAdapter na = new NotificationAdapter(v.getContext(), nots);
                lv.setAdapter(na);
            }

            @Override
            public void onError(Exception e) {

            }
        }, cUser.getUid());

        all.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    NotificationAdapter na = new NotificationAdapter(v.getContext(), notsAll);
                    lv.setAdapter(na);
                }
                else if(read.isChecked())
                {
                    ArrayList<Notification> seenNots = new ArrayList<>();
                    for(Notification n : notsAll)
                        if(n.getSeen())
                            seenNots.add(n);
                    NotificationAdapter na = new NotificationAdapter(v.getContext(), seenNots);
                    lv.setAdapter(na);
                }
                else if(unread.isChecked())
                {
                    ArrayList<Notification> seenNots = new ArrayList<>();
                    for(Notification n : notsAll)
                        if(!n.getSeen())
                            seenNots.add(n);
                    NotificationAdapter na = new NotificationAdapter(v.getContext(), seenNots);
                    lv.setAdapter(na);
                }
            }
        });

        read.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(all.isChecked())
                {
                    NotificationAdapter na = new NotificationAdapter(v.getContext(), notsAll);
                    lv.setAdapter(na);
                }
                else if(isChecked)
                {
                    ArrayList<Notification> seenNots = new ArrayList<>();
                    for(Notification n : notsAll)
                        if(n.getSeen())
                            seenNots.add(n);
                    NotificationAdapter na = new NotificationAdapter(v.getContext(), seenNots);
                    lv.setAdapter(na);
                }
                else if(unread.isChecked())
                {
                    ArrayList<Notification> seenNots = new ArrayList<>();
                    for(Notification n : notsAll)
                        if(!n.getSeen())
                            seenNots.add(n);
                    NotificationAdapter na = new NotificationAdapter(v.getContext(), seenNots);
                    lv.setAdapter(na);
                }
            }
        });

        unread.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(all.isChecked())
                {
                    NotificationAdapter na = new NotificationAdapter(v.getContext(), notsAll);
                    lv.setAdapter(na);
                }
                else if(read.isChecked())
                {
                    ArrayList<Notification> seenNots = new ArrayList<>();
                    for(Notification n : notsAll)
                        if(n.getSeen())
                            seenNots.add(n);
                    NotificationAdapter na = new NotificationAdapter(v.getContext(), seenNots);
                    lv.setAdapter(na);
                }
                else if(isChecked)
                {
                    ArrayList<Notification> seenNots = new ArrayList<>();
                    for(Notification n : notsAll)
                        if(!n.getSeen())
                            seenNots.add(n);
                    NotificationAdapter na = new NotificationAdapter(v.getContext(), seenNots);
                    lv.setAdapter(na);
                }
            }
        });

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        sensorManager.registerListener(shakeDetector, accelerometer, SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    public void onPause() {
        super.onPause();
        sensorManager.unregisterListener(shakeDetector);
    }

    private void onShake(View v) {
        // Rotate through the notification view modes
        switch (currentViewMode) {
            case ALL:
                currentViewMode = NotificationViewMode.UNREAD;
                break;
            case UNREAD:
                currentViewMode = NotificationViewMode.READ;
                break;
            case READ:
                currentViewMode = NotificationViewMode.ALL;
                break;
        }
        updateNotificationView(v);
    }

    private void updateNotificationView(View v) {
        RadioButton all = v.findViewById(R.id.notif_all_button);
        RadioButton read = v.findViewById(R.id.notif_read_button);
        RadioButton unread = v.findViewById(R.id.notif_unread_button);
        ListView lv = v.findViewById(R.id.notifications_list);
        switch (currentViewMode) {
            case ALL:
                all.setChecked(true);
                unread.setChecked(false);
                read.setChecked(false);
                NotificationAdapter na = new NotificationAdapter(v.getContext(), notsAll);
                lv.setAdapter(na);
                break;
            case UNREAD:
                all.setChecked(false);
                unread.setChecked(true);
                read.setChecked(false);
                ArrayList<Notification> useenNots = new ArrayList<>();
                for(Notification n : notsAll)
                    if(!n.getSeen())
                        useenNots.add(n);
                NotificationAdapter na2 = new NotificationAdapter(v.getContext(), useenNots);
                lv.setAdapter(na2);
                break;
            case READ:
                all.setChecked(false);
                unread.setChecked(false);
                read.setChecked(true);
                ArrayList<Notification> seenNots = new ArrayList<>();
                for(Notification n : notsAll)
                    if(n.getSeen())
                        seenNots.add(n);
                NotificationAdapter na3 = new NotificationAdapter(v.getContext(), seenNots);
                lv.setAdapter(na3);
                break;
        }
    }
}