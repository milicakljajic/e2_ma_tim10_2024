package rs.ftn.ma.mplanner.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SearchView;

import com.google.android.material.button.MaterialButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.Arrays;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.adapters.EmployeeAdapter;
import rs.ftn.ma.mplanner.interfaces.EmployeeCallback;
import rs.ftn.ma.mplanner.interfaces.EventProviderCallback;
import rs.ftn.ma.mplanner.model.Employee;
import rs.ftn.ma.mplanner.model.EventProvider;
import rs.ftn.ma.mplanner.repo.EmployeeRepository;
import rs.ftn.ma.mplanner.repo.EventProviderRepository;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EmployeesListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EmployeesListFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private final EmployeeRepository repo;
    private final EventProviderRepository prepo;
    private EventProvider provider;
    private FirebaseAuth mAuth;
    private String compName;


    public EmployeesListFragment() {
        // Required empty public constructor

        repo = new EmployeeRepository();
        prepo = new EventProviderRepository();
        mAuth = FirebaseAuth.getInstance();
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment EmployeesListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EmployeesListFragment newInstance(String param1, String param2) {
        EmployeesListFragment fragment = new EmployeesListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_employees_list, container, false);

        FirebaseUser cUser = mAuth.getCurrentUser();
        prepo.selectByFirebaseId(new EventProviderCallback() {
            @Override
            public void onEventProviderReceived(EventProvider[] providers) {
                provider = providers[0];

                repo.selectByCompany(new EmployeeCallback() {
                    @Override
                    public void onEmployeeRecieved(Employee[] employees) {
                        ListView lw = view.findViewById(R.id.employees_list);
                        SearchView sv = view.findViewById(R.id.employee_list_search);

                        EmployeeAdapter adapter = new EmployeeAdapter(requireContext(), employees);
                        lw.setAdapter(adapter);
                        lw.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                Employee item = employees[position];
                                Bundle bundle = new Bundle();
                                bundle.putString("employeeId", item.getFirebaseUserId());
                                Navigation.findNavController(view).navigate(R.id.profileFragment2, bundle);
                            }
                        });


                        Button button = (Button) view.findViewById(R.id.employee_list_button);
                        button.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                Navigation.findNavController(view).navigate(R.id.changeToAddEmployeeBasic3);
                            }
                        });
                    }

                    @Override
                    public void onError(Exception e) {

                    }
                }, provider.getCompany().getName());
                compName = provider.getCompany().getName();
            }

            @Override
            public void onError(Exception e) {

            }
        }, cUser.getUid());



        //--------------------------------------------------------------------------------------------
        MaterialButton button = (MaterialButton) view.findViewById(R.id.employee_list_search_button);

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                repo.selectByCompany(new EmployeeCallback() {
                    @Override
                    public void onEmployeeRecieved(Employee[] employees) {
                        ListView lw = view.findViewById(R.id.employees_list);
                        SearchView sv = view.findViewById(R.id.employee_list_search);
                        ArrayList<Employee> emps = new ArrayList<>(Arrays.asList(employees));
                        ArrayList<Employee> filter = new ArrayList<>();
                        for(Employee e: emps){
                            if(e.getFirstName().contains(sv.getQuery().toString()) || e.getLastName().contains(sv.getQuery().toString())
                                    ||e.getEmail().contains(sv.getQuery().toString()))
                            {
                                filter.add(e);
                            }
                        }
                        EmployeeAdapter adapter = new EmployeeAdapter(requireContext(), filter);
                        lw.setAdapter(adapter);
                        lw.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                Employee item = employees[position];
                                Bundle bundle = new Bundle();
                                bundle.putString("employeeId", item.getFirebaseUserId());
                                Navigation.findNavController(view).navigate(R.id.profileFragment2, bundle);
                            }
                        });

                        Button button = (Button) view.findViewById(R.id.employee_list_button);
                        button.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View v) {
                                Navigation.findNavController(view).navigate(R.id.changeToAddEmployeeBasic3);
                            }
                        });
                    }

                    @Override
                    public void onError(Exception e) {

                    }
                }, compName);

            }
        });
        //------------------------------------------------------------------------------------------------------------
        return view;
    }
}