package rs.ftn.ma.mplanner.interfaces;

public interface SubcategoryInsertCallback {
    void onSubcategoryInserted(String subcategoryId);
    void onInsertError(Exception e);
}
