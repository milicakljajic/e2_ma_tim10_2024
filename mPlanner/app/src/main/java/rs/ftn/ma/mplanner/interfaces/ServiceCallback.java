package rs.ftn.ma.mplanner.interfaces;

import rs.ftn.ma.mplanner.model.Service;

public interface ServiceCallback {
    void onServiceReceived(Service[] services);
    void onError(Exception ex);
}
