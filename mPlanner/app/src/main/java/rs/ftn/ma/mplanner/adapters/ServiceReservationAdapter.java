package rs.ftn.ma.mplanner.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.model.ServiceReservation;

public class ServiceReservationAdapter extends BaseAdapter {
    Context context;
    ArrayList<ServiceReservation> serviceReservations;
    LayoutInflater inflater;

    Integer fragmentType;
    int toInflate;

    public static final Integer FRAGMENT_TYPE_DEFAULT = 0;
    // Dodajte druge tipove fragmenta ovde ako je potrebno

    public ServiceReservationAdapter(Context applicationContext, ServiceReservation[] serviceReservations, Integer fragmentType) {
        this.context = applicationContext;
        this.serviceReservations = new ArrayList<>(Arrays.asList(serviceReservations));
        this.fragmentType = fragmentType;
        inflater = LayoutInflater.from(applicationContext);
        this.toInflate = -1;
    }

    public ServiceReservationAdapter(Context applicationContext, ArrayList<ServiceReservation> serviceReservations, Integer fragmentType) {
        this.context = applicationContext;
        this.serviceReservations = serviceReservations;
        this.fragmentType = fragmentType;
        inflater = LayoutInflater.from(applicationContext);
        this.toInflate = -1;
    }

    public ServiceReservationAdapter(Context applicationContext, ServiceReservation[] serviceReservations, Integer fragmentType, int layout) {
        this.context = applicationContext;
        this.serviceReservations = new ArrayList<>(Arrays.asList(serviceReservations));
        this.fragmentType = fragmentType;
        inflater = LayoutInflater.from(applicationContext);
        toInflate = layout;
    }

    public ServiceReservationAdapter(Context applicationContext, ArrayList<ServiceReservation> serviceReservations, Integer fragmentType, int layout) {
        this.context = applicationContext;
        this.serviceReservations = serviceReservations;
        this.fragmentType = fragmentType;
        inflater = LayoutInflater.from(applicationContext);
        toInflate = layout;
    }

    @Override
    public int getCount() {
        return serviceReservations.size();
    }

    @Override
    public Object getItem(int i) {
        return serviceReservations.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            if (toInflate != -1) {
                view = inflater.inflate(toInflate, null);
            } else {
                view = inflater.inflate(R.layout.fragment_service_reservation_list_item, null);
            }
        }

        ServiceReservation serviceReservation = serviceReservations.get(i);

        if (Objects.equals(fragmentType, FRAGMENT_TYPE_DEFAULT)) {

           //todo listitem logika ovde
        } else {
            // Implementirajte druge tipove fragmenta ovde
        }

        return view;
    }
}
