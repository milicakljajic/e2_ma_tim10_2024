package rs.ftn.ma.mplanner.interfaces;

import rs.ftn.ma.mplanner.model.EventType;

public interface EventTypeCallback {
    void onEventTypeReceived(EventType[] types);
    void onError(Exception e);
}