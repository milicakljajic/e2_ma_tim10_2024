package rs.ftn.ma.mplanner.repo;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import rs.ftn.ma.mplanner.interfaces.SuggestionCallback;
import rs.ftn.ma.mplanner.interfaces.SuggestionInsertCallback;
import rs.ftn.ma.mplanner.interfaces.SuggestionUpdateCallback;
import rs.ftn.ma.mplanner.model.Suggestion;

public class SuggestionRepository {
    public void insert(Suggestion s, SuggestionInsertCallback callback) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("suggestions")
                .add(s)
                .addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentReference> task) {
                        if (task.isSuccessful()) {
                            DocumentReference documentReference = task.getResult();
                            String sId = documentReference.getId();
                            Log.d("REZ_DB", "DocumentSnapshot added with ID: " + sId);
                            callback.onSuggestionInserted(sId);
                        } else {
                            Log.w("REZ_DB", "Error adding document", task.getException());
                            callback.onInsertError(task.getException());
                        }
                    }
                });
    }


    public void select(SuggestionCallback callback) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("suggestions")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            List<Suggestion> suggestions = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Suggestion suggestion = document.toObject(Suggestion.class);
                                suggestion.setId(document.getId());
                                suggestions.add(suggestion);
                                Log.d("REZ_DB", document.getId() + " => " + document.getData());
                            }
                            callback.onSuggestionReceived(suggestions.toArray(new Suggestion[0]));
                        } else {
                            Log.w("REZ_DB", "Error getting documents.", task.getException());
                            callback.onError(task.getException());
                        }
                    }
                });
    }


    public void update(String id, Suggestion s, SuggestionUpdateCallback callback) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("suggestions").document(id);

        docRef.update("subcategory", s.getSubcategory())
                .addOnSuccessListener(aVoid -> {
                    Log.d("REZ_DB", "Subcategory successfully changed");
                    callback.onSuggestionUpdated();
                })
                .addOnFailureListener(e -> {
                    Log.w("REZ_DB", "Error getting documents.", e);
                    callback.onUpdateError(e);
                });
    }


    public void selectById(SuggestionCallback callback, String id) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("suggestions").document(id);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot documentSnapshot = task.getResult();
                    if (documentSnapshot.exists()) {
                        Suggestion[] s = new Suggestion[]{documentSnapshot.toObject(Suggestion.class)};
                        Log.d("REZ_DB", documentSnapshot.getId() + " => " + documentSnapshot.getData());
                        callback.onSuggestionReceived(s);
                    } else {
                        Log.d("REZ_DB", "No such document");
                        callback.onError(new Exception("No such document"));
                    }
                } else {
                    Log.w("REZ_DB", "Error getting document", task.getException());
                    callback.onError(task.getException());
                }
            }
        });
    }
}
