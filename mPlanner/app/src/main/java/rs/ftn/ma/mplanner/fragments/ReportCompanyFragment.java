package rs.ftn.ma.mplanner.fragments;

import static android.content.ContentValues.TAG;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.model.Company;
import rs.ftn.ma.mplanner.model.EventProvider;
import rs.ftn.ma.mplanner.model.Report;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ReportCompanyFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class ReportCompanyFragment extends Fragment {

    private FirebaseAuth mAuth;

    private FirebaseFirestore firestore;
    private Company reportedCompany;
    private Report report;

    private EditText description;
    private Button reportButton;

    //private NotificationApi apiManager = RetrofitClient.getApi();
    public ReportCompanyFragment() {
        // Required empty public constructor
    }

    public ReportCompanyFragment(Company company){
        this.reportedCompany = company;
    }
    public static ReportCompanyFragment newInstance() {
        ReportCompanyFragment fragment = new ReportCompanyFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        firestore = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_report_company, container, false);

        description = view.findViewById(R.id.report_reason);
        reportButton = view.findViewById(R.id.report_button);

        FirebaseUser user = mAuth.getCurrentUser();

        if(user != null){
            findUserByEmail(user.getEmail(), new OnSuccessListener<EventProvider>() {
                @Override
                public void onSuccess(EventProvider user) {
                    getMaxId(new OnSuccessListener<Long>() {
                        @Override
                        public void onSuccess(Long aLong) {
                            //report = new Report(aLong,user,reportedCompany, LocalDateTime.now().toString(),"","OD");
                        }
                    });
                }
            });
        }
        reportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // report.setRequestDescription(description.getText().toString());
                saveReport(report);
            }
        });

        return view;
    }

    private void findUserByEmail(String email, OnSuccessListener<EventProvider> onSuccessListener) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        CollectionReference usersRef = db.collection("event-providers");

        usersRef.whereEqualTo("email", email)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    for (DocumentSnapshot document : queryDocumentSnapshots.getDocuments()) {
                        EventProvider foundUser = document.toObject(EventProvider.class);
                        onSuccessListener.onSuccess(foundUser);
                        return;
                    }
                    onSuccessListener.onSuccess(null);
                })
                .addOnFailureListener(e -> Log.w(TAG, "Error getting user.", e));
    }

    private void saveReport(Report report){
        firestore.collection("reports")
                .add(report)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        //sendNotificationToAdmin(reportedCompany);
                        requireActivity().getSupportFragmentManager().popBackStack();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }
                });
    }

    public void getMaxId(OnSuccessListener<Long> onSuccessListener) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        CollectionReference collectionRef = db.collection("reports");

        collectionRef.orderBy("reportId", Query.Direction.DESCENDING)
                .limit(1)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    if (!queryDocumentSnapshots.isEmpty()) {
                        // Get the first document in the result
                        DocumentSnapshot document = queryDocumentSnapshots.getDocuments().get(0);
                        Long maxId = document.getLong("reportId");
                        onSuccessListener.onSuccess(maxId + 1);
                    } else {
                        // If the collection is empty, return a default value or handle it appropriately
                        onSuccessListener.onSuccess((long)1);
                    }
                })
                .addOnFailureListener(e -> Log.w("TAG", "Error getting max ID.", e));
    }
}