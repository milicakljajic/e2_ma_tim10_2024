package rs.ftn.ma.mplanner.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.helper.widget.MotionEffect;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.interfaces.NotificationApi;
import rs.ftn.ma.mplanner.model.Notification;
import rs.ftn.ma.mplanner.model.Report;
import rs.ftn.ma.mplanner.model.enums.ReportStatus;
import rs.ftn.ma.mplanner.model.enums.ReservationStatus;
import rs.ftn.ma.mplanner.notifications.NotificationData;
import rs.ftn.ma.mplanner.notifications.PushNotification;
import rs.ftn.ma.mplanner.notifications.RetrofitClient;

public class ReportAdapter extends RecyclerView.Adapter<ReportAdapter.ReportViewHolder> {
    private Context context;
    private List<Report> reportList;

    public FragmentActivity fragmentActivity;
    private NotificationApi apiManager = RetrofitClient.getApi();

    public ReportAdapter(Context context, List<Report> reportList) {
        this.context = context;
        this.reportList = reportList;
        this.fragmentActivity = (FragmentActivity)context;
    }

    @NonNull
    @Override
    public ReportViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_report, parent, false);
        return new ReportAdapter.ReportViewHolder(view, context, reportList);
    }

    @Override
    public void onBindViewHolder(@NonNull ReportViewHolder holder, int position) {
        Report report = reportList.get(position);
        holder.reportSubmitterTextView.setText(report.getRequestMaker());
        holder.odTextView.setText(report.getRequestSubmitter().getFirstName() + " " + report.getRequestSubmitter().getLastName());
        holder.companyTextView.setText(report.getReportedCompany().getName());
        holder.timestampTextView.setText(report.getTimestamp().toString());
        holder.descriptionTextView.setText(report.getRequestDescription());
        holder.reportStatusTextView.setText(report.getReportStatus().toString());
    }

    @Override
    public int getItemCount() {
        return reportList.size();
    }


    private void sendNotification(String email,String body) {
        FirebaseFirestore firestore = FirebaseFirestore.getInstance();
        firestore.collection("users")
                .whereEqualTo("email", email)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    for (DocumentSnapshot document : queryDocumentSnapshots.getDocuments()) {
                        String adminToken = document.getString("token");
                        if (adminToken != null && !adminToken.isEmpty()) {

                            // Pravljanje objekta notifikacije
                            String title = "New notification";


                            Notification notification = new Notification(
                                    body,
                                    "zupermancile@gmail.com", // Sender email
                                    email, // Receiver email (Admin's email)
                                    false
                            );

                            // Čuvanje notifikacije u Firestore kolekciji "notifications"
                            firestore.collection("notifications")
                                    .add(notification)
                                    .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                        @Override
                                        public void onSuccess(DocumentReference documentReference) {
                                            Log.d(MotionEffect.TAG, "Notification added to Firestore");
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Log.e(MotionEffect.TAG, "Error adding notification to Firestore", e);
                                        }
                                    });

                            PushNotification pushNotification = new PushNotification(
                                    new NotificationData(title, body),
                                    adminToken
                            );

                            Call<ResponseBody> call = apiManager.postNotification(pushNotification);
                            call.enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                    if (response.isSuccessful()) {
                                        Log.d(MotionEffect.TAG, "Notification sent");
                                    } else {
                                        String errorMessage = "Error sending notification: ";
                                        if (response.errorBody() != null) {
                                            try {
                                                errorMessage += response.errorBody().string();
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                            }
                                        } else {
                                            errorMessage += response.message();
                                        }
                                        Log.e(MotionEffect.TAG, errorMessage);
                                    }
                                }

                                @Override
                                public void onFailure(Call<ResponseBody> call, Throwable t) {
                                    Log.e(MotionEffect.TAG, "Failed to send notification", t);
                                }
                            });
                        } else {
                            Log.d(MotionEffect.TAG, "Error finding token");
                        }
                    }
                })
                .addOnFailureListener(e -> {
                    Log.e(MotionEffect.TAG, "Error getting users with userType 'A'", e);
                });
    }

    public class ReportViewHolder extends RecyclerView.ViewHolder {

        public TextView odTextView;
        public TextView companyTextView;

        public TextView timestampTextView;

        public TextView descriptionTextView;

        public TextView reportStatusTextView;

        public TextView reportSubmitterTextView;

        public Button acceptButton;
        public Button denyButton;
        public Button odProfileButton;
        public Button companyProfileButton;
        private Context context;
        private List<Report> reportList;

        private FirebaseFirestore firestore;

        public ReportViewHolder(@NonNull View itemView,Context context,List<Report> reportList) {
            super(itemView);
            this.context = context;
            this.reportList = reportList;
            this.firestore = FirebaseFirestore.getInstance();
            odTextView = itemView.findViewById(R.id.report_od);
            companyTextView = itemView.findViewById(R.id.report_company);
            timestampTextView = itemView.findViewById(R.id.report_time);
            descriptionTextView = itemView.findViewById(R.id.report_description);
            reportStatusTextView = itemView.findViewById(R.id.report_status);
            reportSubmitterTextView = itemView.findViewById(R.id.report_submitter);
            acceptButton = itemView.findViewById(R.id.report_accept);
            denyButton = itemView.findViewById(R.id.report_deny);
            odProfileButton = itemView.findViewById(R.id.od_profile);
            companyProfileButton = itemView.findViewById(R.id.company_profile);
            String email = "mihailodjajic44@gmail.com";
            acceptButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAbsoluteAdapterPosition();
                    Report report = reportList.get(position);
                    report.setReportStatus(ReportStatus.ACCEPTED);
                    firestore.collection("reports").whereEqualTo("timestamp",report.getTimestamp())
                            .limit(1)
                            .get()
                            .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                                @Override
                                public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                                    if(!queryDocumentSnapshots.isEmpty()) {
                                        DocumentSnapshot documentSnapshot = queryDocumentSnapshots.getDocuments().get(0);
                                        DocumentReference docRef = documentSnapshot.getReference();
                                        docRef.update("reportStatus",ReportStatus.ACCEPTED)
                                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                    @Override
                                                    public void onSuccess(Void unused) {
                                                        Log.d("REZ_DB", "Report status successfully updated!");
                                                        if(report.getRequestMaker().equals("OD")){
                                                            cancelPUPVReservations(email);
                                                            disableProducts(email);
                                                            fuckHimUp(email);
                                                        }else {
                                                            cancelODReservations(report.getRequestSubmitter().getEmail());
                                                            fuckHimUp(report.getRequestSubmitter().getEmail());
                                                        }
                                                    }
                                                })
                                                .addOnFailureListener(new OnFailureListener() {
                                                    @Override
                                                    public void onFailure(@NonNull Exception e) {
                                                        Log.d("REZ_DB", "Error updating report status", e);
                                                    }
                                                });
                                    }

                                }
                            });
                }
            });

            denyButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAbsoluteAdapterPosition();
                    Report report = reportList.get(position);

                    firestore.collection("reports").whereEqualTo("timestamp",report.getTimestamp())
                            .limit(1)
                            .get()
                            .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                                @Override
                                public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                                    if(!queryDocumentSnapshots.isEmpty()) {
                                        DocumentSnapshot documentSnapshot = queryDocumentSnapshots.getDocuments().get(0);
                                        DocumentReference docRef = documentSnapshot.getReference();
                                        docRef.update("reportStatus",ReportStatus.DENIED)
                                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                    @Override
                                                    public void onSuccess(Void unused) {
                                                        Log.d("REZ_DB", "Report status successfully updated!");
                                                        if(report.getRequestMaker().equals("COMPANY")){
                                                            sendNotification("mihailodjajic44@gmail.com","Your report request has been denied");
                                                        }
                                                        if(report.getRequestMaker().equals("OD")){
                                                            sendNotification(report.getRequestSubmitter().getEmail(),"Your report request has been denied");
                                                        }
                                                    }
                                                })
                                                .addOnFailureListener(new OnFailureListener() {
                                                    @Override
                                                    public void onFailure(@NonNull Exception e) {
                                                        Log.d("REZ_DB", "Error updating report status", e);
                                                    }
                                                });

                                    }

                                }
                            });
                }
            });

            odProfileButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Report report = reportList.get(getAbsoluteAdapterPosition());
                   /* OdProfileFragment odProfileFragment = new OdProfileFragment(report.getRequestSubmitter(),report.getReportedCompany());
                    FragmentTransaction transaction = fragmentActivity.getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.fragment_container, odProfileFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();*/
                }
            });

            companyProfileButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Report report = reportList.get(getAbsoluteAdapterPosition());
                  /*  CompanyInfoODFragment odProfileFragment = new CompanyInfoODFragment(report.getReportedCompany());
                    FragmentTransaction transaction = fragmentActivity.getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.fragment_container, odProfileFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();*/
                }
            });
        }

        public void cancelODReservations(String userEmail) {
            // Get Firestore instance
            FirebaseFirestore firestore = FirebaseFirestore.getInstance();

            // Reference to the "service_reservation" collection
            CollectionReference reservationsRef = firestore.collection("service_reservations");

            // Query for documents where the user's email matches the provided email
            reservationsRef.whereEqualTo("user.email", userEmail)
                    .get()
                    .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                        @Override
                        public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                            if (!queryDocumentSnapshots.isEmpty()) {
                                // Iterate through the documents
                                for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots.getDocuments()) {
                                    DocumentReference docRef = documentSnapshot.getReference();

                                    docRef.update("status", ReservationStatus.CANCELED)
                                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void unused) {
                                                    Log.d("REZ_DB", "Reservation status successfully updated to CANCELED for document ID: " + documentSnapshot.getId());
                                                }
                                            })
                                            .addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(@NonNull Exception e) {
                                                    Log.d("REZ_DB", "Error updating reservation status for document ID: " + documentSnapshot.getId(), e);
                                                }
                                            });
                                }
                            } else {
                                Log.d("REZ_DB", "No matching reservations found for user email: " + userEmail);
                            }
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.d("REZ_DB", "Error querying reservations", e);
                        }
                    });
        }

        public void disableProducts(String userEmail){
            // Get Firestore instance
            FirebaseFirestore firestore = FirebaseFirestore.getInstance();

            // Reference to the "service_reservation" collection
            CollectionReference reservationsRef1 = firestore.collection("products");
            CollectionReference reservationsRef2 = firestore.collection("services");
            CollectionReference reservationsRef3 = firestore.collection("packages");

            // Query for documents where the user's email matches the provided email
            reservationsRef1.whereEqualTo("userPupV.user.email", userEmail)
                    .get()
                    .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                        @Override
                        public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                            if (!queryDocumentSnapshots.isEmpty()) {
                                // Iterate through the documents
                                for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots.getDocuments()) {
                                    DocumentReference docRef = documentSnapshot.getReference();

                                    docRef.update("available", false)
                                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void unused) {
                                                    Log.d("REZ_DB", "Reservation status successfully updated to CANCELED for document ID: " + documentSnapshot.getId());
                                                }
                                            })
                                            .addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(@NonNull Exception e) {
                                                    Log.d("REZ_DB", "Error updating reservation status for document ID: " + documentSnapshot.getId(), e);
                                                }
                                            });
                                }
                            } else {
                                Log.d("REZ_DB", "No matching reservations found for user email: " + userEmail);
                            }
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.d("REZ_DB", "Error querying reservations", e);
                        }
                    });

            reservationsRef2.whereEqualTo("userPupV.user.email", userEmail)
                    .get()
                    .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                        @Override
                        public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                            if (!queryDocumentSnapshots.isEmpty()) {
                                // Iterate through the documents
                                for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots.getDocuments()) {
                                    DocumentReference docRef = documentSnapshot.getReference();

                                    docRef.update("available", false)
                                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void unused) {
                                                    Log.d("REZ_DB", "Reservation status successfully updated to CANCELED for document ID: " + documentSnapshot.getId());
                                                }
                                            })
                                            .addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(@NonNull Exception e) {
                                                    Log.d("REZ_DB", "Error updating reservation status for document ID: " + documentSnapshot.getId(), e);
                                                }
                                            });
                                }
                            } else {
                                Log.d("REZ_DB", "No matching reservations found for user email: " + userEmail);
                            }
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.d("REZ_DB", "Error querying reservations", e);
                        }
                    });

            reservationsRef3.whereEqualTo("userPupV.user.email", userEmail)
                    .get()
                    .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                        @Override
                        public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                            if (!queryDocumentSnapshots.isEmpty()) {
                                // Iterate through the documents
                                for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots.getDocuments()) {
                                    DocumentReference docRef = documentSnapshot.getReference();

                                    docRef.update("available", false)
                                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void unused) {
                                                    Log.d("REZ_DB", "Reservation status successfully updated to CANCELED for document ID: " + documentSnapshot.getId());
                                                }
                                            })
                                            .addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(@NonNull Exception e) {
                                                    Log.d("REZ_DB", "Error updating reservation status for document ID: " + documentSnapshot.getId(), e);
                                                }
                                            });
                                }
                            } else {
                                Log.d("REZ_DB", "No matching reservations found for user email: " + userEmail);
                            }
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.d("REZ_DB", "Error querying reservations", e);
                        }
                    });
        }

        public void cancelPUPVReservations(String userEmail) {
            // Get Firestore instance
            FirebaseFirestore firestore = FirebaseFirestore.getInstance();

            // Reference to the "service_reservation" collection
            CollectionReference reservationsRef = firestore.collection("service_reservations");

            // Query for documents where the user's email matches the provided email
            reservationsRef.whereEqualTo("service.userPupV.user.email", userEmail)
                    .get()
                    .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                        @Override
                        public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                            if (!queryDocumentSnapshots.isEmpty()) {
                                // Iterate through the documents
                                for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots.getDocuments()) {
                                    DocumentReference docRef = documentSnapshot.getReference();
                                    // Update the 'status' field to 'CANCELED'
                                    String odEmail = documentSnapshot.getString("user.email");
                                    docRef.update("status", ReservationStatus.CANCELED)
                                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void unused) {
                                                    sendNotification(odEmail,"Some of your reservations have been canceled");
                                                    Log.d("REZ_DB", "Reservation status successfully updated to CANCELED for document ID: " + documentSnapshot.getId());
                                                }
                                            })
                                            .addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(@NonNull Exception e) {
                                                    Log.d("REZ_DB", "Error updating reservation status for document ID: " + documentSnapshot.getId(), e);
                                                }
                                            });
                                }
                            } else {
                                Log.d("REZ_DB", "No matching reservations found for user email: " + userEmail);
                            }
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.d("REZ_DB", "Error querying reservations", e);
                        }
                    });
        }

        private void fuckHimUp(String email){
            firestore.collection("users")
                    .whereEqualTo("email",email)
                    .limit(1)
                    .get()
                    .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                        @Override
                        public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                            if(!queryDocumentSnapshots.isEmpty()) {
                                DocumentSnapshot documentSnapshot = queryDocumentSnapshots.getDocuments().get(0);
                                DocumentReference docRef = documentSnapshot.getReference();
                                docRef.update("deactivated",true)
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void unused) {
                                                Log.d("REZ_DB","User deactivated");
                                            }
                                        });
                            }
                        }
                    });
        }
    }
}
