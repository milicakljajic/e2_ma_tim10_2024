package rs.ftn.ma.mplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class PackageBundle implements Parcelable {

    private Long id;
    private String name;
    private String description;
    private List<Integer> images;
    private double price;
    private double discount;
    private boolean available;
    private boolean visible;
    private Category category;
    private List<Product> products;
    private List<Service> services;
    private List<Subcategory> subcategories;
    private List<EventType> eventTypes;
    private double cancellationDeadline;
    private double reservationDeadline;
    private String reservationMethod;
    private Boolean deleted;
    private double discountedPrice;

    public PackageBundle(){}

    public PackageBundle(Long id, String name, String description, List<Integer> images, double price, double discount, boolean available, boolean visible, Category category, List<Product> products, List<Service> services, List<Subcategory> subcategories, List<EventType> eventTypes, double cancellationDeadline, double reservationDeadline, String reservationMethod, Boolean deleted, double discountedPrice) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.images = images;
        this.price = price;
        this.discount = discount;
        this.available = available;
        this.visible = visible;
        this.category = category;
        this.products = products;
        this.services = services;
        this.subcategories = subcategories;
        this.eventTypes = eventTypes;
        this.cancellationDeadline = cancellationDeadline;
        this.reservationDeadline = reservationDeadline;
        this.reservationMethod = reservationMethod;
        this.deleted = deleted;
        this.discountedPrice = discountedPrice;
    }

    protected PackageBundle(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readLong();
        }
        name = in.readString();
        description = in.readString();
        price = in.readDouble();
        discount = in.readDouble();
        available = in.readByte() != 0;
        visible = in.readByte() != 0;
        category = in.readParcelable(Category.class.getClassLoader());
        products = in.createTypedArrayList(Product.CREATOR);
        services = in.createTypedArrayList(Service.CREATOR);
        subcategories = in.createTypedArrayList(Subcategory.CREATOR);
        eventTypes = in.createTypedArrayList(EventType.CREATOR);
        cancellationDeadline = in.readDouble();
        reservationDeadline = in.readDouble();
        reservationMethod = in.readString();
        byte tmpDeleted = in.readByte();
        deleted = tmpDeleted == 0 ? null : tmpDeleted == 1;
        discountedPrice = in.readDouble();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(id);
        }
        dest.writeString(name);
        dest.writeString(description);
        dest.writeDouble(price);
        dest.writeDouble(discount);
        dest.writeByte((byte) (available ? 1 : 0));
        dest.writeByte((byte) (visible ? 1 : 0));
        dest.writeParcelable(category, flags);
        dest.writeTypedList(products);
        dest.writeTypedList(services);
        dest.writeTypedList(subcategories);
        dest.writeTypedList(eventTypes);
        dest.writeDouble(cancellationDeadline);
        dest.writeDouble(reservationDeadline);
        dest.writeString(reservationMethod);
        dest.writeByte((byte) (deleted == null ? 0 : deleted ? 1 : 2));
        dest.writeDouble(discountedPrice);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PackageBundle> CREATOR = new Creator<PackageBundle>() {
        @Override
        public PackageBundle createFromParcel(Parcel in) {
            return new PackageBundle(in);
        }

        @Override
        public PackageBundle[] newArray(int size) {
            return new PackageBundle[size];
        }
    };

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Integer> getImages() {
        return images;
    }

    public void setImages(List<Integer> images) {
        this.images = images;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

    public List<Subcategory> getSubcategories() {
        return subcategories;
    }

    public void setSubcategories(List<Subcategory> subcategories) {
        this.subcategories = subcategories;
    }

    public List<EventType> getEventTypes() {
        return eventTypes;
    }

    public void setEventTypes(List<EventType> eventTypes) {
        this.eventTypes = eventTypes;
    }

    public double getCancellationDeadline() {
        return cancellationDeadline;
    }

    public void setCancellationDeadline(double cancellationDeadline) {
        this.cancellationDeadline = cancellationDeadline;
    }

    public double getReservationDeadline() {
        return reservationDeadline;
    }

    public void setReservationDeadline(double reservationDeadline) {
        this.reservationDeadline = reservationDeadline;
    }

    public String getReservationMethod() {
        return reservationMethod;
    }

    public void setReservationMethod(String reservationMethod) {
        this.reservationMethod = reservationMethod;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public double getDiscountedPrice() {
        return discountedPrice;
    }

    public void setDiscountedPrice(double discountedPrice) {
        this.discountedPrice = discountedPrice;
    }
}
