package rs.ftn.ma.mplanner.fragments;

import android.content.ContentValues;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.os.Build;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Nonnull;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.interfaces.ActivityCallback;
import rs.ftn.ma.mplanner.interfaces.ActivityInsertCallback;
import rs.ftn.ma.mplanner.interfaces.EventCallback;
import rs.ftn.ma.mplanner.model.Activity;
import rs.ftn.ma.mplanner.model.Event;
import rs.ftn.ma.mplanner.repo.ActivityRepository;
import rs.ftn.ma.mplanner.repo.EventRepository;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EventAgendaFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EventAgendaFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private Spinner spinnerEvents;
    private EditText etActivityName, etActivityDescription, etActivityFromTime, etActivityToTime, etActivityLocation;
    private Button btnAddActivity, btnGeneratePdf;
    private List<Event> eventList = new ArrayList<>();

    private Activity newActivity;
    private DatabaseReference databaseReference;

    public EventAgendaFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment EventAgendaFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EventAgendaFragment newInstance(String param1, String param2) {
        EventAgendaFragment fragment = new EventAgendaFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        this.newActivity = new Activity();
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(@Nonnull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_event_agenda, container, false);

        spinnerEvents = view.findViewById(R.id.spinner_events);
        etActivityName = view.findViewById(R.id.et_activity_name);
        etActivityDescription = view.findViewById(R.id.et_activity_description);
        etActivityFromTime = view.findViewById(R.id.et_activity_from_time);
        etActivityToTime = view.findViewById(R.id.et_activity_to_time);
        etActivityLocation = view.findViewById(R.id.et_activity_location);
        btnAddActivity = view.findViewById(R.id.btn_add_activity);
        btnGeneratePdf = view.findViewById(R.id.btn_generate_pdf);

        databaseReference = FirebaseDatabase.getInstance().getReference("events");

        loadEvents(view);

        spinnerEvents.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Event e = (Event) parent.getItemAtPosition(position);
                if(!e.getName().equals("--"))
                    newActivity.setSelectedEvent(e);
                // Dodaj dodatne logike ovde ako je potrebno
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Prazno telo metode - možeš dodati logiku ako je potrebno
            }
        });

        btnAddActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addActivity();
            }
        });

        btnGeneratePdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generatePdf();
            }
        });

        return view;
    }

    private void loadEvents(View v) {

        EventRepository erepo = new EventRepository();
        Event defaultEv = new Event();
        defaultEv.setName("--");
        eventList.add(defaultEv);
        erepo.select(new EventCallback() {
            @Override
            public void onEventReceived(Event[] events) {
                eventList.addAll(Arrays.asList(events));
                ArrayAdapter<Event> adapter = new ArrayAdapter<>(v.getContext(), android.R.layout.simple_spinner_item, eventList);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerEvents.setAdapter(adapter);
            }

            @Override
            public void onError(Exception e) {

            }
        }, false);
    }

    private void addActivity() {
        Event selectedEvent = (Event) spinnerEvents.getSelectedItem();
       /* if (selectedEvent == null) {
            Toast.makeText(getContext(), "Please select an event", Toast.LENGTH_SHORT).show();
            return;
        }*/

        String name = etActivityName.getText().toString();
        String description = etActivityDescription.getText().toString();
        String fromTime = etActivityFromTime.getText().toString();
        String toTime = etActivityToTime.getText().toString();
        String location = etActivityLocation.getText().toString();

        if (name.isEmpty() || description.isEmpty() || fromTime.isEmpty() || toTime.isEmpty() || location.isEmpty()) {
            Toast.makeText(getContext(), "Please fill all fields", Toast.LENGTH_SHORT).show();
            return;
        }

        Activity activity = new Activity("0", name, description, fromTime, toTime, location);
        //Jedan actvitivty ima jedan event
        activity.setSelectedEvent(newActivity.getSelectedEvent());
        //selectedEvent.getActivities().add(activity);

        // Update Firebase database
        ActivityRepository repo = new ActivityRepository();

        repo.create(activity, new ActivityInsertCallback() {
            @Override
            public void onActivityCreated(String activityId) {
                Toast.makeText(getContext(), "Activity added", Toast.LENGTH_SHORT).show();

                // Clear fields
                etActivityName.setText("");
                etActivityDescription.setText("");
                etActivityFromTime.setText("");
                etActivityToTime.setText("");
                etActivityLocation.setText("");
            }

            @Override
            public void onCreateError(Exception e) {

            }
        });


    }

    private void generatePdf() {
        Event selectedEvent = (Event) spinnerEvents.getSelectedItem();
        if (selectedEvent == null) {
            Toast.makeText(getContext(), "Please select an event", Toast.LENGTH_SHORT).show();
            return;
        }





        // Write activities to the PDF


        ActivityRepository arepo = new ActivityRepository();

        arepo.getAllByEName(newActivity.getSelectedEvent().getName(), new ActivityCallback() {
            @Override
            public void onActivityReceived(Activity[] activities) {
                // Create a new PDF document
                PdfDocument document = new PdfDocument();
                PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(300, 600, 1).create();
                PdfDocument.Page page = document.startPage(pageInfo);
                int y = 25;
                Paint paint = new Paint();
                paint.setColor(Color.BLACK); // Podesi boju teksta
                paint.setTextSize(12); // Podesi veličinu teksta
                paint.setTextAlign(Paint.Align.LEFT); // Podesi poravnanje teksta
                //Guest guest : currentEvent.getGuestList
                for (Activity activity : activities) {
                    String text = activity.getName() + " - " + activity.getDescription() + " - " + activity.getStartTime() + " to " + activity.getEndTime() + " at " + activity.getLocation();
                    page.getCanvas().drawText(text, 10, y, paint);
                    y += 25;
                }

                document.finishPage(page);

                // Save the PDF to the device
                String fileName = "Event_Agenda.pdf";
                OutputStream fos;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, fileName);
                    contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "application/pdf");
                    contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_DOWNLOADS);

                    try {
                        fos = getContext().getContentResolver().openOutputStream(
                                getContext().getContentResolver().insert(MediaStore.Downloads.EXTERNAL_CONTENT_URI, contentValues));
                    } catch (FileNotFoundException e) {
                        throw new RuntimeException(e);
                    }
                } else {
                    File downloadsDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                    File file = new File(downloadsDir, fileName);
                    try {
                        fos = new FileOutputStream(file);
                    } catch (FileNotFoundException e) {
                        throw new RuntimeException(e);
                    }
                }

                try {
                    document.writeTo(fos);
                    fos.close();
                    Toast.makeText(getContext(), "PDF saved in Downloads", Toast.LENGTH_SHORT).show();
                    Log.d("", "PDF saved in Downloads");
                } catch (IOException e) {
                    Log.e("", "Error generating PDF", e);
                    Toast.makeText(getContext(), "Error generating PDF", Toast.LENGTH_SHORT).show();
                }

                document.close();
            }

            @Override
            public void onError(Exception e) {

            }
        });


    }
}
