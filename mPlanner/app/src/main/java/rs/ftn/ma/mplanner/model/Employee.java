package rs.ftn.ma.mplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Employee implements Parcelable {
    private String id;
    private String firebaseUserId;
    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private String address;
    private String phoneNumber;
    private Schedule schedule;

    //TODO prebaci u blob kasnije
    private String photoUrl;

    private Company company;
    private boolean isEnabled;

    private ArrayList<Event> scheduledEvents;

    public Employee() {
        company = new Company();
        schedule = new Schedule();
        scheduledEvents = new ArrayList<>();
    }

    public Employee(String id, String firebaseUserId, String email, String password, String firstName, String lastName, String address, String phoneNumber, Schedule schedule, String photoUrl, Company company, boolean isEnabled, ArrayList<Event> scheduledEvents) {
        this.id = id;
        this.firebaseUserId = firebaseUserId;
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.schedule = schedule;
        this.photoUrl = photoUrl;
        this.company = company;
        this.isEnabled = isEnabled;
        this.scheduledEvents = scheduledEvents;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirebaseUserId() {
        return firebaseUserId;
    }

    public void setFirebaseUserId(String firebaseUserId) {
        this.firebaseUserId = firebaseUserId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Schedule getSchedule() {
        if(schedule == null)
            schedule = new Schedule();
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public boolean getIsEnabled() {
        return isEnabled;
    }

    public void setIsEnabled(boolean enabled) {
        isEnabled = enabled;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public ArrayList<Event> getScheduledEvents() {
        return scheduledEvents;
    }

    public void setScheduledEvents(ArrayList<Event> scheduledEvents) {
        this.scheduledEvents = scheduledEvents;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.firebaseUserId);
        dest.writeString(this.email);
        dest.writeString(this.password);
        dest.writeString(this.firstName);
        dest.writeString(this.lastName);
        dest.writeString(this.address);
        dest.writeString(this.phoneNumber);
        dest.writeParcelable(this.schedule, flags);
        dest.writeString(this.photoUrl);
        dest.writeParcelable(this.company, flags);
        dest.writeByte(this.isEnabled ? (byte) 1 : (byte) 0);
        dest.writeList(this.scheduledEvents);
    }

    public void readFromParcel(Parcel source) {
        this.id = source.readString();
        this.firebaseUserId = source.readString();
        this.email = source.readString();
        this.password = source.readString();
        this.firstName = source.readString();
        this.lastName = source.readString();
        this.address = source.readString();
        this.phoneNumber = source.readString();
        this.schedule = source.readParcelable(Schedule.class.getClassLoader());
        this.photoUrl = source.readString();
        this.company = source.readParcelable(Company.class.getClassLoader());
        this.isEnabled = source.readByte() != 0;
        this.scheduledEvents = new ArrayList<Event>();
        source.readList(this.scheduledEvents, Event.class.getClassLoader());
    }

    protected Employee(Parcel in) {
        this.id = in.readString();
        this.firebaseUserId = in.readString();
        this.email = in.readString();
        this.password = in.readString();
        this.firstName = in.readString();
        this.lastName = in.readString();
        this.address = in.readString();
        this.phoneNumber = in.readString();
        this.schedule = in.readParcelable(Schedule.class.getClassLoader());
        this.photoUrl = in.readString();
        this.company = in.readParcelable(Company.class.getClassLoader());
        this.isEnabled = in.readByte() != 0;
        this.scheduledEvents = new ArrayList<Event>();
        in.readList(this.scheduledEvents, Event.class.getClassLoader());
    }

    public static final Creator<Employee> CREATOR = new Creator<Employee>() {
        @Override
        public Employee createFromParcel(Parcel source) {
            return new Employee(source);
        }

        @Override
        public Employee[] newArray(int size) {
            return new Employee[size];
        }
    };
}
