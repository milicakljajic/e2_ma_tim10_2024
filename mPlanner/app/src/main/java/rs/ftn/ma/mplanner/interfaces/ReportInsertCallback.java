package rs.ftn.ma.mplanner.interfaces;

public interface ReportInsertCallback {
    void onReportInserted(String reportId);
    void onInsertError(Exception e);
}
