package rs.ftn.ma.mplanner.fragments;

import static rs.ftn.ma.mplanner.adapters.ProductAdapter.FRAGMENT_TYPE_PRODUCT_LIST;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.adapters.ProductAdapter;
import rs.ftn.ma.mplanner.model.Product;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BudgetPlanFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BudgetPlanFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "SentProducts";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private ArrayList<Product> mParam1;
    private String mParam2;

    public BudgetPlanFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BudgetPlanFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BudgetPlanFragment newInstance(ArrayList<Product> param1, String param2) {
        BudgetPlanFragment fragment = new BudgetPlanFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getParcelableArrayList(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_budget_plan, container, false);

        Spinner spinner = (Spinner) view.findViewById(R.id.product_service_subcategory_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                requireContext(),
                R.array.product_service_subcategory_array,
                android.R.layout.simple_spinner_item
        );

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        TextView button = (TextView) view.findViewById(R.id.details_budget);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Navigation.findNavController(view).navigate(R.id.changeToDetailsBudgetFragment);
            }
        });

        if(mParam1 != null)
        {
            ListView lv = view.findViewById(R.id.sent_products_listview);
            ProductAdapter ad = new ProductAdapter(view.getContext(), mParam1, FRAGMENT_TYPE_PRODUCT_LIST);
            TextView t = view.findViewById(R.id.send_product_total);
            Double totalPrice = 0.0;
            for(Product p : mParam1) {
                totalPrice += p.getPrice();
            }
            t.setText(totalPrice.toString());
            lv.setAdapter(ad);
        }

        return view;
    }
}