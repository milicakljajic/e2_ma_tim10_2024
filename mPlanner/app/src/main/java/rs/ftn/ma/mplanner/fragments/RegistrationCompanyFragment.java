package rs.ftn.ma.mplanner.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.model.EventProvider;

public class RegistrationCompanyFragment extends Fragment {

    private static final String ARG_PARAM1 = "EventProvider";

    private EventProvider newEventProvider;


    public RegistrationCompanyFragment() {
        // Required empty public constructor
    }


    public static RegistrationCompanyFragment newInstance(EventProvider newEventProvider) {
        RegistrationCompanyFragment fragment = new RegistrationCompanyFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, newEventProvider);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if(getArguments().getParcelable(ARG_PARAM1) != null)
                newEventProvider = getArguments().getParcelable(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_registration_company, container, false);
        //Button button = (Button) view.findViewById(R.id.login_registration);
        Button button = (Button) view.findViewById(R.id.registration_company_button);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                EditText name = view.findViewById(R.id.registration_company_name);
                String nameText = String.valueOf(name.getText());
                EditText email = view.findViewById(R.id.registration_company_email);
                String emailText = String.valueOf(email.getText());
                EditText address = view.findViewById(R.id.registration_company_address);
                String addressText = String.valueOf(address.getText());
                EditText phone = view.findViewById(R.id.registration_company_phone);
                String phoneText = String.valueOf(phone.getText());
                EditText about = view.findViewById(R.id.registration_company_about);
                String aboutText = String.valueOf(about.getText());
                if(newEventProvider != null) {
                    newEventProvider.getCompany().setName(nameText);
                    newEventProvider.getCompany().setEmail(emailText);
                    newEventProvider.getCompany().setAddress(addressText);
                    newEventProvider.getCompany().setPhone(phoneText);
                    newEventProvider.getCompany().setAbout(aboutText);
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("EventProvider", newEventProvider);
                    Navigation.findNavController(view).navigate(R.id.changeToRegistrationCategory1, bundle);
                }

            }
        });

        return view;
    }
}