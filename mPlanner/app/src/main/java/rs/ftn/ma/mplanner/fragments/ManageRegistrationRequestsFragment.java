package rs.ftn.ma.mplanner.fragments;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TimePicker;

import com.google.android.material.button.MaterialButton;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.adapters.EventProviderAdapter;
import rs.ftn.ma.mplanner.adapters.EventTypeAdapter;
import rs.ftn.ma.mplanner.adapters.ProductAdapter;
import rs.ftn.ma.mplanner.adapters.ServiceAdapter;
import rs.ftn.ma.mplanner.interfaces.EventProviderCallback;
import rs.ftn.ma.mplanner.model.EventProvider;
import rs.ftn.ma.mplanner.repo.EventProviderRepository;
import rs.ftn.ma.mplanner.repo.EventTypeRepository;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ManageRegistrationRequestsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ManageRegistrationRequestsFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private ProductAdapter productAdapter;
    private ServiceAdapter serviceAdapter;

    private EventTypeAdapter eventTypeAdapter;

    private EventProviderAdapter eventProviderAdapter;

    private EventTypeRepository eventtyperepo;

    private EventProviderRepository eventproviderrepo;

    private EventProvider searchProvider;

    private String from;
    private String to;
    private String fromDate;
    private String toDate;

    private ArrayList<EventProvider> prs;


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public ManageRegistrationRequestsFragment() {
        // Required empty public constructor
        eventtyperepo = new EventTypeRepository();
        eventproviderrepo = new EventProviderRepository();
        searchProvider = new EventProvider();
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ManageRegistrationRequestsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ManageRegistrationRequestsFragment newInstance(String param1, String param2) {
        ManageRegistrationRequestsFragment fragment = new ManageRegistrationRequestsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_manage_registration_request_list, container, false);
        prs = new ArrayList<>();
        //setupSpinnersAndListViews(view);

        //sati i minute ------------------------
        Button buttonFrom = view.findViewById(R.id.manage_registration_requests_start_time);
        buttonFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mHour = c.get(Calendar.HOUR_OF_DAY);
                int mMinute = c.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(requireContext(), new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        //text.setText(hourOfDay);
                        String timeFrom = "";
                        if(hourOfDay < 10)
                            timeFrom = "0"+hourOfDay;
                        else
                            timeFrom += hourOfDay;
                        if(minute < 10)
                            timeFrom += ":0" + minute;
                        else
                            timeFrom += ":" + minute;
                        from = String.valueOf(timeFrom);
                    }
                }, mHour, mMinute, true);
                timePickerDialog.show();
            }
        });

        Button buttonTo = view.findViewById(R.id.manage_registration_requests_end_time);
        buttonTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mHour = c.get(Calendar.HOUR_OF_DAY);
                int mMinute = c.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(requireContext(), new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        //text.setText(hourOfDay);
                        String timeFrom = "";
                        if(hourOfDay < 10)
                            timeFrom = "0" + hourOfDay;
                        else
                            timeFrom +=  hourOfDay;
                        if(minute < 10)
                            timeFrom += ":0" + minute;
                        else
                            timeFrom += ":" + minute;
                        to = String.valueOf(timeFrom);
                    }
                }, mHour, mMinute, true);
                timePickerDialog.show();
            }
        });
        //sati i minute - end ------------------


        //datumi from i to ------------------------
        Button buttonStartDate = view.findViewById(R.id.manage_registration_requests_from_date);
        buttonStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(requireContext(),new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        month += 1;
                        String s = String.valueOf(dayOfMonth) + "." + String.valueOf(month) + "." + String.valueOf(year);
                        //text.setText(String.valueOf(year));
                        fromDate = s;
                    }
                }, year, month, 15);
                datePickerDialog.show();
            }
        });

        Button buttonStartEnd = view.findViewById(R.id.manage_registration_requests_to_date);
        buttonStartEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(requireContext(),new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        month += 1;
                        String s = String.valueOf(dayOfMonth) + "." + String.valueOf(month) + "." + String.valueOf(year);
                        //text.setText(String.valueOf(year));
                        toDate = s;
                    }
                }, year, month, 15);
                datePickerDialog.show();
            }
        });
        //datumi from i to - end ------------------

        MaterialButton filter = view.findViewById(R.id.manage_registration_requests_filter_button);
        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                eventproviderrepo.selectRequests(new EventProviderCallback() {
                    @Override
                    public void onEventProviderReceived(EventProvider[] providers) {
                        // Define the formatter for the input string
                        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy hh:mm");
                        prs = new ArrayList<>(Arrays.asList(providers));
                        // Parse the string to LocalDateTime
                        // Split the date string and time string
                        String[] dateComponents = fromDate.split("\\.");
                        String[] timeComponents = from.split(":");

                        // Convert strings to integers
                        int year = Integer.parseInt(dateComponents[2]);
                        int month = Integer.parseInt(dateComponents[1]);
                        int dayOfMonth = Integer.parseInt(dateComponents[0]);
                        int hour = Integer.parseInt(timeComponents[0]);
                        int minute = Integer.parseInt(timeComponents[1]);

                        // Create LocalDateTime object
                        LocalDateTime localDateTimeFrom = LocalDateTime.of(year, month, dayOfMonth, hour, minute);
                        // Example strings
                        // Split the date string and time string
                        dateComponents = toDate.split("\\.");
                        timeComponents = to.split(":");

                        // Convert strings to integers
                        year = Integer.parseInt(dateComponents[2]);
                        month = Integer.parseInt(dateComponents[1]);
                        dayOfMonth = Integer.parseInt(dateComponents[0]);
                        hour = Integer.parseInt(timeComponents[0]);
                        minute = Integer.parseInt(timeComponents[1]);

                        // Create LocalDateTime object
                        LocalDateTime localDateTimeTo = LocalDateTime.of(year, month, dayOfMonth, hour, minute);

                        ArrayList<EventProvider> fil = new ArrayList<>();
                        for(EventProvider e : prs) {
                            LocalDateTime stamp = e.getLastModified().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
                            if(stamp.isAfter(localDateTimeFrom) && stamp.isBefore(localDateTimeTo))
                                fil.add(e);
                        }
                        prs = fil;
                        ListView reqlv = view.findViewById(R.id.registration_request_list);
                        //setupRequestListview(view, reqlv, prs);
                    }

                    @Override
                    public void onError(Exception e) {

                    }
                }, searchProvider);

            }
        });

        MaterialButton reset = view.findViewById(R.id.manage_registration_requests_reset_button);
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eventproviderrepo.selectRequests(new EventProviderCallback() {
                    @Override
                    public void onEventProviderReceived(EventProvider[] providers) {
                        ArrayList<EventProvider> reqs = new ArrayList<>(Arrays.asList(providers));
                        prs = reqs;
                        ListView reqlv = view.findViewById(R.id.registration_request_list);
                        //setupRequestListview(view, reqlv, reqs);
                    }

                    @Override
                    public void onError(Exception e) {

                    }
                }, searchProvider);
            }
        });

        MaterialButton sear = view.findViewById(R.id.manage_request_search_button);
        SearchView s = view.findViewById(R.id.manage_request_search);
        sear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        return view;
    }


  /*  private void setupSpinnersAndListViews(View view) {

        //Deo za proizvode---------------------------------------------------------------------------------------------------
        //TODO OVO DOBAVITI PREKO REPO KADA URADE
        //Popunjavanje spinnera
        Product[] products = new Product[]{
                new Product(-1, "-", "", 0.0, "", "", 0, false),
                new Product(1, "Camera", "Professional DSLR camera for photography enthusiasts", 899.99, "Electronics", "Cameras", 0, true),
                new Product(2, "Tripod", "Sturdy tripod for stable photography shots", 49.99, "Accessories", "Tripods", 0, true)};

        ListView listViewProduct = view.findViewById(R.id.reg_req_product_category_lv);
        Spinner spinnerProduct = view.findViewById(R.id.reg_req_product_spinner);
        ArrayAdapter<Product> spinnerProductAdapter = new ArrayAdapter<Product>(
                requireContext(),
                android.R.layout.simple_spinner_item,
                products
        );
        spinnerProductAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerProduct.setAdapter(spinnerProductAdapter);

        //Spiner kada se odabere proizvod da doda u listViewProduct
        final ListView finalLv = listViewProduct;
        spinnerProduct.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                Product selectedProduct = (Product) parent.getItemAtPosition(position);
                if(!searchProvider.containsProduct(selectedProduct) && selectedProduct.getId() != -1) {
                    searchProvider.addProduct(selectedProduct);
                    setupProductListView(view, finalLv);
                }
            }
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });
        if(!this.searchProvider.getProvidedProducts().isEmpty()) {
            setupProductListView(view, listViewProduct);
        }

        //DEO ZA SERVISE---------------------------------------------------------------------------------------------------
        //TODO OVO DOBAVITI PREKO REPO KADA URADE
        Service[] services = new Service[]{
                new Service(-1,"-", "", "", "", 0, 0.10),
                new Service(1,"Service Name", "Service Description", "Category", "Subcategory", 100, 0.10),
                new Service(2,"Photo Service", "Professional photos for homes", "Home Services", "Photo", 80, 0.05)};

        ListView listViewService = view.findViewById(R.id.reg_req_service_category_lv);
        Spinner spinnerService = view.findViewById(R.id.reg_req_service_spinner);
        ArrayAdapter<Service> adapterService = new ArrayAdapter<Service>(
                requireContext(),
                android.R.layout.simple_spinner_item,
                services
        );
        adapterService.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerService.setAdapter(adapterService);

        //kada se klikne na elem iz spinner a da nije onaj '-' dodaj ga u lv
        spinnerService.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                Service selectedService = (Service) parent.getItemAtPosition(position);
                if(!searchProvider.containsService(selectedService) && selectedService.getId() != -1) {
                    searchProvider.addService(selectedService);
                    setupServiceListView(view, listViewService);
                }
            }
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });
        setupServiceListView(view, listViewService);

        //DEO ZA EVENTTYPE---------------------------------------------------------------------------------------------------
        //TODO OVO DOBAVITI PREKO REPO KADA URADE

        eventtyperepo.select(new EventTypeCallback() {
            @Override
            public void onEventTypeReceived(EventType[] types) {
                ListView listViewEventType = view.findViewById(R.id.reg_req_event_type_lv);
                Spinner spinnerEventType = view.findViewById(R.id.reg_req_eventtype_spinner);
                ArrayAdapter<EventType> adapterEventType = new ArrayAdapter<EventType>(
                        requireContext(),
                        android.R.layout.simple_spinner_item,
                        types
                );
                adapterEventType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerEventType.setAdapter(adapterEventType);

                //kada se klikne na elem iz spinner a da nije onaj '-' dodaj ga u lv
                spinnerEventType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
                {
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
                    {

                        EventType selectedEventType = (EventType) parent.getItemAtPosition(position);
                        if(!searchProvider.containsEventType(selectedEventType) && !Objects.equals(selectedEventType.getId(), "-1")) {
                            searchProvider.addEventType(selectedEventType);
                            setupEventTypeListView(view, listViewEventType);
                        }
                    }
                    public void onNothingSelected(AdapterView<?> parent)
                    {

                    }
                });
                setupEventTypeListView(view, listViewEventType);
            }
            @Override
            public void onError(Exception e) {}
        }, false);

        //DEO ZA ZAHTEVE---------------------------------------------------------------------------------------------------

        eventproviderrepo.selectRequests(new EventProviderCallback() {
            @Override
            public void onEventProviderReceived(EventProvider[] providers) {
                ArrayList<EventProvider> reqs = new ArrayList<>(Arrays.asList(providers));
                prs = reqs;
                ListView reqlv = view.findViewById(R.id.registration_request_list);
                        setupRequestListview(view, reqlv, reqs);
            }

            @Override
            public void onError(Exception e) {

            }
        }, searchProvider);
    }

    //funkc za uklanjanje obj iz service lv
    private void setupServiceListView(View view, ListView listViewService) {
        serviceAdapter = new ServiceAdapter(view.getContext(), this.searchProvider.getProvidedServices(), 0, R.layout.fragment_subcategory_list_item);
        serviceAdapter.setOnDeleteButtonClickListener(new OnDeleteButtonClickListener() {
            @Override
            public void onDeleteClicked(int position) {
                searchProvider.removeService(position);
                serviceAdapter.notifyDataSetChanged();
            }
        });
        listViewService.setAdapter(serviceAdapter);
    }

    //funkc za uklanjanje obj iz product lv
   private void setupProductListView(View view, ListView lv) {
        productAdapter = new ProductAdapter(view.getContext(), this.searchProvider.getProvidedProducts(), 0, R.layout.fragment_subcategory_list_item);
        productAdapter.setOnDeleteButtonClickListener(new OnDeleteButtonClickListener() {
            @Override
            public void onDeleteClicked(int position) {
                searchProvider.removeProduct(position);
                productAdapter.notifyDataSetChanged();
            }
        });
        lv.setAdapter(productAdapter);
    }

    private void setupRequestListview(View view, ListView lv, ArrayList<EventProvider> requests) {
        eventProviderAdapter = new EventProviderAdapter(view.getContext(), requests, 0);
        lv.setAdapter(eventProviderAdapter);
    }

    //Funck za eventtype
    private void setupEventTypeListView(View view, ListView lv) {
        eventTypeAdapter = new EventTypeAdapter(view.getContext(), this.searchProvider.getProvidedEventTypes(), R.layout.fragment_subcategory_list_item);
        eventTypeAdapter.setOnDeleteButtonClickListener(new OnDeleteButtonClickListener() {
            @Override
            public void onDeleteClicked(int position) {
                searchProvider.removeEventType(position);
                eventTypeAdapter.notifyDataSetChanged();
            }
        });
        lv.setAdapter(eventTypeAdapter);
    }

    private void search(String searchQuery)
    {
        searchQuery = searchQuery.toLowerCase();
        ArrayList<EventProvider> temp = new ArrayList<>();
        for(EventProvider ep : prs) {
            if(ep.getEmail().toLowerCase().contains(searchQuery) ||
                ep.getCompany().getEmail().toLowerCase().contains(searchQuery) || ep.getFirstName().toLowerCase().contains(searchQuery) ||
                    ep.getLastName().toLowerCase().contains(searchQuery) || ep.getCompany().getName().toLowerCase().contains(searchQuery))
                    temp.add(ep);
        }
        prs = temp;
        View view = this.getView();
        ListView reqlv = view.findViewById(R.id.registration_request_list);
        setupRequestListview(view, reqlv, prs);
    } */
}