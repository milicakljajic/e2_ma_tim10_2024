package rs.ftn.ma.mplanner.fragments;

import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import rs.ftn.ma.mplanner.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link RoleHomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RoleHomeFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public RoleHomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment RoleHomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static RoleHomeFragment newInstance(String param1, String param2) {
        RoleHomeFragment fragment = new RoleHomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_role_home, container, false);

        CardView button = (CardView) view.findViewById(R.id.role_home_owner);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Navigation.findNavController(view).navigate(R.id.changeToPupvHome);
            }
        });

        CardView button2 = (CardView) view.findViewById(R.id.role_home_admin);
        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Navigation.findNavController(view).navigate(R.id.changeToAdminHomeFragment1);
            }
        });

        CardView button3 = (CardView) view.findViewById(R.id.role_home_employee);
        button3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Navigation.findNavController(view).navigate(R.id.changeToPupzNav1);
            }
        });

        CardView button4 = (CardView) view.findViewById(R.id.role_home_event_organizer);
        button4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Navigation.findNavController(view).navigate(R.id.changeToOdHome);
            }
        });

        return view;
    }
}