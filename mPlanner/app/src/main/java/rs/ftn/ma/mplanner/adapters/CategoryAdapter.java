package rs.ftn.ma.mplanner.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.interfaces.OnDeleteButtonClickListener;
import rs.ftn.ma.mplanner.model.Category;

public class CategoryAdapter extends BaseAdapter {
    Context context;
    ArrayList<Category> categories;
    LayoutInflater inflter;

    private OnDeleteButtonClickListener deleteButtonClickListener;
    public void setOnDeleteButtonClickListener(OnDeleteButtonClickListener listener) {
        this.deleteButtonClickListener = listener;
    }

    public CategoryAdapter(Context applicationContext, ArrayList<Category> categories) {
        this.context = applicationContext;
        this.categories = categories;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return categories.size();
    }

    @Override
    public Object getItem(int i) {
        return categories.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.fragment_category_list_item, viewGroup, false);
        TextView name = (TextView) view.findViewById(R.id.category_item_name);
        TextView desc = (TextView) view.findViewById(R.id.category_item_description);
        ImageView icon = (ImageView) view.findViewById(R.id.category_item_delete);
        icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (deleteButtonClickListener != null) {
                    deleteButtonClickListener.onDeleteClicked(i);
                }
            }
        });
        //country.setText(countryList[i]);
        //icon.setImageResource(flags[i]);
        name.setText(categories.get(i).getName());
        desc.setText(categories.get(i).getDescription());
        return view;
    }
}