package rs.ftn.ma.mplanner.interfaces;

import rs.ftn.ma.mplanner.model.Subcategory;

public interface SubcategoryCallback {
    void onSubcategoryReceived(Subcategory[] subcategories);
    void onError(Exception e);
}
