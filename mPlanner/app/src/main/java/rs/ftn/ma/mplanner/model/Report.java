package rs.ftn.ma.mplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.time.format.DateTimeFormatter;

import rs.ftn.ma.mplanner.model.enums.ReportStatus;

public class Report implements Parcelable {
    private Long reportId;
    private EventProvider requestSubmitter;

    private Company reportedCompany;

    private String timestamp;

    private String requestDescription;

    private ReportStatus reportStatus;

    private String requestMaker;

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ISO_LOCAL_DATE_TIME;

    public Report() {
    }

    public Report(Long id,EventProvider requestSubmitter, Company reportedCompany, String timestamp, String requestDescription,String requestMaker) {
        this.reportId = id;
        this.requestSubmitter = requestSubmitter;
        this.reportedCompany = reportedCompany;
        this.timestamp = timestamp;
        this.requestDescription = requestDescription;
        this.reportStatus = ReportStatus.PENDING;
        this.requestMaker = requestMaker;
    }

    protected Report(Parcel in) {
        reportId = in.readLong();
        requestSubmitter = in.readParcelable(EventProvider.class.getClassLoader());
        reportedCompany = in.readParcelable(Company.class.getClassLoader());
        timestamp = in.readString();
        requestDescription = in.readString();
        requestMaker = in.readString();
        reportStatus = ReportStatus.valueOf(in.readString());
    }

    public Long getReportId() {
        return reportId;
    }

    public void setReportId(Long reportId) {
        this.reportId = reportId;
    }

    public EventProvider getRequestSubmitter() {
        return requestSubmitter;
    }

    public void setRequestSubmitter(EventProvider requestSubmitter) {
        this.requestSubmitter = requestSubmitter;
    }

    public Company getReportedCompany() {
        return reportedCompany;
    }

    public void setReportedCompany(Company reportedCompany) {
        this.reportedCompany = reportedCompany;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getRequestDescription() {
        return requestDescription;
    }

    public void setRequestDescription(String requestDescription) {
        this.requestDescription = requestDescription;
    }

    public String getRequestMaker() {
        return requestMaker;
    }

    public void setRequestMaker(String requestMaker) {
        this.requestMaker = requestMaker;
    }

    public ReportStatus getReportStatus() {
        return reportStatus;
    }

    public void setReportStatus(ReportStatus reportStatus) {
        this.reportStatus = reportStatus;
    }

    public static final Creator<Report> CREATOR = new Creator<Report>() {
        @Override
        public Report createFromParcel(Parcel in) {
            return new Report(in);
        }

        @Override
        public Report[] newArray(int size) {
            return new Report[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeLong(reportId);
        dest.writeParcelable(requestSubmitter, flags);
        dest.writeParcelable(reportedCompany, flags);
        dest.writeString(timestamp);
        dest.writeString(requestDescription);
        dest.writeString(requestMaker);
        dest.writeString(reportStatus.name());
    }
}
