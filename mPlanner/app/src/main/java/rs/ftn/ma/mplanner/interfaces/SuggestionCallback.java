package rs.ftn.ma.mplanner.interfaces;

import rs.ftn.ma.mplanner.model.Suggestion;

public interface SuggestionCallback {
    void onSuggestionReceived(Suggestion[] suggestions);
    void onError(Exception e);
}
