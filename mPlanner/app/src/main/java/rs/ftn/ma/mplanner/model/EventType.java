package rs.ftn.ma.mplanner.model;


import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.Objects;

public class EventType implements Parcelable {
    public String id;
    public String name;
    public String description;
    public ArrayList<Service> suggestedServices;
    public ArrayList<Product> suggestedProducts;

    public Boolean isEnabled;

    public EventType() {
        suggestedServices = new ArrayList<>();
        suggestedProducts = new ArrayList<>();
        isEnabled = true;
    }

    public EventType(String name, String description) {
        this.name = name;
        this.description = description;
        suggestedServices = new ArrayList<>();
        suggestedProducts = new ArrayList<>();
        isEnabled = true;
    }

    public EventType(String id, String name, String description, ArrayList<Service> suggestedServices, ArrayList<Product> suggestedProducts, Boolean enabled) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.suggestedServices = suggestedServices;
        this.suggestedProducts = suggestedProducts;
        this.isEnabled = enabled;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;

    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<Service> getSuggestedServices() {
        if(this.suggestedServices == null)
            suggestedServices = new ArrayList<>();
        return suggestedServices;
    }

    public void setSuggestedServices(ArrayList<Service> suggestedServices) {
        this.suggestedServices = suggestedServices;
    }

    public ArrayList<Product> getSuggestedProducts() {
        if(this.suggestedProducts == null)
            suggestedProducts = new ArrayList<>();
        return suggestedProducts;
    }

    public void setSuggestedProducts(ArrayList<Product> suggestedProducts) {
        this.suggestedProducts = suggestedProducts;
    }

    public Boolean getIsEnabled() {
        return isEnabled;
    }

    public void setIsEnabled(Boolean enabled) {
        this.isEnabled = enabled;
    }

    public Boolean containsProduct(Product product) {
        if(suggestedProducts == null)
            suggestedProducts = new ArrayList<>();
        for(Product p : suggestedProducts)
            if(Objects.equals(p.getId(), product.getId()))
                return true;
        return false;
    }

    public Boolean containsService(Service service) {
        if(suggestedServices == null)
            suggestedServices = new ArrayList<>();
        for(Service s : suggestedServices)
            if(Objects.equals(s.getId(), service.getId()))
                return true;
        return false;
    }

    public void addProduct(Product p){
        if(suggestedProducts == null)
            this.suggestedProducts = new ArrayList<>();
        suggestedProducts.add(p);
    }

    public void removeProduct(int position) {
        if(suggestedProducts == null)
            this.suggestedProducts = new ArrayList<>();
        suggestedProducts.remove(position);
    }

    public void addService(Service s){
        if(suggestedServices == null)
            this.suggestedServices = new ArrayList<>();
        suggestedServices.add(s);
    }

    public void removeService(int position) {
        if(suggestedServices == null)
            this.suggestedServices = new ArrayList<>();
        suggestedServices.remove(position);
    }


    @NonNull
    @Override
    public String toString() {
        return name;
    }


    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.description);
        dest.writeTypedList(this.suggestedServices);
        dest.writeTypedList(this.suggestedProducts);
        dest.writeValue(this.isEnabled);
    }

    public void readFromParcel(Parcel source) {
        this.id = source.readString();
        this.name = source.readString();
        this.description = source.readString();
        this.suggestedServices = source.createTypedArrayList(Service.CREATOR);
        this.suggestedProducts = source.createTypedArrayList(Product.CREATOR);
        this.isEnabled = (Boolean) source.readValue(Boolean.class.getClassLoader());
    }

    protected EventType(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.description = in.readString();
        this.suggestedServices = in.createTypedArrayList(Service.CREATOR);
        this.suggestedProducts = in.createTypedArrayList(Product.CREATOR);
        this.isEnabled = (Boolean) in.readValue(Boolean.class.getClassLoader());
    }

    public static final Parcelable.Creator<EventType> CREATOR = new Parcelable.Creator<EventType>() {
        @Override
        public EventType createFromParcel(Parcel source) {
            return new EventType(source);
        }

        @Override
        public EventType[] newArray(int size) {
            return new EventType[size];
        }
    };
}
