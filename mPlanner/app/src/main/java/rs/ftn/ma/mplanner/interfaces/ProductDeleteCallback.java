package rs.ftn.ma.mplanner.interfaces;

public interface ProductDeleteCallback {
    void onProductDeleted();
    void onDeleteError(Exception ex);
}
