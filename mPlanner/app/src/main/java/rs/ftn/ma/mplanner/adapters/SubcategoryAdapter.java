package rs.ftn.ma.mplanner.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.interfaces.OnDeleteButtonClickListener;
import rs.ftn.ma.mplanner.model.Subcategory;

public class SubcategoryAdapter extends BaseAdapter {
    Context context;
    ArrayList<Subcategory> subcategories;
    LayoutInflater inflter;

    private OnDeleteButtonClickListener deleteButtonClickListener;
    public void setOnDeleteButtonClickListener(OnDeleteButtonClickListener listener) {
        this.deleteButtonClickListener = listener;
    }

    public SubcategoryAdapter(Context applicationContext, ArrayList<Subcategory> subcategories) {
        this.context = applicationContext;
        this.subcategories = subcategories;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return subcategories.size();
    }

    @Override
    public Object getItem(int i) {
        return subcategories.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.fragment_subcategory_list_item, viewGroup, false);
        TextView name = (TextView) view.findViewById(R.id.subcategory_item_name);
        TextView description = (TextView) view.findViewById(R.id.subcategory_item_description);
        ImageView icon = (ImageView) view.findViewById(R.id.subcategory_item_delete);
        icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (deleteButtonClickListener != null) {
                    deleteButtonClickListener.onDeleteClicked(i);
                }
            }
        });
        name.setText(subcategories.get(i).getName());
        description.setText(subcategories.get(i).getDescription());

        return view;
    }
}
