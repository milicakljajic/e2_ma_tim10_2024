package rs.ftn.ma.mplanner.interfaces;

public interface ActivityUpdateCallback {
    void onActivityUpdated();
    void onUpdateError(Exception e);
}

