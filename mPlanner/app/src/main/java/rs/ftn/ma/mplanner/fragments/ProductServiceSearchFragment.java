package rs.ftn.ma.mplanner.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.adapters.ServiceAdapter;
import rs.ftn.ma.mplanner.model.Service;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProductServiceSearchFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProductServiceSearchFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public ProductServiceSearchFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProductServiceSearchFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProductServiceSearchFragment newInstance(String param1, String param2) {
        ProductServiceSearchFragment fragment = new ProductServiceSearchFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_product_service_search, container, false);

        Spinner spinner = (Spinner) view.findViewById(R.id.created_event_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                requireContext(),
                R.array.created_events_array,
                android.R.layout.simple_spinner_item
        );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        Spinner spinner2 = (Spinner) view.findViewById(R.id.product_service_package_filter_spinner);
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(
                requireContext(),
                R.array.product_service_package_filter_array,
                android.R.layout.simple_spinner_item
        );
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(adapter2);



        Service[] services = new Service[]{new Service(1, "Portrait Photography", "Professional portrait photography services", "Photography", "Portraits", 150, 0.10),
                new Service(2, "Event Photography", "Capture memories of your special events", "Photography", "Events", 200, 0.15),};

        ListView lw = view.findViewById(R.id.product_service_list);
        ServiceAdapter adapter3 = new ServiceAdapter(requireContext(), services,5);
        lw.setAdapter(adapter3);

        return view;
    }
}