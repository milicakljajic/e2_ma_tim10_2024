package rs.ftn.ma.mplanner.repo;



import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import rs.ftn.ma.mplanner.interfaces.ActivityCallback;
import rs.ftn.ma.mplanner.interfaces.ActivityDeleteCallback;
import rs.ftn.ma.mplanner.interfaces.ActivityInsertCallback;
import rs.ftn.ma.mplanner.model.Activity;

public class ActivityRepository {

    public void create(Activity activity, ActivityInsertCallback callback){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("activities")
                .add(activity)
                .addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentReference> task) {
                        if(task.isSuccessful()){
                            DocumentReference documentReference = task.getResult();
                            String activityId = documentReference.getId();
                            Log.d("REZ_DB","Activity added with Id: " + activityId);
                            callback.onActivityCreated(activityId);
                        } else {
                            Log.w("REZ_DB","Error creating activity", task.getException());
                            callback.onCreateError(task.getException());
                        }
                    }
                });
    }

    public void getAll(ActivityCallback callback){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("activities")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if(task.isSuccessful()){
                            List<Activity> activityList = new ArrayList<>();
                            for(QueryDocumentSnapshot document : task.getResult()){
                                Activity activity = document.toObject(Activity.class);
                                activity.setId(document.getId());
                                activityList.add(activity);
                                Log.d("REZ_DB", document.getId() + " => " + document.getData());
                            }
                            callback.onActivityReceived(activityList.toArray(new Activity[0]));
                        } else {
                            Log.w("REZ_DB", "Error getting documents.", task.getException());
                            callback.onError(task.getException());
                        }
                    }
                });
    }

    public void getAllByEName(String ename, ActivityCallback callback){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("activities")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if(task.isSuccessful()){
                            List<Activity> activityList = new ArrayList<>();
                            for(QueryDocumentSnapshot document : task.getResult()){
                                Activity activity = document.toObject(Activity.class);
                                activity.setId(document.getId());
                                if(activity.getSelectedEvent().getName().equals(ename))
                                    activityList.add(activity);
                                Log.d("REZ_DB", document.getId() + " => " + document.getData());
                            }
                            callback.onActivityReceived(activityList.toArray(new Activity[0]));
                        } else {
                            Log.w("REZ_DB", "Error getting documents.", task.getException());
                            callback.onError(task.getException());
                        }
                    }
                });
    }

    /*public void update(String id, Activity activity, ActivityUpdateCallback callback){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("activities").document(id);

        docRef.update("name", activity.getName(),
                        "description", activity.getDescription(),
                        "durationFrom", activity.getDurationFrom(),
                        "durationTo", activity.getDurationTo(),
                        "location", activity.getLocation())
                .addOnSuccessListener(aVoid -> {
                    Log.d("REZ_DB", "Activity updated");
                    callback.onActivityUpdated();
                })
                .addOnFailureListener(e -> {
                    Log.w("REZ_DB", "Error updating document", e);
                    callback.onUpdateError(e);
                });
    }*/

    public void getById(ActivityCallback callback, String id){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("activities").document(id);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful()){
                    DocumentSnapshot snapshot = task.getResult();
                    if(snapshot.exists()){
                        Activity[] activity = new Activity[]{snapshot.toObject(Activity.class)};
                        Log.d("REZ_DB", snapshot.getId() + " => " + snapshot.getData());
                        callback.onActivityReceived(activity);
                    } else {
                        Log.d("REZ_DB", "No such document");
                        callback.onError(new Exception("No such document"));
                    }
                } else {
                    Log.w("REZ_DB", "Error getting document", task.getException());
                    callback.onError(task.getException());
                }
            }
        });
    }

    public void delete(String id, ActivityDeleteCallback callback){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("activities").document(id);

        docRef.delete()
                .addOnSuccessListener(aVoid -> {
                    Log.d("REZ_DB", "Activity successfully deleted");
                    callback.onActivityDeleted();
                })
                .addOnFailureListener(e -> {
                    Log.w("REZ_DB", "Error deleting activity", e);
                    callback.onDeleteError(e);
                });
    }
}
