package rs.ftn.ma.mplanner.interfaces;

import rs.ftn.ma.mplanner.model.EventOrganizer;

public interface EventOrganizerCallback {
    void onEventOrganizerReceived(EventOrganizer[] types);
    void onError(Exception e);
}
