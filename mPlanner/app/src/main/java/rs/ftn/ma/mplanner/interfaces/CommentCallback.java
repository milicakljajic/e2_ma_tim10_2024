package rs.ftn.ma.mplanner.interfaces;

import rs.ftn.ma.mplanner.model.Comment;

public interface CommentCallback {
    void onCommentRecieved(Comment[] comments);
    void onError(Exception e);
}
