package rs.ftn.ma.mplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Schedule implements Parcelable {
    private String mondayFrom;
    private String mondayTo;
    private String tuesdayFrom;
    private String tuesdayTo;
    private String wednesdayFrom;
    private String wednesdayTo;
    private String thursdayFrom;
    private String thursdayTo;
    private String fridayFrom;
    private String fridayTo;
    private String saturdayFrom;
    private String saturdayTo;
    private String sundayFrom;
    private String sundayTo;
    private String startDate;
    private String endDate;

    public Schedule() {
    }

    public Schedule(String mondayFrom, String mondayTo, String tuesdayFrom, String tuesdayTo, String wednesdayFrom, String wednesdayTo, String thursdayFrom, String thursdayTo, String fridayFrom, String fridayTo, String saturdayFrom, String saturdayTo, String sundayFrom, String sundayTo, String startDate, String endDate) {
        this.mondayFrom = mondayFrom;
        this.mondayTo = mondayTo;
        this.tuesdayFrom = tuesdayFrom;
        this.tuesdayTo = tuesdayTo;
        this.wednesdayFrom = wednesdayFrom;
        this.wednesdayTo = wednesdayTo;
        this.thursdayFrom = thursdayFrom;
        this.thursdayTo = thursdayTo;
        this.fridayFrom = fridayFrom;
        this.fridayTo = fridayTo;
        this.saturdayFrom = saturdayFrom;
        this.saturdayTo = saturdayTo;
        this.sundayFrom = sundayFrom;
        this.sundayTo = sundayTo;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public String getMondayFrom() {
        return mondayFrom;
    }

    public void setMondayFrom(String mondayFrom) {
        this.mondayFrom = mondayFrom;
    }

    public String getMondayTo() {
        return mondayTo;
    }

    public void setMondayTo(String mondayTo) {
        this.mondayTo = mondayTo;
    }

    public String getTuesdayFrom() {
        return tuesdayFrom;
    }

    public void setTuesdayFrom(String tuesdayFrom) {
        this.tuesdayFrom = tuesdayFrom;
    }

    public String getTuesdayTo() {
        return tuesdayTo;
    }

    public void setTuesdayTo(String tuesdayTo) {
        this.tuesdayTo = tuesdayTo;
    }

    public String getWednesdayFrom() {
        return wednesdayFrom;
    }

    public void setWednesdayFrom(String wednesdayFrom) {
        this.wednesdayFrom = wednesdayFrom;
    }

    public String getWednesdayTo() {
        return wednesdayTo;
    }

    public void setWednesdayTo(String wednesdayTo) {
        this.wednesdayTo = wednesdayTo;
    }

    public String getThursdayFrom() {
        return thursdayFrom;
    }

    public void setThursdayFrom(String thursdayFrom) {
        this.thursdayFrom = thursdayFrom;
    }

    public String getThursdayTo() {
        return thursdayTo;
    }

    public void setThursdayTo(String thursdayTo) {
        this.thursdayTo = thursdayTo;
    }

    public String getFridayFrom() {
        return fridayFrom;
    }

    public void setFridayFrom(String fridayFrom) {
        this.fridayFrom = fridayFrom;
    }

    public String getFridayTo() {
        return fridayTo;
    }

    public void setFridayTo(String fridayTo) {
        this.fridayTo = fridayTo;
    }

    public String getSaturdayFrom() {
        return saturdayFrom;
    }

    public void setSaturdayFrom(String saturdayFrom) {
        this.saturdayFrom = saturdayFrom;
    }

    public String getSaturdayTo() {
        return saturdayTo;
    }

    public void setSaturdayTo(String saturdayTo) {
        this.saturdayTo = saturdayTo;
    }

    public String getSundayFrom() {
        return sundayFrom;
    }

    public void setSundayFrom(String sundayFrom) {
        this.sundayFrom = sundayFrom;
    }

    public String getSundayTo() {
        return sundayTo;
    }

    public void setSundayTo(String sundayTo) {
        this.sundayTo = sundayTo;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mondayFrom);
        dest.writeString(this.mondayTo);
        dest.writeString(this.tuesdayFrom);
        dest.writeString(this.tuesdayTo);
        dest.writeString(this.wednesdayFrom);
        dest.writeString(this.wednesdayTo);
        dest.writeString(this.thursdayFrom);
        dest.writeString(this.thursdayTo);
        dest.writeString(this.fridayFrom);
        dest.writeString(this.fridayTo);
        dest.writeString(this.saturdayFrom);
        dest.writeString(this.saturdayTo);
        dest.writeString(this.sundayFrom);
        dest.writeString(this.sundayTo);
        dest.writeString(this.startDate);
        dest.writeString(this.endDate);
    }

    public void readFromParcel(Parcel source) {
        this.mondayFrom = source.readString();
        this.mondayTo = source.readString();
        this.tuesdayFrom = source.readString();
        this.tuesdayTo = source.readString();
        this.wednesdayFrom = source.readString();
        this.wednesdayTo = source.readString();
        this.thursdayFrom = source.readString();
        this.thursdayTo = source.readString();
        this.fridayFrom = source.readString();
        this.fridayTo = source.readString();
        this.saturdayFrom = source.readString();
        this.saturdayTo = source.readString();
        this.sundayFrom = source.readString();
        this.sundayTo = source.readString();
        this.startDate = source.readString();
        this.endDate = source.readString();
    }

    protected Schedule(Parcel in) {
        this.mondayFrom = in.readString();
        this.mondayTo = in.readString();
        this.tuesdayFrom = in.readString();
        this.tuesdayTo = in.readString();
        this.wednesdayFrom = in.readString();
        this.wednesdayTo = in.readString();
        this.thursdayFrom = in.readString();
        this.thursdayTo = in.readString();
        this.fridayFrom = in.readString();
        this.fridayTo = in.readString();
        this.saturdayFrom = in.readString();
        this.saturdayTo = in.readString();
        this.sundayFrom = in.readString();
        this.sundayTo = in.readString();
        this.startDate = in.readString();
        this.endDate = in.readString();
    }

    public static final Creator<Schedule> CREATOR = new Creator<Schedule>() {
        @Override
        public Schedule createFromParcel(Parcel source) {
            return new Schedule(source);
        }

        @Override
        public Schedule[] newArray(int size) {
            return new Schedule[size];
        }
    };
}
