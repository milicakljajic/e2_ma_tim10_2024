package rs.ftn.ma.mplanner.interfaces;

public interface ProductInsertCallback {
    void onProductCreated(String productId);
    void onCreateError(Exception e);
}
