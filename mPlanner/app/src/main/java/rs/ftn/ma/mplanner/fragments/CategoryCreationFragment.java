package rs.ftn.ma.mplanner.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.interfaces.CategoryCallback;
import rs.ftn.ma.mplanner.interfaces.CategoryInsertCallback;
import rs.ftn.ma.mplanner.interfaces.CategoryUpdateCallback;
import rs.ftn.ma.mplanner.model.Category;
import rs.ftn.ma.mplanner.repo.CategoryRepository;


public class CategoryCreationFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "mode";
    private static final String ARG_PARAM2 = "id";

    // TODO: Rename and change types of parameters
    private String mode;
    private String id;
    private Category category;

    private CategoryRepository repo;

    public CategoryCreationFragment() {
        // Required empty public constructor
        category = new Category();
        repo = new CategoryRepository();
    }


    public static CategoryCreationFragment newInstance(String mode, String id) {
        CategoryCreationFragment fragment = new CategoryCreationFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, mode);
        args.putString(ARG_PARAM2, id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if(getArguments().getString(ARG_PARAM1) != null)
                mode = getArguments().getString(ARG_PARAM1);
            if(getArguments().getString(ARG_PARAM2) != null)
                id = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_category_creation, container, false);

        MaterialButton button = view.findViewById(R.id.category_creation_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText name = view.findViewById(R.id.category_creation_name);
                String nameText = String.valueOf(name.getText());
                EditText desc = view.findViewById(R.id.category_creation_description);
                String descText = String.valueOf(desc.getText());
                if(mode.equals("update"))
                {
                    repo.selectById(new CategoryCallback() {
                        @Override
                        public void onCategoryReceived(Category[] categories) {
                            category = categories[0];
                            category.setName(nameText);
                            category.setDescription(descText);
                            repo.update(id, category, new CategoryUpdateCallback() {
                                @Override
                                public void onCategoryUpdated() {
                                    Toast.makeText(v.getContext(), "Successfully updated category", Toast.LENGTH_SHORT).show();
                                    Log.d("Update", "success");
                                }

                                @Override
                                public void onUpdateError(Exception e) {
                                    Log.d("Update", "fail");
                                }
                            });
                        }

                        @Override
                        public void onError(Exception e) {

                        }
                    }, id);
                }
                if(mode.equals("create")){
                    category.setName(nameText);
                    category.setDescription(descText);
                    repo.insert(category, new CategoryInsertCallback() {
                        @Override
                        public void onCategoryInserted(String categoryId) {
                            Log.d("Insert", "success");
                            Toast.makeText(v.getContext(), "Successfully created category", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onInsertError(Exception e) {
                            Log.d("Insert", "failure");
                        }
                    });
                }
            }
        });



        return view;
    }
}