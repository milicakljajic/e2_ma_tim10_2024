package rs.ftn.ma.mplanner.interfaces;

public interface EmployeeInsertCallback {
    void onEmployeeInserted(String employeeId);
    void onInsertError(Exception e);
}
