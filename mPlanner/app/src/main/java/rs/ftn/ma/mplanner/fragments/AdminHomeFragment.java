package rs.ftn.ma.mplanner.fragments;

import static android.content.Context.NOTIFICATION_SERVICE;

import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.button.MaterialButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.activities.AuthActivity;
import rs.ftn.ma.mplanner.interfaces.NotificationCallback;
import rs.ftn.ma.mplanner.interfaces.NotificationUpdateCallback;
import rs.ftn.ma.mplanner.model.Notification;
import rs.ftn.ma.mplanner.repo.NotificationRepository;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AdminHomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AdminHomeFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private FirebaseAuth mAuth;

    private NotificationRepository nrepo;

    public AdminHomeFragment() {
        // Required empty public constructor
        mAuth = FirebaseAuth.getInstance();
        nrepo = new NotificationRepository();
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AdminHomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AdminHomeFragment newInstance(String param1, String param2) {
        AdminHomeFragment fragment = new AdminHomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_admin_home, container, false);

        CardView button = (CardView) view.findViewById(R.id.event_type_creation_button);

        FirebaseUser cUser = mAuth.getCurrentUser();
        nrepo.select(new NotificationCallback() {
            @Override
            public void onNotificationReceived(Notification[] notifications) {
                for(Notification n : notifications)
                {
                    if(n.getRecieverId().equals(cUser.getUid()) && !n.getSeen())
                    {
                        NotificationCompat.Builder builder = new NotificationCompat.Builder(requireContext(), "unique-channel-id-1")
                                .setSmallIcon(R.drawable.logo_light)
                                .setContentTitle(n.getTitle())
                                .setContentText(n.getMessage())
                                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
                        NotificationManager notificationManager = (NotificationManager) requireContext().getSystemService(NOTIFICATION_SERVICE);
                        notificationManager.notify(1, builder.build());
                        n.setSeen(true);
                        nrepo.update(n.getId(), n, new NotificationUpdateCallback() {
                            @Override
                            public void onNotificationUpdated() {

                            }

                            @Override
                            public void onUpdateError(Exception e) {

                            }
                        });

                    }
                }
            }

            @Override
            public void onError(Exception e) {

            }
        });





        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final Bundle bundle = new Bundle();
                bundle.putString("event-type-id", null);
                bundle.putString("mode", "create");
                Navigation.findNavController(view).navigate(R.id.changeToEventTypeCreationFragment1, bundle);
            }
        });

        CardView button2 = (CardView) view.findViewById(R.id.admin_update_eventtype_button);

        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Navigation.findNavController(view).navigate(R.id.changeToEventsTypeList1);
            }
        });

        CardView button3 = (CardView) view.findViewById(R.id.admin_update_eventtype_status);

        button3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Navigation.findNavController(view).navigate(R.id.changeToEventsTypeList1);
            }
        });

        CardView button4 = (CardView) view.findViewById(R.id.admin_create_category);

        button4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("mode", "create");
                Navigation.findNavController(view).navigate(R.id.changeToCategoryCreation1, bundle);
            }
        });


        CardView button5 = (CardView) view.findViewById(R.id.admin_manage_categories);

        button5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Navigation.findNavController(view).navigate(R.id.changeToCategoryList1);
            }
        });

        CardView button6 = (CardView) view.findViewById(R.id.admin_create_subcategory);

        button6.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("mode", "create");
                Navigation.findNavController(view).navigate(R.id.changeToSubcategoryCreation1, bundle);
            }
        });

        CardView button7 = (CardView) view.findViewById(R.id.admin_manage_subcategories);

        button7.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Navigation.findNavController(view).navigate(R.id.changeToSubcategoryList1);
            }
        });

        CardView button8 = (CardView) view.findViewById(R.id.admin_manage_suggestions);

        button8.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Navigation.findNavController(view).navigate(R.id.changeToSubcategorySuggestion1);
            }
        });

        MaterialButton button9 = (MaterialButton) view.findViewById(R.id.admin_home_logout_button);
        button9.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
                mAuth.signOut();
                Intent myIntent = new Intent(requireContext(), AuthActivity.class);
                startActivity(myIntent);
            }
        });

        CardView button10 = (CardView) view.findViewById(R.id.admin_home_manage_registration_requests_button);
        button10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(view).navigate(R.id.changeToRegistrationRequests1);
            }
        });

        CardView button11 = (CardView) view.findViewById(R.id.admin_manage_reviews);
        button11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(view).navigate(R.id.changeToAdminReports);
            }
        });

        CardView button12 = (CardView) view.findViewById(R.id.admin_notification);
        button12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(view).navigate(R.id.changeToAdminNotifications);
            }
        });


        return view;
    }



}