package rs.ftn.ma.mplanner.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.adapters.ProductAdapter;
import rs.ftn.ma.mplanner.adapters.ServiceAdapter;
import rs.ftn.ma.mplanner.model.EventType;
import rs.ftn.ma.mplanner.repo.EventTypeRepository;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CreationEventTypeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CreationEventTypeFragment extends Fragment {

    private static final String ARG_PARAM1 = "event-type-id";
    private static final String ARG_PARAM2 = "mode";

    private String eventTypeId;
    private String mode;

    private EventType currentType;
    private final EventTypeRepository repo;

    private ProductAdapter productAdapter = null;
    private ServiceAdapter serviceAdapter = null;

    public CreationEventTypeFragment() {
        // Required empty public constructor
        if(currentType == null)
            currentType = new EventType();
        repo = new EventTypeRepository();
    }

    public static CreationEventTypeFragment newInstance(String eventTypeId, String mode) {
        CreationEventTypeFragment fragment = new CreationEventTypeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, eventTypeId);
        args.putString(ARG_PARAM2, mode);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            eventTypeId = getArguments().getString(ARG_PARAM1);
            mode = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_creation_event_type, container, false);
     /*   if(mode != null) {
            if (mode.equals("update")) {

                //Ucitavanje postojecih podataka
                repo.selectById(new EventTypeCallback() {
                    @Override
                    public void onEventTypeReceived(EventType[] types) {
                        //Postavljanje polja forme TEK kada pristignu podaci
                        currentType = types[0];
                        currentType.setId(eventTypeId);
                        EditText nameText = (EditText) view.findViewById(R.id.event_type_name);
                        nameText.setText(currentType.getName());
                        nameText.setFocusable(false);
                        EditText editText = (EditText) view.findViewById(R.id.event_type_description);
                        editText.setText(currentType.getDescription());
                        TextView textView = (TextView) view.findViewById(R.id.event_type_creation_title);
                        textView.setText(R.string.event_type_creation_fragment_title_alt);
                        setupSpinnersAndListViews(view);
                    }

                    @Override
                    public void onError(Exception e) {
                        // Logika za obradu greške
                    }
                }, eventTypeId);

                //Postavi tekst dugmeta da bude update
                MaterialButton button = (MaterialButton) view.findViewById(R.id.event_type_create_item_button);
                button.setText(R.string.update);

                //Promeni status na enabled
                button = (MaterialButton) view.findViewById(R.id.event_type_activate);
                button.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        currentType.setIsEnabled(true);
                    }
                });

                //Promeni status na disabled
                button = (MaterialButton) view.findViewById(R.id.event_type_deactivate);
                button.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        currentType.setIsEnabled(false);
                    }
                });

                //UPDATE - izvrsavaje
                EditText editText = view.findViewById(R.id.event_type_description);
                button = (MaterialButton) view.findViewById(R.id.event_type_create_item_button);
                button.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        currentType.setDescription(String.valueOf(editText.getText()));
                        repo.update(currentType.getId(), currentType, new EventTypeUpdateCallback() {
                            @Override
                            public void onEventTypeUpdated() {
                                // Obrada kada je događaj uspešno ažuriran
                                Toast.makeText(getActivity(), "Successfully updated", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onUpdateError(Exception e) {
                                // Obrada greške prilikom ažuriranja događaja
                            }
                        });

                    }
                });

            }
            else {
                //CREATE
                //Popunjavanje spinnera i lv
                setupSpinnersAndListViews(view);

                //Sakrivanje update dugmica
                MaterialButton button = (MaterialButton) view.findViewById(R.id.event_type_activate);
                button.setVisibility(View.GONE);
                button = (MaterialButton) view.findViewById(R.id.event_type_deactivate);
                button.setVisibility(View.GONE);

                //Dobavljanje kontroli da mapiramo na obj kasnije
                EditText nameText = (EditText) view.findViewById(R.id.event_type_name);
                EditText editText = (EditText) view.findViewById(R.id.event_type_description);


                //Create izvrsavanje
                button = (MaterialButton) view.findViewById(R.id.event_type_create_item_button);
                button.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        currentType.setName(String.valueOf(nameText.getText()));
                        currentType.setDescription(String.valueOf(editText.getText()));
                        //Proizvodi/servisi su namapirani vec drugim funkcijama

                        repo.insert(currentType, new EventTypeInsertCallback() {
                            @Override
                            public void onEventTypeInserted(String eventId) {
                                Toast.makeText(getActivity(), "Successfully created", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onInsertError(Exception e) {

                            }
                        });
                    }
                });
            }
        } */
        return view;
    }

 /*   private void setupSpinnersAndListViews(View view) {

        //Deo za proizvode
        //TODO OVO DOBAVITI PREKO REPO KADA URADE
        //Popunjavanje spinnera
        Product[] products = new Product[]{
                new Product(-1, "-", "", 0.0, "", "", 0, false),
                new Product(1, "Camera", "Professional DSLR camera for photography enthusiasts", 899.99, "Electronics", "Cameras", 0, true),
                new Product(2, "Tripod", "Sturdy tripod for stable photography shots", 49.99, "Accessories", "Tripods", 0, true)};

        ListView listViewProduct = view.findViewById(R.id.event_type_product_lw);
        Spinner spinnerProduct = view.findViewById(R.id.event_type_product_spinner);
        ArrayAdapter<Product> spinnerProductAdapter = new ArrayAdapter<Product>(
                requireContext(),
                android.R.layout.simple_spinner_item,
                products
        );
        spinnerProductAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerProduct.setAdapter(spinnerProductAdapter);

        //Spiner kada se odabere proizvod da doda u listViewProduct
        final ListView finalLv = listViewProduct;
        spinnerProduct.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                Product selectedProduct = (Product) parent.getItemAtPosition(position);
                if(!currentType.containsProduct(selectedProduct) && selectedProduct.getId() != -1) {
                    currentType.addProduct(selectedProduct);
                    setupProductListView(view, finalLv);
                }
            }
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });
        if(!this.currentType.getSuggestedProducts().isEmpty()) {
            setupProductListView(view, listViewProduct);
        }

        //DEO ZA SERVISE
        //TODO OVO DOBAVITI PREKO REPO KADA URADE
        Service[] services = new Service[]{
                new Service(-1,"-", "", "", "", 0, 0.10),
                new Service(1,"Service Name", "Service Description", "Category", "Subcategory", 100, 0.10),
                new Service(2,"Photo Service", "Professional photos for homes", "Home Services", "Photo", 80, 0.05)};

        ListView listViewService = view.findViewById(R.id.event_type_service_lw);
        Spinner spinnerService = view.findViewById(R.id.event_type_service_spinner);
        ArrayAdapter<Service> adapterService = new ArrayAdapter<Service>(
                requireContext(),
                android.R.layout.simple_spinner_item,
                services
        );
        adapterService.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerService.setAdapter(adapterService);

        //kada se klikne na elem iz spinner a da nije onaj '-' dodaj ga u lv
        spinnerService.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                Service selectedService = (Service) parent.getItemAtPosition(position);
                if(!currentType.containsService(selectedService) && selectedService.getId() != -1) {
                    currentType.addService(selectedService);
                    setupServiceListView(view, listViewService);
                }
            }
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });
        setupServiceListView(view, listViewService);
    }

    //funkc za uklanjanje obj iz service lv
    private void setupServiceListView(View view, ListView listViewService) {
            serviceAdapter = new ServiceAdapter(view.getContext(), this.currentType.getSuggestedServices(), 0, R.layout.fragment_subcategory_list_item);
            serviceAdapter.setOnDeleteButtonClickListener(new OnDeleteButtonClickListener() {
                @Override
                public void onDeleteClicked(int position) {
                    currentType.removeService(position);
                    serviceAdapter.notifyDataSetChanged();
                }
            });
            listViewService.setAdapter(serviceAdapter);
    }

    //funkc za uklanjanje obj iz product lv
    private void setupProductListView(View view, ListView lv) {
        productAdapter = new ProductAdapter(view.getContext(), this.currentType.getSuggestedProducts(), 0, R.layout.fragment_subcategory_list_item);
        productAdapter.setOnDeleteButtonClickListener(new OnDeleteButtonClickListener() {
            @Override
            public void onDeleteClicked(int position) {
                currentType.removeProduct(position);
                productAdapter.notifyDataSetChanged();
            }
        });
        lv.setAdapter(productAdapter);
    }*/
}