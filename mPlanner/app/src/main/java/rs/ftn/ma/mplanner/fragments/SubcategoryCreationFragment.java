package rs.ftn.ma.mplanner.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;

import java.util.ArrayList;
import java.util.Arrays;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.interfaces.CategoryCallback;
import rs.ftn.ma.mplanner.interfaces.SubcategoryInsertCallback;
import rs.ftn.ma.mplanner.interfaces.SubcategoryUpdateCallback;
import rs.ftn.ma.mplanner.model.Category;
import rs.ftn.ma.mplanner.model.Subcategory;
import rs.ftn.ma.mplanner.repo.CategoryRepository;
import rs.ftn.ma.mplanner.repo.SubcategoryRepository;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SubcategoryCreationFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SubcategoryCreationFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "mode";
    private static final String ARG_PARAM2 = "id";

    // TODO: Rename and change types of parameters
    private String mode;
    private String id;

    private Category selectedCategory;

    private Subcategory subcategory;
    private CategoryRepository crepo;

    private SubcategoryRepository repo;
    public SubcategoryCreationFragment() {
        // Required empty public constructor
        crepo = new CategoryRepository();
        repo = new SubcategoryRepository();
        selectedCategory = new Category();
        subcategory = new Subcategory();
    }


    public static SubcategoryCreationFragment newInstance(String mode, String id) {
        SubcategoryCreationFragment fragment = new SubcategoryCreationFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, mode);
        args.putString(ARG_PARAM2, id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if(getArguments().getString(ARG_PARAM1) != null)
                mode = getArguments().getString(ARG_PARAM1);
            if(getArguments().getString(ARG_PARAM2) != null)
                id = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_subcategory_creation, container, false);



        ArrayList<String>  type = new ArrayList<String>();
        type.add("Service");
        type.add("Product");


        crepo.select(new CategoryCallback() {
            @Override
            public void onCategoryReceived(Category[] categories) {
                ArrayList<Category>  courses = new ArrayList<Category>(Arrays.asList(categories));


                Spinner spinner = view.findViewById(R.id.subcategory_category_spinner);
                ArrayAdapter<Category> adapter = new ArrayAdapter<Category>(
                        requireContext(),
                        android.R.layout.simple_spinner_item,
                        courses
                );
                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
                {
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
                    {
                        Category c = (Category) parent.getItemAtPosition(position);
                        if(!subcategory.getCategory().equals(c)) {
                            subcategory.setCategory(c);
                        }
                    }
                    public void onNothingSelected(AdapterView<?> parent)
                    {

                    }
                });

                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(adapter);
            }

            @Override
            public void onError(Exception e) {

            }
        });



        Spinner spinner = view.findViewById(R.id.subcategory_type_spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                requireContext(),
                android.R.layout.simple_spinner_item,
                type
        );
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                String s = (String) parent.getItemAtPosition(position);
                if(!subcategory.getType().equals(s)) {
                    subcategory.setType(s);
                }
            }
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);


        MaterialButton button = view.findViewById(R.id.subcategory_creation_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText name = view.findViewById(R.id.subcategory_name);
                String nameText = String.valueOf(name.getText());
                EditText desc = view.findViewById(R.id.subcategory_desc);
                String descText = String.valueOf(desc.getText());
                if(mode.equals("create"))
                {
                    subcategory.setName(nameText);
                    subcategory.setDescription(descText);
                    repo.insert(subcategory, new SubcategoryInsertCallback() {
                        @Override
                        public void onSubcategoryInserted(String subcategoryId) {
                            Toast.makeText(v.getContext(), "Successfully created subcategory", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onInsertError(Exception e) {

                        }
                    });
                }
                else if(mode.equals("update")) {
                    subcategory.setName(nameText);
                    subcategory.setDescription(descText);
                    repo.update(id, subcategory, new SubcategoryUpdateCallback() {
                        @Override
                        public void onSubcategoryUpdated() {
                            Toast.makeText(v.getContext(), "Successfully upated subcategory", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onUpdateError(Exception e) {

                        }
                    });
                }
            }
        });

        return view;
    }
}