package rs.ftn.ma.mplanner.adapters;

import static android.app.PendingIntent.getActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.navigation.Navigation;

import java.util.ArrayList;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.interfaces.NotificationInsertCallback;
import rs.ftn.ma.mplanner.interfaces.SubcategoryInsertCallback;
import rs.ftn.ma.mplanner.model.Notification;
import rs.ftn.ma.mplanner.model.Suggestion;
import rs.ftn.ma.mplanner.repo.NotificationRepository;
import rs.ftn.ma.mplanner.repo.SubcategoryRepository;

public class SuggestionAdapter extends BaseAdapter {
    Context context;
    ArrayList<Suggestion> suggestions;
    SubcategoryRepository srepo;
    NotificationRepository nrepo;
    LayoutInflater inflter;

    public SuggestionAdapter(Context applicationContext, ArrayList<Suggestion> suggestions) {
        this.context = applicationContext;
        this.suggestions = suggestions;
        inflter = (LayoutInflater.from(applicationContext));
        srepo = new SubcategoryRepository();
        nrepo = new NotificationRepository();
    }

    @Override
    public int getCount() {
        return suggestions.size();
    }

    @Override
    public Object getItem(int i) {
        return suggestions.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = inflter.inflate(R.layout.fragment_subcategory_suggestion_item, viewGroup, false);
        }
        TextView name = (TextView) view.findViewById(R.id.subcategory_item_name);
        TextView description = (TextView) view.findViewById(R.id.subcategory_item_description);

        name.setText(suggestions.get(i).getName());
        description.setText(suggestions.get(i).getName());



        ImageView accept = (ImageView) view.findViewById(R.id.subcategory_item_accept);
        ImageView modify = (ImageView) view.findViewById(R.id.subcategory_item_modify);
        ImageView exist = (ImageView) view.findViewById(R.id.subcategory_item_exist);

        modify.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("id",  suggestions.get(i).getSubcategory().getId());
                bundle.putString("mode", "update");
                Navigation.findNavController(v).navigate(R.id.changeToSubcategoryCreation3, bundle);

                nrepo.insert(new Notification(suggestions.get(i).getEventProvider().getFirebaseUserId(), "Suggestion updated", "See it later",false), new NotificationInsertCallback() {
                    @Override
                    public void onNotificationInserted(String notificationId) {
                        Toast.makeText(v.getContext(), "Notified user", Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onInsertError(Exception e) {

                    }
                });
            }

        });

        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                srepo.insert(suggestions.get(i).getSubcategory(), new SubcategoryInsertCallback() {
                    @Override
                    public void onSubcategoryInserted(String subcategoryId) {
                        Toast.makeText(v.getContext(), "Successfully created subcategory", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onInsertError(Exception e) {

                    }
                });
            }
        });

        exist.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                nrepo.insert(new Notification(suggestions.get(i).getEventProvider().getFirebaseUserId(), "Suggestion updated", "See it later",false), new NotificationInsertCallback() {
                    @Override
                    public void onNotificationInserted(String notificationId) {
                        Toast.makeText(v.getContext(), "Notified user", Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onInsertError(Exception e) {

                    }
                });
                Navigation.findNavController(v).navigate(R.id.changeToSuggestionExistingFragment1);
            }

        });

        return view;
    }
}
