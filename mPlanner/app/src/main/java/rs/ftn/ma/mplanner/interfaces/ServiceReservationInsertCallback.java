package rs.ftn.ma.mplanner.interfaces;

public interface ServiceReservationInsertCallback {
    void onServiceReservationInserted(String id);
    void onInsertError(Exception e);
}
