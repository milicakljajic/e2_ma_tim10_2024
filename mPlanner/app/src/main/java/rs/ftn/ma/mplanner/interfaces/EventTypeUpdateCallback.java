package rs.ftn.ma.mplanner.interfaces;

public interface EventTypeUpdateCallback {
    void onEventTypeUpdated();
    void onUpdateError(Exception e);
}
