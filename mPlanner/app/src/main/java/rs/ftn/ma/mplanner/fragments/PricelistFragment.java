package rs.ftn.ma.mplanner.fragments;

import static android.content.ContentValues.TAG;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.adapters.PricelistAdapter;
import rs.ftn.ma.mplanner.model.Pricelist;
import rs.ftn.ma.mplanner.model.enums.PricelistType;

public class PricelistFragment extends Fragment {

    private RecyclerView recyclerViewProducts;
    private RecyclerView recyclerViewServices;
    private RecyclerView recyclerViewPackages;
    private PricelistAdapter productsAdapter;
    private PricelistAdapter servicesAdapter;
    private PricelistAdapter packagesAdapter;
    private List<Pricelist> productsList;
    private List<Pricelist> servicesList;
    private List<Pricelist> packagesList;
    private FirebaseFirestore db;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pricelist, container, false);

        recyclerViewProducts = view.findViewById(R.id.pricelistProductRecyclerView);
        recyclerViewServices = view.findViewById(R.id.pricelistServiceRecyclerView);
        recyclerViewPackages = view.findViewById(R.id.pricelistPackageRecyclerView);
        Button exportButton = view.findViewById(R.id.exportToPdfButton);

        recyclerViewProducts.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerViewServices.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerViewPackages.setLayoutManager(new LinearLayoutManager(getContext()));

        productsList = new ArrayList<>();
        servicesList = new ArrayList<>();
        packagesList = new ArrayList<>();

        productsAdapter = new PricelistAdapter(getContext(), productsList);
        servicesAdapter = new PricelistAdapter(getContext(), servicesList);
        packagesAdapter = new PricelistAdapter(getContext(), packagesList);

        recyclerViewProducts.setAdapter(productsAdapter);
        recyclerViewServices.setAdapter(servicesAdapter);
        recyclerViewPackages.setAdapter(packagesAdapter);

        db = FirebaseFirestore.getInstance();
        loadPricelistData();

        exportButton.setOnClickListener(v -> {
            generatePdf();
        });

        return view;
    }

    private void loadPricelistData() {
        db.collection("pricelists")
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        productsList.clear();
                        servicesList.clear();
                        packagesList.clear();

                        for (QueryDocumentSnapshot document : task.getResult()) {
                            Pricelist pricelist = document.toObject(Pricelist.class);
                            if (pricelist.getType() == PricelistType.PRODUCT) {
                                productsList.add(pricelist);
                            } else if (pricelist.getType() == PricelistType.SERVICE) {
                                servicesList.add(pricelist);
                            } else if (pricelist.getType() == PricelistType.PACKAGE) {
                                packagesList.add(pricelist);
                            }
                        }

                      // productsAdapter.notifyDataSetChanged();
                       // servicesAdapter.notifyDataSetChanged();
                        // packagesAdapter.notifyDataSetChanged();
                    } else {
                        // Handle errors here
                    }
                });
    }

    private void generatePdf() {
        PdfDocument document = new PdfDocument();
        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(595, 842, 1).create();
        PdfDocument.Page page = document.startPage(pageInfo);

        int x = 10, y = 25;
        Paint paint = new Paint();

        String title = "Pricelist";
        paint.setTextSize(24);
        float titleWidth = paint.measureText(title);
        float xPos = (pageInfo.getPageWidth() - titleWidth) / 2;
        page.getCanvas().drawText(title, xPos, y, paint);

        y += 100;
        paint.setTextSize(12);

        page.getCanvas().drawText("Products", x, y, paint);
        y += 30;

        for (Pricelist item : productsList) {
            String text = String.format(Locale.getDefault(), "%s: %s $ (Discounted: %s $ with %s%% discount)",
                    item.getName(), item.getPrice(), item.getDiscountPrice(), item.getDiscount());
            page.getCanvas().drawText(text, x, y, paint);
            y += 25;
        }

        y += 30;
        page.getCanvas().drawText("Services", x, y, paint);
        y += 30;

        for (Pricelist item : servicesList) {
            String text = String.format(Locale.getDefault(), "%s: %s $ (Discounted: %s $ with %s%% discount)",
                    item.getName(), item.getPrice(), item.getDiscountPrice(), item.getDiscount());
            page.getCanvas().drawText(text, x, y, paint);
            y += 25;
        }

        y += 30;
        page.getCanvas().drawText("Packages", x, y, paint);
        y += 30;

        for (Pricelist item : packagesList) {
            String text = String.format(Locale.getDefault(), "%s: %s $ (Discounted: %s $ with %s%% discount)",
                    item.getName(), item.getPrice(), item.getDiscountPrice(), item.getDiscount());
            page.getCanvas().drawText(text, x, y, paint);
            y += 25;
        }

        document.finishPage(page);

        // Save PDF using MediaStore for Android Q and later
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            savePdfToMediaStore(document);
        } else {
            savePdfToExternalStorage(document);
        }

        document.close();
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    private void savePdfToMediaStore(PdfDocument document) {
        ContentResolver resolver = getContext().getContentResolver();
        ContentValues contentValues = new ContentValues();
        contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, "pricelist.pdf");
        contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "application/pdf");
        contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_DOWNLOADS);

        Uri uri = resolver.insert(MediaStore.Downloads.EXTERNAL_CONTENT_URI, contentValues);

        try (OutputStream outputStream = resolver.openOutputStream(uri)) {
            document.writeTo(outputStream);
            Toast.makeText(getContext(), "PDF saved to Downloads", Toast.LENGTH_LONG).show();
            Log.d(TAG, "PDF saved to Downloads");
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(getContext(), "Failed to save PDF", Toast.LENGTH_SHORT).show();
            Log.d(TAG, "Failed to save PDF");
        }
    }

    private void savePdfToExternalStorage(PdfDocument document) {
        File directory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        if (!directory.exists()) {
            directory.mkdirs();
        }
        File file = new File(directory, "pricelist.pdf");

        try {
            Log.d(TAG, "Writing PDF to file: " + file.getAbsolutePath());
            document.writeTo(new FileOutputStream(file));
            Toast.makeText(getContext(), "PDF saved to " + file.getAbsolutePath(), Toast.LENGTH_LONG).show();
            Log.d(TAG, "PDF saved to " + file.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(getContext(), "Failed to save PDF", Toast.LENGTH_SHORT).show();
            Log.d(TAG, "Failed to save PDF");
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                generatePdf();
            } else {
                Toast.makeText(getContext(), "Permission denied to write external storage", Toast.LENGTH_SHORT).show();
            }
        }
    }
}