package rs.ftn.ma.mplanner.interfaces;

import rs.ftn.ma.mplanner.model.ServiceReservation;

public interface ServiceReservationCallback {
    void onServiceReservationReceived(ServiceReservation[] serviceReservations);
    void onError(Exception e);
}
