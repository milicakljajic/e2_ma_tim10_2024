package rs.ftn.ma.mplanner.adapters;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.interfaces.EventUpdateCallback;
import rs.ftn.ma.mplanner.model.Event;
import rs.ftn.ma.mplanner.model.Guest;
import rs.ftn.ma.mplanner.repo.EventRepository;

public class GuestAdapter extends RecyclerView.Adapter<GuestAdapter.GuestViewHolder> {
    private List<Guest> guestList;

    private Event event;
    private Context context;

    public GuestAdapter(List<Guest> guestList, Event eve, Context context) {
        this.guestList = guestList;
        this.context = context;
        event = eve;
    }

    @NonNull
    @Override
    public GuestViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_guest_list_item, parent, false);
        return new GuestViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GuestViewHolder holder, int position) {
        Guest guest = guestList.get(position);
        holder.guestName.setText(guest.getName());
        holder.guestLastName.setText(guest.getLastName());
        holder.guestAgeCategory.setText("Age Category: " + getAgeCategoryString(guest.getAgeCategory()));
        holder.guestInvited.setText("Invited: " + (guest.isInvited() ? "Yes" : "No"));
        holder.guestAccepted.setText("Accepted: " + (guest.hasAccepted() ? "Yes" : "No"));
        holder.guestSpecialRequest.setText("Special Request: " + guest.getSpecialRequest());

        holder.editButton.setOnClickListener(v -> {
            // Otvori ekran za uređivanje gosta
            Bundle bundle = new Bundle();
            bundle.putString("guestName", guest.getName());
            bundle.putString("eventId", event.getId());
            Navigation.findNavController(v).navigate(R.id.changeToEditGuestFragment2, bundle);
        });

        holder.deleteButton.setOnClickListener(v -> {
            // Izbriši gosta

            Guest gg = guestList.get(position);
            Guest found = new Guest();
            for(Guest g: event.getGuestList())
            {
                if(gg.getName().equals(g.getName()))
                    found = gg;
            }
            event.getGuestList().remove(found);
            EventRepository erepo = new EventRepository();
            erepo.updateGuestList(event.getId(), event.getGuestList(), new EventUpdateCallback() {
                @Override
                public void onEventUpdated() {
                    Toast.makeText(v.getContext(), "Success", Toast.LENGTH_SHORT).show();
                    //guestList.remove(position);
                    //notifyItemRemoved(position);
                }

                @Override
                public void onUpdateError(Exception e) {

                }
            });
        });
    }

    @Override
    public int getItemCount() {
        return guestList.size();
    }

    private String getAgeCategoryString(int ageCategory) {
        switch (ageCategory) {
            case 0: return "0-3";
            case 1: return "3-10";
            case 2: return "10-18";
            case 3: return "18-30";
            case 4: return "30-50";
            case 5: return "50-70";
            case 6: return "70+";
            default: return "Unknown";
        }
    }

    public void updateGuestList(List<Guest> guestList) {
        this.guestList = guestList;
        notifyDataSetChanged();
    }

    public static class GuestViewHolder extends RecyclerView.ViewHolder {
        TextView guestName;
        TextView guestLastName;
        TextView guestAgeCategory;
        TextView guestInvited;
        TextView guestAccepted;
        TextView guestSpecialRequest;
        Button editButton;
        Button deleteButton;

        public GuestViewHolder(@NonNull View itemView) {
            super(itemView);
            guestName = itemView.findViewById(R.id.guest_name);
            guestLastName = itemView.findViewById(R.id.guest_last_name);
            guestAgeCategory = itemView.findViewById(R.id.guest_age_category);
            guestInvited = itemView.findViewById(R.id.guest_invited);
            guestAccepted = itemView.findViewById(R.id.guest_accepted);
            guestSpecialRequest = itemView.findViewById(R.id.guest_special_request);
            editButton = itemView.findViewById(R.id.edit_button);
            deleteButton = itemView.findViewById(R.id.delete_button);
        }
    }
}

