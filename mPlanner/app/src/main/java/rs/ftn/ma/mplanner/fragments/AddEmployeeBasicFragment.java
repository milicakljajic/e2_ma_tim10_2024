package rs.ftn.ma.mplanner.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.model.Employee;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AddEmployeeBasicFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddEmployeeBasicFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private Employee newEmployee;

    public AddEmployeeBasicFragment() {
        // Required empty public constructor
        newEmployee = new Employee();
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AddEmployeeBasicFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AddEmployeeBasicFragment newInstance(String param1, String param2) {
        AddEmployeeBasicFragment fragment = new AddEmployeeBasicFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_employee_basic, container, false);
        //Button button = (Button) view.findViewById(R.id.login_registration);
        Button button = (Button) view.findViewById(R.id.ae_basic_next);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                EditText email = view.findViewById(R.id.add_employee_email);
                EditText password1 = view.findViewById(R.id.add_employee_password_1);
                EditText password2 = view.findViewById(R.id.add_employee_password_2);
                String em = String.valueOf(email.getText());
                String ps1 = String.valueOf(password1.getText());
                String ps2 = String.valueOf(password2.getText());
                if(ps1.equals(ps2))
                {
                    newEmployee.setEmail(em);
                    newEmployee.setPassword(ps1);
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("Employee", newEmployee);
                    Navigation.findNavController(view).navigate(R.id.changeToAddEmployeePersonal1, bundle);
                }

            }
        });
        return view;
    }
}