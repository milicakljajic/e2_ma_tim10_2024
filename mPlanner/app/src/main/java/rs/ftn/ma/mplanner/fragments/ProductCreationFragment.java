package rs.ftn.ma.mplanner.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.adapters.ProductAdapter;
import rs.ftn.ma.mplanner.interfaces.ProductCallback;
import rs.ftn.ma.mplanner.interfaces.ProductInsertCallback;
import rs.ftn.ma.mplanner.interfaces.ProductUpdateCallback;
import rs.ftn.ma.mplanner.model.Category;
import rs.ftn.ma.mplanner.model.Product;
import rs.ftn.ma.mplanner.repo.ProductRepository;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProductCreationFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProductCreationFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "product-id";
    private static final String ARG_PARAM2 = "mode";

    // TODO: Rename and change types of parameters
    private String productId;
    private String mode;
    private Product currentProduct;

    private ProductAdapter productAdapter = null;
    private final ProductRepository repo;

    public ProductCreationFragment() {
        // Required empty public constructor
        if(currentProduct == null){
            currentProduct = new Product();
        }
        repo = new ProductRepository();
    }


    // TODO: Rename and change types and number of parameters
    public static ProductCreationFragment newInstance(String productId, String mode) {
        ProductCreationFragment fragment = new ProductCreationFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, productId);
        args.putString(ARG_PARAM2, mode);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            productId = getArguments().getString(ARG_PARAM1);
            mode = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_product_creation, container, false);
        if(mode!= null){
            if(mode.toLowerCase().equals("update")){
                repo.getById(new ProductCallback() {
                    @Override
                    public void onProductReceived(Product[] products) {
                        currentProduct = products[0];
                        currentProduct.setId(Integer.valueOf(productId));
                        TextView headerText = (TextView) view.findViewById(R.id.enterNameEditText);
                        headerText.setText("Update a product");
                        EditText descriptionText = (EditText) view.findViewById(R.id.enterDescriptionEditText);
                        descriptionText.setText(currentProduct.getDescription());
                        EditText priceText = (EditText) view.findViewById(R.id.enterPriceEditText);
                        priceText.setText(currentProduct.getPrice().toString());
                        //EditText eventText = (EditText) view.findViewById(R.id.product_create_event);
                        EditText discountText = (EditText) view.findViewById(R.id.enterDiscountEditText);
                        discountText.setText(currentProduct.getDiscountPercentage());

                        fillSpinners(view);
                    }

                    @Override
                    public void onError(Exception e) {

                    }
                },productId);

                Button button = (Button) view.findViewById(R.id.createButton);
                button.setText("Update");
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        TextView headerText = (TextView) view.findViewById(R.id.enterNameEditText);
                        headerText.setText("Update a product");
                        EditText descriptionText = (EditText) view.findViewById(R.id.enterDescriptionEditText);
                        descriptionText.setText(currentProduct.getDescription());
                        EditText priceText = (EditText) view.findViewById(R.id.enterPriceEditText);
                        priceText.setText(currentProduct.getPrice().toString());
                        //EditText eventText = (EditText) view.findViewById(R.id.product_create_event);
                        EditText discountText = (EditText) view.findViewById(R.id.enterDiscountEditText);
                        discountText.setText(currentProduct.getDiscountPercentage());


                        repo.update(currentProduct.getId().toString(), currentProduct, new ProductUpdateCallback() {
                            @Override
                            public void onProductUpdated() {

                            }

                            @Override
                            public void onUpdateError(Exception e) {

                            }
                        });
                    }
                });
            }
        }else{
            fillSpinners(view);
            Button button = (Button) view.findViewById(R.id.createButton);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TextView headerText = (TextView) view.findViewById(R.id.enterNameEditText);
                    headerText.setText("Update a product");
                    EditText descriptionText = (EditText) view.findViewById(R.id.enterDescriptionEditText);
                    descriptionText.setText(currentProduct.getDescription());
                    EditText priceText = (EditText) view.findViewById(R.id.enterPriceEditText);
                    priceText.setText(currentProduct.getPrice().toString());
                    //EditText eventText = (EditText) view.findViewById(R.id.product_create_event);
                    EditText discountText = (EditText) view.findViewById(R.id.enterDiscountEditText);
                    discountText.setText(currentProduct.getDiscountPercentage());

                    repo.create(currentProduct, new ProductInsertCallback() {
                        @Override
                        public void onProductCreated(String productId) {

                        }

                        @Override
                        public void onCreateError(Exception e) {

                        }
                    });

                }
            });
        }

        return view;
    }

    private void fillSpinners(View view){
        Category[] categories = new Category[]{
                new Category("1","Cat 1","Des 1"),
                new Category("2","Cat 2","Des 2")
        };

        Spinner spinner = (Spinner) view.findViewById(R.id.spinner2);
        ArrayAdapter<Category> categoryAdapter = new ArrayAdapter<>(
                requireContext(),
                android.R.layout.simple_spinner_item,
                categories
        );

        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(categoryAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Category selectedCategory = (Category) parent.getItemAtPosition(position);
                currentProduct.setCategoryId(selectedCategory.getId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}