package rs.ftn.ma.mplanner.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.interfaces.OnDeleteButtonClickListener;
import rs.ftn.ma.mplanner.model.EventType;

public class EventTypeAdapter extends BaseAdapter {
    Context context;
    ArrayList<EventType> eventTypes;
    LayoutInflater inflater;

    int toInflate;

    private OnDeleteButtonClickListener deleteButtonClickListener;

    public EventTypeAdapter(Context applicationContext, EventType[] eventTypes) {
        this.context = context;
        this.eventTypes = new ArrayList<>(Arrays.asList(eventTypes));
        inflater = (LayoutInflater.from(applicationContext));
        toInflate = -1;
    }

    public EventTypeAdapter(Context applicationContext, EventType[] eventTypes, int toInflate) {
        this.context = context;
        this.eventTypes = new ArrayList<>(Arrays.asList(eventTypes));
        inflater = (LayoutInflater.from(applicationContext));
        this.toInflate = toInflate;
    }

    public EventTypeAdapter(Context applicationContext, ArrayList<EventType> eventTypes, int toInflate) {
        this.context = context;
        this.eventTypes = eventTypes;
        inflater = (LayoutInflater.from(applicationContext));
        this.toInflate = toInflate;
    }

    public void setOnDeleteButtonClickListener(OnDeleteButtonClickListener listener) {
        this.deleteButtonClickListener = listener;
    }

    @Override
    public int getCount() {
        return eventTypes.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if(toInflate == R.layout.fragment_subcategory_list_item)
        {
            view = inflater.inflate(R.layout.fragment_subcategory_list_item, null);
            TextView name = (TextView) view.findViewById(R.id.subcategory_item_name);
            name.setText(eventTypes.get(i).getName());
            ImageView delete = (ImageView) view.findViewById(R.id.subcategory_item_delete);
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (deleteButtonClickListener != null) {
                        deleteButtonClickListener.onDeleteClicked(i);
                    }
                }
            });
        }
        else{
            view = inflater.inflate(R.layout.fragment_event_types_list_item, null);
            TextView name = (TextView) view.findViewById(R.id.event_type_item_name);
            name.setText(eventTypes.get(i).name);
        }
        return view;
    }
}
