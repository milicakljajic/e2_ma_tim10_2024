package rs.ftn.ma.mplanner.repo;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import rs.ftn.ma.mplanner.interfaces.ServiceCallback;
import rs.ftn.ma.mplanner.interfaces.ServiceDeleteCallback;
import rs.ftn.ma.mplanner.interfaces.ServiceInsertCallback;
import rs.ftn.ma.mplanner.interfaces.ServiceUpdateCallback;
import rs.ftn.ma.mplanner.model.Service;

public class ServiceRepository {
    public void create(Service service, ServiceInsertCallback callback){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("services")
                .add(service)
                .addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentReference> task) {
                        if(task.isSuccessful()){
                            DocumentReference documentReference = task.getResult();
                            String serviceId = documentReference.getId();
                            Log.d("REZ_DB","Service added with Id: " + serviceId);
                            callback.onServiceCreated(serviceId);
                        }else{
                            Log.w("REZ_DB","Error creating service",task.getException());
                            callback.onCreateError(task.getException());
                        }
                    }
                });
    }

    public void getAll(ServiceCallback callback){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("services")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if(task.isSuccessful()){
                            List<Service> serviceList = new ArrayList<>();
                            for(QueryDocumentSnapshot document: task.getResult()){
                                Service service = document.toObject(Service.class);
                                service.setId(Integer.valueOf(document.getId()));
                                serviceList.add(service);
                                Log.d("REZ_DB",document.getId() + "=>" + document.getData());
                            }
                            callback.onServiceReceived(serviceList.toArray(new Service[0]));
                        }else{
                            Log.w("REZ_DB","Error getting documents",task.getException());
                        }    callback.onError(task.getException());
                    }
                });
    }

    public void update(String id, Service service, ServiceUpdateCallback callback){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("services").document(id);

        docRef.update("name",service.getName(),
                    "description",service.getDescription(),
                    "category",service.getCategory(),
                    "subcategory",service.getSubcategory(),
                    "price",service.getPrice(),
                    "discount",service.getDiscount(),
                    "state",service.getState(),
                    "isAvailable",service.isAvailable(),
                    "durationInHours",service.getDurationInHours(),
                    "reservationDeadline",service.getReservationDeadline(),
                    "cancelationDeadline",service.getCancelationDeadline())
                .addOnSuccessListener(aVoid -> {
                    Log.d("REZ_DB","Service updated");
                    callback.onServiceUpdated();
                })
                .addOnFailureListener(e -> {
                    Log.w("REZ_DB","Error getting documents",e);
                    callback.onUpdateError(e);
                });
    }

    public void getById(ServiceCallback callback,String id){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("services").document(id);

        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful()){
                    DocumentSnapshot snapshot = task.getResult();
                    if(snapshot.exists()){
                        Service[] services = new Service[]{snapshot.toObject(Service.class)};
                        Log.d("REZ_DB",snapshot.getId() + "=>" + snapshot.getData());
                        callback.onServiceReceived(services);
                    }else {
                        Log.d("REZ_DB","No such document");
                        callback.onError(new Exception("No such document"));
                    }
                }else {
                    Log.w("REZ_DB","Error getting document",task.getException());
                    callback.onError(task.getException());
                }
            }
        });
    }

    public void delete(String id, ServiceDeleteCallback callback){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("products").document(id);

        docRef.delete()
                .addOnSuccessListener(aVoid -> {
                    Log.d("REZ_DB","Product successfully deleted");
                    callback.onServiceDeleted();
                })
                .addOnFailureListener(e -> {
                    Log.w("REZ_DB","Error deleting product",e);
                    callback.onDeleteError(e);
                });

    }
}
