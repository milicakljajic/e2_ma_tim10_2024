package rs.ftn.ma.mplanner.fragments;

import android.content.ContentValues;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.os.Build;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.interfaces.EventCallback;
import rs.ftn.ma.mplanner.interfaces.EventUpdateCallback;
import rs.ftn.ma.mplanner.model.Event;
import rs.ftn.ma.mplanner.model.Guest;
import rs.ftn.ma.mplanner.repo.EventRepository;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CreationGuestListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CreationGuestListFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private EventRepository repo;

    private EditText guestNameEditText;
    private EditText guestLastNameEditText;
    private Spinner ageSpinner;
    private Switch invitedSwitch;
    private Switch acceptedSwitch;
    private Switch veganSwitch;
    private Switch vegetarianSwitch;
    private Button addGuestButton, btnGeneratePdf, seeGuestsButton;
    private Event currentEvent;
    private String eventId = "YOUR_EVENT_ID"; // Postavite ID događaja koji želite da preuzmete

    public CreationGuestListFragment() {
        repo = new EventRepository();
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CreationGuestListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CreationGuestListFragment newInstance(String param1, String param2) {
        CreationGuestListFragment fragment = new CreationGuestListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@Nonnull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_creation_guest_list, container, false);
        Spinner spinner = (Spinner) view.findViewById(R.id.event_spinner);
        repo.select(new EventCallback() {
            @Override
            public void onEventReceived(Event[] events) {

                ArrayAdapter<Event> adapter = new ArrayAdapter<Event>(
                        requireContext(),
                        android.R.layout.simple_spinner_item,
                        events
                );
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(adapter);
            }

            @Override
            public void onError(Exception e) {

            }
        }, false);

        guestNameEditText = view.findViewById(R.id.guest_name);
        guestLastNameEditText = view.findViewById(R.id.guest_last_name);
        ageSpinner = view.findViewById(R.id.age_spinner);
        invitedSwitch = view.findViewById(R.id.invited_switch);
        acceptedSwitch = view.findViewById(R.id.accepted_switch);
        veganSwitch = view.findViewById(R.id.vegan_switch);
        vegetarianSwitch = view.findViewById(R.id.vegetarian_switch);
        addGuestButton = view.findViewById(R.id.add_guest_button);
        btnGeneratePdf = view.findViewById(R.id.generate_pdf_button);
        seeGuestsButton = view.findViewById(R.id.see_guest_list_button);

        /*Button button = (Button) view.findViewById(R.id.add_guest_button);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Navigation.findNavController(view).navigate(R.id.changeToGuestListFragment);
            }
        });*/

        // Disable the addGuestButton initially
        addGuestButton.setEnabled(true);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Event e = (Event) parent.getItemAtPosition(position);
                if(!e.getName().equals("--"))
                    currentEvent = e;
                // Dodaj dodatne logike ovde ako je potrebno
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Prazno telo metode - možeš dodati logiku ako je potrebno
            }
        });


        addGuestButton.setOnClickListener(v -> {
            String name = guestNameEditText.getText().toString();
            String lastName = guestLastNameEditText.getText().toString();
            int ageCategory = ageSpinner.getSelectedItemPosition(); // Pretvaramo poziciju spinnera u int
            boolean isInvited = invitedSwitch.isChecked();
            boolean hasAccepted = acceptedSwitch.isChecked();
            String specialRequest = "";

            if (veganSwitch.isChecked()) {
                specialRequest = "Vegan";
            } else if (vegetarianSwitch.isChecked()) {
                specialRequest = "Vegetarian";
            }

            Guest newGuest = new Guest(name, lastName, ageCategory, isInvited, hasAccepted, specialRequest);



            // Add the new guest to the event's guest list
            if (currentEvent != null) {
                if (currentEvent.getGuestList() == null) {
                    currentEvent.setGuestList(new ArrayList<>());
                }
                currentEvent.getGuestList().add(newGuest);

                repo.updateGuestList(currentEvent.getId(), currentEvent.getGuestList(), new EventUpdateCallback() {
                    @Override
                    public void onEventUpdated() {
                        Bundle bundle = new Bundle();
                        bundle.putString("id", currentEvent.getId());
                        Navigation.findNavController(view).navigate(R.id.changeToGuestListFragment, bundle);
                    }

                    @Override
                    public void onUpdateError(Exception e) {

                    }
                });
            } else {
                Toast.makeText(getContext(), "Event is not loaded yet", Toast.LENGTH_SHORT).show();
            }
        });

        seeGuestsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                repo.updateGuestList(currentEvent.getId(), currentEvent.getGuestList(), new EventUpdateCallback() {
                    @Override
                    public void onEventUpdated() {
                        Bundle bundle = new Bundle();
                        bundle.putString("id", currentEvent.getId());
                        Navigation.findNavController(view).navigate(R.id.changeToGuestListFragment2, bundle);
                    }

                    @Override
                    public void onUpdateError(Exception e) {

                    }
                });
            }
        });

        btnGeneratePdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generatePdf();
            }
        });


        return view;
    }


    private void generatePdf() {
        /*Event selectedEvent = (Event) spinnerEvents.getSelectedItem();
        if (selectedEvent == null) {
            Toast.makeText(getContext(), "Please select an event", Toast.LENGTH_SHORT).show();
            return;
        }*/

        // Create a new PDF document
        PdfDocument document = new PdfDocument();
        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(300, 600, 1).create();
        PdfDocument.Page page = document.startPage(pageInfo);
        int y = 25;
        Paint paint = new Paint();
        paint.setColor(Color.BLACK); // Podesi boju teksta
        paint.setTextSize(12); // Podesi veličinu teksta
        paint.setTextAlign(Paint.Align.LEFT); // Podesi poravnanje teksta
        //Guest guest : currentEvent.getGuestList
        for (Guest guest : currentEvent.getGuestList()) {
            String text = guest.getName() + " - " + guest.getLastName() + " - " + guest.getAgeCategory() + " invited: " + guest.isInvited() + " accepted: " + guest.hasAccepted() + " special requests: " + guest.getSpecialRequest();
            page.getCanvas().drawText(text, 10, y, paint);
            y += 25;
        }

        document.finishPage(page);

        // Save the PDF to the device
        String fileName = "Guest_List.pdf";
        OutputStream fos;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, fileName);
            contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "application/pdf");
            contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_DOWNLOADS);

            try {
                fos = getContext().getContentResolver().openOutputStream(
                        getContext().getContentResolver().insert(MediaStore.Downloads.EXTERNAL_CONTENT_URI, contentValues));
            } catch (FileNotFoundException e) {
                throw new RuntimeException(e);
            }
        } else {
            File downloadsDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
            File file = new File(downloadsDir, fileName);
            try {
                fos = new FileOutputStream(file);
            } catch (FileNotFoundException e) {
                throw new RuntimeException(e);
            }
        }

        try {
            document.writeTo(fos);
            fos.close();
            Toast.makeText(getContext(), "PDF saved in Downloads", Toast.LENGTH_SHORT).show();
            Log.d("", "PDF saved in Downloads");
        } catch (IOException e) {
            Log.e("", "Error generating PDF", e);
            Toast.makeText(getContext(), "Error generating PDF", Toast.LENGTH_SHORT).show();
        }

        document.close();

        /*@Override
        public void onError(Exception e) {

        }*/


    }

}