package rs.ftn.ma.mplanner.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;

import android.widget.Spinner;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.adapters.ProductAdapter;
import rs.ftn.ma.mplanner.interfaces.EventCallback;
import rs.ftn.ma.mplanner.interfaces.EventOrganizerCallback;
import rs.ftn.ma.mplanner.interfaces.EventUpdateCallback;
import rs.ftn.ma.mplanner.model.Event;
import rs.ftn.ma.mplanner.model.EventOrganizer;
import rs.ftn.ma.mplanner.model.Product;
import rs.ftn.ma.mplanner.repo.EventOrganizerRepo;
import rs.ftn.ma.mplanner.repo.EventRepository;
import rs.ftn.ma.mplanner.repo.ProductRepository;


public class ProductListFragment extends Fragment {

    private ProductRepository repo;

    private ArrayList<Event> availableEvents;

    private Event selectedEvent;

    private ExecutorService executorService;

    public ProductListFragment() {
        // Required empty public constructor
        repo = new ProductRepository();
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProductListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProductListFragment newInstance(String param1, String param2) {
        ProductListFragment fragment = new ProductListFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        executorService = Executors.newSingleThreadExecutor(); // Inicijalizujte ExecutorService
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.product_list_fragment, container, false);
        Spinner spinner = view.findViewById(R.id.product_page_event_spinner);
        EventRepository erepo = new EventRepository();
        EventOrganizerRepo eorepo = new EventOrganizerRepo();
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        FirebaseUser cUser = mAuth.getCurrentUser();
        eorepo.selectByFirebaseId(new EventOrganizerCallback() {
            @Override
            public void onEventOrganizerReceived(EventOrganizer[] types) {
                erepo.selectByCreator(new EventCallback() {
                    @Override
                    public void onEventReceived(Event[] events) {
                        availableEvents = new ArrayList<>(Arrays.asList(events));
                        ArrayAdapter<Event> adapter = new ArrayAdapter<Event>(
                                view.getContext(),
                                android.R.layout.simple_spinner_item,
                                availableEvents
                        );

                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner.setAdapter(adapter);
                    }

                    @Override
                    public void onError(Exception e) {

                    }
                }, false, types[0]);
            }

            @Override
            public void onError(Exception e) {

            }
        }, cUser.getUid());

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.d("ss", "onItemSelected: " + availableEvents.get(position));
                selectedEvent = availableEvents.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /*repo.getAll(new ProductCallback() {
            @Override
            public void onProductReceived(Product[] products) {
                ListView lv = view.findViewById(R.id.product_list);
                ArrayList<Product> prods = new ArrayList<>(Arrays.asList(products));
                ProductAdapter adapter = new ProductAdapter(requireContext(),prods,0);
                adapter.setOnDeleteButtonClickListener(new OnDeleteButtonClickListener() {
                    @Override
                    public void onDeleteClicked(int position) {
                        Product temp = prods.get(position);

                        repo.delete(temp.getId().toString(), new ProductDeleteCallback() {
                            @Override
                            public void onProductDeleted() {

                            }

                            @Override
                            public void onDeleteError(Exception ex) {

                            }
                        });
                        prods.remove(position);
                        adapter.notifyDataSetChanged();
                    }
                });
                lv.setOnItemClickListener(((parent, view1, position, id) -> {
                    Product temp = products[position];
                    Bundle bundle = new Bundle();
                    bundle.putString("mode","update");
                    bundle.putString("id",temp.getId().toString());
                    Navigation.findNavController(view1).navigate(R.id.changeToProductCreation1);
                }));
                lv.setAdapter(adapter);
            }

            @Override
            public void onError(Exception e) {

            }
        });*/


        Product[] products = new Product[]{
                new Product(1, "Camera", "Professional DSLR camera for photography enthusiasts", 899.99, "Electronics", "Cameras", 0, true, false),
                new Product(2, "Tripod", "Sturdy tripod for stable photography shots", 49.99, "Accessories", "Tripods", 0, true, false),
                new Product(3, "Laptop", "High-performance laptop for gaming and productivity", 1299.99, "Electronics", "Computers", 10, true, false),
                new Product(4, "Smartphone", "Latest model smartphone with advanced features", 799.99, "Electronics", "Mobile Phones", 5, true, false),
                new Product(5, "Headphones", "Noise-cancelling headphones for immersive sound experience", 199.99, "Accessories", "Audio", 2, true, false),
                new Product(6, "Smartwatch", "Waterproof smartwatch with fitness tracking", 249.99, "Electronics", "Wearables", 8, true, false),
                new Product(7, "External Hard Drive", "Portable external hard drive with 2TB storage", 99.99, "Electronics", "Storage", 20, true, false),
                new Product(8, "Gaming Console", "Next-gen gaming console with 4K support", 499.99, "Electronics", "Gaming", 3, true, false),
                new Product(9, "Camera Lens", "Wide-angle lens for landscape photography", 299.99, "Accessories", "Camera Lenses", 4, true, false),
                new Product(10, "Bluetooth Speaker", "Portable Bluetooth speaker with high-quality sound", 59.99, "Accessories", "Audio", 15, true, false)
        };

        RecyclerView lv = view.findViewById(R.id.product_list);
        ProductAdapter adapter = new ProductAdapter(requireContext(),products,0);


        //Spinner spinner = view.findViewById(R.id.products_sort);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_item,
                getResources().getStringArray(R.array.sort_array));

        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        //spinner.setAdapter(arrayAdapter);
        /*spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/

        Button addToBudget = view.findViewById(R.id.btnAddToBudget);
        addToBudget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<Product> send = new ArrayList<>();
                for(Product p : products)
                {
                    //Log.d("dd", "onClick: " + p.getSelected());
                    if(p.getSelected())
                        send.add(p);
                }


                if (selectedEvent != null && !selectedEvent.getName().equals("--")) {
                    executorService.execute(new Runnable() {
                        @Override
                        public void run() {
                            erepo.updateEventItems(selectedEvent.id, selectedEvent, new EventUpdateCallback() {
                                @Override
                                public void onEventUpdated() {
                                    // Log uspešnog ažuriranja na glavnoj niti
                                    requireActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Log.d("EventUpdate", "Event items successfully updated.");
                                        }
                                    });
                                }

                                @Override
                                public void onUpdateError(Exception e) {
                                    // Log greške na glavnoj niti
                                    requireActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Log.d("EventUpdate", "Error updating event items: " + e.getMessage());
                                        }
                                    });
                                }
                            });
                        }
                    });
                }




                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList("SentProducts", send);
                Navigation.findNavController(v).navigate(R.id.changeSendProductsToBudget, bundle);
            }
        });

        return view;
    }
}