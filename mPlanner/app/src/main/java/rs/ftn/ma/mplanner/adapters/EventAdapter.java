package rs.ftn.ma.mplanner.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.model.Event;

public class EventAdapter extends BaseAdapter {
    Context context;
    Event[] events;
    LayoutInflater inflter;

    public EventAdapter(Context applicationContext, Event[] events) {
        this.context = context;
        this.events = events;
        inflter = (LayoutInflater.from(applicationContext));
    }


    @Override
    public int getCount() {
        return events.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.fragment_calendar_event_list_item,null);
        TextView name = (TextView) view.findViewById(R.id.event_name);
        TextView status = (TextView) view.findViewById(R.id.event_status);
        TextView date = (TextView) view.findViewById(R.id.event_date);
        TextView time = (TextView) view.findViewById(R.id.event_time);
        name.setText(events[i].getName());
        status.setText(events[i].getStatus());
        date.setText(events[i].getDate());
        time.setText(events[i].getTime());
        return view;
    }
}
