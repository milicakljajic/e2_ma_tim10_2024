package rs.ftn.ma.mplanner.interfaces;

public interface EventOrganizerInsertCallback {
    void onEventOrganizerInserted(String eventId);
    void onInsertError(Exception e);
}
