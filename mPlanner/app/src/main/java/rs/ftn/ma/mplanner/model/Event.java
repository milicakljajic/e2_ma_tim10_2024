package rs.ftn.ma.mplanner.model;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;


public class Event implements Parcelable {
    public String id;
    public EventType eventType;

    public String name;

    private EventOrganizer creator;

    public String description;
    public int maxParticipants;
    public Boolean isPrivate;
    public String location;
    public int maxKilometers;
    public String status;
    public String date;
    public String time;

    public String from;

    public String to;
    public List<Guest> guestList;
    private List<Activity> activities;

    private ArrayList<Product> selectedProducts;
    private ArrayList<Service> selectedServices;
    private ArrayList<Bundle> selectedBundles;

    public Event() {
        this.activities = new ArrayList<>();
        this.guestList = new ArrayList<>();
    }

    public Event(EventType eventType, String name, String description, int maxParticipants, Boolean isPrivate, String location, int maxKilometers, String status, String date, String time) {
        this.eventType = eventType;
        this.name = name;
        this.description = description;
        this.maxParticipants = maxParticipants;
        this.isPrivate = isPrivate;
        this.location = location;
        this.maxKilometers = maxKilometers;
        this.status = status;
        this.date = date;
        this.time = time;
        this.guestList = new ArrayList<>();
        this.activities = new ArrayList<>();
        creator = new EventOrganizer();
        selectedProducts = new ArrayList<>();
        selectedBundles = new ArrayList<>();
        selectedServices = new ArrayList<>();
    }

    public Event(String id, EventType eventType, String name, String description, int maxParticipants, Boolean isPrivate, String location, int maxKilometers, String status, String date, String time, String from, String to) {
        this.id = id;
        this.eventType = eventType;
        this.name = name;
        this.description = description;
        this.maxParticipants = maxParticipants;
        this.isPrivate = isPrivate;
        this.location = location;
        this.maxKilometers = maxKilometers;
        this.status = status;
        this.date = date;
        this.time = time;
        this.from = from;
        this.to = to;
        this.guestList = new ArrayList<>();
        this.activities = new ArrayList<>();
        creator = new EventOrganizer();
        selectedProducts = new ArrayList<>();
        selectedBundles = new ArrayList<>();
        selectedServices = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getMaxParticipants() {
        return maxParticipants;
    }

    public void setMaxParticipants(int maxParticipants) {
        this.maxParticipants = maxParticipants;
    }

    public Boolean getIsPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(Boolean isPrivate) {
        this.isPrivate = isPrivate;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getMaxKilometers() {
        return maxKilometers;
    }

    public void setMaxKilometers(int maxKilometers) {
        this.maxKilometers = maxKilometers;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }
    public List<Guest> getGuestList() {
        return guestList;
    }

    public void setGuestList(List<Guest> guestList) {
        this.guestList = guestList;
    }
    public List<Activity> getActivities() {
        return activities;
    }

    public void setActivities(List<Activity> activities) {
        this.activities = activities;
    }

    public EventOrganizer getCreator() {
        return creator;
    }

    public void setCreator(EventOrganizer creator) {
        this.creator = creator;
    }

    public ArrayList<Product> getSelectedProducts() {
        return selectedProducts;
    }

    public void setSelectedProducts(ArrayList<Product> selectedProducts) {
        this.selectedProducts = selectedProducts;
    }

    public ArrayList<Service> getSelectedServices() {
        return selectedServices;
    }

    public void setSelectedServices(ArrayList<Service> selectedServices) {
        this.selectedServices = selectedServices;
    }

    public ArrayList<Bundle> getSelectedBundles() {
        return selectedBundles;
    }

    public void setSelectedBundles(ArrayList<Bundle> selectedBundles) {
        this.selectedBundles = selectedBundles;
    }



    @Override
    public String toString() {
        return name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeParcelable(this.eventType, flags);
        dest.writeString(this.name);
        dest.writeParcelable(this.creator, flags);
        dest.writeString(this.description);
        dest.writeInt(this.maxParticipants);
        dest.writeValue(this.isPrivate);
        dest.writeString(this.location);
        dest.writeInt(this.maxKilometers);
        dest.writeString(this.status);
        dest.writeString(this.date);
        dest.writeString(this.time);
        dest.writeString(this.from);
        dest.writeString(this.to);
        dest.writeTypedList(this.guestList);
        dest.writeList(this.activities);
        dest.writeTypedList(this.selectedProducts);
        dest.writeTypedList(this.selectedServices);
        dest.writeTypedList(this.selectedBundles);
    }

    public void readFromParcel(Parcel source) {
        this.id = source.readString();
        this.eventType = source.readParcelable(EventType.class.getClassLoader());
        this.name = source.readString();
        this.creator = source.readParcelable(EventOrganizer.class.getClassLoader());
        this.description = source.readString();
        this.maxParticipants = source.readInt();
        this.isPrivate = (Boolean) source.readValue(Boolean.class.getClassLoader());
        this.location = source.readString();
        this.maxKilometers = source.readInt();
        this.status = source.readString();
        this.date = source.readString();
        this.time = source.readString();
        this.from = source.readString();
        this.to = source.readString();
        this.guestList = source.createTypedArrayList(Guest.CREATOR);
        this.activities = new ArrayList<Activity>();
        source.readList(this.activities, Activity.class.getClassLoader());
        this.selectedProducts = source.createTypedArrayList(Product.CREATOR);
        this.selectedServices = source.createTypedArrayList(Service.CREATOR);
        this.selectedBundles = source.createTypedArrayList(Bundle.CREATOR);
    }

    protected Event(Parcel in) {
        this.id = in.readString();
        this.eventType = in.readParcelable(EventType.class.getClassLoader());
        this.name = in.readString();
        this.creator = in.readParcelable(EventOrganizer.class.getClassLoader());
        this.description = in.readString();
        this.maxParticipants = in.readInt();
        this.isPrivate = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.location = in.readString();
        this.maxKilometers = in.readInt();
        this.status = in.readString();
        this.date = in.readString();
        this.time = in.readString();
        this.from = in.readString();
        this.to = in.readString();
        this.guestList = in.createTypedArrayList(Guest.CREATOR);
        this.activities = new ArrayList<Activity>();
        in.readList(this.activities, Activity.class.getClassLoader());
        this.selectedProducts = in.createTypedArrayList(Product.CREATOR);
        this.selectedServices = in.createTypedArrayList(Service.CREATOR);
        this.selectedBundles = in.createTypedArrayList(Bundle.CREATOR);
    }

    public static final Creator<Event> CREATOR = new Creator<Event>() {
        @Override
        public Event createFromParcel(Parcel source) {
            return new Event(source);
        }

        @Override
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };
}

