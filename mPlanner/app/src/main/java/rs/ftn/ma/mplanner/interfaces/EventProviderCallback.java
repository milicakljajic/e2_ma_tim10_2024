package rs.ftn.ma.mplanner.interfaces;

import rs.ftn.ma.mplanner.model.EventProvider;

public interface EventProviderCallback {
    void onEventProviderReceived(EventProvider[] providers);
    void onError(Exception e);
}
