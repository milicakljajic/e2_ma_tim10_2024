package rs.ftn.ma.mplanner.fragments;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.model.EventProvider;

public class RegistrationScheduleFragment extends Fragment {

    private static final String ARG_PARAM1 = "EventProvider";
    private EventProvider newEventProvider;
    private TextView text;

    public RegistrationScheduleFragment() {
        // Required empty public constructor
    }

    public static RegistrationScheduleFragment newInstance(EventProvider newEventProvider) {
        RegistrationScheduleFragment fragment = new RegistrationScheduleFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, newEventProvider);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            newEventProvider = getArguments().getParcelable(ARG_PARAM1);
        }
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_registration_schedule, container, false);
        View view =  inflater.inflate(R.layout.fragment_registration_schedule, container, false);

        //Button button = (Button) view.findViewById(R.id.login_registration);
        Button button = (Button) view.findViewById(R.id.registration_schedule_next);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("EventProvider", newEventProvider);
                Navigation.findNavController(view).navigate(R.id.changeToRegistrationPhoto1, bundle);
            }
        });
        button = (Button) view.findViewById(R.id.registration_schedule_skip);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Navigation.findNavController(view).navigate(R.id.changeToRegistrationPhoto1);
            }
        });

        Button buttonMondayFrom = view.findViewById(R.id.registration_schedule_time_from_monday);
        buttonMondayFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mHour = c.get(Calendar.HOUR_OF_DAY);
                int mMinute = c.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(requireContext(),new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        //text.setText(hourOfDay);
                        String h = String.valueOf(hourOfDay) + ":" + String.valueOf(minute);
                        newEventProvider.getCompany().getSchedule().setMondayFrom(h);
                    }
                }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });

        Button buttonMondayTo = view.findViewById(R.id.registration_schedule_time_to_monday);
        buttonMondayTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mHour = c.get(Calendar.HOUR_OF_DAY);
                int mMinute = c.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(requireContext(),new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        //text.setText(hourOfDay);
                        String h = String.valueOf(hourOfDay) + ":" + String.valueOf(minute);
                        newEventProvider.getCompany().getSchedule().setMondayTo(h);
                    }
                }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });

        Button buttonTuesdayFrom = view.findViewById(R.id.registration_schedule_time_from_tuesday);
        buttonTuesdayFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mHour = c.get(Calendar.HOUR_OF_DAY);
                int mMinute = c.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(requireContext(),new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        //text.setText(hourOfDay);
                        String h = String.valueOf(hourOfDay) + ":" + String.valueOf(minute);
                        newEventProvider.getCompany().getSchedule().setTuesdayFrom(h);
                    }
                }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });

        Button buttonTuesdayTo = view.findViewById(R.id.registration_schedule_time_to_tuesday);
        buttonTuesdayTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mHour = c.get(Calendar.HOUR_OF_DAY);
                int mMinute = c.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(requireContext(),new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        //text.setText(hourOfDay);
                        String h = String.valueOf(hourOfDay) + ":" + String.valueOf(minute);
                        newEventProvider.getCompany().getSchedule().setTuesdayTo(h);
                    }
                }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });

        Button buttonWednesdayFrom = view.findViewById(R.id.registration_schedule_time_from_wednesday);
        buttonWednesdayFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mHour = c.get(Calendar.HOUR_OF_DAY);
                int mMinute = c.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(requireContext(),new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        //text.setText(hourOfDay);
                        String h = String.valueOf(hourOfDay) + ":" + String.valueOf(minute);
                        newEventProvider.getCompany().getSchedule().setWednesdayFrom(h);
                    }
                }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });

        Button buttonWednesdayTo = view.findViewById(R.id.registration_schedule_time_to_wednesday);
        buttonWednesdayTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mHour = c.get(Calendar.HOUR_OF_DAY);
                int mMinute = c.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(requireContext(),new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        //text.setText(hourOfDay);
                        String h = String.valueOf(hourOfDay) + ":" + String.valueOf(minute);
                        newEventProvider.getCompany().getSchedule().setWednesdayTo(h);
                    }
                }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });

        Button buttonThursdayFrom = view.findViewById(R.id.registration_schedule_time_from_thursday);
        buttonThursdayFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mHour = c.get(Calendar.HOUR_OF_DAY);
                int mMinute = c.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(requireContext(),new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        //text.setText(hourOfDay);
                        String h = String.valueOf(hourOfDay) + ":" + String.valueOf(minute);
                        newEventProvider.getCompany().getSchedule().setThursdayFrom(h);
                    }
                }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });

        Button buttonThursdayTo = view.findViewById(R.id.registration_schedule_time_to_thursday);
        buttonThursdayTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mHour = c.get(Calendar.HOUR_OF_DAY);
                int mMinute = c.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(requireContext(),new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        //text.setText(hourOfDay);
                        String h = String.valueOf(hourOfDay) + ":" + String.valueOf(minute);
                        newEventProvider.getCompany().getSchedule().setThursdayTo(h);
                    }
                }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });

        Button buttonFridayFrom = view.findViewById(R.id.registration_schedule_time_from_friday);
        buttonFridayFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mHour = c.get(Calendar.HOUR_OF_DAY);
                int mMinute = c.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(requireContext(),new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        //text.setText(hourOfDay);
                        String h = String.valueOf(hourOfDay) + ":" + String.valueOf(minute);
                        newEventProvider.getCompany().getSchedule().setFridayFrom(h);
                    }
                }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });

        Button buttonFridayTo = view.findViewById(R.id.registration_schedule_time_to_friday);
        buttonFridayTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mHour = c.get(Calendar.HOUR_OF_DAY);
                int mMinute = c.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(requireContext(),new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        //text.setText(hourOfDay);
                        String h = String.valueOf(hourOfDay) + ":" + String.valueOf(minute);
                        newEventProvider.getCompany().getSchedule().setFridayTo(h);
                    }
                }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });

        Button buttonSaturdayFrom = view.findViewById(R.id.registration_schedule_time_from_saturday);
        buttonSaturdayFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mHour = c.get(Calendar.HOUR_OF_DAY);
                int mMinute = c.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(requireContext(),new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        //text.setText(hourOfDay);
                        String h = String.valueOf(hourOfDay) + ":" + String.valueOf(minute);
                        newEventProvider.getCompany().getSchedule().setSaturdayFrom(h);
                    }
                }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });

        Button buttonSaturdayTo = view.findViewById(R.id.registration_schedule_time_to_saturday);
        buttonSaturdayTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mHour = c.get(Calendar.HOUR_OF_DAY);
                int mMinute = c.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(requireContext(),new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        //text.setText(hourOfDay);
                        String h = String.valueOf(hourOfDay) + ":" + String.valueOf(minute);
                        newEventProvider.getCompany().getSchedule().setSaturdayTo(h);
                    }
                }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });

        Button buttonSundayFrom = view.findViewById(R.id.registration_schedule_time_from_sunday);
        buttonSundayFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mHour = c.get(Calendar.HOUR_OF_DAY);
                int mMinute = c.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(requireContext(),new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        //text.setText(hourOfDay);
                        String h = String.valueOf(hourOfDay) + ":" + String.valueOf(minute);
                        newEventProvider.getCompany().getSchedule().setSundayFrom(h);
                    }
                }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });

        Button buttonSundayTo = view.findViewById(R.id.registration_schedule_time_to_sunday);
        buttonSundayTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mHour = c.get(Calendar.HOUR_OF_DAY);
                int mMinute = c.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(requireContext(),new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        //text.setText(hourOfDay);
                        String h = String.valueOf(hourOfDay) + ":" + String.valueOf(minute);
                        newEventProvider.getCompany().getSchedule().setSundayTo(h);
                    }
                }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });

        Button buttonStartDate = view.findViewById(R.id.registration_schedule_start_date);
        buttonStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(requireContext(),new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        month += 1;
                        String s = String.valueOf(dayOfMonth) + "." + String.valueOf(month) + "." + String.valueOf(year);
                        newEventProvider.getCompany().getSchedule().setStartDate(s);
                        //text.setText(String.valueOf(year));
                    }
                }, year, month, 15);
                datePickerDialog.show();
            }
        });

        Button buttonEndDate = view.findViewById(R.id.registration_schedule_end_date);
        buttonEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(requireContext(),new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        month += 1;
                        String s = String.valueOf(dayOfMonth) + "." + String.valueOf(month) + "." + String.valueOf(year);
                        newEventProvider.getCompany().getSchedule().setEndDate(s);
                        //text.setText(String.valueOf(year));
                    }
                }, year, month, 15);
                datePickerDialog.show();
            }
        });


        return view;
    }
}