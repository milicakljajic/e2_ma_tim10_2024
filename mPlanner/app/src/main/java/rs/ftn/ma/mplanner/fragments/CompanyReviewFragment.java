package rs.ftn.ma.mplanner.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Date;
import java.util.Objects;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.interfaces.CommentInsertCallback;
import rs.ftn.ma.mplanner.interfaces.EventOrganizerCallback;
import rs.ftn.ma.mplanner.interfaces.NotificationInsertCallback;
import rs.ftn.ma.mplanner.model.Comment;
import rs.ftn.ma.mplanner.model.EventOrganizer;
import rs.ftn.ma.mplanner.model.EventProvider;
import rs.ftn.ma.mplanner.model.Notification;
import rs.ftn.ma.mplanner.repo.CommentRepository;
import rs.ftn.ma.mplanner.repo.EventOrganizerRepo;
import rs.ftn.ma.mplanner.repo.NotificationRepository;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CompanyReviewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CompanyReviewFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "EventProvider";
    Comment newComment;
    CommentRepository crepo;

    EventOrganizerRepo erepo;

    NotificationRepository nrepo;

    // TODO: Rename and change types of parameters
    private EventProvider mParam1;

    public CompanyReviewFragment() {
        // Required empty public constructor4
        erepo = new EventOrganizerRepo();
        crepo = new CommentRepository();
        nrepo = new NotificationRepository();
    }

    // TODO: Rename and change types and number of parameters
    public static CompanyReviewFragment newInstance(EventProvider param1) {
        CompanyReviewFragment fragment = new CompanyReviewFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getParcelable(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_company_review, container, false);

        MaterialButton submit = v.findViewById(R.id.company_review_create);
        MaterialButton home = v.findViewById(R.id.company_review_home);

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(v).navigate(R.id.reviewChangeToHome);
            }
        });

        RatingBar bar = v.findViewById(R.id.company_review_rate_stars);
        TextInputEditText mes = v.findViewById(R.id.commentInput);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newComment = new Comment();
                FirebaseAuth mauth = FirebaseAuth.getInstance();
                FirebaseUser cuser = mauth.getCurrentUser();
                erepo.selectByFirebaseId(new EventOrganizerCallback() {
                    @Override
                    public void onEventOrganizerReceived(EventOrganizer[] types) {
                        newComment.setUserId(types[0].getId());
                        newComment.setFirstname(types[0].getFirstName());
                        newComment.setLastname(types[0].getLastName());
                        newComment.setUsername(types[0].getEmail());
                        newComment.setLastModified(new Date());
                        newComment.setCompany(mParam1.getCompany().getName());

                        newComment.setRate(String.valueOf(bar.getRating()));

                        newComment.setMessage(Objects.requireNonNull(mes.getText()).toString());
                        //rate, message
                        crepo.insert(newComment, new CommentInsertCallback() {
                            @Override
                            public void onCommentInserted(String commentId) {
                                Log.d("Comment", "onCommentInserted: Success");
                                Notification n = new Notification(mParam1.getFirebaseUserId(),"Attention","You have a new rating",false);
                                nrepo.insert(n, new NotificationInsertCallback() {
                                    @Override
                                    public void onNotificationInserted(String notificationId) {
                                        Toast.makeText(v.getContext(), "Succefully reviewed", Toast.LENGTH_SHORT).show();
                                    }

                                    @Override
                                    public void onInsertError(Exception e) {

                                    }
                                });
                            }

                            @Override
                            public void onInsertError(Exception e) {
                                Log.d("err", "onInsertError: " + e.toString());
                            }
                        });
                    }

                    @Override
                    public void onError(Exception e) {

                    }
                }, cuser.getUid());

            }
        });



        return v;
    }
}