package rs.ftn.ma.mplanner.fragments;

import static androidx.constraintlayout.helper.widget.MotionEffect.TAG;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.ArrayList;
import java.util.List;

import rs.ftn.ma.mplanner.model.Category;
import rs.ftn.ma.mplanner.model.EventType;
import rs.ftn.ma.mplanner.model.PackageBundle;
import rs.ftn.ma.mplanner.model.Pricelist;
import rs.ftn.ma.mplanner.model.Product;
import rs.ftn.ma.mplanner.model.Service;
import rs.ftn.ma.mplanner.model.Subcategory;
import rs.ftn.ma.mplanner.repo.CategoryRepository;
import rs.ftn.ma.mplanner.repo.EventTypeRepository;
import rs.ftn.ma.mplanner.repo.ProductRepository;
import rs.ftn.ma.mplanner.repo.ServiceRepository;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EditPackageFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EditPackageFragment extends Fragment {

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private EditText enterDescriptionEditText;

    private List<Product> selectedProducts = new ArrayList<>();

    private List<Service> selectedServices = new ArrayList<>();
    private CategoryRepository categoryRepository = new CategoryRepository();
    private EditText enterNameEditText;
    private EventTypeRepository eventTypeRepository;
    private Category selectedCategory;
    private FirebaseUser user;
    private ProductRepository productRepository;
    private ServiceRepository serviceRepository;
    private double sumPrice;

    private RecyclerView serviceRecyclerView;

    public EditPackageFragment() {
        // Required empty public constructor
    }

    public static EditPackageFragment newInstance() {
        EditPackageFragment fragment = new EditPackageFragment();
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        selectedCategory = new Category();
        eventTypeRepository = new EventTypeRepository();
        productRepository = new ProductRepository();
        serviceRepository = new ServiceRepository();
        user = FirebaseAuth.getInstance().getCurrentUser();
        sumPrice = 0.0;
    }

    private void addProductsAndServicesToPackage(PackageBundle newPackage) {
        List<Integer> images = new ArrayList<>();
        List<EventType> eventTypes = new ArrayList<>();
        List<Subcategory> subcategories = new ArrayList<>();
        List<EventType> eventTypes1 = new ArrayList<>();
        double shortestReservationDeadline = Double.MAX_VALUE;
        double shortestCancellationDeadline = Double.MAX_VALUE;
        boolean hasManualReservationService = false;
        for (Product product : selectedProducts) {

            for(EventType types : eventTypes1)
            {
                eventTypes.add(types);
            }

        }
        newPackage.setPrice(sumPrice);
        newPackage.setEventTypes(eventTypes);
        newPackage.setReservationDeadline(shortestReservationDeadline);
        newPackage.setCancellationDeadline(shortestCancellationDeadline);
        newPackage.setReservationMethod(hasManualReservationService ? "MANUALLY" : "AUTOMATICALLY");
        newPackage.setSubcategories(subcategories);

    }

    private void savePackageToFirestore(Package newPackage)
    {
        db.collection("packages")
                .add(newPackage)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Toast.makeText(getActivity(), "Package added successfully!", Toast.LENGTH_SHORT).show();
                        savePricelistToFirestore(newPackage);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getActivity(), "Failed to add package!", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void savePricelistToFirestore(Package pkg) {

        Pricelist pricelist = new Pricelist();
        generateUniquePricelistId().addOnCompleteListener(new OnCompleteListener<Long>() {
            @Override
            public void onComplete(@NonNull Task<Long> task) {
                if (task.isSuccessful()) {
                    Long pricelistId = task.getResult();
                    pricelist.setId(pricelistId);

                    db.collection("pricelists")
                            .document(String.valueOf(pricelistId)) // Koristite document() sa String reprezentacijom ID-a
                            .set(pricelist) // Postavljanje pricelist-a
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Toast.makeText(getActivity(), "Pricelist added successfully!", Toast.LENGTH_SHORT).show();
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Toast.makeText(getActivity(), "Failed to add pricelist!", Toast.LENGTH_SHORT).show();
                                }
                            });
                } else {
                    Toast.makeText(getActivity(), "Failed to generate unique ID for pricelist!", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "Error generating unique ID: ", task.getException());
                }
            }
        });
    }


    private Task<Long> generateUniquePricelistId() {
        TaskCompletionSource<Long> taskCompletionSource = new TaskCompletionSource<>();

        db.collection("pricelists")
                .orderBy("id", Query.Direction.DESCENDING)
                .limit(1)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        Long maxId = 0L;
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            Long id = document.getLong("id");
                            if (id != null && id > maxId) {
                                maxId = id;
                            }
                        }
                        taskCompletionSource.setResult(maxId + 1);
                    } else {
                        taskCompletionSource.setException(task.getException());
                    }
                });

        return taskCompletionSource.getTask();
    }
}