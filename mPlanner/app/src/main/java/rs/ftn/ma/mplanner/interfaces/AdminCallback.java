package rs.ftn.ma.mplanner.interfaces;

import rs.ftn.ma.mplanner.model.Admin;

public interface AdminCallback {
    void onAdminReceived(Admin[] admins);
    void onError(Exception e);
}
