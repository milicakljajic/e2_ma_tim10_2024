package rs.ftn.ma.mplanner.fragments;

import static androidx.constraintlayout.helper.widget.MotionEffect.TAG;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.adapters.CategoryAdapter;
import rs.ftn.ma.mplanner.adapters.EventTypeAdapter;
import rs.ftn.ma.mplanner.adapters.SubcategoryAdapter;
import rs.ftn.ma.mplanner.interfaces.CategoryCallback;
import rs.ftn.ma.mplanner.interfaces.SubcategoryCallback;
import rs.ftn.ma.mplanner.model.Category;
import rs.ftn.ma.mplanner.model.Employee;
import rs.ftn.ma.mplanner.model.EventProvider;
import rs.ftn.ma.mplanner.model.EventType;
import rs.ftn.ma.mplanner.model.Pricelist;
import rs.ftn.ma.mplanner.model.Service;
import rs.ftn.ma.mplanner.model.Subcategory;
import rs.ftn.ma.mplanner.model.enums.ConfirmationMethod;
import rs.ftn.ma.mplanner.model.enums.PricelistType;
import rs.ftn.ma.mplanner.model.enums.ProductState;
import rs.ftn.ma.mplanner.repo.CategoryRepository;
import rs.ftn.ma.mplanner.repo.EmployeeRepository;
import rs.ftn.ma.mplanner.repo.EventTypeRepository;
import rs.ftn.ma.mplanner.repo.SubcategoryRepository;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ServiceCreationFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ServiceCreationFragment extends Fragment {

    private Spinner categorySpinner;
    private Spinner subcategorySpinner;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private Service service;
    private CategoryRepository categoryRepository;
    private SubcategoryRepository subcategoryRepository;
    private EventTypeRepository eventTypeRepository;
    private EmployeeRepository employeeRepository;
    private Category selectedCategory;
    private Subcategory selectedSubcategory;
    private List<EventType> selectedEventTypes;
    private Long serviceProductId;
    private FirebaseUser user;
    private List<Employee> selectedEmployees;
    //private UserPupV userPupV;

    public ServiceCreationFragment() {
        // Required empty public constructor
    }

    public static ServiceCreationFragment newInstance(String param1, String param2) {
        ServiceCreationFragment fragment = new ServiceCreationFragment();
        Bundle args = new Bundle();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        selectedCategory = new Category();
        selectedSubcategory = new Subcategory();
        selectedEventTypes = new ArrayList<>();
        selectedEmployees = new ArrayList<>();
        categoryRepository = new CategoryRepository();
        subcategoryRepository = new SubcategoryRepository();
        eventTypeRepository = new EventTypeRepository();
        serviceProductId = -1L;
        employeeRepository = new EmployeeRepository();
        user = FirebaseAuth.getInstance().getCurrentUser();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.service_creation_fragment, container, false);

        categorySpinner = view.findViewById(R.id.spinner1);
        subcategorySpinner = view.findViewById(R.id.spinner2);
        categorySpinner = view.findViewById(R.id.spinner1);
        subcategorySpinner = view.findViewById(R.id.spinner2);
        subcategorySpinner.setVisibility(View.INVISIBLE);

        if (user != null) {
            findUserByEmail(user, new OnSuccessListener<EventProvider>() {
                @Override
                public void onSuccess(EventProvider eventProvider) {
                    categoryRepository.select(new CategoryCallback() {
                        @Override
                        public void onCategoryReceived(Category[] categories) {
                            ArrayList<Category> subs = new ArrayList<Category>(Arrays.asList(categories));
                            CategoryAdapter categoryAdapter = new CategoryAdapter(getContext(), subs);
                            categorySpinner.setAdapter(categoryAdapter);
                        }

                        @Override
                        public void onError(Exception e) {

                        }
                    });

                    categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            selectedCategory = (Category) parent.getItemAtPosition(position);
                            loadSubcategories(Long.valueOf(selectedCategory.getId()));
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                            // Implementacija po potrebi
                        }
                    });

                    subcategorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            selectedSubcategory = (Subcategory) parent.getItemAtPosition(position);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                            // Implementacija po potrebi
                        }
                    });

                    eventTypeRepository.select().addOnCompleteListener(new OnCompleteListener<List<EventType>>() {
                        @Override
                        public void onComplete(@NonNull Task<List<EventType>> task) {
                            if (task.isSuccessful()) {
                                List<EventType> eventTypes = task.getResult();
                                EventType[] eventTypesArray = eventTypes.toArray(new EventType[0]);
                                EventTypeAdapter ETadapter = new EventTypeAdapter(requireContext(),eventTypesArray);
                                RecyclerView recyclerView = view.findViewById(R.id.eventTypesRecyclerView);
                                recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                                //recyclerView.setAdapter(ETadapter);
                            } else {
                                Log.e(TAG, "Error getting categories", task.getException());
                                // Handle error
                            }
                        }
                    });


                    RadioButton automaticallyRadioButton = view.findViewById(R.id.automaticallyRadioButton);
                    RadioButton manuallyRadioButton = view.findViewById(R.id.manuallyRadioButton);

                    ConfirmationMethod method = automaticallyRadioButton.isChecked() ? ConfirmationMethod.AUTO : ConfirmationMethod.MANUAL;

                    Button suggestButton = view.findViewById(R.id.suggestButton);
                    suggestButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            EditText durationEditText = view.findViewById(R.id.service_create_duration);
                            EditText durationMaxEditText = view.findViewById(R.id.enterMaxDurationEditText);
                            EditText reservationDeadlineEditText = view.findViewById(R.id.service_create_resdeadline);
                            EditText cancellationDeadlineEditText = view.findViewById(R.id.service_create_candeadline);
                            EditText specificsEditText = view.findViewById(R.id.enterSpecificsEditText);
                            EditText nameEditText = view.findViewById(R.id.service_create_name);
                            EditText descriptionEditText = view.findViewById(R.id.service_create_description);
                            EditText priceEditText = view.findViewById(R.id.service_create_price);
                            EditText discountEditText = view.findViewById(R.id.enterDiscountEditText);

                            double minDuration = Double.parseDouble(durationEditText.getText().toString());
                            double maxDuration = Double.parseDouble(durationMaxEditText.getText().toString());
                            int reservationDeadline = Integer.parseInt(reservationDeadlineEditText.getText().toString());
                            int cancellationDeadline = Integer.parseInt(cancellationDeadlineEditText.getText().toString());
                            String specifics = specificsEditText.getText().toString();
                            String name = nameEditText.getText().toString();
                            String description = descriptionEditText.getText().toString();
                            double price = Double.parseDouble(priceEditText.getText().toString());
                            int discount = Integer.parseInt(discountEditText.getText().toString());
                            double finalPrice = price - (discount / 100.0) * price;

                            List<EventType> eventTypes = new ArrayList<>();
                        }
                    });

                    Button editButton = view.findViewById(R.id.createButton);
                    editButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            EditText nameEditText = view.findViewById(R.id.service_create_name);
                            EditText descriptionEditText = view.findViewById(R.id.service_create_description);
                            EditText priceEditText = view.findViewById(R.id.service_create_price);
                            EditText discountEditText = view.findViewById(R.id.enterDiscountEditText);
                            EditText durationEditText = view.findViewById(R.id.service_create_duration);
                            EditText durationMaxEditText = view.findViewById(R.id.enterMaxDurationEditText);
                            EditText reservationDeadlineEditText = view.findViewById(R.id.service_create_resdeadline);
                            EditText cancellationDeadlineEditText = view.findViewById(R.id.service_create_candeadline);
                            EditText specificsEditText = view.findViewById(R.id.enterSpecificsEditText);

                            String name = nameEditText.getText().toString();
                            String description = descriptionEditText.getText().toString();
                            double price = Double.parseDouble(priceEditText.getText().toString());
                            int discount = Integer.parseInt(discountEditText.getText().toString());
                            double finalPrice = price - (discount / 100.0) * price;
                            double minDuration = Double.parseDouble(durationEditText.getText().toString());
                            double maxDuration = Double.parseDouble(durationMaxEditText.getText().toString());
                            int reservationDeadline = Integer.parseInt(reservationDeadlineEditText.getText().toString());
                            int cancellationDeadline = Integer.parseInt(cancellationDeadlineEditText.getText().toString());
                            String specifics = specificsEditText.getText().toString();
                            List<EventType> eventTypes = new ArrayList<>();
                            Category category = new Category();
                            Subcategory subcategory = new Subcategory();
                            boolean available = false;
                            boolean visible = false;

                            RadioGroup availableRadioGroup = view.findViewById(R.id.availableRadioGroup);
                            int selectedAvailableRadioButtonId = availableRadioGroup.getCheckedRadioButtonId();
                            if (selectedAvailableRadioButtonId == R.id.yesRadioButton) {
                                available = true;
                            }

                            RadioGroup visibleRadioGroup = view.findViewById(R.id.visibleRadioGroup);
                            int selectedVisibleRadioButtonId = visibleRadioGroup.getCheckedRadioButtonId();
                            if (selectedVisibleRadioButtonId == R.id.visibleYesRadioButton) {
                                visible = true;
                            }

                            Category selectedCategory = (Category) categorySpinner.getSelectedItem();
                            Subcategory selectedSubcategory = (Subcategory) subcategorySpinner.getSelectedItem();

                            boolean finalVisible = visible;
                            boolean finalAvailable = available;
                            db.collection("services")
                                    .orderBy("id", Query.Direction.DESCENDING)
                                    .limit(1)
                                    .get()
                                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                        @Override
                                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                            if (task.isSuccessful()) {
                                                Long newId = 1L;
                                                for (QueryDocumentSnapshot document : task.getResult()) {
                                                    Long id = document.getLong("id");
                                                    if (id != null) {
                                                        newId = id + 1;
                                                    }
                                                }
                                                service = new Service(newId, name, description, specifics, price, finalPrice, discount, new ArrayList<>(), minDuration, maxDuration, selectedCategory, selectedSubcategory, ProductState.CREATED, selectedEventTypes, finalVisible, finalAvailable, selectedEmployees, reservationDeadline, cancellationDeadline, method, false);

                                                saveServiceToFirestore();
                                            } else {
                                                Log.d(TAG, "Error getting documents: ", task.getException());
                                            }
                                        }
                                    });
                        }
                    });
                }
            });
        }

        return view;
    }


    private void loadSubcategories(Long categoryId) {
        subcategoryRepository.selectById(new SubcategoryCallback() {
            @Override
            public void onSubcategoryReceived(Subcategory[] subcategories) {
                ArrayList<Subcategory> subs = new ArrayList<Subcategory>(Arrays.asList(subcategories));
                SubcategoryAdapter adapter = new SubcategoryAdapter(requireContext(), subs);
                subcategorySpinner.setAdapter(adapter);
                subcategorySpinner.setVisibility(View.VISIBLE);
            }

            @Override
            public void onError(Exception e) {
                Log.e("CreateServiceFragment", "Error getting subcategories", e);
            }
        },categoryId.toString());
    }
    private void saveServiceToFirestore() {
        db.collection("services")
                .add(service)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Toast.makeText(getActivity(), "Service added successfully!", Toast.LENGTH_SHORT).show();
                        savePricelistToFirestore(service);
                        requireActivity().getSupportFragmentManager().popBackStack();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getActivity(), "Failed to add service!", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private Task<Long> generateUniquePricelistId() {
        TaskCompletionSource<Long> taskCompletionSource = new TaskCompletionSource<>();

        db.collection("pricelists")
                .orderBy("id", Query.Direction.DESCENDING)
                .limit(1)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        Long maxId = 0L;
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            Long id = document.getLong("id");
                            if (id != null && id > maxId) {
                                maxId = id;
                            }
                        }
                        taskCompletionSource.setResult(maxId + 1);
                    } else {
                        taskCompletionSource.setException(task.getException());
                    }
                });

        return taskCompletionSource.getTask();
    }

    private void findUserByEmail(FirebaseUser firebaseUser,OnSuccessListener<EventProvider> onSuccessListener) {
        CollectionReference usersRef = FirebaseFirestore.getInstance().collection("users");
        Query query = usersRef.whereEqualTo("email", firebaseUser.getEmail());

        query.get().addOnCompleteListener(task -> {
            if (task.isSuccessful() && task.getResult() != null) {
                for (QueryDocumentSnapshot document : task.getResult()) {
                    EventProvider userPupV = document.toObject(EventProvider.class);
                    onSuccessListener.onSuccess(userPupV);
                }
            } else {
                Log.e(TAG, "Error getting user by email: ", task.getException());
                onSuccessListener.onSuccess(null);
            }
        });
    }

    private void savePricelistToFirestore(Service service) {
        Pricelist pricelist = new Pricelist(service.getName(), service.getPrice(), service.getDiscount(),
                service.getDiscount(), PricelistType.SERVICE);

        generateUniquePricelistId().addOnCompleteListener(new OnCompleteListener<Long>() {
            @Override
            public void onComplete(@NonNull Task<Long> task) {
                if (task.isSuccessful()) {
                    Long pricelistId = task.getResult();
                    pricelist.setId(pricelistId);

                    db.collection("pricelists")
                            .document(String.valueOf(pricelistId))
                            .set(pricelist)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Toast.makeText(getActivity(), "Pricelist added successfully!", Toast.LENGTH_SHORT).show();
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                }
                            });
                } else {
                    Log.d(TAG, "Error generating unique ID: ", task.getException());
                }
            }
        });
    }

}