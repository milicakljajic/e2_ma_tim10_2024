package rs.ftn.ma.mplanner.interfaces;

public interface ProductUpdateCallback {
    void onProductUpdated();
    void onUpdateError(Exception e);
}
