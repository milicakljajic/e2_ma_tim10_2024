package rs.ftn.ma.mplanner.fragments;

import static androidx.constraintlayout.helper.widget.MotionEffect.TAG;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.adapters.CategoryAdapter;
import rs.ftn.ma.mplanner.adapters.PackageBundleAdapter;
import rs.ftn.ma.mplanner.adapters.SubcategoryAdapter;
import rs.ftn.ma.mplanner.interfaces.CategoryCallback;
import rs.ftn.ma.mplanner.interfaces.SubcategoryCallback;
import rs.ftn.ma.mplanner.model.Category;
import rs.ftn.ma.mplanner.model.EventType;
import rs.ftn.ma.mplanner.model.PackageBundle;
import rs.ftn.ma.mplanner.model.Product;
import rs.ftn.ma.mplanner.model.Service;
import rs.ftn.ma.mplanner.model.Subcategory;
import rs.ftn.ma.mplanner.repo.CategoryRepository;
import rs.ftn.ma.mplanner.repo.EventTypeRepository;
import rs.ftn.ma.mplanner.repo.SubcategoryRepository;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PackageListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PackageListFragment extends Fragment {

    private RecyclerView recyclerView;
    private PackageBundleAdapter packageAdapter;
    private List<PackageBundle> packageList;
    private Spinner categorySpinner;
    private Spinner subcategorySpinner;

    private Spinner eventTypeSpinner;

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private EditText searchEditNameText;
    private EditText searchEditProductNameText;
    private EditText searchEditServiceNameText;
    private List<Product> productList;
    private List<Service> serviceList;
    private CategoryRepository categoryRepository = new CategoryRepository();
    private SubcategoryRepository subcategoryRepository;
    private EventTypeRepository eventTypeRepository;
    private Category selectedCategory;
    private Subcategory selectedSubcategory;
    private EventType selectedEventType;



    public PackageListFragment() {

    }

    public static PackageListFragment newInstance(String param1, String param2) {
        PackageListFragment fragment = new PackageListFragment();
        Bundle args = new Bundle();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        packageList = new ArrayList<PackageBundle>();
        productList = new ArrayList<>();
        serviceList = new ArrayList<>();
        subcategoryRepository = new SubcategoryRepository();
        eventTypeRepository = new EventTypeRepository();
        selectedCategory = new Category();
        selectedSubcategory = new Subcategory();
        selectedEventType = new EventType();
        getAllProducts();
        getAllServices();
        getAllPackages();
    }

    public void getAllPackages() {
        packageList.clear();
        db.collection("packages")
                .whereEqualTo("deleted", false)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                PackageBundle pkg = document.toObject(PackageBundle.class);
                                packageList.add(pkg);
                            }
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });
    }

    public void getAllProducts()
    {
        db.collection("products")
                .whereEqualTo("deleted", false)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Product product = document.toObject(Product.class);
                                productList.add(product);
                            }
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });
    }

    public void getAllServices()
    {
        db.collection("services")
                .whereEqualTo("deleted", false)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Service service = document.toObject(Service.class);
                                serviceList.add(service);
                            }
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.package_list_fragment, container, false);

        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        categorySpinner = view.findViewById(R.id.spinner1);
        subcategorySpinner = view.findViewById(R.id.spinner2);
        eventTypeSpinner = view.findViewById(R.id.spinner3);

        categorySpinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {

                    categoryRepository.select(new CategoryCallback() {
                        @Override
                        public void onCategoryReceived(Category[] categories) {
                            ArrayList<Category> cats = new ArrayList<Category>(Arrays.asList(categories));
                            CategoryAdapter categoryAdapter = new CategoryAdapter(getActivity(), cats);
                            categorySpinner.setAdapter(categoryAdapter);
                        }

                        @Override
                        public void onError(Exception e) {

                        }
                    });
                }
                return false;
            }
        });

        subcategorySpinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {

                    subcategoryRepository.select(new SubcategoryCallback() {
                        @Override
                        public void onSubcategoryReceived(Subcategory[] subcategories) {
                            ArrayList<Subcategory> subs = new ArrayList<Subcategory>(Arrays.asList(subcategories));
                            SubcategoryAdapter adapter = new SubcategoryAdapter(requireContext(), subs);
                            subcategorySpinner.setAdapter(adapter);
                        }

                        @Override
                        public void onError(Exception e) {

                        }
                    });
                }
                return false;
            }
        });


        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedCategory = (Category) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        subcategorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedSubcategory = (Subcategory) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        eventTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedEventType = (EventType) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        searchEditNameText = view.findViewById(R.id.searchNameEditText);
        searchEditProductNameText = view.findViewById(R.id.searchProductNameEditText);
        searchEditServiceNameText = view.findViewById(R.id.searchServiceNameEditText);
        Button searchButton = view.findViewById(R.id.searchButton);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String searchTerm = searchEditNameText.getText().toString().toLowerCase().trim();
                String searchProductTerm = searchEditProductNameText.getText().toString().toLowerCase().trim();
                String searchServiceTerm = searchEditServiceNameText.getText().toString().toLowerCase().trim();
                List<PackageBundle> filteredPackages = new ArrayList<>();
            }
        });

        Button resetButton = view.findViewById(R.id.resetButton);
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchEditServiceNameText.setText("");
                searchEditProductNameText.setText("");
                searchEditNameText.setText("");
                getAllPackages();
            }
        });


        Button filterButton = view.findViewById(R.id.filterButton);
        EditText priceFrom = view.findViewById(R.id.priceFromEditText);
        EditText priceTo = view.findViewById(R.id.priceToEditText);

        filterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String priceFromStr = priceFrom.getText().toString().trim();
                String priceToStr = priceTo.getText().toString().trim();

                double priceFrom = priceFromStr.isEmpty() ? Double.MIN_VALUE : Double.parseDouble(priceFromStr);
                double priceTo = priceToStr.isEmpty() ? Double.MAX_VALUE : Double.parseDouble(priceToStr);

                List<PackageBundle> filteredPackages = new ArrayList<>();
                for (PackageBundle pkg : packageList) {
                    if ((selectedCategory.getId() == null || (pkg.getCategory() != null && pkg.getCategory().getId().equals(selectedCategory.getId())))
                            && (selectedSubcategory.getId() == null)
                            && (pkg.getPrice() >= priceFrom)
                            && (pkg.getPrice() <= priceTo)) {
                        filteredPackages.add(pkg);
                    }
                }

                packageList.clear();

            }

            private boolean containsEventType(List<EventType> eventTypes, Long eventTypeId) {
                for (EventType eventType : eventTypes) {
                    if (eventType.getId().equals(eventTypeId)) {
                        return true;
                    }
                }
                return false;
            }

            private boolean containsSubcategory(List<Subcategory> subcategories, Long subcategoryId) {
                for (Subcategory subcategory : subcategories) {
                    if (subcategory.getId().equals(subcategoryId)) {
                        return true;
                    }
                }
                return false;
            }
        });

        Button resetFilterButton = view.findViewById(R.id.resetFilterButton);
        resetFilterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        return view;
    }
}