package rs.ftn.ma.mplanner.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.model.EventProvider;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ManageRequestDetailedFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ManageRequestDetailedFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "EventProvider";

    // TODO: Rename and change types of parameters
    private EventProvider mParam1;

    public ManageRequestDetailedFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static ManageRequestDetailedFragment newInstance(String param1) {
        ManageRequestDetailedFragment fragment = new ManageRequestDetailedFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getParcelable(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_manage_request_detailed, container, false);
        TextView lastModifiedTextView = v.findViewById(R.id.manage_request_detailed_last_modified);
        TextView firstNameTextView = v.findViewById(R.id.manage_request_detailed_firstname);
        TextView lastNameTextView = v.findViewById(R.id.manage_request_detailed_lastname);
        TextView emailTextView = v.findViewById(R.id.manage_request_detailed_email);
        TextView companyNameTextView = v.findViewById(R.id.manage_request_detailed_company_name);
        TextView companyEmailTextView = v.findViewById(R.id.manage_request_detailed_company_email);

        // Set the TextViews' text based on mParam1
        if (mParam1 != null) {
            lastModifiedTextView.setText(mParam1.getLastModified().toLocaleString());
            firstNameTextView.setText(mParam1.getFirstName());
            lastNameTextView.setText(mParam1.getLastName());
            emailTextView.setText(mParam1.getEmail());
            companyNameTextView.setText(mParam1.getCompany().getName());
            companyEmailTextView.setText(mParam1.getCompany().getEmail());
        }
        return v;
    }
}