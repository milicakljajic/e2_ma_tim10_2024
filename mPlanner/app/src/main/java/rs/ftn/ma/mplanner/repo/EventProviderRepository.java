package rs.ftn.ma.mplanner.repo;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import rs.ftn.ma.mplanner.interfaces.EventProviderCallback;
import rs.ftn.ma.mplanner.interfaces.EventProviderInsertCallback;
import rs.ftn.ma.mplanner.interfaces.EventTypeUpdateCallback;
import rs.ftn.ma.mplanner.model.EventProvider;
import rs.ftn.ma.mplanner.model.EventType;
import rs.ftn.ma.mplanner.model.Product;
import rs.ftn.ma.mplanner.model.Service;

public class EventProviderRepository {

    public void insert(EventProvider ep, EventProviderInsertCallback callback) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("event-providers")
                .add(ep)
                .addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentReference> task) {
                        if (task.isSuccessful()) {
                            DocumentReference documentReference = task.getResult();
                            String epId = documentReference.getId();
                            Log.d("REZ_DB", "DocumentSnapshot added with ID: " + epId);
                            callback.onEventProviderInserted(epId);
                        } else {
                            Log.w("REZ_DB", "Error adding document", task.getException());
                            callback.onInsertError(task.getException());
                        }
                    }
                });
    }


    public void select(EventProviderCallback callback) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("event-providers")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            List<EventProvider> eventProviders = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                EventProvider eventProvider = document.toObject(EventProvider.class);
                                eventProvider.setId(document.getId());
                                    eventProviders.add(eventProvider);
                                Log.d("REZ_DB", document.getId() + " => " + document.getData());
                            }
                            callback.onEventProviderReceived(eventProviders.toArray(new EventProvider[0]));
                        } else {
                            Log.w("REZ_DB", "Error getting documents.", task.getException());
                            callback.onError(task.getException());
                        }
                    }
                });
    }

    public void selectRequests(EventProviderCallback callback, EventProvider searchProvider) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        ArrayList<EventProvider> filtered = new ArrayList<>();
        ArrayList<EventProvider> temp = new ArrayList<>();
        db.collection("event-providers")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            List<EventProvider> eventProviders = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                EventProvider eventProvider = document.toObject(EventProvider.class);
                                eventProvider.setId(document.getId());
                                if(!eventProvider.getApproved())
                                    eventProviders.add(eventProvider);
                                Log.d("REZ_DB", document.getId() + " => " + document.getData());
                            }

                            for (EventProvider current : eventProviders) {
                                // trenutno se svi neodobreni provider nalaze ovde

                                boolean hasMatchingProduct = true;
                                boolean hasMatchingService = true;
                                boolean hasMatchingEventType = true;

                                // nadji one koji imaju proizvode
                                if (!searchProvider.getProvidedProducts().isEmpty()) {
                                    hasMatchingProduct = false;
                                    for (Product searchPr : searchProvider.getProvidedProducts()) {
                                        for (Product currentPr : current.getProvidedProducts()) {
                                            if (searchPr.getName().equals(currentPr.getName()) || searchPr.getName().equals("-")) {
                                                hasMatchingProduct = true;
                                                break;
                                            }
                                        }
                                        if (hasMatchingProduct) break;
                                    }
                                }

                                // ti koji imaju proizvode moraju da imaju i usluge
                                if (!searchProvider.getProvidedServices().isEmpty()) {
                                    hasMatchingService = false;
                                    for (Service searchSr : searchProvider.getProvidedServices()) {
                                        for (Service currentSr : current.getProvidedServices()) {
                                            if (searchSr.getName().equals(currentSr.getName()) || searchSr.getName().equals("-")) {
                                                hasMatchingService = true;
                                                break;
                                            }
                                        }
                                        if (hasMatchingService) break;
                                    }
                                }

                                // ti koji imaju proizvode i usluge moraju da imaju i tipove događaja
                                if (!searchProvider.getProvidedEventTypes().isEmpty()) {
                                    hasMatchingEventType = false;
                                    for (EventType searchEt : searchProvider.getProvidedEventTypes()) {
                                        for (EventType currentEt : current.getProvidedEventTypes()) {
                                            if (searchEt.getName().equals(currentEt.getName()) || searchEt.getName().equals("-")) {
                                                hasMatchingEventType = true;
                                                break;
                                            }
                                        }
                                        if (hasMatchingEventType) break;
                                    }
                                }

                                // Dodaj u filtriranu listu samo ako zadovoljava sve uslove
                                if (hasMatchingProduct && hasMatchingService && hasMatchingEventType) {
                                    if (!filtered.contains(current)) {
                                        filtered.add(current);
                                    }
                                }
                            }

                            if(searchProvider.getProvidedEventTypes().isEmpty() && searchProvider.getProvidedServices().isEmpty() && searchProvider.getProvidedProducts().isEmpty())
                                callback.onEventProviderReceived(eventProviders.toArray(new EventProvider[0]));
                            else
                                callback.onEventProviderReceived(filtered.toArray(new EventProvider[0]));
                        } else {
                            Log.w("REZ_DB", "Error getting documents.", task.getException());
                            callback.onError(task.getException());
                        }
                    }
                });
    }

    //FIXME Implement if needed
    public void update(String id, EventProvider ep, EventTypeUpdateCallback callback) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("event-providers").document(id);

        docRef.update("approved", ep.getApproved())
                .addOnSuccessListener(aVoid -> {
                    Log.d("REZ_DB", "Approvedsuccessfully changed");
                    callback.onEventTypeUpdated();
                })
                .addOnFailureListener(e -> {
                    Log.w("REZ_DB", "Error getting documents.", e);
                    callback.onUpdateError(e);
                });

        docRef.update("isEnabled", ep.getIsEnabled())
                .addOnSuccessListener(aVoid -> {
                    Log.d("REZ_DB", "IsEnabled successfully changed");
                    callback.onEventTypeUpdated();
                })
                .addOnFailureListener(e -> {
                    Log.w("REZ_DB", "Error getting documents.", e);
                    callback.onUpdateError(e);
                });

        docRef.update("lastModified", ep.getLastModified())
                .addOnSuccessListener(aVoid -> {
                    Log.d("REZ_DB", "last modified successfully changed");
                    callback.onEventTypeUpdated();
                })
                .addOnFailureListener(e -> {
                    Log.w("REZ_DB", "Error getting documents.", e);
                    callback.onUpdateError(e);
                });
    }


    public void selectById(EventProviderCallback callback, String id) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("event-providers").document(id);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot documentSnapshot = task.getResult();
                    if (documentSnapshot.exists()) {
                        EventProvider[] ep = new EventProvider[]{documentSnapshot.toObject(EventProvider.class)};
                        Log.d("REZ_DB", documentSnapshot.getId() + " => " + documentSnapshot.getData());
                        callback.onEventProviderReceived(ep);
                    } else {
                        Log.d("REZ_DB", "No such document");
                        callback.onError(new Exception("No such document"));
                    }
                } else {
                    Log.w("REZ_DB", "Error getting document", task.getException());
                    callback.onError(task.getException());
                }
            }
        });
    }

    public void selectByFirebaseId(EventProviderCallback callback, String id) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("event-providers")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            List<EventProvider> eventProviders = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                EventProvider eventProvider = document.toObject(EventProvider.class);
                                eventProvider.setId(document.getId());
                                if(eventProvider.getFirebaseUserId().equals(id))
                                    eventProviders.add(eventProvider);
                                Log.d("REZ_DB", document.getId() + " => " + document.getData());
                            }
                            callback.onEventProviderReceived(eventProviders.toArray(new EventProvider[0]));
                        } else {
                            Log.w("REZ_DB", "Error getting documents.", task.getException());
                            callback.onError(task.getException());
                        }
                    }
                });
    }

    public void selectByCompany(EventProviderCallback callback, String cname) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("event-providers")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            List<EventProvider> eventProviders = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                EventProvider eventProvider = document.toObject(EventProvider.class);
                                eventProvider.setId(document.getId());
                                if(eventProvider.getCompany().getName().equals(cname))
                                    eventProviders.add(eventProvider);
                                Log.d("REZ_DB", document.getId() + " => " + document.getData());
                            }
                            callback.onEventProviderReceived(eventProviders.toArray(new EventProvider[0]));
                        } else {
                            Log.w("REZ_DB", "Error getting documents.", task.getException());
                            callback.onError(task.getException());
                        }
                    }
                });
    }

}
