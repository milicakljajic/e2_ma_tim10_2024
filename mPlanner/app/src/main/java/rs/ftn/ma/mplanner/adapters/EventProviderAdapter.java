package rs.ftn.ma.mplanner.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.navigation.Navigation;

import com.google.firebase.auth.ActionCodeSettings;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Objects;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.interfaces.EventTypeUpdateCallback;
import rs.ftn.ma.mplanner.model.EventProvider;
import rs.ftn.ma.mplanner.repo.EventProviderRepository;
import rs.ftn.ma.mplanner.task.SendMailTask;

public class EventProviderAdapter extends BaseAdapter {
    Context context;
    ArrayList<EventProvider> eventProviders;
    LayoutInflater inflater;

    String mode;
    Integer fragmentType;
    int toInflate;

    public static final Integer FRAGMENT_TYPE_DEFAULT = 0;
    // Dodajte druge tipove fragmenta ovde ako je potrebno

    public EventProviderAdapter(Context applicationContext, EventProvider[] eventProviders, Integer fragmentType) {
        this.context = applicationContext;
        this.eventProviders = new ArrayList<>(Arrays.asList(eventProviders));
        this.fragmentType = fragmentType;
        inflater = LayoutInflater.from(applicationContext);
        this.toInflate = fragmentType;
    }

    public EventProviderAdapter(Context applicationContext, ArrayList<EventProvider> eventProviders, Integer fragmentType) {
        this.context = applicationContext;
        this.eventProviders = eventProviders;
        this.fragmentType = fragmentType;
        inflater = LayoutInflater.from(applicationContext);
        this.toInflate = fragmentType;
    }

    public EventProviderAdapter(Context applicationContext, ArrayList<EventProvider> eventProviders, Integer fragmentType, String mode) {
        this.context = applicationContext;
        this.eventProviders = eventProviders;
        this.fragmentType = fragmentType;
        inflater = LayoutInflater.from(applicationContext);
        this.toInflate = fragmentType;
        this.mode = mode;
    }

    public EventProviderAdapter(Context applicationContext, EventProvider[] eventProviders, Integer fragmentType, int layout) {
        this.context = applicationContext;
        this.eventProviders = new ArrayList<>(Arrays.asList(eventProviders));
        this.fragmentType = fragmentType;
        inflater = LayoutInflater.from(applicationContext);
        toInflate = layout;
    }

    public EventProviderAdapter(Context applicationContext, ArrayList<EventProvider> eventProviders, Integer fragmentType, int layout) {
        this.context = applicationContext;
        this.eventProviders = eventProviders;
        this.fragmentType = fragmentType;
        inflater = LayoutInflater.from(applicationContext);
        toInflate = layout;
    }

    @Override
    public int getCount() {
        return eventProviders.size();
    }

    @Override
    public Object getItem(int i) {
        return eventProviders.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if(toInflate == 69){
            view = inflater.inflate(R.layout.fragment_company_display_list_item, viewGroup, false);
        }
        else {
            view = inflater.inflate(R.layout.fragment_manage_registration_request_list_item, viewGroup, false);
        }

        EventProvider eventProvider = eventProviders.get(i);

        if (Objects.equals(fragmentType, FRAGMENT_TYPE_DEFAULT)) {
            LinearLayout l = view.findViewById(R.id.request_item_container);
            l.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("EventProvider", eventProviders.get(i));
                    Navigation.findNavController(v).navigate(R.id.changeToRequestDetailed1, bundle);
                }
            });

            TextView name = (TextView) view.findViewById(R.id.request_name);
            TextView email = (TextView) view.findViewById(R.id.request_email);
            TextView company = (TextView) view.findViewById(R.id.request_company_name);
            TextView date = view.findViewById(R.id.request_date);

            name.setText(eventProvider.getFirstName());
            email.setText(eventProvider.getEmail());
            company.setText(eventProvider.getCompany().getName());
            date.setText(eventProvider.getLastModified().toLocaleString());

            ImageView accept = view.findViewById(R.id.request_accept);
            ImageView decline = view.findViewById(R.id.request_reject);
            accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventProviderRepository repo = new EventProviderRepository();
                    EventProvider current = eventProviders.get(i);
                    current.setApproved(true);
                    current.setIsEnabled(true);
                    current.setLastModified(new Date());
                    FirebaseAuth mAuth = FirebaseAuth.getInstance();
                    ActionCodeSettings actionCodeSettings = ActionCodeSettings.newBuilder()
                            .setAndroidPackageName("rs.ftn.ma.mplanner", true, "12")
                            .setHandleCodeInApp(true) // Ovo će omogućiti otvaranje linka u vašoj aplikaciji
                            .setUrl("https://mobilne-7656f.firebaseapp.com") // Postavite dinamički link ako ga koristite
                            .build();
                    mAuth.sendSignInLinkToEmail(current.getEmail(), actionCodeSettings);
                    repo.update(current.getId(), current, new EventTypeUpdateCallback() {
                        @Override
                        public void onEventTypeUpdated() {
                            Log.d("OK", "onEventTypeUpdated: ok");
                        }

                        @Override
                        public void onUpdateError(Exception e) {

                        }
                    });
                }
            });

            decline.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final EditText input = new EditText(v.getContext());

                    // Kreiramo AlertDialog
                    new AlertDialog.Builder(v.getContext())
                            .setTitle("Unesite tekst")
                            .setMessage("Molimo vas unesite vaš tekst:")
                            .setView(input) // Postavljamo EditText kao view u dijalogu
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    // Dobijamo unos korisnika
                                    EventProvider current = eventProviders.get(i);
                                    current.setApproved(false);
                                    current.setIsEnabled(false);
                                    current.setLastModified(new Date());
                                    String enteredText = input.getText().toString();
                                    // Prikazujemo unos u Toast poruci
                                    //Toast.makeText(v.getContext(), "Uneli ste: " + enteredText, Toast.LENGTH_SHORT).show();
                                    new SendMailTask().execute(current.getEmail(), "You have been declined", enteredText);

                                }
                            })
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    // Radimo nešto ako korisnik otkaže unos
                                    dialog.cancel();
                                }
                            })
                            .show();
                }
            });
        }
        else if (Objects.equals(fragmentType, 69)) {
            TextView name = view.findViewById(R.id.company_display_list_item_name);
            EventProvider e = eventProviders.get(i);
            name.setText(e.getCompany().getName());

            name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //todo uslov ovde, 5 dana i cuda kada ognjen uradi
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("EventProvider", eventProviders.get(i));
                    if(mode != null)
                    {
                        if(mode.equals("create"))
                            Navigation.findNavController(v).navigate(R.id.changeToCompanyReview1, bundle);

                        if(mode.equals("read"))
                            Navigation.findNavController(v).navigate(R.id.changeToCompanyReviews, bundle);
                    }

                }
            });
        }

        return view;
    }
}
