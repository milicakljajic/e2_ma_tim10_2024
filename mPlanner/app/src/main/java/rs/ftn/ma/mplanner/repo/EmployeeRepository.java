package rs.ftn.ma.mplanner.repo;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import rs.ftn.ma.mplanner.interfaces.EmployeeCallback;
import rs.ftn.ma.mplanner.interfaces.EmployeeInsertCallback;
import rs.ftn.ma.mplanner.interfaces.EmployeeUpdateCallback;
import rs.ftn.ma.mplanner.model.Company;
import rs.ftn.ma.mplanner.model.Employee;
import rs.ftn.ma.mplanner.model.Event;
import rs.ftn.ma.mplanner.model.Schedule;

public class EmployeeRepository {
    public void initDB() {
        Employee employee = new Employee("id0",
                "firebaseUserIdValue",
                "email@example.com",
                "password123",
                "John",
                "Doe",
                "123 Main Street",
                "123456789",
                new Schedule(),
                "",
                new Company(),
                true,
                new ArrayList<>());
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("employees")
                .add(employee)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d("REZ_DB", "DocumentSnapshot added with ID: " + documentReference.getId());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("REZ_DB", "Error adding document", e);
                    }
                });
    }

    public void insert(Employee employee, EmployeeInsertCallback callback) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("employees")
                .add(employee)
                .addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentReference> task) {
                        if (task.isSuccessful()) {
                            DocumentReference documentReference = task.getResult();
                            String employeeId = documentReference.getId();
                            Log.d("REZ_DB", "DocumentSnapshot added with ID: " + employeeId);
                            callback.onEmployeeInserted(employeeId);
                        } else {
                            Log.w("REZ_DB", "Error adding document", task.getException());
                            callback.onInsertError(task.getException());
                        }
                    }
                });
    }

    public void select(EmployeeCallback callback) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("employees")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            List<Employee> employees = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Employee employee = document.toObject(Employee.class);
                                employee.setId(document.getId());
                                employees.add(employee);
                                Log.d("REZ_DB", document.getId() + " => " + document.getData());
                            }
                            callback.onEmployeeRecieved(employees.toArray(new Employee[0]));
                        } else {
                            Log.w("REZ_DB", "Error getting documents.", task.getException());
                            callback.onError(task.getException());
                        }
                    }
                });
    }


    public void selectByCompany(EmployeeCallback callback, String companyName) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("employees")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            List<Employee> employees = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Employee employee = document.toObject(Employee.class);
                                employee.setId(document.getId());
                                if(employee.getCompany().getName().equals(companyName))
                                    employees.add(employee);
                                Log.d("REZ_DB", document.getId() + " => " + document.getData());
                            }
                            callback.onEmployeeRecieved(employees.toArray(new Employee[0]));
                        } else {
                            Log.w("REZ_DB", "Error getting documents.", task.getException());
                            callback.onError(task.getException());
                        }
                    }
                });
    }

    public void selectByEvent(EmployeeCallback callback, String eventName) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("employees")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            List<Employee> employees = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Employee employee = document.toObject(Employee.class);
                                employee.setId(document.getId());
                                for(Event e : employee.getScheduledEvents())
                                    if(e.getName().equals(eventName))
                                        employees.add(employee);
                                Log.d("REZ_DB", document.getId() + " => " + document.getData());
                            }
                            callback.onEmployeeRecieved(employees.toArray(new Employee[0]));
                        } else {
                            Log.w("REZ_DB", "Error getting documents.", task.getException());
                            callback.onError(task.getException());
                        }
                    }
                });
    }


    public void update(String id, Employee employee, EmployeeUpdateCallback callback) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("employees").document(id);
        docRef.update("isEnabled", employee.getIsEnabled())
                .addOnSuccessListener(aVoid -> {
                    Log.d("REZ_DB", "Is enabled successfully changed");
                    callback.onEmployeeUpdated();
                })
                .addOnFailureListener(e -> {
                    Log.w("REZ_DB", "Error getting documents.", e);
                    callback.onUpdateError(e);
                });
        docRef.update("schedule", employee.getSchedule())
                .addOnSuccessListener(aVoid -> {
                    Log.d("REZ_DB", "Is enabled successfully changed");
                    callback.onEmployeeUpdated();
                })
                .addOnFailureListener(e -> {
                    Log.w("REZ_DB", "Error getting documents.", e);
                    callback.onUpdateError(e);
                });
        docRef.update("scheduledEvents", employee.getScheduledEvents())
                .addOnSuccessListener(aVoid -> {
                    Log.d("REZ_DB", "Scheduled events successfully changed");
                    callback.onEmployeeUpdated();
                })
                .addOnFailureListener(e -> {
                    Log.w("REZ_DB", "Error getting documents.", e);
                    callback.onUpdateError(e);
                });
    }


    public void selectById(EmployeeCallback callback, String id) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("employees").document(id);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot documentSnapshot = task.getResult();
                    if (documentSnapshot.exists()) {
                        Employee[] employee = new Employee[]{documentSnapshot.toObject(Employee.class)};
                        Log.d("REZ_DB", documentSnapshot.getId() + " => " + documentSnapshot.getData());
                        callback.onEmployeeRecieved(employee);
                    } else {
                        Log.d("REZ_DB", "No such document");
                        callback.onError(new Exception("No such document"));
                    }
                } else {
                    Log.w("REZ_DB", "Error getting document", task.getException());
                    callback.onError(task.getException());
                }
            }
        });
    }


    public void selectByFirebaseId(EmployeeCallback callback, String id) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("employees")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            List<Employee> employees = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Employee employee = document.toObject(Employee.class);
                                employee.setId(document.getId());
                                if(employee.getFirebaseUserId().equals(id))
                                    employees.add(employee);
                                Log.d("REZ_DB", document.getId() + " => " + document.getData());
                            }
                            callback.onEmployeeRecieved(employees.toArray(new Employee[0]));
                        } else {
                            Log.w("REZ_DB", "Error getting documents.", task.getException());
                            callback.onError(task.getException());
                        }
                    }
                });
    }

}
