package rs.ftn.ma.mplanner.repo;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import rs.ftn.ma.mplanner.interfaces.NotificationCallback;
import rs.ftn.ma.mplanner.interfaces.NotificationInsertCallback;
import rs.ftn.ma.mplanner.interfaces.NotificationUpdateCallback;
import rs.ftn.ma.mplanner.model.Notification;

public class NotificationRepository {
    public void insert(Notification n, NotificationInsertCallback callback) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("notifications")
                .add(n)
                .addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentReference> task) {
                        if (task.isSuccessful()) {
                            DocumentReference documentReference = task.getResult();
                            String nId = documentReference.getId();
                            Log.d("REZ_DB", "DocumentSnapshot added with ID: " + nId);
                            callback.onNotificationInserted(nId);
                        } else {
                            Log.w("REZ_DB", "Error adding document", task.getException());
                            callback.onInsertError(task.getException());
                        }
                    }
                });
    }


    public void select(NotificationCallback callback) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("notifications")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            List<Notification> notifications = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Notification notification = document.toObject(Notification.class);
                                notification.setId(document.getId());
                                notifications.add(notification);
                                Log.d("REZ_DB", document.getId() + " => " + document.getData());
                            }
                            callback.onNotificationReceived(notifications.toArray(new Notification[0]));
                        } else {
                            Log.w("REZ_DB", "Error getting documents.", task.getException());
                            callback.onError(task.getException());
                        }
                    }
                });
    }


    public void selectByFirebaseID(NotificationCallback callback, String firebaseID) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("notifications")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            List<Notification> notifications = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Notification notification = document.toObject(Notification.class);
                                notification.setId(document.getId());
                                if(notification.getRecieverId().equals(firebaseID))
                                    notifications.add(notification);
                                Log.d("REZ_DB", document.getId() + " => " + document.getData());
                            }
                            callback.onNotificationReceived(notifications.toArray(new Notification[0]));
                        } else {
                            Log.w("REZ_DB", "Error getting documents.", task.getException());
                            callback.onError(task.getException());
                        }
                    }
                });
    }


    public void update(String id, Notification n, NotificationUpdateCallback callback) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("notifications").document(id);

        docRef.update("seen", n.getSeen())
                .addOnSuccessListener(aVoid -> {
                    Log.d("REZ_DB", "Seen successfully changed");
                    callback.onNotificationUpdated();
                })
                .addOnFailureListener(e -> {
                    Log.w("REZ_DB", "Error getting documents.", e);
                    callback.onUpdateError(e);
                });
    }



    public void selectById(NotificationCallback callback, String id) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("notifications").document(id);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot documentSnapshot = task.getResult();
                    if (documentSnapshot.exists()) {
                        Notification[] n = new Notification[]{documentSnapshot.toObject(Notification.class)};
                        Log.d("REZ_DB", documentSnapshot.getId() + " => " + documentSnapshot.getData());
                        callback.onNotificationReceived(n);
                    } else {
                        Log.d("REZ_DB", "No such document");
                        callback.onError(new Exception("No such document"));
                    }
                } else {
                    Log.w("REZ_DB", "Error getting document", task.getException());
                    callback.onError(task.getException());
                }
            }
        });
    }
}
