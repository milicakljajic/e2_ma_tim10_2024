package rs.ftn.ma.mplanner.repo;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import rs.ftn.ma.mplanner.interfaces.EventOrganizerCallback;
import rs.ftn.ma.mplanner.interfaces.EventOrganizerInsertCallback;
import rs.ftn.ma.mplanner.model.EventOrganizer;

public class EventOrganizerRepo {
    public void insert(EventOrganizer eo, EventOrganizerInsertCallback callback) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("event-organizers")
                .add(eo)
                .addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentReference> task) {
                        if (task.isSuccessful()) {
                            DocumentReference documentReference = task.getResult();
                            String eoId = documentReference.getId();
                            Log.d("REZ_DB", "DocumentSnapshot added with ID: " + eoId);
                            callback.onEventOrganizerInserted(eoId);
                        } else {
                            Log.w("REZ_DB", "Error adding document", task.getException());
                            callback.onInsertError(task.getException());
                        }
                    }
                });
    }


    public void select(EventOrganizerCallback callback, boolean selectDisabled) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("event-organizers")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            List<EventOrganizer> eventOrganizers = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                EventOrganizer eventOrganizer = document.toObject(EventOrganizer.class);
                                eventOrganizer.setId(document.getId());
                                if (eventOrganizer.getIsEnabled())
                                    eventOrganizers.add(eventOrganizer);
                                else if (selectDisabled)
                                    eventOrganizers.add(eventOrganizer);
                                Log.d("REZ_DB", document.getId() + " => " + document.getData());
                            }
                            callback.onEventOrganizerReceived(eventOrganizers.toArray(new EventOrganizer[0]));
                        } else {
                            Log.w("REZ_DB", "Error getting documents.", task.getException());
                            callback.onError(task.getException());
                        }
                    }
                });
    }

    //FIXME Implement if needed
   /* public void update(String id, EventOrganizer eo, EventTypeUpdateCallback callback) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("event-types").document(id);

        docRef.update("suggestedServices", eo.getSuggestedServices())
                .addOnSuccessListener(aVoid -> {
                    Log.d("REZ_DB", "Suggested services successfully changed");
                    callback.onEventTypeUpdated();
                })
                .addOnFailureListener(e -> {
                    Log.w("REZ_DB", "Error getting documents.", e);
                    callback.onUpdateError(e);
                });
    }*/


    public void selectById(EventOrganizerCallback callback, String id) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("event-organizers").document(id);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot documentSnapshot = task.getResult();
                    if (documentSnapshot.exists()) {
                        EventOrganizer[] eo = new EventOrganizer[]{documentSnapshot.toObject(EventOrganizer.class)};
                        Log.d("REZ_DB", documentSnapshot.getId() + " => " + documentSnapshot.getData());
                        callback.onEventOrganizerReceived(eo);
                    } else {
                        Log.d("REZ_DB", "No such document");
                        callback.onError(new Exception("No such document"));
                    }
                } else {
                    Log.w("REZ_DB", "Error getting document", task.getException());
                    callback.onError(task.getException());
                }
            }
        });
    }

    public void selectByFirebaseId(EventOrganizerCallback callback, String id) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("event-organizers")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            List<EventOrganizer> eventOrganizers = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                EventOrganizer eventOrganizer = document.toObject(EventOrganizer.class);
                                eventOrganizer.setId(document.getId());
                                if(eventOrganizer.getFirebaseUserId().equals(id))
                                    eventOrganizers.add(eventOrganizer);
                                Log.d("REZ_DB", document.getId() + " => " + document.getData());
                            }
                            callback.onEventOrganizerReceived(eventOrganizers.toArray(new EventOrganizer[0]));
                        } else {
                            Log.w("REZ_DB", "Error getting documents.", task.getException());
                            callback.onError(task.getException());
                        }
                    }
                });
    }

}



