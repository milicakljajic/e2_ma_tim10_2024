package rs.ftn.ma.mplanner.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.model.EventType;
import rs.ftn.ma.mplanner.repo.EventTypeRepository;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EventTypesListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EventTypesListFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private EventTypeRepository repo;
    EventType[] eventTypes = new EventType[]{};

    public EventTypesListFragment() {
        // Required empty public constructor
        repo = new EventTypeRepository();
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment EventTypesListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EventTypesListFragment newInstance(String param1, String param2) {
        EventTypesListFragment fragment = new EventTypesListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_event_types_list, container, false);

        eventTypes = new EventType[]{new EventType("Svadba", ""),
                new EventType("Rodjendan", ""),
        new EventType("Sahrana", "")};


        return view;
    }
}