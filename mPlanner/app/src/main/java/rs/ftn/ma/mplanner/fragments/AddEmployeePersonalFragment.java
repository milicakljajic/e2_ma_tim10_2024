package rs.ftn.ma.mplanner.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.model.Employee;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AddEmployeePersonalFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddEmployeePersonalFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "Employee";

    // TODO: Rename and change types of parameters
    private Employee newEmployee;

    public AddEmployeePersonalFragment() {
        // Required empty public constructor
    }

    public static AddEmployeePersonalFragment newInstance(Employee newEmployee) {
        AddEmployeePersonalFragment fragment = new AddEmployeePersonalFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, newEmployee);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            newEmployee = getArguments().getParcelable(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_employee_personal, container, false);

        Button button = (Button) view.findViewById(R.id.ae_personal_next);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(newEmployee != null){
                    EditText firstname = view.findViewById(R.id.add_employee_personal_firstname);
                    EditText lastname = view.findViewById(R.id.add_employee_personal_lastname);
                    EditText phone = view.findViewById(R.id.add_employee_personal_phone);
                    EditText address = view.findViewById(R.id.add_employee_personal_address);
                    String name = String.valueOf(firstname.getText());
                    String lname = String.valueOf(lastname.getText());
                    String pho = String.valueOf(phone.getText());
                    String adr = String.valueOf(address.getText());
                    if(validate(name, lname, pho, adr)){
                        newEmployee.setFirstName(name);
                        newEmployee.setLastName(lname);
                        newEmployee.setPhoneNumber(pho);
                        newEmployee.setAddress(adr);
                        Bundle bundle = new Bundle();
                        bundle.putParcelable("Employee", newEmployee);
                        Navigation.findNavController(view).navigate(R.id.changeToAddEmployeeSchedule1, bundle);
                    }
                }
            }
        });
        return view;
    }

    public Boolean validate(String firstname, String lastname, String phone, String address) {
        return !firstname.isEmpty() && !lastname.isEmpty() && !phone.isEmpty() && !address.isEmpty();
    }
}