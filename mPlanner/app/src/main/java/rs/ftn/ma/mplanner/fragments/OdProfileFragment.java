package rs.ftn.ma.mplanner.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.interfaces.EventOrganizerCallback;
import rs.ftn.ma.mplanner.model.EventOrganizer;
import rs.ftn.ma.mplanner.repo.EventOrganizerRepo;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link OdProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OdProfileFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "eventOrganizerId";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public OdProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment OdProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static OdProfileFragment newInstance(String param1, String param2) {
        OdProfileFragment fragment = new OdProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_od_profile, container, false);

        Button button = (Button) view.findViewById(R.id.button_edit_profile);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Navigation.findNavController(view).navigate(R.id.changeToEditOdProfileFragment);
            }
        });


        // Prikazivanje podataka trenutno ulogovanog organizatora događaja
        String firebaseUserId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        TextView textName = view.findViewById(R.id.text_name);
        TextView textSurname = view.findViewById(R.id.text_surname);
        TextView textEmail = view.findViewById(R.id.text_email);
        TextView textAddress = view.findViewById(R.id.text_address);
        TextView textPhone = view.findViewById(R.id.text_phone);

        EventOrganizerRepo eventOrganizerRepo = new EventOrganizerRepo();

        eventOrganizerRepo.selectByFirebaseId(new EventOrganizerCallback() {
            @Override
            public void onEventOrganizerReceived(EventOrganizer[] eventOrganizers) {
                if (eventOrganizers.length > 0) {
                    EventOrganizer organizer = eventOrganizers[0];
                    textName.setText("Name: " + organizer.getFirstName());
                    textSurname.setText("Surname: " + organizer.getLastName());
                    textEmail.setText("Email: " + organizer.getEmail());
                    textAddress.setText("Address: " + organizer.getAddress());
                    textPhone.setText("Phone number: " + organizer.getPhoneNumber());
                }
            }

            @Override
            public void onError(Exception e) {
                Log.e("OdProfileFragment", "Error fetching event organizer", e);
            }
        }, firebaseUserId);

        return view;
    }
}