package rs.ftn.ma.mplanner.fragments;


import android.app.DatePickerDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;

//import com.prolificinteractive.materialcalendarview.MaterialCalendarView;


import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.adapters.EventAdapter;
import rs.ftn.ma.mplanner.interfaces.EventProviderCallback;
import rs.ftn.ma.mplanner.model.Employee;
import rs.ftn.ma.mplanner.model.Event;
import rs.ftn.ma.mplanner.model.EventProvider;
import rs.ftn.ma.mplanner.repo.EmployeeRepository;
import rs.ftn.ma.mplanner.repo.EventProviderRepository;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CalendarEventListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CalendarEventListFragment extends Fragment {

    private static final String ARG_PARAM1 = "Employee";

    private Employee employee;

    private EmployeeRepository repo;

    private EventProviderRepository prepo;

    private FirebaseAuth mAuth;

    private String from;
    private String to;


    public CalendarEventListFragment() {
        // Required empty public constructor
        //employee = new Employee();
        repo = new EmployeeRepository();
        prepo = new EventProviderRepository();
        mAuth = FirebaseAuth.getInstance();
    }


    public static CalendarEventListFragment newInstance(Employee employee) {
        CalendarEventListFragment fragment = new CalendarEventListFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, employee);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            employee = getArguments().getParcelable(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_calendar_event_list, container, false);


        /*Spinner spinner = (Spinner) view.findViewById(R.id.week_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                requireContext(),
                R.array.weeks_example_array,
                android.R.layout.simple_spinner_item
        );

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);*/

                /*Event[] events = new Event[]{new Event(new EventType(), "Naziv događaja", "Opis događaja", 10, true, "Lokacija događaja", 50, "Aktivan", "2024-05-10", "12:00"),
                        new Event(new EventType(), "Naziv događaja", "Opis događaja", 10, true, "Lokacija događaja", 50, "Aktivan", "2024-05-10", "12:00")
                };*/
        //Event[] events = new Event[]{new Event(), new Event()};





        ListView lw = view.findViewById(R.id.events_list);
        EventAdapter adapter2 = new EventAdapter(requireContext(), employee.getScheduledEvents().toArray(new Event[0]));
        lw.setAdapter(adapter2);

        Button buttonConfirm = (Button) view.findViewById(R.id.calendar_event_list_confirm);

        buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Event[] events = new Event[]{new Event(new EventType(), "Naziv događaja", "Opis događaja", 10, true, "Lokacija događaja", 50, "Aktivan", "2024-05-10", "12:00"),
                        new Event(new EventType(), "Naziv događaja", "Opis događaja", 10, true, "Lokacija događaja", 50, "Aktivan", "2024-05-10", "12:00")
                };*/
                //Event[] events = new Event[]{new Event(), new Event()};


                ArrayList<Event> filter = new ArrayList<>();
                for(Event e : employee.getScheduledEvents())
                {
                    if(isDateBetween(from,to,e.getDate()))
                        filter.add(e);
                }


                ListView lw = view.findViewById(R.id.events_list);
                EventAdapter adapter2 = new EventAdapter(requireContext(), filter.toArray(new Event[0]));
                lw.setAdapter(adapter2);
            }
        });


        Button button2 = (Button) view.findViewById(R.id.event_list_schedule_new_event);
        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FirebaseUser cUser = mAuth.getCurrentUser();
                prepo.selectByFirebaseId(new EventProviderCallback() {
                    @Override
                    public void onEventProviderReceived(EventProvider[] providers) {
                        Bundle bundle = new Bundle();
                        bundle.putParcelable("Employee", employee);
                        bundle.putParcelable("Provider", providers[0]);
                        Navigation.findNavController(view).navigate(R.id.changeToScheduleEvent, bundle);
                    }

                    @Override
                    public void onError(Exception e) {

                    }
                }, cUser.getUid());
            }
        });

        Button buttonStartDate = view.findViewById(R.id.calendar_event_list_start_date);
        buttonStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(requireContext(),new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        month += 1;
                        String s = String.valueOf(dayOfMonth) + "." + String.valueOf(month) + "." + String.valueOf(year);
                        //text.setText(String.valueOf(year));
                        from = s;
                    }
                }, year, month, 15);
                datePickerDialog.show();
            }
        });

        Button buttonEndDate = view.findViewById(R.id.calendar_event_list_end_date);
        buttonEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(requireContext(),new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        month += 1;
                        String s = String.valueOf(dayOfMonth) + "." + String.valueOf(month) + "." + String.valueOf(year);
                        //newEmployee.getSchedule().setEndDate(s);
                        //text.setText(String.valueOf(year));
                        to = s;
                    }
                }, year, month, 15);
                datePickerDialog.show();
            }
        });

        return view;
    }

    public boolean isDateBetween(String dateString1, String dateString2, String dateString3) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        try {
            Date date1 = sdf.parse(dateString1);
            Date date2 = sdf.parse(dateString2);
            Date date3 = sdf.parse(dateString3);

            // Provera da li je date3 između date1 i date2
            return date3.after(date1) && date3.before(date2);
        } catch (ParseException e) {
            //e.printStackTrace();
            // U slučaju greške vraćamo false
            return false;
        }
    }

}