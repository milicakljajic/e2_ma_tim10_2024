package rs.ftn.ma.mplanner.repo;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import rs.ftn.ma.mplanner.interfaces.AdminCallback;
import rs.ftn.ma.mplanner.interfaces.AdminInsertCallback;
import rs.ftn.ma.mplanner.model.Admin;

public class AdminRepository {
    public void insert(Admin a, AdminInsertCallback callback) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("admins")
                .add(a)
                .addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentReference> task) {
                        if (task.isSuccessful()) {
                            DocumentReference documentReference = task.getResult();
                            String aId = documentReference.getId();
                            Log.d("REZ_DB", "DocumentSnapshot added with ID: " + aId);
                            callback.onAdminInserted(aId);
                        } else {
                            Log.w("REZ_DB", "Error adding document", task.getException());
                            callback.onInsertError(task.getException());
                        }
                    }
                });
    }


    public void select(AdminCallback callback) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("admins")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            List<Admin> admins = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Admin admin = document.toObject(Admin.class);
                                admin.setId(document.getId());
                                admins.add(admin);

                                Log.d("REZ_DB", document.getId() + " => " + document.getData());
                            }
                            callback.onAdminReceived(admins.toArray(new Admin[0]));
                        } else {
                            Log.w("REZ_DB", "Error getting documents.", task.getException());
                            callback.onError(task.getException());
                        }
                    }
                });
    }


    public void selectById(AdminCallback callback, String id) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("admins").document(id);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot documentSnapshot = task.getResult();
                    if (documentSnapshot.exists()) {
                        Admin[] a = new Admin[]{documentSnapshot.toObject(Admin.class)};
                        Log.d("REZ_DB", documentSnapshot.getId() + " => " + documentSnapshot.getData());
                        callback.onAdminReceived(a);
                    } else {
                        Log.d("REZ_DB", "No such document");
                        callback.onError(new Exception("No such document"));
                    }
                } else {
                    Log.w("REZ_DB", "Error getting document", task.getException());
                    callback.onError(task.getException());
                }
            }
        });
    }

    public void selectByFirebaseId(AdminCallback callback, String id) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("admins")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            List<Admin> admins = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Admin admin = document.toObject(Admin.class);
                                admin.setId(document.getId());
                                if(admin.getFirebaseUserId().equals(id))
                                    admins.add(admin);
                                Log.d("REZ_DB", document.getId() + " => " + document.getData());
                            }
                            callback.onAdminReceived(admins.toArray(new Admin[0]));
                        } else {
                            Log.w("REZ_DB", "Error getting documents.", task.getException());
                            callback.onError(task.getException());
                        }
                    }
                });
    }
}
