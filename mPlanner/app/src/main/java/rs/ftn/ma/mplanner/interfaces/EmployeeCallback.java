package rs.ftn.ma.mplanner.interfaces;

import rs.ftn.ma.mplanner.model.Employee;

public interface EmployeeCallback {
    void onEmployeeRecieved(Employee[] employees);
    void onError(Exception e);
}
