package rs.ftn.ma.mplanner.interfaces;

public interface ReportDeleteCallback {
    void onReportDeleted();
    void onDeleteError(Exception e);
}
