package rs.ftn.ma.mplanner.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.interfaces.NotificationInsertCallback;
import rs.ftn.ma.mplanner.interfaces.ReportInsertCallback;
import rs.ftn.ma.mplanner.model.Comment;
import rs.ftn.ma.mplanner.model.EventProvider;
import rs.ftn.ma.mplanner.model.Notification;
import rs.ftn.ma.mplanner.model.Report;
import rs.ftn.ma.mplanner.repo.NotificationRepository;
import rs.ftn.ma.mplanner.repo.ReportRepository;

public class CommentAdapter extends BaseAdapter {
    Context context;
    ArrayList<Comment> comments;
    LayoutInflater inflter;

    EventProvider pr;

    public CommentAdapter(Context applicationContext, Comment[] comments) {
        this.context = applicationContext;
        this.comments = new ArrayList<>(Arrays.asList(comments));
        inflter = (LayoutInflater.from(applicationContext));
    }

    public CommentAdapter(Context applicationContext, Comment[] comments, EventProvider pr) {
        this.context = applicationContext;
        this.comments = new ArrayList<>(Arrays.asList(comments));
        inflter = (LayoutInflater.from(applicationContext));
        this.pr = pr;
    }


    public CommentAdapter(Context applicationContext, ArrayList<Comment> comments) {
        this.context = applicationContext;
        this.comments = comments;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return comments.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.fragment_comment_item, null);
        TextView name = (TextView) view.findViewById(R.id.comment_name);
        TextView username = (TextView) view.findViewById(R.id.comment_username);
        TextView date = (TextView) view.findViewById(R.id.comment_date);
        TextView rate = (TextView) view.findViewById(R.id.comment_rate);
        TextView commentText = (TextView) view.findViewById(R.id.comment_text);

        String nameStirng = comments.get(i).getFirstname() + " " + comments.get(i).getLastname();
        name.setText(nameStirng);
        username.setText(comments.get(i).getUsername());
        date.setText(comments.get(i).getLastModified().toLocaleString());
        rate.setText(comments.get(i).getRate());
        commentText.setText(comments.get(i).getMessage());

        ImageView img = view.findViewById(R.id.report_comment);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Kreiramo AlertDialog
                final EditText input = new EditText(v.getContext());
                new AlertDialog.Builder(v.getContext())
                        .setTitle("Reason of report")
                        .setMessage("Please enter your text here:")
                        .setView(input) // Postavljamo EditText kao view u dijalogu
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                ReportRepository rrepo = new ReportRepository();
                                NotificationRepository nrepo = new NotificationRepository();
                                // Dobijamo unos korisnika
                                String enteredText = input.getText().toString();
                                Report r = new Report("", comments.get(i),pr,new Date(),enteredText, "reported");

                                rrepo.insert(r, new ReportInsertCallback() {
                                    @Override
                                    public void onReportInserted(String reportId) {
                                        Notification n = new Notification("GkoDer6HeafPJ4sRSZjirj4VPdk2", "New report", "There has been a report for comment!", false);
                                        nrepo.insert(n, new NotificationInsertCallback() {
                                            @Override
                                            public void onNotificationInserted(String notificationId) {
                                                Toast.makeText(v.getContext(), "Report success!", Toast.LENGTH_SHORT).show();
                                            }

                                            @Override
                                            public void onInsertError(Exception e) {

                                            }
                                        });
                                    }

                                    @Override
                                    public void onInsertError(Exception e) {

                                    }
                                });

                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                // Radimo nešto ako korisnik otkaže unos
                                dialog.cancel();
                            }
                        })
                        .show();
            }
        });

        return view;
    }
}
