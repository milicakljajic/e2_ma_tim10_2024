package rs.ftn.ma.mplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class ServiceReservation implements Parcelable {
    public String ID;
    public Service SelectedService;

    public boolean IssInBundle;
    public Date ReservationDateFrom;
    public Date ReservationDateTo;
    public Employee SelectedEmployee;


    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public Service getSelectedService() {
        return SelectedService;
    }

    public void setSelectedService(Service selectedService) {
        SelectedService = selectedService;
    }

    public boolean isIssInBundle() {
        return IssInBundle;
    }

    public void setIssInBundle(boolean issInBundle) {
        IssInBundle = issInBundle;
    }

    public Date getReservationDateFrom() {
        return ReservationDateFrom;
    }

    public void setReservationDateFrom(Date reservationDateFrom) {
        ReservationDateFrom = reservationDateFrom;
    }

    public Date getReservationDateTo() {
        return ReservationDateTo;
    }

    public void setReservationDateTo(Date reservationDateTo) {
        ReservationDateTo = reservationDateTo;
    }

    public Employee getSelectedEmployee() {
        return SelectedEmployee;
    }

    public void setSelectedEmployee(Employee selectedEmployee) {
        SelectedEmployee = selectedEmployee;
    }

    public ServiceReservation() {
    }


    public ServiceReservation(String ID, Service selectedService, boolean issInBundle, Date reservationDateFrom, Date reservationDateTo, Employee selectedEmployee) {
        this.ID = ID;
        SelectedService = selectedService;
        IssInBundle = issInBundle;
        ReservationDateFrom = reservationDateFrom;
        ReservationDateTo = reservationDateTo;
        SelectedEmployee = selectedEmployee;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.ID);
        dest.writeParcelable(this.SelectedService, flags);
        dest.writeByte(this.IssInBundle ? (byte) 1 : (byte) 0);
        dest.writeSerializable(this.ReservationDateFrom);
        dest.writeSerializable(this.ReservationDateTo);
        dest.writeParcelable(this.SelectedEmployee, flags);
    }

    public void readFromParcel(Parcel source) {
        this.ID = source.readString();
        this.SelectedService = source.readParcelable(Service.class.getClassLoader());
        this.IssInBundle = source.readByte() != 0;
        this.ReservationDateFrom = (Date)source.readSerializable();
        this.ReservationDateTo = (Date)source.readSerializable();
        this.SelectedEmployee = source.readParcelable(Employee.class.getClassLoader());
    }

    protected ServiceReservation(Parcel in) {
        this.ID = in.readString();
        this.SelectedService = in.readParcelable(Service.class.getClassLoader());
        this.IssInBundle = in.readByte() != 0;
        this.ReservationDateFrom = (Date)in.readSerializable();
        this.ReservationDateTo = (Date)in.readSerializable();
        this.SelectedEmployee = in.readParcelable(Employee.class.getClassLoader());
    }

    public static final Parcelable.Creator<ServiceReservation> CREATOR = new Parcelable.Creator<ServiceReservation>() {
        @Override
        public ServiceReservation createFromParcel(Parcel source) {
            return new ServiceReservation(source);
        }

        @Override
        public ServiceReservation[] newArray(int size) {
            return new ServiceReservation[size];
        }
    };
}
