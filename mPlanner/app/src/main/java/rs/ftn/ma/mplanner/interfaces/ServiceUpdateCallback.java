package rs.ftn.ma.mplanner.interfaces;

public interface ServiceUpdateCallback {
    void onServiceUpdated();
    void onUpdateError(Exception e);
}
