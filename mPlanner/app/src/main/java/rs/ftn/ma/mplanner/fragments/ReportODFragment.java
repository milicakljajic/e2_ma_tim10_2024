package rs.ftn.ma.mplanner.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.model.Company;
import rs.ftn.ma.mplanner.model.EventProvider;
import rs.ftn.ma.mplanner.model.Report;

public class ReportODFragment extends Fragment {

    private EventProvider user;
    private Company company;

    private EditText descriptionEditText;
    private Button reportButton;

    //private NotificationApi apiManager = RetrofitClient.getApi();

    public ReportODFragment() {
        // Required empty public constructor
    }

    public ReportODFragment(Company company, EventProvider user){
        this.company = company;
        this.user = user;
    }

    public static ReportODFragment newInstance() {
        ReportODFragment fragment = new ReportODFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_report_od, container, false);

        descriptionEditText = view.findViewById(R.id.od_report_reason);
        reportButton = view.findViewById(R.id.od_report_button);

        reportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMaxId(new OnSuccessListener<Long>() {
                    @Override
                    public void onSuccess(Long aLong) {
                        //Report report = new Report(aLong,user,company, LocalDateTime.now().toString(),descriptionEditText.getText().toString(),"COMPANY");
                        Report report = new Report();
                        saveReport(report);
                    }
                });
            }
        });


        return view;
    }

    public void getMaxId(OnSuccessListener<Long> onSuccessListener) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        CollectionReference collectionRef = db.collection("reports");

        collectionRef.orderBy("reportId", Query.Direction.DESCENDING)
                .limit(1)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    if (!queryDocumentSnapshots.isEmpty()) {
                        // Get the first document in the result
                        DocumentSnapshot document = queryDocumentSnapshots.getDocuments().get(0);
                        Long maxId = document.getLong("reportId");
                        onSuccessListener.onSuccess(maxId + 1);
                    } else {
                        // If the collection is empty, return a default value or handle it appropriately
                        onSuccessListener.onSuccess((long)1);
                    }
                })
                .addOnFailureListener(e -> Log.w("TAG", "Error getting max ID.", e));
    }

    private void saveReport(Report report){
        FirebaseFirestore firestore = FirebaseFirestore.getInstance();
        firestore.collection("reports")
                .add(report)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        requireActivity().getSupportFragmentManager().popBackStack();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }
                });
    }
}