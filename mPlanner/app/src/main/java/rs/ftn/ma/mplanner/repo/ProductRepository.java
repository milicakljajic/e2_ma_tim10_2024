package rs.ftn.ma.mplanner.repo;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import rs.ftn.ma.mplanner.interfaces.ProductCallback;
import rs.ftn.ma.mplanner.interfaces.ProductDeleteCallback;
import rs.ftn.ma.mplanner.interfaces.ProductInsertCallback;
import rs.ftn.ma.mplanner.interfaces.ProductUpdateCallback;
import rs.ftn.ma.mplanner.model.Product;

public class ProductRepository {

    public void create(Product product, ProductInsertCallback callback){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("products")
                .add(product)
                .addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentReference> task) {
                        if(task.isSuccessful()){
                            DocumentReference documentReference = task.getResult();
                            String productId = documentReference.getId();
                            Log.d("REZ_DB","Product added with Id: " + productId);
                            callback.onProductCreated(productId);
                        }else{
                            Log.w("REZ_DB","Error creating product",task.getException());
                            callback.onCreateError(task.getException());
                        }
                    }
                });
    }

    public void getAll(ProductCallback callback){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("products")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if(task.isSuccessful()){
                            List<Product> productList = new ArrayList<>();
                            for(QueryDocumentSnapshot document : task.getResult()){
                                Product product = document.toObject(Product.class);
                                product.setId(Integer.valueOf(document.getId()));
                                productList.add(product);
                                Log.d("REZ_DB",document.getId() + "=>" + document.getData());
                            }
                            callback.onProductReceived(productList.toArray(new Product[0]));
                        }else{
                            Log.w("REZ_DB","Error getting documents.",task.getException());
                            callback.onError(task.getException());
                        }
                    }
                });
    }

    public void update(String id, Product product, ProductUpdateCallback callback){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("products").document(id);

        docRef.update("name",product.getName(),
                "description",product.getDescription(),
                "price",product.getPrice(),
                "subcategoryId",product.getSubcategoryId(),
                "discountPercentage",product.getDiscountPercentage(),
                "isAvailable",product.getAvailable()
                        )
                .addOnSuccessListener(aVoid -> {
                    Log.d("REZ_DB","Product updated");
                    callback.onProductUpdated();
                })
                .addOnFailureListener(e -> {
                    Log.w("REZ_DB","Error getting documents",e);
                    callback.onUpdateError(e);
                });

    }

    public void getById(ProductCallback callback, String id){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("products").document(id);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful()){
                    DocumentSnapshot snapshot = task.getResult();
                    if(snapshot.exists()){
                        Product[] product = new Product[]{snapshot.toObject(Product.class)};
                        Log.d("REZ_DB",snapshot.getId() + "=>" + snapshot.getData());
                        callback.onProductReceived(product);
                    }else{
                        Log.d("REZ_DB", "No such document");
                        callback.onError(new Exception("No such document"));
                    }
                }else{
                    Log.w("REZ_DB","Error getting document",task.getException());
                    callback.onError(task.getException());
                }
            }
        });
    }

    public void delete(String id, ProductDeleteCallback callback){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("products").document(id);

        docRef.delete()
                .addOnSuccessListener(avoid -> {
                    Log.d("REZ_DB","Product successfully deleted");
                    callback.onProductDeleted();
                })
                .addOnFailureListener(e -> {
                    Log.w("REZ_DB","Error deleting product",e);
                    callback.onDeleteError(e);
                });
    }

}
