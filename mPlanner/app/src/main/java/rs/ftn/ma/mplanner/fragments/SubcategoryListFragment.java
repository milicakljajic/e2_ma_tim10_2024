package rs.ftn.ma.mplanner.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.adapters.SubcategoryAdapter;
import rs.ftn.ma.mplanner.interfaces.OnDeleteButtonClickListener;
import rs.ftn.ma.mplanner.interfaces.SubcategoryCallback;
import rs.ftn.ma.mplanner.interfaces.SubcategoryDeleteCallback;
import rs.ftn.ma.mplanner.model.Subcategory;
import rs.ftn.ma.mplanner.repo.SubcategoryRepository;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SubcategoryListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SubcategoryListFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private SubcategoryRepository repo;

    public SubcategoryListFragment() {
        // Required empty public constructor
        repo = new SubcategoryRepository();
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SubcategoryList.
     */
    // TODO: Rename and change types and number of parameters
    public static SubcategoryListFragment newInstance(String param1, String param2) {
        SubcategoryListFragment fragment = new SubcategoryListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_subcategory_list, container, false);
        /*Subcategory[] subcategories = new Subcategory[]{
                new Subcategory("", new Category(), "PRODUCT", "Subcategory1", "Description1"),
                new Subcategory("", new Category(), "PRODUCT", "Subcategory2", "Description2"),
        };*/

        repo.select(new SubcategoryCallback() {
            @Override
            public void onSubcategoryReceived(Subcategory[] subcategories) {
                ListView lw = view.findViewById(R.id.subcategory_list);
                ArrayList<Subcategory> subs = new ArrayList<Subcategory>(Arrays.asList(subcategories));
                SubcategoryAdapter adapter = new SubcategoryAdapter(requireContext(), subs);
                lw.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Subcategory temp = subcategories[position];
                        Bundle bundle = new Bundle();
                        bundle.putString("mode", "update");
                        bundle.putString("id", temp.getId());
                        Navigation.findNavController(view).navigate(R.id.changeToSubCategoryCreation2, bundle);
                    }
                });
                adapter.setOnDeleteButtonClickListener(new OnDeleteButtonClickListener() {
                    @Override
                    public void onDeleteClicked(int position) {
                        //currentType.removeService(position);
                        Subcategory temp = subs.get(position);
                        repo.delete(temp.getId(), new SubcategoryDeleteCallback() {
                            @Override
                            public void onSubcategoryDeleted() {

                            }

                            @Override
                            public void onDeleteError(Exception e) {

                            }
                        });
                        subs.remove(position);
                        adapter.notifyDataSetChanged();
                    }
                });
                lw.setAdapter(adapter);
            }

            @Override
            public void onError(Exception e) {

            }
        });



        return view;
    }
}