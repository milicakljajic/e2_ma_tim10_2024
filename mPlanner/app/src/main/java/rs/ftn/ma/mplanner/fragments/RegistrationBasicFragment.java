package rs.ftn.ma.mplanner.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.model.EventOrganizer;
import rs.ftn.ma.mplanner.model.EventProvider;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link RegistrationBasicFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RegistrationBasicFragment extends Fragment {

    private static final String ARG_PARAM1 = "EventOrganizer";
    private static final String ARG_PARAM2 = "EventProvider";
    private EventOrganizer newEventOrganizer;
    private EventProvider newEventProvider;
    public RegistrationBasicFragment() {
        // Required empty public constructor
    }

    public static RegistrationBasicFragment newInstance(EventOrganizer newEventOrganizer, EventProvider newEventProvider) {
        RegistrationBasicFragment fragment = new RegistrationBasicFragment();
        Bundle args = new Bundle();
        args.putParcelable("EventOrganizer", newEventOrganizer);
        args.putParcelable("EventProvider", newEventProvider);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if(getArguments().getParcelable(ARG_PARAM1) != null)
                newEventOrganizer = getArguments().getParcelable(ARG_PARAM1);
            if(getArguments().getParcelable(ARG_PARAM2) != null)
                newEventProvider = getArguments().getParcelable(ARG_PARAM2);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_registration_basic, container, false);
        View view =  inflater.inflate(R.layout.fragment_registration_basic, container, false);

        //Button button = (Button) view.findViewById(R.id.login_registration);
        Button button = (Button) view.findViewById(R.id.registration_basic_next);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(newEventOrganizer != null) {
                    EditText email = view.findViewById(R.id.registration_basic_email);
                    EditText password1 = view.findViewById(R.id.registration_basic_password_1);
                    EditText password2 = view.findViewById(R.id.registration_basic_password_2);
                    if(String.valueOf(password1.getText()).equals(String.valueOf(password2.getText()))){
                        newEventOrganizer.setEmail(String.valueOf(email.getText()));
                        newEventOrganizer.setPassword(String.valueOf(password1.getText()));
                        Bundle bundle = new Bundle();
                        bundle.putParcelable("EventOrganizer", newEventOrganizer);
                        Navigation.findNavController(view).navigate(R.id.changeToRegistrationPersonal1, bundle);
                    }
                }
                else if(newEventProvider != null) {
                    EditText email = view.findViewById(R.id.registration_basic_email);
                    EditText password1 = view.findViewById(R.id.registration_basic_password_1);
                    EditText password2 = view.findViewById(R.id.registration_basic_password_2);
                    if(String.valueOf(password1.getText()).equals(String.valueOf(password2.getText()))){
                        newEventProvider.setEmail(String.valueOf(email.getText()));
                        newEventProvider.setPassword(String.valueOf(password1.getText()));
                        Bundle bundle = new Bundle();
                        bundle.putParcelable("EventProvider", newEventProvider);
                        Navigation.findNavController(view).navigate(R.id.changeToRegistrationPersonal1, bundle);
                    }
                }
            }
        });
        return view;
    }
}