package rs.ftn.ma.mplanner.repo;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.List;

import rs.ftn.ma.mplanner.interfaces.EventTypeCallback;
import rs.ftn.ma.mplanner.interfaces.EventTypeInsertCallback;
import rs.ftn.ma.mplanner.interfaces.EventTypeUpdateCallback;
import rs.ftn.ma.mplanner.model.EventType;

public class EventTypeRepository {

    public void initDB() {
        EventType type = new EventType("Initial event type", "Initial description of event type");
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("event-types")
                .add(type)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d("REZ_DB", "DocumentSnapshot added with ID: " + documentReference.getId());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("REZ_DB", "Error adding document", e);
                    }
                });
    }

    public void insert(EventType event, EventTypeInsertCallback callback) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("event-types")
                .add(event)
                .addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentReference> task) {
                        if (task.isSuccessful()) {
                            DocumentReference documentReference = task.getResult();
                            String eventId = documentReference.getId();
                            Log.d("REZ_DB", "DocumentSnapshot added with ID: " + eventId);
                            callback.onEventTypeInserted(eventId);
                        } else {
                            Log.w("REZ_DB", "Error adding document", task.getException());
                            callback.onInsertError(task.getException());
                        }
                    }
                });
    }



    public Task<List<EventType>> select() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        CollectionReference eventTypesRef = db.collection("eventTypes");
        return eventTypesRef.get().continueWith(task -> {
            List<EventType> eventTypes = new ArrayList<>();
            for (DocumentSnapshot document : task.getResult()) {
                EventType eventType = document.toObject(EventType.class);
                eventTypes.add(eventType);
            }
            return eventTypes;
        });
    }

    public void update(String id, EventType type, EventTypeUpdateCallback callback) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("event-types").document(id);
        docRef.update("suggestedServices", type.getSuggestedServices())
                .addOnSuccessListener(aVoid -> {
                    Log.d("REZ_DB", "Suggested services successfully changed");
                    callback.onEventTypeUpdated();
                })
                .addOnFailureListener(e -> {
                    Log.w("REZ_DB", "Error getting documents.", e);
                    callback.onUpdateError(e);
                });
        docRef.update("suggestedProducts", type.getSuggestedProducts())
                .addOnSuccessListener(aVoid -> {
                    Log.d("REZ_DB", "Suggested products successfully changed");
                    callback.onEventTypeUpdated();
                })
                .addOnFailureListener(e -> {
                    Log.w("REZ_DB", "Error getting documents.", e);
                    callback.onUpdateError(e);
                });
        docRef.update("isEnabled", type.getIsEnabled())
                .addOnSuccessListener(aVoid -> {
                    Log.d("REZ_DB", "isEnabled successfully changed");
                    callback.onEventTypeUpdated();
                })
                .addOnFailureListener(e -> {
                    Log.w("REZ_DB", "Error getting documents.", e);
                    callback.onUpdateError(e);
                });
        docRef.update("description", type.getDescription())
                .addOnSuccessListener(aVoid -> {
                    Log.d("REZ_DB", "Description successfully changed");
                    callback.onEventTypeUpdated();
                })
                .addOnFailureListener(e -> {
                    Log.w("REZ_DB", "Error getting documents.", e);
                    callback.onUpdateError(e);
                });
    }


    public void selectById(EventTypeCallback callback, String id) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("event-types").document(id);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot documentSnapshot = task.getResult();
                    if (documentSnapshot.exists()) {
                        EventType[] type = new EventType[]{documentSnapshot.toObject(EventType.class)};
                        Log.d("REZ_DB", documentSnapshot.getId() + " => " + documentSnapshot.getData());
                        callback.onEventTypeReceived(type);
                    } else {
                        Log.d("REZ_DB", "No such document");
                        callback.onError(new Exception("No such document"));
                    }
                } else {
                    Log.w("REZ_DB", "Error getting document", task.getException());
                    callback.onError(task.getException());
                }
            }
        });
    }
}
