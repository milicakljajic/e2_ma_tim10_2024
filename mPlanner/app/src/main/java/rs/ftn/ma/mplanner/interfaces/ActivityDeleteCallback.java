package rs.ftn.ma.mplanner.interfaces;

public interface ActivityDeleteCallback {
    void onActivityDeleted();
    void onDeleteError(Exception e);
}
