package rs.ftn.ma.mplanner.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.adapters.ServiceAdapter;
import rs.ftn.ma.mplanner.interfaces.EventCallback;
import rs.ftn.ma.mplanner.interfaces.EventOrganizerCallback;
import rs.ftn.ma.mplanner.model.Event;
import rs.ftn.ma.mplanner.model.EventOrganizer;
import rs.ftn.ma.mplanner.model.Service;
import rs.ftn.ma.mplanner.repo.EventOrganizerRepo;
import rs.ftn.ma.mplanner.repo.EventRepository;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ServicesListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ServicesListFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private ArrayList<Event> availableEvents;

    private Event selectedEvent;

    private ExecutorService executorService;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public ServicesListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ServicesListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ServicesListFragment newInstance(String param1, String param2) {
        ServicesListFragment fragment = new ServicesListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.services_list_fragment, container, false);
        View view = inflater.inflate(R.layout.services_list_fragment, container, false);


        Spinner spinner2 = view.findViewById(R.id.service_page_event_spinner);
        EventRepository erepo = new EventRepository();
        EventOrganizerRepo eorepo = new EventOrganizerRepo();

        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        FirebaseUser cUser = mAuth.getCurrentUser();
        eorepo.selectByFirebaseId(new EventOrganizerCallback() {
            @Override
            public void onEventOrganizerReceived(EventOrganizer[] types) {
                erepo.selectByCreator(new EventCallback() {
                    @Override
                    public void onEventReceived(Event[] events) {
                        availableEvents = new ArrayList<>(Arrays.asList(events));
                        ArrayAdapter<Event> adapter = new ArrayAdapter<Event>(
                                view.getContext(),
                                android.R.layout.simple_spinner_item,
                                availableEvents
                        );

                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner2.setAdapter(adapter);
                    }

                    @Override
                    public void onError(Exception e) {

                    }
                }, false, types[0]);
            }

            @Override
            public void onError(Exception e) {

            }
        }, cUser.getUid());


        Service[] services = new Service[]{
                new Service(3, "Gardening Service", "Expert gardening and landscaping", "Home Services", "Gardening", 120, 0.08),
                new Service(4, "Cleaning Service", "Thorough cleaning for homes and offices", "Home Services", "Cleaning", 90, 0.07),
        };

        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.d("ss", "onItemSelected: " + availableEvents.get(position));
                selectedEvent = availableEvents.get(position);
                ServiceAdapter adapter = new ServiceAdapter(requireContext(),services,0, selectedEvent, rootview);
                //lv.setAdapter(adapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        ServiceAdapter adapter = new ServiceAdapter(requireContext(),services,0, selectedEvent, rootview);
        //lv.setAdapter(adapter);

        Spinner spinner = view.findViewById(R.id.services_sort);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_item,
                getResources().getStringArray(R.array.sort_array));

        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(arrayAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return view;
    }
}