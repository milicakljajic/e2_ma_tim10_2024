package rs.ftn.ma.mplanner.notifications;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import rs.ftn.ma.mplanner.interfaces.NotificationApi;

public class RetrofitClient {
    private static final String BASE_URL = "https://fcm.googleapis.com";
    private static Retrofit retrofit;

    private static Retrofit getRetrofitInstance() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public static NotificationApi getApi() {
        return getRetrofitInstance().create(NotificationApi.class);
    }
}
