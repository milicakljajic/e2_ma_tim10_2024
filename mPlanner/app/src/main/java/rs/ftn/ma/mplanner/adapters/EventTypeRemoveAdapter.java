package rs.ftn.ma.mplanner.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.fragments.EditProductFragment;
import rs.ftn.ma.mplanner.fragments.EditServiceFragment;
import rs.ftn.ma.mplanner.model.EventType;

public class EventTypeRemoveAdapter extends RecyclerView.Adapter<EventTypeRemoveAdapter.ViewHolder>{
    private List<EventType> eventTypes;
    private EditServiceFragment fragment;

    private EditProductFragment fragment1;


    public EventTypeRemoveAdapter(List<EventType> eventTypes, EditServiceFragment fragment) {
        this.fragment = fragment;
        this.eventTypes = eventTypes;
    }

    public EventTypeRemoveAdapter(List<EventType> eventTypes, EditProductFragment fragment) {
        this.fragment1 = fragment;
        this.eventTypes = eventTypes;
    }

    public void addEventTypeToProduct(EventType eventType) {
        eventTypes.add(eventType);
        notifyDataSetChanged();
    }

    public void removeEventTypeFromProduct(EventType eventType) {
        eventTypes.remove(eventType);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.remove_event_type, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        EventType eventType = eventTypes.get(position);
        holder.eventTypeNameTextView.setText(eventType.getName());
        holder.eventTypeDescriptionView.setText(eventType.getDescription());
        holder.removeEventTypeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int adapterPosition = holder.getAdapterPosition();
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    EventType selectedEventType = eventTypes.get(adapterPosition);
                    //fragment.removeEventTypeFromService(selectedEventType);
                    eventTypes.remove(selectedEventType);
                    notifyItemRemoved(adapterPosition);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return eventTypes.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView eventTypeNameTextView;
        public TextView eventTypeDescriptionView;
        public Button removeEventTypeButton;

        public ViewHolder(View view) {
            super(view);
            eventTypeNameTextView = view.findViewById(R.id.eventTypeNameTextView);
            eventTypeDescriptionView = view.findViewById(R.id.eventTypeDescriptionTextView);
            removeEventTypeButton = view.findViewById(R.id.removeButton);
        }
    }
}
