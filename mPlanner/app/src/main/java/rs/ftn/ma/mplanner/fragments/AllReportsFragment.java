package rs.ftn.ma.mplanner.fragments;

import static androidx.constraintlayout.helper.widget.MotionEffect.TAG;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.adapters.ReportAdapter;
import rs.ftn.ma.mplanner.model.Report;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AllReportsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AllReportsFragment extends Fragment {

    private RecyclerView recyclerView;
    private ReportAdapter reportAdapter;
    private List<Report> reportList;
    private FirebaseFirestore firestore;
    public AllReportsFragment() {
        // Required empty public constructor
    }

    public static AllReportsFragment newInstance() {
        AllReportsFragment fragment = new AllReportsFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        reportList = new ArrayList<Report>();
        firestore = FirebaseFirestore.getInstance();
        firestore.collection("reports")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if(task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Report report = document.toObject(Report.class);
                                //if(report.getReportStatus().equals(ReportStatus.PENDING)) {
                                reportList.add(report);
                                //}
                            }
                            //reportAdapter.notifyDataSetChanged();
                        }else{
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_all_reports, container, false);

        recyclerView = view.findViewById(R.id.reports_recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        reportAdapter = new ReportAdapter(getActivity(),reportList);
        recyclerView.setAdapter(reportAdapter);

        return view;
    }
}