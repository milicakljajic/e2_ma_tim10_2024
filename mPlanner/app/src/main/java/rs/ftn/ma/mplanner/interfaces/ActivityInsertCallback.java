package rs.ftn.ma.mplanner.interfaces;

public interface ActivityInsertCallback {
    void onActivityCreated(String activityId);
    void onCreateError(Exception e);
}
