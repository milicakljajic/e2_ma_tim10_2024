package rs.ftn.ma.mplanner.interfaces;

public interface EventUpdateCallback {
    void onEventUpdated();
    void onUpdateError(Exception e);
}