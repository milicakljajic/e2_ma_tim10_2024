package rs.ftn.ma.mplanner.repo;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import rs.ftn.ma.mplanner.interfaces.ServiceReservationCallback;
import rs.ftn.ma.mplanner.interfaces.ServiceReservationInsertCallback;
import rs.ftn.ma.mplanner.interfaces.ServiceReservationUpdateCallback;
import rs.ftn.ma.mplanner.model.ServiceReservation;

public class ServiceReservationRepository {

    public void insert(ServiceReservation sr, ServiceReservationInsertCallback callback) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("service-reservations")
                .add(sr)
                .addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentReference> task) {
                        if (task.isSuccessful()) {
                            DocumentReference documentReference = task.getResult();
                            String srId = documentReference.getId();
                            Log.d("REZ_DB", "DocumentSnapshot added with ID: " + srId);
                            callback.onServiceReservationInserted(srId);
                        } else {
                            Log.w("REZ_DB", "Error adding document", task.getException());
                            callback.onInsertError(task.getException());
                        }
                    }
                });
    }

    public void select(ServiceReservationCallback callback) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("service-reservations")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            List<ServiceReservation> serviceReservations = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                ServiceReservation serviceReservation = document.toObject(ServiceReservation.class);
                                serviceReservation.setID(document.getId());
                                serviceReservations.add(serviceReservation);
                                Log.d("REZ_DB", document.getId() + " => " + document.getData());
                            }
                            callback.onServiceReservationReceived(serviceReservations.toArray(new ServiceReservation[0]));
                        } else {
                            Log.w("REZ_DB", "Error getting documents.", task.getException());
                            callback.onError(task.getException());
                        }
                    }
                });
    }

    public void selectById(ServiceReservationCallback callback, String id) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("service-reservations").document(id);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot documentSnapshot = task.getResult();
                    if (documentSnapshot.exists()) {
                        ServiceReservation[] sr = new ServiceReservation[]{documentSnapshot.toObject(ServiceReservation.class)};
                        Log.d("REZ_DB", documentSnapshot.getId() + " => " + documentSnapshot.getData());
                        callback.onServiceReservationReceived(sr);
                    } else {
                        Log.d("REZ_DB", "No such document");
                        callback.onError(new Exception("No such document"));
                    }
                } else {
                    Log.w("REZ_DB", "Error getting document", task.getException());
                    callback.onError(task.getException());
                }
            }
        });
    }

    public void update(String id, ServiceReservation sr, ServiceReservationUpdateCallback callback) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("service-reservations").document(id);

        docRef.set(sr)
                .addOnSuccessListener(aVoid -> {
                    Log.d("REZ_DB", "Document successfully updated");
                    callback.onServiceReservationUpdated();
                })
                .addOnFailureListener(e -> {
                    Log.w("REZ_DB", "Error updating document", e);
                    callback.onUpdateError(e);
                });
    }

    public void delete(String id, ServiceReservationUpdateCallback callback) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("service-reservations").document(id);

        docRef.delete()
                .addOnSuccessListener(aVoid -> {
                    Log.d("REZ_DB", "Document successfully deleted");
                    callback.onServiceReservationDeleted();
                })
                .addOnFailureListener(e -> {
                    Log.w("REZ_DB", "Error deleting document", e);
                    callback.onDeleteError(e);
                });
    }
}
