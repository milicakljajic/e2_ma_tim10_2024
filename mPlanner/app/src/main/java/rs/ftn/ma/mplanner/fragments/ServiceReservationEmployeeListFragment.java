package rs.ftn.ma.mplanner.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.adapters.EmployeeAdapter2;
import rs.ftn.ma.mplanner.interfaces.EmployeeCallback;
import rs.ftn.ma.mplanner.model.Employee;
import rs.ftn.ma.mplanner.model.Event;
import rs.ftn.ma.mplanner.model.EventProvider;
import rs.ftn.ma.mplanner.repo.EmployeeRepository;
import rs.ftn.ma.mplanner.repo.EventProviderRepository;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ServiceReservationEmployeeListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ServiceReservationEmployeeListFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "Event";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private Event mParam1;
    private String mParam2;

    private final EmployeeRepository repo;
    private final EventProviderRepository prepo;
    private EventProvider provider;
    private FirebaseAuth mAuth;
    private String compName;

    public ServiceReservationEmployeeListFragment() {
        // Required empty public constructor
        repo = new EmployeeRepository();
        prepo = new EventProviderRepository();
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ServiceReservationEmployeeListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ServiceReservationEmployeeListFragment newInstance(Event param1, String param2) {
        ServiceReservationEmployeeListFragment fragment = new ServiceReservationEmployeeListFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getParcelable(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_service_reservation_employee_list, container, false);
        ListView lv = rootview.findViewById(R.id.servicereservationemployee);



        //--------------------------------------------------------------------------------------------
                repo.selectByEvent(new EmployeeCallback() {
                    @Override
                    public void onEmployeeRecieved(Employee[] employees) {
                        EmployeeAdapter2 ad = new EmployeeAdapter2(rootview.getContext(), employees);
                        lv.setAdapter(ad);
                    }

                    @Override
                    public void onError(Exception e) {

                    }
                }, mParam1.getName());

        //------------------------------------------------------------------------------------------------------------
        return rootview;
    }
}