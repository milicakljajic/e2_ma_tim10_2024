package rs.ftn.ma.mplanner.interfaces;

import rs.ftn.ma.mplanner.model.Category;

public interface CategoryCallback {
    void onCategoryReceived(Category[] categories);
    void onError(Exception e);
}
