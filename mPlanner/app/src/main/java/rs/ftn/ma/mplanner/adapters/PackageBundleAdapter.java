package rs.ftn.ma.mplanner.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.model.PackageBundle;

public class PackageBundleAdapter extends BaseAdapter {
    Context context;
    PackageBundle[] packages;
    LayoutInflater inflater;

    public PackageBundleAdapter(Context applicationContext, PackageBundle[] packages){
        this.context = applicationContext;
        this.packages = packages;
    }

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {

        view = inflater.inflate(R.layout.package_list_item_fragment, null);
        TextView name = (TextView) view.findViewById(R.id.package_item_name);
        TextView description = (TextView) view.findViewById(R.id.package_item_description);
        name.setText(packages[i].getName());
        description.setText(packages[i].getDescription());

        return view;
    }
}
