package rs.ftn.ma.mplanner.fragments;

import static androidx.constraintlayout.helper.widget.MotionEffect.TAG;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.adapters.EventTypeRemoveAdapter;
import rs.ftn.ma.mplanner.adapters.SubcategoryAdapter;
import rs.ftn.ma.mplanner.interfaces.SubcategoryCallback;
import rs.ftn.ma.mplanner.model.Employee;
import rs.ftn.ma.mplanner.model.EventType;
import rs.ftn.ma.mplanner.model.Service;
import rs.ftn.ma.mplanner.model.Subcategory;
import rs.ftn.ma.mplanner.model.enums.ConfirmationMethod;
import rs.ftn.ma.mplanner.repo.SubcategoryRepository;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EditServiceFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EditServiceFragment extends Fragment {

    private Spinner subcategorySpinner;
    private Service service;
    private Subcategory selectedSubcategory;
    private List<EventType> eventTypesProduct;
    private List<Employee> employeesService;
    private RecyclerView eventTypesAddRecyclerView;
    private RecyclerView employeesAddRecyclerView;
    private RecyclerView eventTypesRecyclerView;
    private RecyclerView employeesRecyclerView;

    private SubcategoryRepository subcategoryRepository;

    public EditServiceFragment() {
        // Required empty public constructor
    }

    public static EditServiceFragment newInstance(Service service) {
        EditServiceFragment fragment = new EditServiceFragment();
        Bundle args = new Bundle();
        args.putParcelable("service", service);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        eventTypesProduct = new ArrayList<>();
        employeesService = new ArrayList<>();
        selectedSubcategory = new Subcategory();
        subcategoryRepository = new SubcategoryRepository();
        super.onCreate(savedInstanceState);
    }

    private void loadSubcategories(Long categoryId) {
        subcategoryRepository.selectById(new SubcategoryCallback() {
            @Override
            public void onSubcategoryReceived(Subcategory[] subcategories) {
                ArrayList<Subcategory> subs = new ArrayList<Subcategory>(Arrays.asList(subcategories));
                SubcategoryAdapter adapter = new SubcategoryAdapter(requireContext(), subs);
                subcategorySpinner.setAdapter(adapter);
                subcategorySpinner.setVisibility(View.VISIBLE);
            }

            @Override
            public void onError(Exception e) {
                Log.e("CreateServiceFragment", "Error getting subcategories", e);
            }
        },categoryId.toString());
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_edit_service, container, false);
        subcategorySpinner = view.findViewById(R.id.spinner2);
        RadioGroup methodRadioGroup = view.findViewById(R.id.methodRadioGroup);
        Bundle args = getArguments();
        if (args != null) {
            service = args.getParcelable("service");
            if (service != null) {
                subcategoryRepository.selectById(new SubcategoryCallback() {
                    @Override
                    public void onSubcategoryReceived(Subcategory[] subcategories) {
                        ArrayList<Subcategory> subs = new ArrayList<Subcategory>(Arrays.asList(subcategories));
                        SubcategoryAdapter adapter = new SubcategoryAdapter(requireContext(), subs);
                        subcategorySpinner.setAdapter(adapter);
                        subcategorySpinner.setVisibility(View.VISIBLE);

                        int initialSelection = -1;
                        for (int i = 0; i < subs.size(); i++) {
                            if (subs.get(i).getId() == service.getSubcategory().getId()) {
                                initialSelection = i;
                                break;
                            }
                        }
                        // Postavi inicijalnu selekciju u Spinner
                        if (initialSelection != -1) {
                            subcategorySpinner.setSelection(initialSelection);
                        }

                        subcategorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                selectedSubcategory = (Subcategory) parent.getItemAtPosition(position);
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {
                                // Implementacija po potrebi
                            }
                        });
                    }

                    @Override
                    public void onError(Exception e) {
                        Log.e("CreateServiceFragment", "Error getting subcategories", e);
                    }
                },"1");

            }
        }

        EditText enterNameEditText = view.findViewById(R.id.enterNameEditText);
        EditText enterDescriptionEditText = view.findViewById(R.id.enterDescriptionEditText);
        EditText enterPriceEditText = view.findViewById(R.id.enterPriceEditText);
        EditText enterDiscountEditText = view.findViewById(R.id.enterDiscountEditText);
        EditText enterMinDurationEditText = view.findViewById(R.id.enterMinDurationEditText);
        EditText enterMaxDurationEditText = view.findViewById(R.id.enterMaxDurationEditText);
        EditText enterReservationDeadlineEditText = view.findViewById(R.id.enterReservationDeadlineEditText);
        EditText enterCancellationDeadlineEditText = view.findViewById(R.id.enterCancellationDeadlineEditText);
        EditText enterSpecificsEditText = view.findViewById(R.id.enterSpecificsEditText);

        RadioButton yesRadioButton = view.findViewById(R.id.yesRadioButton);
        RadioButton noRadioButton = view.findViewById(R.id.noRadioButton);
        RadioButton visibleYesRadioButton = view.findViewById(R.id.visibleYesRadioButton);
        RadioButton visibleNoRadioButton = view.findViewById(R.id.visibleNoRadioButton);


        if (args != null && args.containsKey("service")) {
            service = (Service) args.getParcelable("service");
            eventTypesProduct = service.getEventTypes();
            employeesService = service.getEmployees();
            eventTypesAddRecyclerView = view.findViewById(R.id.eventTypesAddRecyclerView);
            employeesAddRecyclerView = view.findViewById(R.id.employeesAddRecyclerView);
            loadEventTypesToAdd();
            if (service != null) {
                enterNameEditText.setText(service.getName());
                enterDescriptionEditText.setText(service.getDescription());
                enterPriceEditText.setText(String.valueOf(service.getPrice()));
                enterDiscountEditText.setText(String.valueOf(service.getDiscount()));
                enterMinDurationEditText.setText(String.valueOf(service.getMinDuration()));
                enterMaxDurationEditText.setText(String.valueOf(service.getMaxDuration()));
                enterReservationDeadlineEditText.setText(String.valueOf(service.getReservationDeadline()));
                enterCancellationDeadlineEditText.setText(String.valueOf(service.getCancellationDeadline()));

                if (service.getAvailable()) {
                    yesRadioButton.setChecked(true);
                } else {
                    noRadioButton.setChecked(true);
                }

                if (service.getVisible()) {
                    visibleYesRadioButton.setChecked(true);
                } else {
                    visibleNoRadioButton.setChecked(true);
                }

                RadioButton automaticallyRadioButton = view.findViewById(R.id.automaticallyRadioButton);
                RadioButton manuallyRadioButton = view.findViewById(R.id.manuallyRadioButton);

                if (service.getMethod() == ConfirmationMethod.AUTO) {
                    automaticallyRadioButton.setChecked(true);
                } else {
                    manuallyRadioButton.setChecked(true);
                }


            }
        }


        eventTypesRecyclerView = view.findViewById(R.id.eventTypesRecyclerView);
        EventTypeRemoveAdapter adapter = new EventTypeRemoveAdapter(eventTypesProduct, this);
        eventTypesRecyclerView.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        eventTypesRecyclerView.setLayoutManager(layoutManager);
        adapter.notifyDataSetChanged();


        Button editButton = view.findViewById(R.id.createButton);
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText enterNameEditText = view.findViewById(R.id.enterNameEditText);
                EditText enterDescriptionEditText = view.findViewById(R.id.enterDescriptionEditText);
                EditText enterPriceEditText = view.findViewById(R.id.enterPriceEditText);
                EditText enterDiscountEditText = view.findViewById(R.id.enterDiscountEditText);
                EditText enterMinDurationEditText = view.findViewById(R.id.enterMinDurationEditText);
                EditText enterMaxDurationEditText = view.findViewById(R.id.enterMaxDurationEditText);
                EditText enterReservationDeadlineEditText = view.findViewById(R.id.enterReservationDeadlineEditText);
                EditText enterCancellationDeadlineEditText = view.findViewById(R.id.enterCancellationDeadlineEditText);
                EditText enterSpecificsEditText = view.findViewById(R.id.enterSpecificsEditText);
                RadioButton availableRadioButton = view.findViewById(R.id.yesRadioButton);
                RadioButton visibleRadioButton = view.findViewById(R.id.visibleYesRadioButton);

                String name = enterNameEditText.getText().toString();
                String description = enterDescriptionEditText.getText().toString();
                double price = Double.parseDouble(enterPriceEditText.getText().toString());
                int discount = Integer.parseInt(enterDiscountEditText.getText().toString());
                double minDuration = Double.parseDouble(enterPriceEditText.getText().toString());
                double maxDuration = Double.parseDouble(enterPriceEditText.getText().toString());
                double reservationDeadline = Double.parseDouble(enterReservationDeadlineEditText.getText().toString());
                double cancellationDeadline = Double.parseDouble(enterCancellationDeadlineEditText.getText().toString());
                String specifics = enterSpecificsEditText.getText().toString();
                boolean available = availableRadioButton.isChecked();
                boolean visible = visibleRadioButton.isChecked();
                final ConfirmationMethod[] methodService = {ConfirmationMethod.AUTO};

                // Dodavanje OnCheckedChangeListener na radio grupu za polje 'method'
                methodRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        if (checkedId == R.id.manuallyRadioButton) {
                            methodService[0] = ConfirmationMethod.MANUAL;
                        }
                    }
                });


                Service updatedService = new Service(
                        service.getId(),
                        name,
                        description,
                        specifics,
                        price,
                        price - (discount / 100.0) * price,
                        discount,
                        service.getImages(),
                        minDuration,
                        maxDuration,
                        service.getCategory(),
                        selectedSubcategory,
                        service.getStatus(),
                        eventTypesProduct,
                        visible,
                        available,
                        service.getEmployees(),
                        reservationDeadline,
                        cancellationDeadline,
                        methodService[0],
                        service.getDeleted()
                );


                updateService(updatedService);
            }
        });

        return view;
    }

    private void updateService(Service service) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        CollectionReference servicesRef = db.collection("services");

        servicesRef.whereEqualTo("id", service.getId())
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                            // Pronađen je dokument sa zadatim id-em, ažuriramo ga
                            String documentId = documentSnapshot.getId();
                            DocumentReference serviceRef = db.collection("services").document(documentId);

                            serviceRef.set(service)
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {

                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Log.w(TAG, "Error updating document", e);
                                        }
                                    });
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error querying documents", e);
                    }
                });
    }

    private void loadEventTypesToAdd() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        CollectionReference eventTypesRef = db.collection("eventTypes");

        eventTypesRef.get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                List<EventType> allEventTypes = new ArrayList<>();
                for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                    EventType eventType = documentSnapshot.toObject(EventType.class);
                    allEventTypes.add(eventType);
                }

                List<EventType> eventTypesToAdd = new ArrayList<>();
                for (EventType eventType : allEventTypes) {
                    boolean found = false;
                    for (EventType pp : eventTypesProduct) {
                        if (pp.getId().equals(eventType.getId())) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        eventTypesToAdd.add(eventType);
                    }
                }

            }
        });
    }
}