package rs.ftn.ma.mplanner.model.enums;

public enum ConfirmationMethod {
    AUTO,
    MANUAL
}
