package rs.ftn.ma.mplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class Comment implements Parcelable {
    private String Id;
    private String UserId;
    private String Firstname;
    private String Lastname;
    private String Username;
    private String Rate;
    private Date LastModified;
    private String Message;

    private String Company;

    public Comment() {
    }

    public Comment(String id, String userId, String firstname, String lastname, String username, String rate, Date lastModified, String message, String company) {
        Id = id;
        UserId = userId;
        Firstname = firstname;
        Lastname = lastname;
        Username = username;
        Rate = rate;
        this.LastModified = lastModified;
        Message = message;
        Company = company;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getFirstname() {
        return Firstname;
    }

    public void setFirstname(String firstname) {
        Firstname = firstname;
    }

    public String getLastname() {
        return Lastname;
    }

    public void setLastname(String lastname) {
        Lastname = lastname;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getRate() {
        return Rate;
    }

    public void setRate(String rate) {
        Rate = rate;
    }

    public Date getLastModified() {
        return LastModified;
    }

    public void setLastModified(Date lastModified) {
        LastModified = lastModified;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getCompany() {
        return Company;
    }

    public void setCompany(String company) {
        Company = company;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.Id);
        dest.writeString(this.UserId);
        dest.writeString(this.Firstname);
        dest.writeString(this.Lastname);
        dest.writeString(this.Username);
        dest.writeString(this.Rate);
        dest.writeSerializable(this.LastModified);
        dest.writeString(this.Message);
    }

    public void readFromParcel(Parcel source) {
        this.Id = source.readString();
        this.UserId = source.readString();
        this.Firstname = source.readString();
        this.Lastname = source.readString();
        this.Username = source.readString();
        this.Rate = source.readString();
        this.LastModified = (Date) source.readSerializable();
        this.Message = source.readString();
    }

    protected Comment(Parcel in) {
        this.Id = in.readString();
        this.UserId = in.readString();
        this.Firstname = in.readString();
        this.Lastname = in.readString();
        this.Username = in.readString();
        this.Rate = in.readString();
        this.LastModified = (Date) in.readSerializable();
        this.Message = in.readString();
    }

    public static final Parcelable.Creator<Comment> CREATOR = new Parcelable.Creator<Comment>() {
        @Override
        public Comment createFromParcel(Parcel source) {
            return new Comment(source);
        }

        @Override
        public Comment[] newArray(int size) {
            return new Comment[size];
        }
    };
}