package rs.ftn.ma.mplanner.interfaces;

public interface EmployeeUpdateCallback {
    void onEmployeeUpdated();
    void onUpdateError(Exception e);
}
