package rs.ftn.ma.mplanner.interfaces;

public interface SubcategoryUpdateCallback {
    void onSubcategoryUpdated();
    void onUpdateError(Exception e);
}
