package rs.ftn.ma.mplanner.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.model.Employee;
import rs.ftn.ma.mplanner.repo.EmployeeRepository;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EmployeeScheduleFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EmployeeScheduleFragment extends Fragment {
    private static final String ARG_PARAM1 = "Employee";

    // TODO: Rename and change types of parameters

    private EmployeeRepository repo;
    private Employee employee;

    public EmployeeScheduleFragment() {
        repo = new EmployeeRepository();
    }

    // TODO: Rename and change types and number of parameters
    public static EmployeeScheduleFragment newInstance(Employee employee) {
        EmployeeScheduleFragment fragment = new EmployeeScheduleFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, employee);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            employee = getArguments().getParcelable(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_employee_schedule, container, false);



            TextView scheduleDate = view.findViewById(R.id.employee_schedule_date);
            String date = employee.getSchedule().getStartDate() + " - " + employee.getSchedule().getEndDate();
            scheduleDate.setText(date);

            TextView scheduleTimeMonday = view.findViewById(R.id.employee_schedule_time_monday);
            String timeMonday = employee.getSchedule().getMondayFrom() + " - " + employee.getSchedule().getMondayTo();
            scheduleTimeMonday.setText(timeMonday);

            TextView scheduleTimeTuesday = view.findViewById(R.id.employee_schedule_time_tuesday);
            String timeTuesday = employee.getSchedule().getTuesdayFrom() + " - " + employee.getSchedule().getTuesdayTo();
            scheduleTimeTuesday.setText(timeTuesday);

            TextView scheduleTimeWednesday = view.findViewById(R.id.employee_schedule_time_wednesday);
            String timeWednesday = employee.getSchedule().getWednesdayFrom() + " - " + employee.getSchedule().getWednesdayTo();
            scheduleTimeWednesday.setText(timeWednesday);

            TextView scheduleTimeThursday = view.findViewById(R.id.employee_schedule_time_thursday);
            String timeThursday = employee.getSchedule().getThursdayFrom() + " - " + employee.getSchedule().getThursdayTo();
            scheduleTimeThursday.setText(timeThursday);

            TextView scheduleTimeFriday = view.findViewById(R.id.employee_schedule_time_friday);
            String timeFriday = employee.getSchedule().getFridayFrom() + " - " + employee.getSchedule().getFridayTo();
            scheduleTimeFriday.setText(timeFriday);

            TextView scheduleTimeSaturday = view.findViewById(R.id.employee_schedule_time_saturday);
            String timeSaturday = employee.getSchedule().getSaturdayFrom() + " - " + employee.getSchedule().getSaturdayTo();
            scheduleTimeSaturday.setText(timeSaturday);

            TextView scheduleTimeSunday = view.findViewById(R.id.employee_schedule_time_sunday);
            String timeSunday = employee.getSchedule().getSundayFrom() + " - " + employee.getSchedule().getSundayTo();
            scheduleTimeSunday.setText(timeSunday);


        MaterialButton buttonUpdate = (MaterialButton) view.findViewById(R.id.employee_schedule_update);

        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("Employee", employee);
                bundle.putString("mode", "update");
                Navigation.findNavController(view).navigate(R.id.action_employeeScheduleFragment_to_registrationScheduleFragment22, bundle);;
            }
        });

        MaterialButton buttonClose = (MaterialButton) view.findViewById(R.id.employee_schedule_close);

        buttonClose.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("Employee", employee);
                bundle.putString("mode", "update");
                Navigation.findNavController(view).navigate(R.id.action_employeeScheduleFragment_to_profileFragment2, bundle);
            }
        });

        return view;
    }
}