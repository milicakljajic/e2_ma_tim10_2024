package rs.ftn.ma.mplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Guest implements Parcelable {
    private String name;
    private String lastName;
    private int ageCategory;
    private boolean isInvited;
    private boolean hasAccepted;
    private String specialRequest;

    public Guest() {
    }

    public Guest(String name, String lastName, int ageCategory, boolean isInvited, boolean hasAccepted, String specialRequest) {
        this.name = name;
        this.lastName = lastName;
        this.ageCategory = ageCategory;
        this.isInvited = isInvited;
        this.hasAccepted = hasAccepted;
        this.specialRequest = specialRequest;
    }

    // Getters and Setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAgeCategory() {
        return ageCategory;
    }

    public void setAgeCategory(int ageCategory) {
        this.ageCategory = ageCategory;
    }

    public boolean isInvited() {
        return isInvited;
    }

    public void setInvited(boolean invited) {
        isInvited = invited;
    }

    public boolean hasAccepted() {
        return hasAccepted;
    }

    public void setAccepted(boolean accepted) {
        hasAccepted = accepted;
    }

    public String getSpecialRequest() {
        return specialRequest;
    }

    public void setSpecialRequest(String specialRequest) {
        this.specialRequest = specialRequest;
    }

    protected Guest(Parcel in) {
        name = in.readString();
        lastName = in.readString();
        ageCategory = in.readInt();
        isInvited = in.readByte() != 0;
        hasAccepted = in.readByte() != 0;
        specialRequest = in.readString();
    }

    public static final Creator<Guest> CREATOR = new Creator<Guest>() {
        @Override
        public Guest createFromParcel(Parcel in) {
            return new Guest(in);
        }

        @Override
        public Guest[] newArray(int size) {
            return new Guest[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(lastName);
        dest.writeInt(ageCategory);
        dest.writeByte((byte) (isInvited ? 1 : 0));
        dest.writeByte((byte) (hasAccepted ? 1 : 0));
        dest.writeString(specialRequest);
    }

    // Method to get all details in a single string
    public String getDetails() {
        return "Age Group: " + ageCategory + "\nInvited: " + (isInvited ? "Yes" : "No") +
                "\nAccepted: " + (hasAccepted ? "Yes" : "No") + "\nSpecial Requirements: " + specialRequest;
    }
}
