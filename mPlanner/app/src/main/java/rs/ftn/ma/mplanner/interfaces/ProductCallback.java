package rs.ftn.ma.mplanner.interfaces;

import rs.ftn.ma.mplanner.model.Product;

public interface ProductCallback {
    void onProductReceived(Product[] products);
    void onError(Exception e);
}
