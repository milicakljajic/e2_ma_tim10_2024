package rs.ftn.ma.mplanner.interfaces;

public interface OnDeleteButtonClickListener {
    void onDeleteClicked(int position);
}
