package rs.ftn.ma.mplanner.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.MainActivity;
import rs.ftn.ma.mplanner.interfaces.AdminCallback;
import rs.ftn.ma.mplanner.interfaces.EmployeeCallback;
import rs.ftn.ma.mplanner.interfaces.EventOrganizerCallback;
import rs.ftn.ma.mplanner.interfaces.EventProviderCallback;
import rs.ftn.ma.mplanner.model.Admin;
import rs.ftn.ma.mplanner.model.Employee;
import rs.ftn.ma.mplanner.model.EventOrganizer;
import rs.ftn.ma.mplanner.model.EventProvider;
import rs.ftn.ma.mplanner.repo.AdminRepository;
import rs.ftn.ma.mplanner.repo.EmployeeRepository;
import rs.ftn.ma.mplanner.repo.EventOrganizerRepo;
import rs.ftn.ma.mplanner.repo.EventProviderRepository;

import android.content.Intent;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link LoginFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LoginFragment extends Fragment {

    private FirebaseAuth mAuth;
    private EventOrganizerRepo eoRepo;
    private EmployeeRepository eRepo;
    private AdminRepository aRepo;
    private EventProviderRepository epRepo;


    public LoginFragment() {
        // Required empty public constructor
        mAuth = FirebaseAuth.getInstance();
        eoRepo = new EventOrganizerRepo();
        eRepo = new EmployeeRepository();
        aRepo = new AdminRepository();
        epRepo = new EventProviderRepository();
    }

    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_login, container, false);

        EditText passwordEditText = view.findViewById(R.id.login_password_text);
        passwordEditText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (passwordEditText.getRight() - passwordEditText.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // Ako je kliknuto na drawableEnd
                        if (passwordEditText.getInputType() == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD) {
                            // Sakrij lozinku
                            passwordEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        } else {
                            // Prikazi lozinku
                            passwordEditText.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                        }
                        return true;
                    }
                }
                return false;
            }
        });


        //Button button = (Button) view.findViewById(R.id.login_registration);
        TextView registrationText = (TextView) view.findViewById(R.id.login_registration);
        registrationText.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Navigation.findNavController(view).navigate(R.id.changeToRegistrationRole1);
            }
        });

        MaterialButton button = view.findViewById(R.id.login_button);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                EditText email = view.findViewById(R.id.login_email_text);
                String emailText = String.valueOf(email.getText());
                EditText password = view.findViewById(R.id.login_password_text);
                String passwordText = String.valueOf(password.getText());
                mAuth.signInWithEmailAndPassword(emailText, passwordText)
                        .addOnSuccessListener(requireActivity(), new OnSuccessListener<AuthResult>() {
                            @Override
                            public void onSuccess(AuthResult authResult) {
                                FirebaseUser user = mAuth.getCurrentUser();
                                //ZA SVAKI REPO OD KORISNIKA, PROVERI DA LI JE TU. MORA BITI.
                                assert user != null;
                                user.getUid();

                                //CHECK EVENTORGANIZER
                                eoRepo.selectByFirebaseId(new EventOrganizerCallback() {
                                    @Override
                                    public void onEventOrganizerReceived(EventOrganizer[] types) {
                                        try {
                                            EventOrganizer found = types[0];
                                            Intent myIntent = new Intent(requireContext(), MainActivity.class);
                                            myIntent.putExtra("EventOrganizer", found);
                                            myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(myIntent);
                                        }
                                        catch (ArrayIndexOutOfBoundsException a) {
                                            Log.d("EO", "Not in eo");
                                        }
                                    }

                                    @Override
                                    public void onError(Exception e) {

                                    }
                                }, user.getUid());

                                //CHECK EMPLOYEE
                                eRepo.selectByFirebaseId(new EmployeeCallback() {
                                    @Override
                                    public void onEmployeeRecieved(Employee[] employees) {
                                        try {
                                            Employee found = employees[0];
                                            if(found.getIsEnabled()) {
                                                Intent myIntent = new Intent(requireContext(), MainActivity.class);
                                                myIntent.putExtra("Employee", found);
                                                myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                startActivity(myIntent);
                                            }
                                            else
                                            {
                                                Toast.makeText(v.getContext(), "user disabled", Toast.LENGTH_SHORT).show();

                                            }
                                        }
                                        catch (ArrayIndexOutOfBoundsException a)
                                        {
                                            Log.d("e", "not in employee");
                                        }
                                    }

                                    @Override
                                    public void onError(Exception e) {

                                    }
                                }, user.getUid());

                                //CHECK ADMIN
                                aRepo.selectByFirebaseId(new AdminCallback() {
                                    @Override
                                    public void onAdminReceived(Admin[] admins) {
                                        try {
                                            Admin found = admins[0];
                                            Intent myIntent = new Intent(requireContext(), MainActivity.class);
                                            myIntent.putExtra("Admin", found);
                                            myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(myIntent);
                                        }
                                        catch (ArrayIndexOutOfBoundsException a)
                                        {
                                            Log.d("e", "not in admin");
                                        }
                                    }

                                    @Override
                                    public void onError(Exception e) {

                                    }
                                }, user.getUid());

                                //CHECK EVENT PROVIDER
                                epRepo.selectByFirebaseId(new EventProviderCallback() {
                                    @Override
                                    public void onEventProviderReceived(EventProvider[] providers) {
                                        try {


                                            EventProvider found = providers[0];
                                            if(found.getApproved()) {
                                                Toast.makeText(getActivity(), "Successfully login", Toast.LENGTH_SHORT).show();
                                                Intent myIntent = new Intent(requireContext(), MainActivity.class);
                                                myIntent.putExtra("EventProvider", found);
                                                myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                startActivity(myIntent);
                                            }
                                            else {
                                                Toast.makeText(getActivity(), "Not approved by admin!", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                        catch (ArrayIndexOutOfBoundsException a)
                                        {
                                            Log.d("e", "not in event provider");
                                        }
                                    }

                                    @Override
                                    public void onError(Exception e) {

                                    }
                                }, user.getUid());
                            }
                        })
                        .addOnFailureListener(requireActivity(), new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {

                            }
                        });
            }
        });

        return view;
    }
}