package rs.ftn.ma.mplanner.fragments;

import static android.content.Context.NOTIFICATION_SERVICE;

import android.app.NotificationManager;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.interfaces.EmployeeCallback;
import rs.ftn.ma.mplanner.interfaces.NotificationCallback;
import rs.ftn.ma.mplanner.interfaces.NotificationUpdateCallback;
import rs.ftn.ma.mplanner.model.Employee;
import rs.ftn.ma.mplanner.model.Notification;
import rs.ftn.ma.mplanner.repo.EmployeeRepository;
import rs.ftn.ma.mplanner.repo.NotificationRepository;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PupzHomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PupzHomeFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private FirebaseAuth mAuth;

    private Employee employee;
    private EmployeeRepository repo;

    private NotificationRepository nrepo;

    public PupzHomeFragment() {
        // Required empty public constructor
        mAuth = FirebaseAuth.getInstance();
        employee = new Employee();
        repo = new EmployeeRepository();
        nrepo = new NotificationRepository();
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PupzHomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PupzHomeFragment newInstance(String param1, String param2) {
        PupzHomeFragment fragment = new PupzHomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pupz_home, container, false);

        FirebaseUser cUser = mAuth.getCurrentUser();


        nrepo.select(new NotificationCallback() {
            @Override
            public void onNotificationReceived(Notification[] notifications) {
                for(Notification n : notifications)
                {
                    if(n.getRecieverId().equals(cUser.getUid()) && !n.getSeen())
                    {
                        NotificationCompat.Builder builder = new NotificationCompat.Builder(requireContext(), "unique-channel-id-2")
                                .setSmallIcon(R.drawable.logo_light)
                                .setContentTitle(n.getTitle())
                                .setContentText(n.getMessage())
                                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
                        NotificationManager notificationManager = (NotificationManager) requireContext().getSystemService(NOTIFICATION_SERVICE);
                        notificationManager.notify(1, builder.build());
                        n.setSeen(true);
                        nrepo.update(n.getId(), n, new NotificationUpdateCallback() {
                            @Override
                            public void onNotificationUpdated() {

                            }

                            @Override
                            public void onUpdateError(Exception e) {

                            }
                        });

                    }
                }
            }

            @Override
            public void onError(Exception e) {

            }
        });


        assert cUser != null;
        repo.selectByFirebaseId(new EmployeeCallback() {
            @Override
            public void onEmployeeRecieved(Employee[] employees) {
                employee = employees[0];
            }

            @Override
            public void onError(Exception e) {

            }
        }, cUser.getUid());


        CardView button1 = (CardView) view.findViewById(R.id.all_products);

        button1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("Employee", employee);
                //Navigation.findNavController(view).navigate(R.id.changeToEventList1, bundle);
            }
        });

        CardView button2 = (CardView) view.findViewById(R.id.all_packages);

        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("Employee", employee);
                //Navigation.findNavController(view).navigate(R.id.changeToScheduleEvent1, bundle);
            }
        });


        CardView button3 = (CardView) view.findViewById(R.id.pupzNotificationButton);

        button3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
               // Navigation.findNavController(view).navigate(R.id.changeToPupzNav1);
            }
        });

        return view;
    }
}