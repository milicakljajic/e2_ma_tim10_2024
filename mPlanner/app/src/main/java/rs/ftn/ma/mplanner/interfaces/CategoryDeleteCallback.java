package rs.ftn.ma.mplanner.interfaces;

public interface CategoryDeleteCallback {
        void onCategoryDeleted();
        void onDeleteError(Exception e);
}
