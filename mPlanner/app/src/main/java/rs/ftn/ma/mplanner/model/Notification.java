package rs.ftn.ma.mplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Notification implements Parcelable {
    private String id;
    private String recieverId;
    private String title;
    private String message;
    private boolean seen;

    public Notification() {
    }

    public Notification(String id, String recieverId, String title, String message, boolean seen) {
        this.id = id;
        this.recieverId = recieverId;
        this.title = title;
        this.message = message;
        this.seen = seen;
    }

    public Notification(String recieverId, String title, String message, boolean seen) {
        this.recieverId = recieverId;
        this.title = title;
        this.message = message;
        this.seen = seen;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRecieverId() {
        return recieverId;
    }

    public void setRecieverId(String recieverId) {
        this.recieverId = recieverId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getSeen() {
        return seen;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
}


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.recieverId);
        dest.writeString(this.title);
        dest.writeString(this.message);
        dest.writeByte(this.seen ? (byte) 1 : (byte) 0);
    }

    public void readFromParcel(Parcel source) {
        this.id = source.readString();
        this.recieverId = source.readString();
        this.title = source.readString();
        this.message = source.readString();
        this.seen = source.readByte() != 0;
    }

    protected Notification(Parcel in) {
        this.id = in.readString();
        this.recieverId = in.readString();
        this.title = in.readString();
        this.message = in.readString();
        this.seen = in.readByte() != 0;
    }

    public static final Creator<Notification> CREATOR = new Creator<Notification>() {
        @Override
        public Notification createFromParcel(Parcel source) {
            return new Notification(source);
        }

        @Override
        public Notification[] newArray(int size) {
            return new Notification[size];
        }
    };
}
