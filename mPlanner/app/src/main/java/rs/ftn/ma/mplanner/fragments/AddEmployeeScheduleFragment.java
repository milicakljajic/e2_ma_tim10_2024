package rs.ftn.ma.mplanner.fragments;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Calendar;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.interfaces.EmployeeUpdateCallback;

import rs.ftn.ma.mplanner.interfaces.EventProviderCallback;
import rs.ftn.ma.mplanner.model.Employee;
import rs.ftn.ma.mplanner.model.EventProvider;
import rs.ftn.ma.mplanner.repo.EmployeeRepository;

import rs.ftn.ma.mplanner.repo.EventProviderRepository;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AddEmployeeScheduleFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddEmployeeScheduleFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "Employee";
    private static final String ARG_PARAM2 = "mode";

    // TODO: Rename and change types of parameters
    private TextView text;

    private Employee newEmployee;
    private String mode;
    private EventProviderRepository prepo;
    private EmployeeRepository repo;
    private EventProvider provider;
    private FirebaseAuth mAuth;

    public AddEmployeeScheduleFragment() {
        // Required empty public constructor
        prepo = new EventProviderRepository();
        repo = new EmployeeRepository();
        mAuth = FirebaseAuth.getInstance();
    }

    // TODO: Rename and change types and number of parameters
    public static AddEmployeeScheduleFragment newInstance(Employee newEmployee, String mode) {
        AddEmployeeScheduleFragment fragment = new AddEmployeeScheduleFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, newEmployee);
        args.putString(ARG_PARAM2, mode);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if(getArguments().getParcelable(ARG_PARAM1) != null)
                newEmployee = getArguments().getParcelable(ARG_PARAM1);
            if(getArguments().getString(ARG_PARAM2) != null)
                mode = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_employee_schedule, container, false);

        //Button button = (Button) view.findViewById(R.id.login_registration);
        Button button = (Button) view.findViewById(R.id.ae_schedule_next);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(newEmployee != null){
                    if(mode == null) {
                        //Log.d("Nesto", "Nesto");
                        Bundle bundle = new Bundle();
                        bundle.putParcelable("Employee", newEmployee);
                        Navigation.findNavController(view).navigate(R.id.changeToAddEmployeePhoto1, bundle);
                    }
                    else if(mode.equals("update"))
                    {
                        repo.update(newEmployee.getId(), newEmployee, new EmployeeUpdateCallback() {
                            @Override
                            public void onEmployeeUpdated() {
                                Toast.makeText(getActivity(), "Successfully updated schedule", Toast.LENGTH_SHORT).show();
                                View view = inflater.inflate(R.layout.fragment_add_employee_schedule, container, false);
                                //vrati se na profil
                                Bundle bundle = new Bundle();
                                bundle.putParcelable("Employee", newEmployee);
                                //Navigation.findNavController(view).navigate(R.id.proradi, bundle);
                            }

                            @Override
                            public void onUpdateError(Exception e) {

                            }
                        });
                    }
                }
            }
        });

        //SKIP--------------------------------------------------------------------
        button = (Button) view.findViewById(R.id.ae_schedule_skip);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                FirebaseUser cUser = mAuth.getCurrentUser();

                prepo.selectByFirebaseId(new EventProviderCallback() {
                    @Override
                    public void onEventProviderReceived(EventProvider[] providers) {
                        if(mode == null) {
                            Bundle bundle = new Bundle();
                            newEmployee.setSchedule(providers[0].getCompany().getSchedule());
                            bundle.putParcelable("Employee", newEmployee);
                            Navigation.findNavController(view).navigate(R.id.changeToAddEmployeePhoto1, bundle);
                        }
                        else if(mode.equals("update")) {

                        }
                    }

                    @Override
                    public void onError(Exception e) {

                    }
                }, cUser.getUid());

            }
        });


        Button buttonMondayFrom = view.findViewById(R.id.ae_schedule_time_from_monday);
        buttonMondayFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mHour = c.get(Calendar.HOUR_OF_DAY);
                int mMinute = c.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(requireContext(),new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        //text.setText(hourOfDay);
                        String h = String.valueOf(hourOfDay) + ":" + String.valueOf(minute);
                        newEmployee.getSchedule().setMondayFrom(h);
                    }
                }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });

        Button buttonMondayTo = view.findViewById(R.id.ae_schedule_time_to_monday);
        buttonMondayTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mHour = c.get(Calendar.HOUR_OF_DAY);
                int mMinute = c.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(requireContext(),new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        //text.setText(hourOfDay);
                        String h = String.valueOf(hourOfDay) + ":" + String.valueOf(minute);
                        newEmployee.getSchedule().setMondayTo(h);
                    }
                }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });

        Button buttonTuesdayFrom = view.findViewById(R.id.ae_schedule_time_from_tuesday);
        buttonTuesdayFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mHour = c.get(Calendar.HOUR_OF_DAY);
                int mMinute = c.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(requireContext(),new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        //text.setText(hourOfDay);
                        String h = String.valueOf(hourOfDay) + ":" + String.valueOf(minute);
                        newEmployee.getSchedule().setTuesdayFrom(h);
                    }
                }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });

        Button buttonTuesdayTo = view.findViewById(R.id.ae_schedule_time_to_tuesday);
        buttonTuesdayTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mHour = c.get(Calendar.HOUR_OF_DAY);
                int mMinute = c.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(requireContext(),new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        //text.setText(hourOfDay);
                        String h = String.valueOf(hourOfDay) + ":" + String.valueOf(minute);
                        newEmployee.getSchedule().setTuesdayTo(h);
                    }
                }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });

        Button buttonWednesdayFrom = view.findViewById(R.id.ae_schedule_time_from_wednesday);
        buttonWednesdayFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mHour = c.get(Calendar.HOUR_OF_DAY);
                int mMinute = c.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(requireContext(),new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        //text.setText(hourOfDay);
                        String h = String.valueOf(hourOfDay) + ":" + String.valueOf(minute);
                        newEmployee.getSchedule().setWednesdayFrom(h);
                    }
                }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });

        Button buttonWednesdayTo = view.findViewById(R.id.ae_schedule_time_to_wednesday);
        buttonWednesdayTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mHour = c.get(Calendar.HOUR_OF_DAY);
                int mMinute = c.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(requireContext(),new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        //text.setText(hourOfDay);
                        String h = String.valueOf(hourOfDay) + ":" + String.valueOf(minute);
                        newEmployee.getSchedule().setWednesdayTo(h);
                    }
                }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });

        Button buttonThursdayFrom = view.findViewById(R.id.ae_schedule_time_from_thursday);
        buttonThursdayFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mHour = c.get(Calendar.HOUR_OF_DAY);
                int mMinute = c.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(requireContext(),new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        //text.setText(hourOfDay);
                        String h = String.valueOf(hourOfDay) + ":" + String.valueOf(minute);
                        newEmployee.getSchedule().setThursdayFrom(h);
                    }
                }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });

        Button buttonThursdayTo = view.findViewById(R.id.ae_schedule_time_to_thursday);
        buttonThursdayTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mHour = c.get(Calendar.HOUR_OF_DAY);
                int mMinute = c.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(requireContext(),new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        //text.setText(hourOfDay);
                        String h = String.valueOf(hourOfDay) + ":" + String.valueOf(minute);
                        newEmployee.getSchedule().setThursdayTo(h);
                    }
                }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });

        Button buttonFridayFrom = view.findViewById(R.id.ae_schedule_time_from_friday);
        buttonFridayFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mHour = c.get(Calendar.HOUR_OF_DAY);
                int mMinute = c.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(requireContext(),new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        //text.setText(hourOfDay);
                        String h = String.valueOf(hourOfDay) + ":" + String.valueOf(minute);
                        newEmployee.getSchedule().setFridayFrom(h);
                    }
                }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });

        Button buttonFridayTo = view.findViewById(R.id.ae_schedule_time_to_friday);
        buttonFridayTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mHour = c.get(Calendar.HOUR_OF_DAY);
                int mMinute = c.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(requireContext(),new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        //text.setText(hourOfDay);
                        String h = String.valueOf(hourOfDay) + ":" + String.valueOf(minute);
                        newEmployee.getSchedule().setFridayTo(h);
                    }
                }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });

        Button buttonSaturdayFrom = view.findViewById(R.id.ae_schedule_time_from_saturday);
        buttonSaturdayFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mHour = c.get(Calendar.HOUR_OF_DAY);
                int mMinute = c.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(requireContext(),new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        //text.setText(hourOfDay);
                        String h = String.valueOf(hourOfDay) + ":" + String.valueOf(minute);
                        newEmployee.getSchedule().setSaturdayFrom(h);
                    }
                }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });

        Button buttonSaturdayTo = view.findViewById(R.id.ae_schedule_time_to_saturday);
        buttonSaturdayTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mHour = c.get(Calendar.HOUR_OF_DAY);
                int mMinute = c.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(requireContext(),new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        //text.setText(hourOfDay);
                        String h = String.valueOf(hourOfDay) + ":" + String.valueOf(minute);
                        newEmployee.getSchedule().setSaturdayTo(h);
                    }
                }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });

        Button buttonSundayFrom = view.findViewById(R.id.ae_schedule_time_from_sunday);
        buttonSundayFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mHour = c.get(Calendar.HOUR_OF_DAY);
                int mMinute = c.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(requireContext(),new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        //text.setText(hourOfDay);
                        String h = String.valueOf(hourOfDay) + ":" + String.valueOf(minute);
                        newEmployee.getSchedule().setSundayFrom(h);
                    }
                }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });

        Button buttonSundayTo = view.findViewById(R.id.ae_schedule_time_to_sunday);
        buttonSundayTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mHour = c.get(Calendar.HOUR_OF_DAY);
                int mMinute = c.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(requireContext(),new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        //text.setText(hourOfDay);
                        String h = String.valueOf(hourOfDay) + ":" + String.valueOf(minute);
                        newEmployee.getSchedule().setSundayTo(h);
                    }
                }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });

        Button buttonStartDate = view.findViewById(R.id.ae_schedule_start_date);
        buttonStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(requireContext(),new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        month += 1;
                        String s = String.valueOf(dayOfMonth) + "." + String.valueOf(month) + "." + String.valueOf(year);
                        newEmployee.getSchedule().setStartDate(s);
                        //text.setText(String.valueOf(year));
                    }
                }, year, month, 15);
                datePickerDialog.show();
            }
        });

        Button buttonEndDate = view.findViewById(R.id.ae_schedule_end_date);
        buttonEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(requireContext(),new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        month += 1;
                        String s = String.valueOf(dayOfMonth) + "." + String.valueOf(month) + "." + String.valueOf(year);
                        newEmployee.getSchedule().setEndDate(s);
                        //text.setText(String.valueOf(year));
                    }
                }, year, month, 15);
                datePickerDialog.show();
            }
        });

        return view;
    }
}