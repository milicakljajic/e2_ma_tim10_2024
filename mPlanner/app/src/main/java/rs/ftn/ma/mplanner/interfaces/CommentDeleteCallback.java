package rs.ftn.ma.mplanner.interfaces;

public interface CommentDeleteCallback {
    void onCommentDeleted();
    void onDeleteError(Exception e);
}
