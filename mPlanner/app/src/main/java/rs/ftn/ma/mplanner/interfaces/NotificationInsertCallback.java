package rs.ftn.ma.mplanner.interfaces;

public interface NotificationInsertCallback {
    void onNotificationInserted(String notificationId);
    void onInsertError(Exception e);
}
