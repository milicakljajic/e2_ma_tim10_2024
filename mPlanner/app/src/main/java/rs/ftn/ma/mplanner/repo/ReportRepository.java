package rs.ftn.ma.mplanner.repo;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import rs.ftn.ma.mplanner.interfaces.ReportCallback;
import rs.ftn.ma.mplanner.interfaces.ReportDeleteCallback;
import rs.ftn.ma.mplanner.interfaces.ReportInsertCallback;
import rs.ftn.ma.mplanner.interfaces.ReportUpdateCallback;
import rs.ftn.ma.mplanner.model.Report;

public class ReportRepository {
    public void insert(Report report, ReportInsertCallback callback) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("reports")
                .add(report)
                .addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentReference> task) {
                        if (task.isSuccessful()) {
                            DocumentReference documentReference = task.getResult();
                            String reportId = documentReference.getId();
                            Log.d("REZ_DB", "DocumentSnapshot added with ID: " + reportId);
                            callback.onReportInserted(reportId);
                        } else {
                            Log.w("REZ_DB", "Error adding document", task.getException());
                            callback.onInsertError(task.getException());
                        }
                    }
                });
    }

    public void select(ReportCallback callback) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("reports")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            List<Report> reports = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Report report = document.toObject(Report.class);
                                report.setId(document.getId());
                                reports.add(report);
                                Log.d("REZ_DB", document.getId() + " => " + document.getData());
                            }
                            callback.onReportRecieved(reports.toArray(new Report[0]));
                        } else {
                            Log.w("REZ_DB", "Error getting documents.", task.getException());
                            callback.onError(task.getException());
                        }
                    }
                });
    }

    public void selectById(ReportCallback callback, String id) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("reports").document(id);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot documentSnapshot = task.getResult();
                    if (documentSnapshot.exists()) {
                        Report[] report = new Report[]{documentSnapshot.toObject(Report.class)};
                        Log.d("REZ_DB", documentSnapshot.getId() + " => " + documentSnapshot.getData());
                        callback.onReportRecieved(report);
                    } else {
                        Log.d("REZ_DB", "No such document");
                        callback.onError(new Exception("No such document"));
                    }
                } else {
                    Log.w("REZ_DB", "Error getting document", task.getException());
                    callback.onError(task.getException());
                }
            }
        });
    }

    public void delete(String id, ReportDeleteCallback callback) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("reports").document(id);

        docRef.delete()
                .addOnSuccessListener(aVoid -> {
                    Log.d("REZ_DB", "Report successfully deleted");
                    callback.onReportDeleted();
                })
                .addOnFailureListener(e -> {
                    Log.w("REZ_DB", "Error deleting report.", e);
                    callback.onDeleteError(e);
                });
    }

    public void update(String id, Report report, ReportUpdateCallback callback) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("reports").document(id);
        docRef.update("status", report.getStatus())
                .addOnSuccessListener(aVoid -> {
                    Log.d("REZ_DB", "Status successfully changed");
                    callback.onReportUpdated();
                })
                .addOnFailureListener(e -> {
                    Log.w("REZ_DB", "Error getting documents.", e);
                    callback.onUpdateError(e);
                });

        docRef.update("lastModified", report.getLastModified())
                .addOnSuccessListener(aVoid -> {
                    Log.d("REZ_DB", "Date successfully changed");
                    callback.onReportUpdated();
                })
                .addOnFailureListener(e -> {
                    Log.w("REZ_DB", "Error getting documents.", e);
                    callback.onUpdateError(e);
                });
    }
}
