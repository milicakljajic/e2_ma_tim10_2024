package rs.ftn.ma.mplanner.interfaces;

public interface AdminInsertCallback {
    void onAdminInserted(String adminId);
    void onInsertError(Exception e);
}
