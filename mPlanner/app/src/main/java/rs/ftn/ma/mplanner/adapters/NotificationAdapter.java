package rs.ftn.ma.mplanner.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.model.Notification;

public class NotificationAdapter extends BaseAdapter {
    Context context;
    ArrayList<Notification> notifications;
    LayoutInflater inflater;

    public NotificationAdapter(Context applicationContext, Notification[] notifications) {
        this.context = applicationContext;
        this.notifications = new ArrayList<>(Arrays.asList(notifications));
        inflater = LayoutInflater.from(applicationContext);
    }

    public NotificationAdapter(Context applicationContext, ArrayList<Notification> notifications) {
        this.context = applicationContext;
        this.notifications = notifications;
        inflater = LayoutInflater.from(applicationContext);
    }

    @Override
    public int getCount() {
        return notifications.size();
    }

    @Override
    public Object getItem(int i) {
        return notifications.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_notification_list_item, null);
        }

        Notification notification = notifications.get(i);

        TextView title = view.findViewById(R.id.notification_title);
        TextView message = view.findViewById(R.id.notification_message);
        TextView status = view.findViewById(R.id.notification_status);

        title.setText(notification.getTitle());
        message.setText(notification.getMessage());
        status.setText(notification.getSeen()? "seen":"unseen");
        return view;
    }
}
