package rs.ftn.ma.mplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

public class EventProvider implements Parcelable {
    private String id;
    private String firebaseUserId;
    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private String address;
    private String phoneNumber;
    private Boolean isEnabled;
    //TODO prebaci u blob kasnije
    private String photoUrl;
    private Company company;
    private ArrayList<Service> providedServices;
    private ArrayList<Product> providedProducts;
    private ArrayList<EventType> providedEventTypes;

    private Boolean isApproved;

    private Date lastModified;

    public EventProvider() {
        company = new Company();
        providedServices = new ArrayList<>();
        providedProducts = new ArrayList<>();
        providedEventTypes = new ArrayList<>();
        isApproved = false;
        lastModified = new Date();
    }

    public EventProvider(String id, String firebaseUserId, String email, String password, String firstName, String lastName, String address, String phoneNumber, Boolean isEnabled, String photoUrl, Company company, ArrayList<Service> providedServices, ArrayList<Product> providedProducts, ArrayList<EventType> providedEventTypes, Date lastModified) {
        this.id = id;
        this.firebaseUserId = firebaseUserId;
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.isEnabled = isEnabled;
        this.photoUrl = photoUrl;
        this.company = company;
        this.providedServices = providedServices;
        this.providedProducts = providedProducts;
        this.providedEventTypes = providedEventTypes;
        isApproved = false;
        this.lastModified = lastModified;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirebaseUserId() {
        return firebaseUserId;
    }

    public void setFirebaseUserId(String firebaseUserId) {
        this.firebaseUserId = firebaseUserId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Boolean getIsEnabled() {
        return isEnabled;
    }

    public void setIsEnabled(Boolean enabled) {
        isEnabled = enabled;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public ArrayList<Service> getProvidedServices() {
        return providedServices;
    }

    public void setProvidedServices(ArrayList<Service> providedServices) {
        this.providedServices = providedServices;
    }

    public ArrayList<Product> getProvidedProducts() {
        return providedProducts;
    }

    public void setProvidedProducts(ArrayList<Product> providedProducts) {
        this.providedProducts = providedProducts;
    }

    public ArrayList<EventType> getProvidedEventTypes() {
        return providedEventTypes;
    }

    public void setProvidedEventTypes(ArrayList<EventType> providedEventTypes) {
        this.providedEventTypes = providedEventTypes;
    }

    public Boolean getApproved() {
        return isApproved;
    }

    public void setApproved(Boolean approved) {
        isApproved = approved;
    }


    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }

    public void addProduct(Product p){
        if(providedProducts == null)
            this.providedProducts = new ArrayList<>();
        providedProducts.add(p);
    }

    public void removeProduct(int position) {
        if(providedProducts == null)
            this.providedProducts = new ArrayList<>();
        providedProducts.remove(position);
    }

    public void addService(Service s){
        if(providedServices == null)
            this.providedServices = new ArrayList<>();
        providedServices.add(s);
    }

    public void removeService(int position) {
        if(providedServices == null)
            this.providedServices = new ArrayList<>();
        providedServices.remove(position);
    }

    public Boolean containsProduct(Product product) {
        if(providedProducts == null)
            providedProducts = new ArrayList<>();
        for(Product p : providedProducts)
            if(Objects.equals(p.getId(), product.getId()))
                return true;
        return false;
    }

    public Boolean containsService(Service service) {
        if(providedServices == null)
            providedServices = new ArrayList<>();
        for(Service s : providedServices)
            if(Objects.equals(s.getId(), service.getId()))
                return true;
        return false;
    }

    public Boolean containsEventType(EventType eventType) {
        if(providedEventTypes == null)
            providedEventTypes = new ArrayList<>();
        for(EventType et: providedEventTypes)
            if(Objects.equals(et.getId(), eventType.getId()))
                return true;
        return false;
    }

    public void addEventType(EventType et){
        if(providedEventTypes == null)
            this.providedEventTypes = new ArrayList<>();
        providedEventTypes.add(et);
    }

    public void removeEventType(int position) {
        if(providedEventTypes == null)
            this.providedEventTypes = new ArrayList<>();
        providedEventTypes.remove(position);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.firebaseUserId);
        dest.writeString(this.email);
        dest.writeString(this.password);
        dest.writeString(this.firstName);
        dest.writeString(this.lastName);
        dest.writeString(this.address);
        dest.writeString(this.phoneNumber);
        dest.writeValue(this.isEnabled);
        dest.writeString(this.photoUrl);
        dest.writeParcelable(this.company, flags);
        dest.writeTypedList(this.providedServices);
        dest.writeTypedList(this.providedProducts);
        dest.writeTypedList(this.providedEventTypes);
        dest.writeValue(this.isApproved);
        dest.writeSerializable(this.lastModified);
    }

    public void readFromParcel(Parcel source) {
        this.id = source.readString();
        this.firebaseUserId = source.readString();
        this.email = source.readString();
        this.password = source.readString();
        this.firstName = source.readString();
        this.lastName = source.readString();
        this.address = source.readString();
        this.phoneNumber = source.readString();
        this.isEnabled = (Boolean) source.readValue(Boolean.class.getClassLoader());
        this.photoUrl = source.readString();
        this.company = source.readParcelable(Company.class.getClassLoader());
        this.providedServices = source.createTypedArrayList(Service.CREATOR);
        this.providedProducts = source.createTypedArrayList(Product.CREATOR);
        this.providedEventTypes = source.createTypedArrayList(EventType.CREATOR);
        this.isApproved = (Boolean) source.readValue(Boolean.class.getClassLoader());
        this.lastModified = (Date) source.readSerializable();
    }

    protected EventProvider(Parcel in) {
        this.id = in.readString();
        this.firebaseUserId = in.readString();
        this.email = in.readString();
        this.password = in.readString();
        this.firstName = in.readString();
        this.lastName = in.readString();
        this.address = in.readString();
        this.phoneNumber = in.readString();
        this.isEnabled = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.photoUrl = in.readString();
        this.company = in.readParcelable(Company.class.getClassLoader());
        this.providedServices = in.createTypedArrayList(Service.CREATOR);
        this.providedProducts = in.createTypedArrayList(Product.CREATOR);
        this.providedEventTypes = in.createTypedArrayList(EventType.CREATOR);
        this.isApproved = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.lastModified = (Date) in.readSerializable();
    }

    public static final Creator<EventProvider> CREATOR = new Creator<EventProvider>() {
        @Override
        public EventProvider createFromParcel(Parcel source) {
            return new EventProvider(source);
        }

        @Override
        public EventProvider[] newArray(int size) {
            return new EventProvider[size];
        }
    };
}
