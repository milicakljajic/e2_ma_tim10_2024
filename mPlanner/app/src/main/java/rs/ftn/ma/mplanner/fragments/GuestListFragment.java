package rs.ftn.ma.mplanner.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.adapters.GuestAdapter;
import rs.ftn.ma.mplanner.interfaces.EventCallback;
import rs.ftn.ma.mplanner.model.Event;
import rs.ftn.ma.mplanner.model.Guest;
import rs.ftn.ma.mplanner.repo.EventRepository;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link GuestListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GuestListFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "id";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private RecyclerView recyclerView;
    private Event currentEvent;
    private String eventId = "YOUR_EVENT_ID"; // Postavite ID događaja koji želite da preuzmete
    private GuestAdapter guestAdapter;
    private List<Guest> guestList;

    public GuestListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment GuestListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static GuestListFragment newInstance(String param1, String param2) {
        GuestListFragment fragment = new GuestListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }
    @Nullable
    @Override
    public View onCreateView(@Nonnull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_guest_list, container, false);

        //guestList = new ArrayList<>();
        //guestAdapter = new GuestAdapter(guestList, getContext());

        recyclerView = view.findViewById(R.id.guest_list_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        //recyclerView.setAdapter(guestAdapter);

        // Provera da li su novi podaci stigli iz CreationGuestListFragment-a
        /*if (getArguments() != null && getArguments().containsKey("new_guest")) {
            Guest newGuest = getArguments().getParcelable("new_guest");
            if (newGuest != null) {
                guestList.add(newGuest);
                guestAdapter.notifyItemInserted(guestList.size() - 1);
            }
        }*/

        /*
        *                      currentEvent = dataSnapshot.getValue(Event.class);
                        // Postavite adapter nakon što preuzmete podatke

        * */

        // Preuzmite trenutni događaj iz Firebase-a
        EventRepository erepo = new EventRepository();
        erepo.selectById(new EventCallback() {
            @Override
            public void onEventReceived(Event[] events) {
                currentEvent = events[0];
                currentEvent.setId(mParam1);
                GuestAdapter guestAdapter = new GuestAdapter(events[0].getGuestList(),events[0], getContext());
                recyclerView.setAdapter(guestAdapter);
            }

            @Override
            public void onError(Exception e) {

            }
        }, mParam1);

        return view;
    }
}