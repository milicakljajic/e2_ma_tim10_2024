package rs.ftn.ma.mplanner.fragments;

import android.app.TimePickerDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TimePicker;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Calendar;


import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.interfaces.EventInsertCallback;
import rs.ftn.ma.mplanner.interfaces.EventOrganizerCallback;
import rs.ftn.ma.mplanner.interfaces.EventTypeCallback;
import rs.ftn.ma.mplanner.model.Event;
import rs.ftn.ma.mplanner.model.EventOrganizer;
import rs.ftn.ma.mplanner.model.EventType;
import rs.ftn.ma.mplanner.repo.EventOrganizerRepo;
import rs.ftn.ma.mplanner.repo.EventRepository;
import rs.ftn.ma.mplanner.repo.EventTypeRepository;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CreationEventFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CreationEventFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "event-id";
    private static final String ARG_PARAM2 = "mode";

    private String eventId;
    private String mode;

    private Event currentEvent;
    private final EventRepository repo;

    private final EventTypeRepository etrepo;
    private String from;
    private String to;

    public CreationEventFragment() {
        // Required empty public constructor
        if(currentEvent == null) {
            currentEvent = new Event();
            currentEvent.setEventType(new EventType());
        }
        repo = new EventRepository();
        etrepo = new EventTypeRepository();
    }

    public static CreationEventFragment newInstance(String eventId, String mode) {
        CreationEventFragment fragment = new CreationEventFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, eventId);
        args.putString(ARG_PARAM2, mode);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            eventId = getArguments().getString(ARG_PARAM1);
            mode = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_creation_event, container, false);

        Button buttonFrom = view.findViewById(R.id.schedule_event_from);
        buttonFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mHour = c.get(Calendar.HOUR_OF_DAY);
                int mMinute = c.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(requireContext(), new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        //text.setText(hourOfDay);
                        String timeFrom = hourOfDay + ":" + minute;
                        currentEvent.setFrom(String.valueOf(timeFrom));
                    }
                }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });

        Button buttonTo = view.findViewById(R.id.schedule_event_to);
        buttonTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mHour = c.get(Calendar.HOUR_OF_DAY);
                int mMinute = c.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(requireContext(), new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        //text.setText(hourOfDay);
                        String timeTo = hourOfDay + ":" + minute;
                        to = String.valueOf(timeTo);
                        currentEvent.setTo(to);
                    }
                }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });


        etrepo.select(new EventTypeCallback() {
            @Override
            public void onEventTypeReceived(EventType[] types) {


                Spinner spinner = (Spinner) view.findViewById(R.id.event_type_spinner);
                ArrayAdapter<EventType> adapter = new ArrayAdapter<EventType>(
                        requireContext(),
                        //R.array.event_types_example_array,
                        android.R.layout.simple_spinner_item,
                        types
                );

                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(adapter);

                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        EventType selectedEventType = (EventType) parent.getItemAtPosition(position);
                        if(!currentEvent.getEventType().equals(selectedEventType) && selectedEventType.getId() != "-1") {
                            currentEvent.setEventType(selectedEventType);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }

            @Override
            public void onError(Exception e) {

            }
        }, false);



        /*//Spiner kada se odabere proizvod da doda u listViewProduct
        final ListView finalLv = listViewProduct;
        spinnerProduct.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                Product selectedProduct = (Product) parent.getItemAtPosition(position);
                if(!currentType.containsProduct(selectedProduct) && selectedProduct.getId() != -1) {
                    currentType.addProduct(selectedProduct);
                    setupProductListView(view, finalLv);
                }
            }
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });*/
        /*//Popunjavanje spinnera
        Product[] products = new Product[]{
                new Product(-1, "-", "", 0.0, "", "", 0, false),
                new Product(1, "Camera", "Professional DSLR camera for photography enthusiasts", 899.99, "Electronics", "Cameras", 0, true),
                new Product(2, "Tripod", "Sturdy tripod for stable photography shots", 49.99, "Accessories", "Tripods", 0, true)};

        ListView listViewProduct = view.findViewById(R.id.event_type_product_lw);
        Spinner spinnerProduct = view.findViewById(R.id.event_type_product_spinner);
        ArrayAdapter<Product> spinnerProductAdapter = new ArrayAdapter<Product>(
                requireContext(),
                android.R.layout.simple_spinner_item,
                products
        );
        spinnerProductAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerProduct.setAdapter(spinnerProductAdapter);*/

        //Dobavljanje kontroli da mapiramo na obj kasnije
        EditText nameText = (EditText) view.findViewById(R.id.event_name);
        EditText descriptionText = (EditText) view.findViewById(R.id.event_description);
        EditText maxParticipantsText = (EditText) view.findViewById(R.id.event_max_participants);
        EditText locationText = (EditText) view.findViewById(R.id.event_location);
        EditText maxKilometersText = (EditText) view.findViewById(R.id.event_max_kilometers);
        EditText dateText = (EditText) view.findViewById(R.id.event_date);
        RadioGroup radioGroup = (RadioGroup) view.findViewById(R.id.event_radio_group);
        RadioButton yesRadioButton = (RadioButton) view.findViewById(R.id.event_private_yes);
        RadioButton noRadioButton = (RadioButton) view.findViewById(R.id.event_private_no);

        //CREATE izvrsavanje
        Button button = (Button) view.findViewById(R.id.event_creation_button);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                currentEvent.setName(String.valueOf(nameText.getText()));
                currentEvent.setDescription(String.valueOf(descriptionText.getText()));
                currentEvent.setMaxParticipants(Integer.parseInt(maxParticipantsText.getText().toString()));
                currentEvent.setLocation(String.valueOf(locationText.getText()));
                currentEvent.setMaxKilometers(Integer.parseInt(maxKilometersText.getText().toString()));
                currentEvent.setDate(String.valueOf(dateText.getText()));

                boolean isPrivate = yesRadioButton.isChecked();
                currentEvent.setIsPrivate(isPrivate);

                FirebaseAuth mAuth = FirebaseAuth.getInstance();
                FirebaseUser cUser = mAuth.getCurrentUser();
                EventOrganizerRepo erepo = new EventOrganizerRepo();
                erepo.selectByFirebaseId(new EventOrganizerCallback() {
                    @Override
                    public void onEventOrganizerReceived(EventOrganizer[] types) {
                        currentEvent.setCreator(types[0]);
                        repo.insert(currentEvent, new EventInsertCallback() {
                            @Override
                            public void onEventInserted(String eventId) {
                            }

                            @Override
                            public void onInsertError(Exception e) {

                            }
                        });

                        Navigation.findNavController(view).navigate(R.id.changeToSuggestionFragment);
                    }

                    @Override
                    public void onError(Exception e) {

                    }
                }, cUser.getUid());


            }
        });

        return view;
    }
}