package rs.ftn.ma.mplanner.fragments;

import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.interfaces.EmployeeCallback;
import rs.ftn.ma.mplanner.interfaces.EmployeeUpdateCallback;
import rs.ftn.ma.mplanner.model.Employee;
import rs.ftn.ma.mplanner.repo.EmployeeRepository;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "employeeId";

    // TODO: Rename and change types of parameters
    private String employeeId;

    private EmployeeRepository repo;
    private Employee employee;

    public ProfileFragment() {
        // Required empty public constructor
        repo = new EmployeeRepository();
    }

    // TODO: Rename and change types and number of parameters
    public static ProfileFragment newInstance(String employeeId) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, employeeId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            employeeId = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        repo.selectByFirebaseId(new EmployeeCallback() {
            @Override
            public void onEmployeeRecieved(Employee[] employees) {
                employee = employees[0];
                TextView name = view.findViewById(R.id.profile_name);
                String n = employee.getFirstName() + " " + employee.getLastName();
                name.setText(n);

                TextView email = view.findViewById(R.id.profile_email);
                email.setText(employee.getEmail());

                TextView phoneNumber = view.findViewById(R.id.profile_phone_number);
                phoneNumber.setText(employee.getPhoneNumber());

                TextView address = view.findViewById(R.id.profile_address);
                address.setText(employee.getAddress());
            }

            @Override
            public void onError(Exception e) {

            }
        }, employeeId);

        CardView button = (CardView) view.findViewById(R.id.employee_calendar);

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("Employee", employee);
                Navigation.findNavController(view).navigate(R.id.changeToCalendar1, bundle);
            }
        });

        CardView button2 = (CardView) view.findViewById(R.id.employee_hours);

        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("Employee", employee);
                Navigation.findNavController(view).navigate(R.id.action_profileFragment2_to_employeeScheduleFragment2, bundle);
            }
        });

        CardView button3 = (CardView) view.findViewById(R.id.employee_see_services);

        button3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Navigation.findNavController(view).navigate(R.id.changeToServices1);
            }
        });

        MaterialButton b = view.findViewById(R.id.profile_pupz_activate_button);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                employee.setIsEnabled(true);
                repo.update(employee.getId(), employee, new EmployeeUpdateCallback() {
                    @Override
                    public void onEmployeeUpdated() {
                        Toast.makeText(v.getContext(), "enabled", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onUpdateError(Exception e) {

                    }
                });
            }
        });

        b = view.findViewById(R.id.profile_pupz_deactivate_button);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                employee.setIsEnabled(false);
                repo.update(employee.getId(), employee, new EmployeeUpdateCallback() {
                    @Override
                    public void onEmployeeUpdated() {
                        Toast.makeText(v.getContext(), "disabled", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onUpdateError(Exception e) {

                    }
                });
            }
        });

        return view;
    }
}