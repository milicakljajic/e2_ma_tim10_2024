package rs.ftn.ma.mplanner.interfaces;

public interface ServiceInsertCallback {
    void onServiceCreated(String serviceId);
    void onCreateError(Exception e);
}
