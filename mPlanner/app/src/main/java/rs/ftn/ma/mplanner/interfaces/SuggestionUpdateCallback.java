package rs.ftn.ma.mplanner.interfaces;

public interface SuggestionUpdateCallback {
    void onSuggestionUpdated();
    void onUpdateError(Exception e);
}
