package rs.ftn.ma.mplanner.interfaces;

public interface NotificationUpdateCallback {
    void onNotificationUpdated();
    void onUpdateError(Exception e);
}
