package rs.ftn.ma.mplanner.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.adapters.EventTypeAdapter;
import rs.ftn.ma.mplanner.adapters.ProductAdapter;
import rs.ftn.ma.mplanner.adapters.ServiceAdapter;
import rs.ftn.ma.mplanner.model.EventProvider;
import rs.ftn.ma.mplanner.model.Product;
import rs.ftn.ma.mplanner.model.Service;
import rs.ftn.ma.mplanner.repo.EventTypeRepository;


public class RegistrationCategoryFragment extends Fragment {

    private static final String ARG_PARAM1 = "EventProvider";

    private EventProvider newEventProvider;
    private ProductAdapter productAdapter;
    private ServiceAdapter serviceAdapter;

    private EventTypeAdapter eventTypeAdapter;

    private EventTypeRepository repo;

    public RegistrationCategoryFragment() {
        // Required empty public constructor
        repo = new EventTypeRepository();
    }


    public static RegistrationCategoryFragment newInstance(EventProvider newEventProvider) {
        RegistrationCategoryFragment fragment = new RegistrationCategoryFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, newEventProvider);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if (getArguments().getParcelable(ARG_PARAM1) != null)
                newEventProvider = getArguments().getParcelable(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_registration_category, container, false);
        Button button = (Button) view.findViewById(R.id.registration_category_button);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("EventProvider", newEventProvider);
                Navigation.findNavController(view).navigate(R.id.changeToRegistrationSchedule3, bundle);
            }
        });
        setupSpinnersAndListViews(view);
        return view;
    }

    private void setupSpinnersAndListViews(View view) {

        //Deo za proizvode---------------------------------------------------------------------------------------------------
        //TODO OVO DOBAVITI PREKO REPO KADA URADE
        //Popunjavanje spinnera
        Product[] products = new Product[]{
                new Product(-1, "-", "", 0.0, "", "", 0, false),
                new Product(1, "Camera", "Professional DSLR camera for photography enthusiasts", 899.99, "Electronics", "Cameras", 0, true),
                new Product(2, "Tripod", "Sturdy tripod for stable photography shots", 49.99, "Accessories", "Tripods", 0, true)};

        ListView listViewProduct = view.findViewById(R.id.registration_category_product_lv);
        Spinner spinnerProduct = view.findViewById(R.id.registration_category_product_spinner);
        ArrayAdapter<Product> spinnerProductAdapter = new ArrayAdapter<Product>(
                requireContext(),
                android.R.layout.simple_spinner_item,
                products
        );
        spinnerProductAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerProduct.setAdapter(spinnerProductAdapter);

        //Spiner kada se odabere proizvod da doda u listViewProduct
        final ListView finalLv = listViewProduct;
        spinnerProduct.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Product selectedProduct = (Product) parent.getItemAtPosition(position);
                if (!newEventProvider.containsProduct(selectedProduct) && selectedProduct.getId() != -1) {
                    newEventProvider.addProduct(selectedProduct);
                    // setupProductListView(view, finalLv);
                }
            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        if (!this.newEventProvider.getProvidedProducts().isEmpty()) {
            //setupProductListView(view, listViewProduct);
        }

        //DEO ZA SERVISE---------------------------------------------------------------------------------------------------
        //TODO OVO DOBAVITI PREKO REPO KADA URADE
       /* Service[] services = new Service[]{
                new Service(-1,"-", "", "", "", 0, 0.10),
                new Service(1,"Service Name", "Service Description", "Category", "Subcategory", 100, 0.10),
                new Service(2,"Photo Service", "Professional photos for homes", "Home Services", "Photo", 80, 0.05)};*/

        ListView listViewService = view.findViewById(R.id.registration_category_service_lv);
        Spinner spinnerService = view.findViewById(R.id.registration_category_service_spinner);
        /*ArrayAdapter<Service> adapterService = new ArrayAdapter<Service>(
                requireContext(),
                android.R.layout.simple_spinner_item,
                services
        );*/
        // adapterService.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // spinnerService.setAdapter(adapterService);

        //kada se klikne na elem iz spinner a da nije onaj '-' dodaj ga u lv
        spinnerService.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Service selectedService = (Service) parent.getItemAtPosition(position);
                if (!newEventProvider.containsService(selectedService) && selectedService.getId() != -1) {
                    newEventProvider.addService(selectedService);
                    // setupServiceListView(view, listViewService);
                }
            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        //setupServiceListView(view, listViewService);

/*
        repo.select(new EventTypeCallback() {
            @Override
            public void onEventTypeReceived(EventType[] types) {
                ListView listViewEventType = view.findViewById(R.id.registration_category_eventtype_lv);
                Spinner spinnerEventType = view.findViewById(R.id.registration_category_eventtype_spinner);
                ArrayAdapter<EventType> adapterEventType = new ArrayAdapter<EventType>(
                        requireContext(),
                        android.R.layout.simple_spinner_item,
                        types
                );
                adapterEventType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerEventType.setAdapter(adapterEventType);

                //kada se klikne na elem iz spinner a da nije onaj '-' dodaj ga u lv
                spinnerEventType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
                {
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
                    {
                        EventType selectedEventType = (EventType) parent.getItemAtPosition(position);
                        if(!newEventProvider.containsEventType(selectedEventType) && !Objects.equals(selectedEventType.getId(), "-1")) {
                            newEventProvider.addEventType(selectedEventType);
                            setupEventTypeListView(view, listViewEventType);
                        }
                    }
                    public void onNothingSelected(AdapterView<?> parent)
                    {

                    }
                });
                setupEventTypeListView(view, listViewEventType);
            }
            @Override
            public void onError(Exception e) {}
        }, false);
    }

    //funkc za uklanjanje obj iz service lv
    private void setupServiceListView(View view, ListView listViewService) {
        serviceAdapter = new ServiceAdapter(view.getContext(), this.newEventProvider.getProvidedServices(), 0, R.layout.fragment_subcategory_list_item);
        serviceAdapter.setOnDeleteButtonClickListener(new OnDeleteButtonClickListener() {
            @Override
            public void onDeleteClicked(int position) {
                newEventProvider.removeService(position);
                serviceAdapter.notifyDataSetChanged();
            }
        });
        listViewService.setAdapter(serviceAdapter);
    }

    //funkc za uklanjanje obj iz product lv
    private void setupProductListView(View view, ListView lv) {
        productAdapter = new ProductAdapter(view.getContext(), this.newEventProvider.getProvidedProducts(), 0, R.layout.fragment_subcategory_list_item);
        productAdapter.setOnDeleteButtonClickListener(new OnDeleteButtonClickListener() {
            @Override
            public void onDeleteClicked(int position) {
                newEventProvider.removeProduct(position);
                productAdapter.notifyDataSetChanged();
            }
        });
        lv.setAdapter(productAdapter);
    }

    //Funck za eventtype
    private void setupEventTypeListView(View view, ListView lv) {
        eventTypeAdapter = new EventTypeAdapter(view.getContext(), this.newEventProvider.getProvidedEventTypes(), R.layout.fragment_subcategory_list_item);
        eventTypeAdapter.setOnDeleteButtonClickListener(new OnDeleteButtonClickListener() {
            @Override
            public void onDeleteClicked(int position) {
                newEventProvider.removeEventType(position);
                eventTypeAdapter.notifyDataSetChanged();
            }
        });
        lv.setAdapter(eventTypeAdapter);
    }
    (
 */
    }
}