package rs.ftn.ma.mplanner.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.model.EventOrganizer;
import rs.ftn.ma.mplanner.model.EventProvider;

public class RegistrationPersonalFragment extends Fragment {

    private static final String ARG_PARAM1 = "EventOrganizer";
    private static final String ARG_PARAM2 = "EventProvider";
    private EventOrganizer newEventOrganizer;
    private EventProvider newEventProvider;

    public RegistrationPersonalFragment() {
        // Required empty public constructor
    }

    public static RegistrationPersonalFragment newInstance(EventOrganizer newEventOrganizer, EventProvider newEventProvider) {
        RegistrationPersonalFragment fragment = new RegistrationPersonalFragment();
        Bundle args = new Bundle();
        args.putParcelable("EventOrganizer", newEventOrganizer);
        args.putParcelable("EventProvider", newEventProvider);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if(getArguments().getParcelable(ARG_PARAM1) != null)
                newEventOrganizer = getArguments().getParcelable(ARG_PARAM1);
            if(getArguments().getParcelable(ARG_PARAM2) != null)
                newEventProvider = getArguments().getParcelable(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_registration_personal, container, false);

        Button button = (Button) view.findViewById(R.id.registration_personal_next);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(newEventOrganizer != null) {
                    EditText firstname = view.findViewById(R.id.registration_personal_firstname);
                    EditText lastname = view.findViewById(R.id.registration_personal_lastname);
                    EditText phone = view.findViewById(R.id.registration_personal_phone);
                    EditText address = view.findViewById(R.id.registration_personal_address);
                    String name = String.valueOf(firstname.getText());
                    String lname = String.valueOf(lastname.getText());
                    String pho = String.valueOf(phone.getText());
                    String adr = String.valueOf(address.getText());
                    if(validate(name, lname, pho, adr)){
                        newEventOrganizer.setFirstName(name);
                        newEventOrganizer.setLastName(lname);
                        newEventOrganizer.setPhoneNumber(pho);
                        newEventOrganizer.setAddress(adr);
                        Bundle bundle = new Bundle();
                        bundle.putParcelable("EventOrganizer", newEventOrganizer);
                        Navigation.findNavController(view).navigate(R.id.changeToRegistrationPhoto2, bundle);
                    }
                }
                else if(newEventProvider != null) {
                    EditText firstname = view.findViewById(R.id.registration_personal_firstname);
                    EditText lastname = view.findViewById(R.id.registration_personal_lastname);
                    EditText phone = view.findViewById(R.id.registration_personal_phone);
                    EditText address = view.findViewById(R.id.registration_personal_address);
                    String name = String.valueOf(firstname.getText());
                    String lname = String.valueOf(lastname.getText());
                    String pho = String.valueOf(phone.getText());
                    String adr = String.valueOf(address.getText());
                    if(validate(name, lname, pho, adr)){
                        newEventProvider.setFirstName(name);
                        newEventProvider.setLastName(lname);
                        newEventProvider.setPhoneNumber(pho);
                        newEventProvider.setAddress(adr);
                        Bundle bundle = new Bundle();
                        bundle.putParcelable("EventProvider", newEventProvider);
                        Navigation.findNavController(view).navigate(R.id.changeToRegistrationCompany1, bundle);
                    }
                }
            }
        });

        TextView t = (TextView) view.findViewById(R.id.shift_to_pupv);
        t.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Navigation.findNavController(view).navigate(R.id.changeToRegistrationCompany1);
            }
        });
        return view;
    }

    public Boolean validate(String firstname, String lastname, String phone, String address) {
        return !firstname.isEmpty() && !lastname.isEmpty() && !phone.isEmpty() && !address.isEmpty();
    }
}