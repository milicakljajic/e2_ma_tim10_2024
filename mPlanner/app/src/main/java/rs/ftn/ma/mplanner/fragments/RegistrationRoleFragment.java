package rs.ftn.ma.mplanner.fragments;

import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.model.EventOrganizer;
import rs.ftn.ma.mplanner.model.EventProvider;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link RegistrationRoleFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RegistrationRoleFragment extends Fragment {

    public RegistrationRoleFragment() {
        // Required empty public constructor
    }

    public static RegistrationRoleFragment newInstance() {
        RegistrationRoleFragment fragment = new RegistrationRoleFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragment_registration_role, container, false);

        CardView button = (CardView) view.findViewById(R.id.registration_role_eo);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("EventOrganizer", new EventOrganizer());
                Navigation.findNavController(view).navigate(R.id.changeToRegistrationBasic1, bundle);
            }
        });

        button = (CardView) view.findViewById(R.id.registration_role_ep);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("EventProvider", new EventProvider());
                Navigation.findNavController(view).navigate(R.id.changeToRegistrationBasic1, bundle);
            }
        });
        return view;
    }
}
