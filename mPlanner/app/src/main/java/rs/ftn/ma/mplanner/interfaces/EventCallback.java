package rs.ftn.ma.mplanner.interfaces;

import rs.ftn.ma.mplanner.model.Event;

public interface EventCallback {
    void onEventReceived(Event[] events);
    void onError(Exception e);
}