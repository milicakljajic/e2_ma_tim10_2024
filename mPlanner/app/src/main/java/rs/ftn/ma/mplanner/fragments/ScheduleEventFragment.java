package rs.ftn.ma.mplanner.fragments;

import android.app.TimePickerDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.interfaces.EmployeeUpdateCallback;
import rs.ftn.ma.mplanner.interfaces.EventCallback;
import rs.ftn.ma.mplanner.interfaces.EventProviderCallback;
import rs.ftn.ma.mplanner.interfaces.NotificationInsertCallback;
import rs.ftn.ma.mplanner.model.Employee;
import rs.ftn.ma.mplanner.model.Event;
import rs.ftn.ma.mplanner.model.EventProvider;
import rs.ftn.ma.mplanner.model.Notification;
import rs.ftn.ma.mplanner.repo.EmployeeRepository;
import rs.ftn.ma.mplanner.repo.EventProviderRepository;
import rs.ftn.ma.mplanner.repo.EventRepository;
import rs.ftn.ma.mplanner.repo.NotificationRepository;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ScheduleEventFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ScheduleEventFragment extends Fragment {
    private TextView text;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "Employee";
    private static final String ARG_PARAM2 = "Provider";

    // TODO: Rename and change types of parameters
    private Employee employee;

    private EventProvider provider;
    private EventRepository repo;

    private EmployeeRepository erepo;

    private EventProviderRepository prepo;
    private NotificationRepository nrepo;

    private Event event;
    private String from;
    private String to;
    private String date;

    public ScheduleEventFragment() {
        // Required empty public constructor
        repo = new EventRepository();
        erepo = new EmployeeRepository();
        event = new Event();
        prepo = new EventProviderRepository();
        nrepo = new NotificationRepository();
    }


    public static ScheduleEventFragment newInstance(Employee employee, EventProvider provider) {
        ScheduleEventFragment fragment = new ScheduleEventFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, employee);
        args.putParcelable(ARG_PARAM2, provider);
        fragment.setArguments(args);
        return fragment;
    }

    private void openDialog() {
        final Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);
        TimePickerDialog timePickerDialog = new TimePickerDialog(requireContext(), new TimePickerDialog.OnTimeSetListener() {

            @Override
            public void onTimeSet(TimePicker view, int hourOfDay,
                                  int minute) {

                text.setText(hourOfDay);
            }
        }, mHour, mMinute, false);
        timePickerDialog.show();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if (getArguments().getParcelable(ARG_PARAM1) != null)
                employee = getArguments().getParcelable(ARG_PARAM1);
            if (getArguments().getParcelable(ARG_PARAM2) != null)
                provider = getArguments().getParcelable(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_schedule_event, container, false);

        repo.select(new EventCallback() {
            @Override
            public void onEventReceived(Event[] events) {


                CalendarView calendarView = view.findViewById(R.id.schedule_date);
                calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
                    @Override
                    public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                        date = dayOfMonth + "." + (month + 1) + "." + year;
                    }
                });

                Spinner spinner = (Spinner) view.findViewById(R.id.schedule_event_spinner);
                ArrayAdapter<Event> adapter = new ArrayAdapter<Event>(
                        requireContext(),
                        android.R.layout.simple_spinner_item,
                        events
                );

                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(adapter);

                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        event = events[position];
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }

            @Override
            public void onError(Exception e) {

            }
        }, false);

        Button buttonFrom = view.findViewById(R.id.schedule_event_from);
        buttonFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mHour = c.get(Calendar.HOUR_OF_DAY);
                int mMinute = c.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(requireContext(), new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        //text.setText(hourOfDay);
                        String timeFrom = hourOfDay + ":" + minute;
                        from = String.valueOf(timeFrom);
                    }
                }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });

        Button buttonTo = view.findViewById(R.id.schedule_event_to);
        buttonTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mHour = c.get(Calendar.HOUR_OF_DAY);
                int mMinute = c.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(requireContext(), new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        //text.setText(hourOfDay);
                        String timeTo = hourOfDay + ":" + minute;
                        to = String.valueOf(timeTo);
                    }
                }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });


        MaterialButton b = view.findViewById(R.id.schedule_event_button);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean fail = false;
                switch (getDayFromDate(event.getDate())) {
                    case "Monday":
                        if (!timeoverlap(event.getFrom(), event.getTo(), employee.getSchedule().getMondayFrom(), employee.getSchedule().getMondayTo()))
                            fail = true;
                        break;
                    case "Tuesday":
                        if (!timeoverlap(event.getFrom(), event.getTo(), employee.getSchedule().getTuesdayFrom(), employee.getSchedule().getTuesdayTo()))
                            fail = true;
                        break;
                    case "Wednesday":
                        if (!timeoverlap(event.getFrom(), event.getTo(), employee.getSchedule().getWednesdayFrom(), employee.getSchedule().getWednesdayTo()))
                            fail = true;
                        break;
                    case "Thursday":
                        if (!timeoverlap(event.getFrom(), event.getTo(), employee.getSchedule().getThursdayFrom(), employee.getSchedule().getThursdayTo()))
                            fail = true;
                        break;
                    case "Friday":
                        if (!timeoverlap(event.getFrom(), event.getTo(), employee.getSchedule().getFridayFrom(), employee.getSchedule().getFridayTo()))
                            fail = true;
                        break;
                    case "Saturday":
                        if (!timeoverlap(event.getFrom(), event.getTo(), employee.getSchedule().getSaturdayFrom(), employee.getSchedule().getSaturdayTo()))
                            fail = true;
                        break;
                    case "Sunday":
                        if (!timeoverlap(event.getFrom(), event.getTo(), employee.getSchedule().getSundayFrom(), employee.getSchedule().getSundayTo()))
                            fail = true;
                        break;
                }
                if (!fail) {
                    for (Event e : employee.getScheduledEvents()) {
                        //FIXME NA ID KADA SE URADI
                        if (e.getName().equals(event.getName())) {
                            fail = true;
                            break;
                        }
                        if (e.getDate().equals(event.getDate()))
                            if (timeoverlap(e.getFrom(), e.getTo(), event.getFrom(), event.getTo()))
                                fail = true;
                    }

                    if (!fail) {
                        employee.getScheduledEvents().add(event);
                        erepo.update(employee.getId(), employee, new EmployeeUpdateCallback() {
                            @Override
                            public void onEmployeeUpdated() {
                                //provajder zakazao
                                if(provider != null && employee != null){
                                    nrepo.insert(new Notification(employee.getFirebaseUserId(), "Boss scheduled new event", event.getName(), false), new NotificationInsertCallback() {
                                        @Override
                                        public void onNotificationInserted(String notificationId) {
                                            Toast.makeText(getActivity(), "Successfully scheduled", Toast.LENGTH_SHORT).show();
                                        }

                                        @Override
                                        public void onInsertError(Exception e) {

                                        }
                                    });
                                }
                                else {
                                    //employee zakazao
                                    prepo.selectByCompany(new EventProviderCallback() {
                                        @Override
                                        public void onEventProviderReceived(EventProvider[] providers) {
                                            provider = providers[0];
                                            nrepo.insert(new Notification(provider.getFirebaseUserId(), "Employee scheduled new event", employee.getEmail(), false), new NotificationInsertCallback() {
                                                @Override
                                                public void onNotificationInserted(String notificationId) {
                                                    Toast.makeText(getActivity(), "Successfully scheduled", Toast.LENGTH_SHORT).show();
                                                }

                                                @Override
                                                public void onInsertError(Exception e) {

                                                }
                                            });
                                        }

                                        @Override
                                        public void onError(Exception e) {

                                        }
                                    }, employee.getCompany().getName());
                                }
                            }

                            @Override
                            public void onUpdateError(Exception e) {

                            }
                        });
                    }
                    else
                    {
                        Toast.makeText(getActivity(), "Employee already busy", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        return view;
    }

    public boolean timeoverlap(String start1, String end1, String start2, String end2) {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        try {
            Date startTime1 = sdf.parse(start1);
            Date endTime1 = sdf.parse(end1);
            Date startTime2 = sdf.parse(start2);
            Date endTime2 = sdf.parse(end2);


            if (startTime1.before(endTime2) && endTime1.after(startTime2)) {
                return true;
            } else {
                return false;
            }
        } catch (ParseException e) {
            return false;
        }
    }

    public String getDayFromDate(String dateString) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
        try {
            Date date = sdf.parse(dateString);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);


            int day = calendar.get(Calendar.DAY_OF_WEEK);

            String dayName = "";
            switch (day) {
                case Calendar.SUNDAY:
                    dayName = "Sunday";
                    break;
                case Calendar.MONDAY:
                    dayName = "Monday";
                    break;
                case Calendar.TUESDAY:
                    dayName = "Tuesday";
                    break;
                case Calendar.WEDNESDAY:
                    dayName = "Wednesday";
                    break;
                case Calendar.THURSDAY:
                    dayName = "Thursday";
                    break;
                case Calendar.FRIDAY:
                    dayName = "Friday";
                    break;
                case Calendar.SATURDAY:
                    dayName = "Saturday";
                    break;
            }

            return dayName;
        } catch (ParseException e) {
            //e.printStackTrace();
            return "";
        }

    }
}