package rs.ftn.ma.mplanner.interfaces;

import rs.ftn.ma.mplanner.model.Notification;

public interface NotificationCallback {
    void onNotificationReceived(Notification[] notifications);
    void onError(Exception e);
}
