package rs.ftn.ma.mplanner.interfaces;

public interface ServiceDeleteCallback {
    void onServiceDeleted();
    void onDeleteError(Exception ex);
}
