package rs.ftn.ma.mplanner.interfaces;

public interface CategoryInsertCallback {
    void onCategoryInserted(String categoryId);
    void onInsertError(Exception e);
}
