package rs.ftn.ma.mplanner.interfaces;

public interface CommentInsertCallback {
    void onCommentInserted(String commentId);
    void onInsertError(Exception e);
}
