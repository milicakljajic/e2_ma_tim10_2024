package rs.ftn.ma.mplanner.interfaces;

public interface EventOrganizerUpdateCallback {
    void onEventOrganizerUpdated();
    void onUpdateError(Exception e);
}
