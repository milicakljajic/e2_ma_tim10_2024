package rs.ftn.ma.mplanner.interfaces;

public interface EventProviderInsertCallback {
    void onEventProviderInserted(String providerId);
    void onInsertError(Exception e);
}
