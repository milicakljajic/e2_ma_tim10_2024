package rs.ftn.ma.mplanner.repo;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import rs.ftn.ma.mplanner.interfaces.CategoryCallback;
import rs.ftn.ma.mplanner.interfaces.CategoryInsertCallback;
import rs.ftn.ma.mplanner.interfaces.CategoryUpdateCallback;
import rs.ftn.ma.mplanner.interfaces.CategoryDeleteCallback;
import rs.ftn.ma.mplanner.model.Category;

public class CategoryRepository {
    public void insert(Category c, CategoryInsertCallback callback) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("categories")
                .add(c)
                .addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentReference> task) {
                        if (task.isSuccessful()) {
                            DocumentReference documentReference = task.getResult();
                            String cId = documentReference.getId();
                            Log.d("REZ_DB", "DocumentSnapshot added with ID: " + cId);
                            callback.onCategoryInserted(cId);
                        } else {
                            Log.w("REZ_DB", "Error adding document", task.getException());
                            callback.onInsertError(task.getException());
                        }
                    }
                });
    }


    public void select(CategoryCallback callback) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("categories")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            List<Category> categories = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Category category = document.toObject(Category.class);
                                category.setId(document.getId());
                                categories.add(category);
                                Log.d("REZ_DB", document.getId() + " => " + document.getData());
                            }
                            callback.onCategoryReceived(categories.toArray(new Category[0]));
                        } else {
                            Log.w("REZ_DB", "Error getting documents.", task.getException());
                            callback.onError(task.getException());
                        }
                    }
                });
    }


   public void update(String id, Category c, CategoryUpdateCallback callback) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("categories").document(id);

        docRef.update("name", c.getName())
                .addOnSuccessListener(aVoid -> {
                    Log.d("REZ_DB", "Name successfully changed");
                    callback.onCategoryUpdated();
                })
                .addOnFailureListener(e -> {
                    Log.w("REZ_DB", "Error getting documents.", e);
                    callback.onUpdateError(e);
                });

       docRef.update("description", c.getDescription())
               .addOnSuccessListener(aVoid -> {
                   Log.d("REZ_DB", "Description successfully changed");
                   callback.onCategoryUpdated();
               })
               .addOnFailureListener(e -> {
                   Log.w("REZ_DB", "Error getting documents.", e);
                   callback.onUpdateError(e);
               });
    }


    public void delete(String id, CategoryDeleteCallback callback) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("categories").document(id);

        docRef.delete()
                .addOnSuccessListener(aVoid -> {
                    Log.d("REZ_DB", "Category successfully deleted");
                    callback.onCategoryDeleted();
                })
                .addOnFailureListener(e -> {
                    Log.w("REZ_DB", "Error deleting category.", e);
                    callback.onDeleteError(e);
                });
    }



    public void selectById(CategoryCallback callback, String id) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("categories").document(id);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot documentSnapshot = task.getResult();
                    if (documentSnapshot.exists()) {
                        Category[] c = new Category[]{documentSnapshot.toObject(Category.class)};
                        Log.d("REZ_DB", documentSnapshot.getId() + " => " + documentSnapshot.getData());
                        callback.onCategoryReceived(c);
                    } else {
                        Log.d("REZ_DB", "No such document");
                        callback.onError(new Exception("No such document"));
                    }
                } else {
                    Log.w("REZ_DB", "Error getting document", task.getException());
                    callback.onError(task.getException());
                }
            }
        });
    }
}
