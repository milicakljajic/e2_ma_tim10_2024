package rs.ftn.ma.mplanner.interfaces;

public interface SubcategoryDeleteCallback {
    void onSubcategoryDeleted();
    void onDeleteError(Exception e);
}
