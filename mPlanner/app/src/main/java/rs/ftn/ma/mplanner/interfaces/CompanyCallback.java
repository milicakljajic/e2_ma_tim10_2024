package rs.ftn.ma.mplanner.interfaces;

import rs.ftn.ma.mplanner.model.Company;

public interface CompanyCallback {
    void onCompanyReceived(Company[] companies);
    void onError(Exception e);
}