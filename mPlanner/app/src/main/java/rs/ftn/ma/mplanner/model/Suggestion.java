package rs.ftn.ma.mplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Suggestion implements Parcelable {
    private String id;
    private String name;
    private EventProvider eventProvider;
    private Subcategory subcategory;

    public Suggestion() {
    }

    public Suggestion(String id, String name, EventProvider eventProvider, Subcategory subcategory) {
        this.id = id;
        this.name = name;
        this.eventProvider = eventProvider;
        this.subcategory = subcategory;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EventProvider getEventProvider() {
        return eventProvider;
    }

    public void setEventProvider(EventProvider eventProvider) {
        this.eventProvider = eventProvider;
    }

    public Subcategory getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(Subcategory subcategory) {
        this.subcategory = subcategory;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeParcelable(this.eventProvider, flags);
        dest.writeParcelable(this.subcategory, flags);
    }

    public void readFromParcel(Parcel source) {
        this.id = source.readString();
        this.name = source.readString();
        this.eventProvider = source.readParcelable(EventProvider.class.getClassLoader());
        this.subcategory = source.readParcelable(Subcategory.class.getClassLoader());
    }

    protected Suggestion(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.eventProvider = in.readParcelable(EventProvider.class.getClassLoader());
        this.subcategory = in.readParcelable(Subcategory.class.getClassLoader());
    }

    public static final Creator<Suggestion> CREATOR = new Creator<Suggestion>() {
        @Override
        public Suggestion createFromParcel(Parcel source) {
            return new Suggestion(source);
        }

        @Override
        public Suggestion[] newArray(int size) {
            return new Suggestion[size];
        }
    };
}
