package rs.ftn.ma.mplanner.model.enums;

public enum ProductState {
    PENDING,
    CREATED,
    DELETED
}
