package rs.ftn.ma.mplanner.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.model.Employee;

public class EmployeeEventWorkAdapter extends BaseAdapter {

    Context context;
    ArrayList<Employee> employees;
    LayoutInflater inflter;

    public EmployeeEventWorkAdapter(Context applicationContext, Employee[] employees) {
        this.context = context;
        this.employees = new ArrayList<>(Arrays.asList(employees));
        inflter = (LayoutInflater.from(applicationContext));
    }


    public EmployeeEventWorkAdapter(Context applicationContext, ArrayList<Employee> employees) {
        this.context = context;
        this.employees = employees;
        inflter = (LayoutInflater.from(applicationContext));
    }


    @Override
    public int getCount() {
        return employees.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.fragment_employees_list_item, null);
        TextView firstname = (TextView) view.findViewById(R.id.employee_item_firstname);
        TextView lastname = (TextView) view.findViewById(R.id.employee_item_lastname);
        TextView email = (TextView) view.findViewById(R.id.employee_item_email);

        firstname.setText(employees.get(i).getFirstName());
        lastname.setText(employees.get(i).getLastName());
        email.setText(employees.get(i).getEmail());

        firstname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("dd", "onClick: ok");
            }
        });

        return view;
    }
}
