package rs.ftn.ma.mplanner.model.enums;

public enum ReportStatus {
    ACCEPTED,
    PENDING,
    DENIED
}
