package rs.ftn.ma.mplanner.interfaces;

public interface EventInsertCallback {
    void onEventInserted(String eventId);
    void onInsertError(Exception e);
}