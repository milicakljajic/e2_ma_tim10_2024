package rs.ftn.ma.mplanner.adapters;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.navigation.Navigation;

import java.util.ArrayList;
import java.util.Arrays;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.interfaces.EventUpdateCallback;
import rs.ftn.ma.mplanner.interfaces.OnDeleteButtonClickListener;
import rs.ftn.ma.mplanner.model.Event;
import rs.ftn.ma.mplanner.model.Service;
import rs.ftn.ma.mplanner.repo.EventRepository;

public class ServiceAdapter extends BaseAdapter {

    Context context;
    ArrayList<Service> services;
    LayoutInflater inflater;
    Integer fragmentType;
    View oldView;

    int toInflate;

    private OnDeleteButtonClickListener deleteButtonClickListener;

    Event event;

    public static final Integer FRAGMENT_TYPE_SERVICE_LIST = 0;


    public ServiceAdapter(Context applicationContext, Service[] services,Integer fragmentType) {
        this.context = context;
        this.services = new ArrayList<>(Arrays.asList(services));
        this.fragmentType = fragmentType;
        inflater = (LayoutInflater.from(applicationContext));
        toInflate = -1;
    }

    public ServiceAdapter(Context applicationContext, Service[] services,Integer fragmentType, Event event, View v) {
        this.context = context;
        this.services = new ArrayList<>(Arrays.asList(services));
        this.fragmentType = fragmentType;
        inflater = (LayoutInflater.from(applicationContext));
        toInflate = -1;
        this.event = event;
        oldView = v;
    }

    public ServiceAdapter(Context context, Service[] services, Integer fragmentType, int toInflate) {
        this.context = context;
        this.services = new ArrayList<>(Arrays.asList(services));
        inflater = (LayoutInflater.from(context));
        this.fragmentType = fragmentType;
        this.toInflate = toInflate;
    }

    public ServiceAdapter(Context context, ArrayList<Service> services, Integer fragmentType, int toInflate) {
        this.context = context;
        this.services = services;
        inflater = (LayoutInflater.from(context));
        this.fragmentType = fragmentType;
        this.toInflate = toInflate;
    }

    public void setOnDeleteButtonClickListener(OnDeleteButtonClickListener listener) {
        this.deleteButtonClickListener = listener;
    }

    @Override
    public int getCount() { return services.size(); }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if(fragmentType == FRAGMENT_TYPE_SERVICE_LIST){
            view = inflater.inflate(R.layout.service_list_item_fragment, null);
            TextView name = (TextView) view.findViewById(R.id.service_item_name);
            TextView description = (TextView) view.findViewById(R.id.service_item_description);

            name.setText(services.get(i).getName());
            description.setText(String.valueOf(services.get(i).getDescription()));

        }
        //Ovako se moze koristiti jedan adapter za vise onih item layout, dodao sam da vam ne dira trenutno, al kad stignete mozete ovako uraditi
        //I iz nekog razloga nece switch case, tako da mora if-else
        if(toInflate != -1) {
            if(toInflate == R.layout.fragment_subcategory_list_item) {
                view = inflater.inflate(R.layout.fragment_subcategory_list_item, null);
                TextView name = (TextView) view.findViewById(R.id.subcategory_item_name);
                //TextView description = (TextView) view.findViewById(R.id.product_item_description);
                name.setText(services.get(i).getName());
                //description.setText(products[i].getDescription());
                ImageView delete = (ImageView) view.findViewById(R.id.subcategory_item_delete);
                delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (deleteButtonClickListener != null) {
                            deleteButtonClickListener.onDeleteClicked(i);
                        }
                    }
                });
            }
        }
        else{
            view = inflater.inflate(R.layout.fragment_product_service_list_item, null);
            TextView name = (TextView) view.findViewById(R.id.product_service_item_name);
            TextView price = (TextView) view.findViewById(R.id.product_service_item_price);

            name.setText(services.get(i).getName());
            price.setText(String.valueOf(services.get(i).getPrice()));

            LinearLayout l = view.findViewById(R.id.select_product_for_reservation);
            View finalView = view;
            l.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //logika za dalje
                    if(event != null)
                    {
                        if(event.getSelectedServices() == null)
                            event.setSelectedServices(new ArrayList<Service>());
                        event.getSelectedServices().add(services.get(i));
                        EventRepository erepo = new EventRepository();
                        erepo.updateEventItems(event.id, event, new EventUpdateCallback() {
                            @Override
                            public void onEventUpdated() {

                            }

                            @Override
                            public void onUpdateError(Exception e) {

                            }
                        });
                        Bundle bundle = new Bundle();
                        bundle.putParcelable("Event", event);
                        Navigation.findNavController(finalView).navigate(R.id.changeToServiceReservationEmployee, bundle);
                    }
                }
            });
        }

        return view;
    }

}
