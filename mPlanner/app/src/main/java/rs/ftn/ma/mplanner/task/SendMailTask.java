package rs.ftn.ma.mplanner.task;

import android.os.AsyncTask;

import javax.mail.MessagingException;

import rs.ftn.ma.mplanner.util.MailUtil;

public class SendMailTask extends AsyncTask<String, Void, Void> {
    @Override
    protected Void doInBackground(String... params) {
        String email = params[0];
        String subject = params[1];
        String body = params[2];

        MailUtil util = new MailUtil();
        try {
            util.sendMail(email, subject, body);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
