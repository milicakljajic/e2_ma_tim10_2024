package rs.ftn.ma.mplanner.repo;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import rs.ftn.ma.mplanner.interfaces.CommentCallback;
import rs.ftn.ma.mplanner.interfaces.CommentDeleteCallback;
import rs.ftn.ma.mplanner.interfaces.CommentInsertCallback;
import rs.ftn.ma.mplanner.model.Comment;

public class CommentRepository {
    public void insert(Comment comment, CommentInsertCallback callback) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("comments")
                .add(comment)
                .addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentReference> task) {
                        if (task.isSuccessful()) {
                            DocumentReference documentReference = task.getResult();
                            String commentId = documentReference.getId();
                            Log.d("REZ_DB", "DocumentSnapshot added with ID: " + commentId);
                            callback.onCommentInserted(commentId);
                        } else {
                            Log.w("REZ_DB", "Error adding document", task.getException());
                            callback.onInsertError(task.getException());
                        }
                    }
                });
    }

    public void select(CommentCallback callback) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("comments")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            List<Comment> comments = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Comment comment = document.toObject(Comment.class);
                                comment.setId(document.getId());
                                comments.add(comment);
                                Log.d("REZ_DB", document.getId() + " => " + document.getData());
                            }
                            callback.onCommentRecieved(comments.toArray(new Comment[0]));
                        } else {
                            Log.w("REZ_DB", "Error getting documents.", task.getException());
                            callback.onError(task.getException());
                        }
                    }
                });
    }



    public void selectByComp(String cname, CommentCallback callback) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("comments")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            List<Comment> comments = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Comment comment = document.toObject(Comment.class);
                                comment.setId(document.getId());
                                if(comment.getCompany().equals(cname))
                                    comments.add(comment);
                                Log.d("REZ_DB", document.getId() + " => " + document.getData());
                            }
                            callback.onCommentRecieved(comments.toArray(new Comment[0]));
                        } else {
                            Log.w("REZ_DB", "Error getting documents.", task.getException());
                            callback.onError(task.getException());
                        }
                    }
                });
    }

    public void selectById(CommentCallback callback, String id) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("comments").document(id);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot documentSnapshot = task.getResult();
                    if (documentSnapshot.exists()) {
                        Comment[] comment = new Comment[]{documentSnapshot.toObject(Comment.class)};
                        Log.d("REZ_DB", documentSnapshot.getId() + " => " + documentSnapshot.getData());
                        callback.onCommentRecieved(comment);
                    } else {
                        Log.d("REZ_DB", "No such document");
                        callback.onError(new Exception("No such document"));
                    }
                } else {
                    Log.w("REZ_DB", "Error getting document", task.getException());
                    callback.onError(task.getException());
                }
            }
        });
    }

    public void delete(String id, CommentDeleteCallback callback) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("comments").document(id);

        docRef.delete()
                .addOnSuccessListener(aVoid -> {
                    Log.d("REZ_DB", "Comment successfully deleted");
                    callback.onCommentDeleted();
                })
                .addOnFailureListener(e -> {
                    Log.w("REZ_DB", "Error deleting comment.", e);
                    callback.onDeleteError(e);
                });
    }
}
