package rs.ftn.ma.mplanner.repo;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import rs.ftn.ma.mplanner.interfaces.SubcategoryDeleteCallback;
import rs.ftn.ma.mplanner.interfaces.SubcategoryUpdateCallback;
import rs.ftn.ma.mplanner.interfaces.SubcategoryCallback;
import rs.ftn.ma.mplanner.interfaces.SubcategoryInsertCallback;
import rs.ftn.ma.mplanner.model.Subcategory;

public class SubcategoryRepository {
    public void insert(Subcategory sc, SubcategoryInsertCallback callback) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("subcategories")
                .add(sc)
                .addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentReference> task) {
                        if (task.isSuccessful()) {
                            DocumentReference documentReference = task.getResult();
                            String cId = documentReference.getId();
                            Log.d("REZ_DB", "DocumentSnapshot added with ID: " + cId);
                            callback.onSubcategoryInserted(cId);
                        } else {
                            Log.w("REZ_DB", "Error adding document", task.getException());
                            callback.onInsertError(task.getException());
                        }
                    }
                });
    }


    public void select(SubcategoryCallback callback) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("subcategories")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            List<Subcategory> subcategories = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Subcategory subcategory = document.toObject(Subcategory.class);
                                subcategory.setId(document.getId());
                                subcategories.add(subcategory);
                                Log.d("REZ_DB", document.getId() + " => " + document.getData());
                            }
                            callback.onSubcategoryReceived(subcategories.toArray(new Subcategory[0]));
                        } else {
                            Log.w("REZ_DB", "Error getting documents.", task.getException());
                            callback.onError(task.getException());
                        }
                    }
                });
    }


    public void update(String id, Subcategory c, SubcategoryUpdateCallback callback) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("subcategories").document(id);

        docRef.update("name", c.getName())
                .addOnSuccessListener(aVoid -> {
                    Log.d("REZ_DB", "Name successfully changed");
                    callback.onSubcategoryUpdated();
                })
                .addOnFailureListener(e -> {
                    Log.w("REZ_DB", "Error getting documents.", e);
                    callback.onUpdateError(e);
                });

        docRef.update("description", c.getDescription())
                .addOnSuccessListener(aVoid -> {
                    Log.d("REZ_DB", "Description successfully changed");
                    callback.onSubcategoryUpdated();
                })
                .addOnFailureListener(e -> {
                    Log.w("REZ_DB", "Error getting documents.", e);
                    callback.onUpdateError(e);
                });

        docRef.update("category", c.getCategory())
                .addOnSuccessListener(aVoid -> {
                    Log.d("REZ_DB", "Category successfully changed");
                    callback.onSubcategoryUpdated();
                })
                .addOnFailureListener(e -> {
                    Log.w("REZ_DB", "Error getting documents.", e);
                    callback.onUpdateError(e);
                });

        docRef.update("type", c.getType())
                .addOnSuccessListener(aVoid -> {
                    Log.d("REZ_DB", "Type successfully changed");
                    callback.onSubcategoryUpdated();
                })
                .addOnFailureListener(e -> {
                    Log.w("REZ_DB", "Error getting documents.", e);
                    callback.onUpdateError(e);
                });
    }


    public void delete(String id, SubcategoryDeleteCallback callback) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("subcategories").document(id);

        docRef.delete()
                .addOnSuccessListener(aVoid -> {
                    Log.d("REZ_DB", "Subategory successfully deleted");
                    callback.onSubcategoryDeleted();
                })
                .addOnFailureListener(e -> {
                    Log.w("REZ_DB", "Error deleting category.", e);
                    callback.onDeleteError(e);
                });
    }



    public void selectById(SubcategoryCallback callback, String id) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        DocumentReference docRef = db.collection("subcategories").document(id);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot documentSnapshot = task.getResult();
                    if (documentSnapshot.exists()) {
                        Subcategory[] sc = new Subcategory[]{documentSnapshot.toObject(Subcategory.class)};
                        Log.d("REZ_DB", documentSnapshot.getId() + " => " + documentSnapshot.getData());
                        callback.onSubcategoryReceived(sc);
                    } else {
                        Log.d("REZ_DB", "No such document");
                        callback.onError(new Exception("No such document"));
                    }
                } else {
                    Log.w("REZ_DB", "Error getting document", task.getException());
                    callback.onError(task.getException());
                }
            }
        });
    }
}
