package rs.ftn.ma.mplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import rs.ftn.ma.mplanner.model.enums.ConfirmationMethod;
import rs.ftn.ma.mplanner.model.enums.ProductState;

public class Service implements Parcelable {

    private Long id;
    private String name;
    private String description;
    private String specifics;
    private double price;
    private double discountedPrice;
    private int discount;
    private List<Integer> images;
    private double minDuration;
    private double maxDuration;
    private Category category;
    private Subcategory subcategory;
    private ProductState status;
    private List<EventType> eventTypes;
    private Boolean visible;
    private Boolean available;
    private List<Employee> employees;
    private double reservationDeadline;
    private double cancellationDeadline;
    private ConfirmationMethod method;
    private Boolean deleted;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSpecifics() {
        return specifics;
    }

    public void setSpecifics(String specifics) {
        this.specifics = specifics;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getDiscountedPrice() {
        return discountedPrice;
    }

    public void setDiscountedPrice(double discountedPrice) {
        this.discountedPrice = discountedPrice;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public List<Integer> getImages() {
        return images;
    }

    public void setImages(List<Integer> images) {
        this.images = images;
    }

    public double getMinDuration() {
        return minDuration;
    }

    public void setMinDuration(double minDuration) {
        this.minDuration = minDuration;
    }

    public double getMaxDuration() {
        return maxDuration;
    }

    public void setMaxDuration(double maxDuration) {
        this.maxDuration = maxDuration;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Subcategory getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(Subcategory subcategory) {
        this.subcategory = subcategory;
    }

    public ProductState getStatus() {
        return status;
    }

    public void setStatus(ProductState status) {
        this.status = status;
    }

    public List<EventType> getEventTypes() {
        return eventTypes;
    }

    public void setEventTypes(List<EventType> eventTypes) {
        this.eventTypes = eventTypes;
    }

    public Boolean getVisible() {
        return visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public double getReservationDeadline() {
        return reservationDeadline;
    }

    public void setReservationDeadline(double reservationDeadline) {
        this.reservationDeadline = reservationDeadline;
    }

    public double getCancellationDeadline() {
        return cancellationDeadline;
    }

    public void setCancellationDeadline(double cancellationDeadline) {
        this.cancellationDeadline = cancellationDeadline;
    }

    public ConfirmationMethod getMethod() {
        return method;
    }

    public void setMethod(ConfirmationMethod method) {
        this.method = method;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }


    protected Service(Parcel in) {
        id = in.readLong();
        name = in.readString();
        description = in.readString();
        specifics = in.readString();
        price = in.readDouble();
        discountedPrice = in.readDouble();
        discount = in.readInt();
        images = new ArrayList<>();
        in.readList(images, Integer.class.getClassLoader());
        minDuration = in.readDouble();
        maxDuration = in.readDouble();
        category = in.readParcelable(Category.class.getClassLoader());
        subcategory = in.readParcelable(Subcategory.class.getClassLoader());
        status = ProductState.valueOf(in.readString());
        eventTypes = new ArrayList<>();
        in.readList(eventTypes, EventType.class.getClassLoader());
        byte tmpVisible = in.readByte();
        visible = tmpVisible == 0 ? null : tmpVisible == 1;
        byte tmpAvailable = in.readByte();
        available = tmpAvailable == 0 ? null : tmpAvailable == 1;
        employees = new ArrayList<>();
        in.readList(employees, Employee.class.getClassLoader());
        reservationDeadline = in.readDouble();
        cancellationDeadline = in.readDouble();
        method = ConfirmationMethod.valueOf(in.readString());
        byte tmpDeleted = in.readByte();
        deleted = tmpDeleted == 0 ? null : tmpDeleted == 1;
    }
    public Service(Long id, String name, String description, String specifics, double price, double discountedPrice, int discount, List<Integer> images, double minDuration, double maxDuration, Category category, Subcategory subcategory, ProductState status, List<EventType> eventTypes, Boolean visible, Boolean available, List<Employee> employees, double reservationDeadline, double cancellationDeadline, ConfirmationMethod method, Boolean deleted) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.specifics = specifics;
        this.price = price;
        this.discountedPrice = discountedPrice;
        this.discount = discount;
        this.images = images;
        this.minDuration = minDuration;
        this.maxDuration = maxDuration;
        this.category = category;
        this.subcategory = subcategory;
        this.status = status;
        this.eventTypes = eventTypes;
        this.visible = visible;
        this.available = available;
        this.employees = employees;
        this.reservationDeadline = reservationDeadline;
        this.cancellationDeadline = cancellationDeadline;
        this.method = method;
        this.deleted = deleted;
    }

    public Service() {}


    public static final Creator<Service> CREATOR = new Creator<Service>() {
        @Override
        public Service createFromParcel(Parcel in) {
            return new Service(in);
        }

        @Override
        public Service[] newArray(int size) {
            return new Service[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(specifics);
        dest.writeDouble(price);
        dest.writeDouble(discountedPrice);
        dest.writeInt(discount);
        dest.writeList(images);
        dest.writeDouble(minDuration);
        dest.writeDouble(maxDuration);
        dest.writeParcelable(category, flags);
        dest.writeParcelable(subcategory, flags);
        dest.writeString(status.name());
        dest.writeList(eventTypes);
        dest.writeByte((byte) (visible == null ? 0 : visible ? 1 : 2));
        dest.writeByte((byte) (available == null ? 0 : available ? 1 : 2));
        dest.writeList(employees);
        dest.writeDouble(reservationDeadline);
        dest.writeDouble(cancellationDeadline);
        dest.writeString(method.name());
        dest.writeByte((byte) (deleted == null ? 0 : deleted ? 1 : 2));
    }
}
