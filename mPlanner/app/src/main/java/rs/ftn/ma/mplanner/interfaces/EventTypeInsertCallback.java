package rs.ftn.ma.mplanner.interfaces;

public interface EventTypeInsertCallback {
    void onEventTypeInserted(String eventId);
    void onInsertError(Exception e);
}