package rs.ftn.ma.mplanner.adapters;

import static androidx.constraintlayout.helper.widget.MotionEffect.TAG;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.model.EventProvider;
import rs.ftn.ma.mplanner.model.Pricelist;
import rs.ftn.ma.mplanner.model.enums.PricelistType;

public class PricelistAdapter extends RecyclerView.Adapter<PricelistAdapter.PricelistViewHolder> {
    private Context context;
    private List<Pricelist> pricelistList;

    public PricelistAdapter(Context context, List<Pricelist> pricelistList) {
        this.context = context;
        this.pricelistList = pricelistList;
    }

    @NonNull
    @Override
    public PricelistAdapter.PricelistViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pricelist, parent, false);
        return new PricelistAdapter.PricelistViewHolder(view, context, pricelistList);
    }


    @Override
    public void onBindViewHolder(@NonNull PricelistAdapter.PricelistViewHolder holder, int position) {
        Pricelist pricelist = pricelistList.get(position);
        holder.nameTextView.setText(pricelist.getName());
        holder.priceTextView.setText("Old price: " + String.valueOf(pricelist.getPrice()) + " $");
        holder.discountedPriceTextView.setText("New price: " + String.valueOf(pricelist.getDiscountPrice()) + " $");

        holder.bind(pricelist);
    }

    @Override
    public int getItemCount() {
        return pricelistList.size();
    }

    public class PricelistViewHolder extends RecyclerView.ViewHolder {
        public TextView nameTextView;
        public TextView priceTextView;
        public TextView discountedPriceTextView;
        public ImageView editButton;
        private Context context;
        private List<Pricelist> pricelistList;
        private Pricelist currentPricelist;

        public PricelistViewHolder(@NonNull View itemView, Context context, List<Pricelist> pricelistList) {
            super(itemView);
            this.context = context;
            this.pricelistList = pricelistList;
            nameTextView = itemView.findViewById(R.id.nameTextView);
            priceTextView = itemView.findViewById(R.id.priceTextView);
            discountedPriceTextView = itemView.findViewById(R.id.discountedPriceTextView);
            editButton = itemView.findViewById(R.id.editButton);

            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

            if (user != null) {
                findUserByEmail(user.getEmail(), new OnSuccessListener<EventProvider>() {
                    @Override
                    public void onSuccess(EventProvider foundUser) {
                        if (foundUser != null) {
                            editButton.setVisibility(View.VISIBLE);
                        }else {
                            editButton.setVisibility(View.GONE);
                        }
                    }
                });
            }


            editButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (currentPricelist != null &&
                            (currentPricelist.getType() == PricelistType.PRODUCT || currentPricelist.getType() == PricelistType.SERVICE)) {

                    }
                }
            });
        }

        public void bind(Pricelist pricelist) {
            currentPricelist = pricelist;
        }

        private void findUserByEmail(String email, OnSuccessListener<EventProvider> onSuccessListener) {
            FirebaseFirestore db = FirebaseFirestore.getInstance();
            CollectionReference usersRef = db.collection("event-providers");

            usersRef.whereEqualTo("email", email)
                    .get()
                    .addOnSuccessListener(queryDocumentSnapshots -> {
                        for (DocumentSnapshot document : queryDocumentSnapshots.getDocuments()) {
                            EventProvider foundUser = document.toObject(EventProvider.class);
                            onSuccessListener.onSuccess(foundUser);
                            return;
                        }
                        onSuccessListener.onSuccess(null);
                    })
                    .addOnFailureListener(e -> Log.w(TAG, "Error getting user.", e));
        }

    }
}
