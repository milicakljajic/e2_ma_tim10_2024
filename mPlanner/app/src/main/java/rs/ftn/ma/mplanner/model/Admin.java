package rs.ftn.ma.mplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Admin implements Parcelable {
    private String id;
    private String firebaseUserId;
    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private String address;
    private String phoneNumber;

    //TODO prebaci u blob kasnije

    private Boolean isEnabled;
    private String photoUrl;

    public Admin() {
    }

    public Admin(String id, String firebaseUserId, String email, String password, String firstName, String lastName, String address, String phoneNumber, Boolean isEnabled, String photoUrl) {
        this.id = id;
        this.firebaseUserId = firebaseUserId;
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.isEnabled = isEnabled;
        this.photoUrl = photoUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirebaseUserId() {
        return firebaseUserId;
    }

    public void setFirebaseUserId(String firebaseUserId) {
        this.firebaseUserId = firebaseUserId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Boolean getIsEnabled() {
        return isEnabled;
    }

    public void setIsEnabled(Boolean enabled) {
        isEnabled = enabled;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.firebaseUserId);
        dest.writeString(this.email);
        dest.writeString(this.password);
        dest.writeString(this.firstName);
        dest.writeString(this.lastName);
        dest.writeString(this.address);
        dest.writeString(this.phoneNumber);
        dest.writeValue(this.isEnabled);
        dest.writeString(this.photoUrl);
    }

    public void readFromParcel(Parcel source) {
        this.id = source.readString();
        this.firebaseUserId = source.readString();
        this.email = source.readString();
        this.password = source.readString();
        this.firstName = source.readString();
        this.lastName = source.readString();
        this.address = source.readString();
        this.phoneNumber = source.readString();
        this.isEnabled = (Boolean) source.readValue(Boolean.class.getClassLoader());
        this.photoUrl = source.readString();
    }

    protected Admin(Parcel in) {
        this.id = in.readString();
        this.firebaseUserId = in.readString();
        this.email = in.readString();
        this.password = in.readString();
        this.firstName = in.readString();
        this.lastName = in.readString();
        this.address = in.readString();
        this.phoneNumber = in.readString();
        this.isEnabled = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.photoUrl = in.readString();
    }

    public static final Parcelable.Creator<Admin> CREATOR = new Parcelable.Creator<Admin>() {
        @Override
        public Admin createFromParcel(Parcel source) {
            return new Admin(source);
        }

        @Override
        public Admin[] newArray(int size) {
            return new Admin[size];
        }
    };
}
