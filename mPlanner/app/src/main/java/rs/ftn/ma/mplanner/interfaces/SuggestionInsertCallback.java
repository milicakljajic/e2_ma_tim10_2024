package rs.ftn.ma.mplanner.interfaces;

public interface SuggestionInsertCallback {
    void onSuggestionInserted(String suggestionId);
    void onInsertError(Exception e);
}
