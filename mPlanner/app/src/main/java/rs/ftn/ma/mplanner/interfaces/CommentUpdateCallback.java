package rs.ftn.ma.mplanner.interfaces;

public interface CommentUpdateCallback {
    void onCommentUpdated();
    void onUpdateError(Exception e);
}
