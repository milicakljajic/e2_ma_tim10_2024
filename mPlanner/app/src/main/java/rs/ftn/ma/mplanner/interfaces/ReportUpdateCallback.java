package rs.ftn.ma.mplanner.interfaces;

public interface ReportUpdateCallback {
    void onReportUpdated();
    void onUpdateError(Exception e);
}
