package rs.ftn.ma.mplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import rs.ftn.ma.mplanner.model.enums.PricelistType;

public class Pricelist implements Parcelable{
    private Long id;
    private String name;
    private double price;
    private double discount;
    private double discountPrice;
    private PricelistType type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(double discountPrice) {
        this.discountPrice = discountPrice;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public PricelistType getType() { return type;}
    public void setType(PricelistType type) { this.type = type; }
    public Pricelist(){}


    public Pricelist(String name,  double price, double discount, double discountPrice, PricelistType type) {
        this.name = name;
        this.price = price;
        this.discount = discount;
        this.discountPrice = discountPrice;
        this.type = type;
    }

    protected Pricelist(Parcel in) {
        name = in.readString();
        price = in.readDouble();
        discount= in.readDouble();
        discountPrice= in.readDouble();
        type = PricelistType.valueOf(in.readString());
    }

    public static final Parcelable.Creator<Pricelist> CREATOR = new Parcelable.Creator<Pricelist>() {
        @Override
        public Pricelist createFromParcel(Parcel in) {
            return new Pricelist(in);
        }

        @Override
        public Pricelist[] newArray(int size) {
            return new Pricelist[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeDouble(price);
        dest.writeDouble(discount);
        dest.writeDouble(discountPrice);
        dest.writeString(type.name());
    }
}
