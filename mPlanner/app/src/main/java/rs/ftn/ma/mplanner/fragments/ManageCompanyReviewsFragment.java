package rs.ftn.ma.mplanner.fragments;

import android.app.DatePickerDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;

import org.w3c.dom.Comment;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.adapters.CommentAdapter;
import rs.ftn.ma.mplanner.interfaces.CommentCallback;
import rs.ftn.ma.mplanner.model.EventProvider;
import rs.ftn.ma.mplanner.repo.CommentRepository;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ManageCompanyReviewsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ManageCompanyReviewsFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "EventProvider";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private EventProvider mParam1;
    private String mParam2;

    private LinearLayout commentsLayout;

    // Primer liste komentara sa ocenama
    private List<Comment> commentsList = new ArrayList<>();

    private String from;
    private String to;

    public ManageCompanyReviewsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ManageCompanyReviewsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ManageCompanyReviewsFragment newInstance(EventProvider param1, String param2) {
        ManageCompanyReviewsFragment fragment = new ManageCompanyReviewsFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getParcelable(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_company_review_list, container, false);

        ListView n = v.findViewById(R.id.commentsCompanyList);
        CommentRepository crepo = new CommentRepository();
        crepo.selectByComp(mParam1.getCompany().getName(), new CommentCallback() {
            @Override
            public void onCommentRecieved(rs.ftn.ma.mplanner.model.Comment[] comments) {
                CommentAdapter ad = new CommentAdapter(v.getContext(), comments, mParam1);
                n.setAdapter(ad);
            }

            @Override
            public void onError(Exception e) {

            }
        });

        Button buttonStartDate = v.findViewById(R.id.company_review_list_from);
        TextView fromText = v.findViewById(R.id.company_review_list_from_text);
        TextView toText = v.findViewById(R.id.company_review_list_to_text);
        buttonStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(requireContext(),new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        month += 1;
                        String s = String.valueOf(dayOfMonth) + "." + String.valueOf(month) + "." + String.valueOf(year);
                        //text.setText(String.valueOf(year));
                        from = s;
                        fromText.setText(from);
                    }
                }, year, month, 15);
                datePickerDialog.show();
            }
        });

        Button buttonEndDate = v.findViewById(R.id.company_review_list_to);
        buttonEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(requireContext(),new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        month += 1;
                        String s = String.valueOf(dayOfMonth) + "." + String.valueOf(month) + "." + String.valueOf(year);
                        //newEmployee.getSchedule().setEndDate(s);
                        //text.setText(String.valueOf(year));
                        to = s;
                        toText.setText(to);
                    }
                }, year, month, 15);
                datePickerDialog.show();
            }
        });

        MaterialButton search = v.findViewById(R.id.company_review_list_search);
        MaterialButton reset = v.findViewById(R.id.company_reviews_reset);
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommentRepository crepo = new CommentRepository();
                crepo.selectByComp(mParam1.getCompany().getName(), new CommentCallback() {
                    @Override
                    public void onCommentRecieved(rs.ftn.ma.mplanner.model.Comment[] comments) {
                        CommentAdapter ad = new CommentAdapter(v.getContext(), comments, mParam1);
                        n.setAdapter(ad);
                    }

                    @Override
                    public void onError(Exception e) {

                    }
                });
            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CommentRepository crepo = new CommentRepository();
                crepo.selectByComp(mParam1.getCompany().getName(), new CommentCallback() {
                    @Override
                    public void onCommentRecieved(rs.ftn.ma.mplanner.model.Comment[] comments) {

                        ArrayList<rs.ftn.ma.mplanner.model.Comment> temp = new ArrayList<>();
                        for(rs.ftn.ma.mplanner.model.Comment c : comments)
                        {
                            Instant instant = c.getLastModified().toInstant();
                            LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
                            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy");

                            // Parsiramo string u LocalDate
                            LocalDate date1 = LocalDate.parse(from, formatter);
                            LocalDate date2 = LocalDate.parse(to, formatter);

                            LocalDateTime dateTime1 = date1.atStartOfDay();
                            LocalDateTime dateTime2 = date2.atStartOfDay();

                            if(dateTime1.isBefore(localDateTime) && dateTime2.isAfter(localDateTime))
                                temp.add(c);

                        }

                        CommentAdapter ad = new CommentAdapter(v.getContext(), temp);
                        n.setAdapter(ad);
                    }

                    @Override
                    public void onError(Exception e) {

                    }
                });
            }
        });
        return v;
    }
}