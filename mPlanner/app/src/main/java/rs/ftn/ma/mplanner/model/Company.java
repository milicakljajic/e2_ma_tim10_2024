package rs.ftn.ma.mplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Company implements Parcelable {

    private String name;
    private String email;
    private String address;
    private String phone;
    private String about;

    private Schedule schedule;

    public Company() {
        schedule = new Schedule();
    }

    public Company(String name, String email, String address, String phone, String about, Schedule schedule) {
        this.name = name;
        this.email = email;
        this.address = address;
        this.phone = phone;
        this.about = about;
        this.schedule = schedule;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.email);
        dest.writeString(this.address);
        dest.writeString(this.phone);
        dest.writeString(this.about);
        dest.writeParcelable(this.schedule, flags);
    }

    public void readFromParcel(Parcel source) {
        this.name = source.readString();
        this.email = source.readString();
        this.address = source.readString();
        this.phone = source.readString();
        this.about = source.readString();
        this.schedule = source.readParcelable(Schedule.class.getClassLoader());
    }

    protected Company(Parcel in) {
        this.name = in.readString();
        this.email = in.readString();
        this.address = in.readString();
        this.phone = in.readString();
        this.about = in.readString();
        this.schedule = in.readParcelable(Schedule.class.getClassLoader());
    }

    public static final Creator<Company> CREATOR = new Creator<Company>() {
        @Override
        public Company createFromParcel(Parcel source) {
            return new Company(source);
        }

        @Override
        public Company[] newArray(int size) {
            return new Company[size];
        }
    };
}
