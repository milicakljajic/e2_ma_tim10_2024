package rs.ftn.ma.mplanner.interfaces;

public interface CategoryUpdateCallback {
    void onCategoryUpdated();
    void onUpdateError(Exception e);
}
