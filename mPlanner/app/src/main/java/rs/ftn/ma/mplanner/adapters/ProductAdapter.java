package rs.ftn.ma.mplanner.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.interfaces.OnDeleteButtonClickListener;
import rs.ftn.ma.mplanner.model.Product;

public class ProductAdapter extends BaseAdapter {

    Context context;
    ArrayList<Product> products;
    LayoutInflater inflater;

    Integer fragmentType;
    int toInflate;

    private OnDeleteButtonClickListener deleteButtonClickListener;


    public static final Integer FRAGMENT_TYPE_PRODUCT_LIST = 0;
    //public static final Integer FRAGMENT_TYPE_PRODUCT_SERVICE_SUGGESTION = 1;


    public ProductAdapter(Context applicationContext, Product[] products,Integer fragmentType) {
        this.context = applicationContext;
        this.products = new ArrayList<Product>(Arrays.asList(products));
        this.fragmentType = fragmentType;
        inflater = (LayoutInflater.from(applicationContext));
        this.toInflate = -1;
    }

    public ProductAdapter(Context applicationContext, ArrayList<Product> products,Integer fragmentType) {
        this.context = applicationContext;
        this.products = products;
        this.fragmentType = fragmentType;
        inflater = (LayoutInflater.from(applicationContext));
        this.toInflate = -1;
    }

    public ProductAdapter(Context applicationContext, ArrayList<Product> products) {
        this.context = applicationContext;
        this.products = products;
    }

    public void setOnDeleteButtonClickListener(OnDeleteButtonClickListener listener) {
        this.deleteButtonClickListener = listener;
    }

    @Override
    public int getCount() { return products.size(); }

    @Override
    public Object getItem(int i) {
        return products.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if(fragmentType == FRAGMENT_TYPE_PRODUCT_LIST){
            view = inflater.inflate(R.layout.fragment_product_card, null);
            TextView name = (TextView) view.findViewById(R.id.product_item_name);
            TextView description = (TextView) view.findViewById(R.id.product_item_description);
            name.setText(products.get(i).getName());
            description.setText(products.get(i).getDescription());

        }
        //Ovako se moze koristiti jedan adapter za vise onih item layout, dodao sam da vam ne dira trenutno, al kad stignete mozete ovako uraditi
        //I iz nekog razloga nece switch case, tako da mora if-else
        if(toInflate != -1) {
            if(toInflate == R.layout.fragment_subcategory_list_item) {
                view = inflater.inflate(R.layout.fragment_subcategory_list_item, null);
                TextView name = (TextView) view.findViewById(R.id.subcategory_item_name);
                name.setText(products.get(i).getName());
                ImageView delete = (ImageView) view.findViewById(R.id.subcategory_item_delete);
                delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (deleteButtonClickListener != null) {
                            deleteButtonClickListener.onDeleteClicked(i);
                        }
                    }
                });
            }
        }
        else{
            view = inflater.inflate(R.layout.fragment_product_service_suggestion_list_item, null);
            TextView name = (TextView) view.findViewById(R.id.product_service_item_name);
            TextView price = (TextView) view.findViewById(R.id.product_service_item_price);
            CheckBox cb = view.findViewById(R.id.product_budget_select_item);
            cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    products.get(i).setSelected(isChecked);
                }
            });
            name.setText(products.get(i).getName());
            price.setText(String.valueOf(products.get(i).getPrice()));
        }

        return view;
    }

}
