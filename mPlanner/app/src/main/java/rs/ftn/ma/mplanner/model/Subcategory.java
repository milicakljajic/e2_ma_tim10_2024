package rs.ftn.ma.mplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Subcategory implements Parcelable {
    String id;
    Category category;
    String type;
    String name;
    String description;

    public Subcategory() {
        type = "";
        category = new Category();
    }

    public Subcategory(String id, rs.ftn.ma.mplanner.model.Category category, String type, String name, String description) {
        this.id = id;
        this.category = category;
        this.type = type;
        this.name = name;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public rs.ftn.ma.mplanner.model.Category getCategory() {
        return category;
    }

    public void setCategory(rs.ftn.ma.mplanner.model.Category category) {
        this.category = category;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    @Override
    public String toString() {
        return name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeParcelable(this.category, flags);
        dest.writeString(this.type);
        dest.writeString(this.name);
        dest.writeString(this.description);
    }

    public void readFromParcel(Parcel source) {
        this.id = source.readString();
        this.category = source.readParcelable(rs.ftn.ma.mplanner.model.Category.class.getClassLoader());
        this.type = source.readString();
        this.name = source.readString();
        this.description = source.readString();
    }

    protected Subcategory(Parcel in) {
        this.id = in.readString();
        this.category = in.readParcelable(rs.ftn.ma.mplanner.model.Category.class.getClassLoader());
        this.type = in.readString();
        this.name = in.readString();
        this.description = in.readString();
    }

    public static final Parcelable.Creator<Subcategory> CREATOR = new Parcelable.Creator<Subcategory>() {
        @Override
        public Subcategory createFromParcel(Parcel source) {
            return new Subcategory(source);
        }

        @Override
        public Subcategory[] newArray(int size) {
            return new Subcategory[size];
        }
    };
}
