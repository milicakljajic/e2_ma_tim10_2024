package rs.ftn.ma.mplanner.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import rs.ftn.ma.mplanner.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PupvHomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PupvHomeFragment extends Fragment {

    private FirebaseAuth mAuth;

    public PupvHomeFragment() {
        mAuth = FirebaseAuth.getInstance();
    }


    public static PupvHomeFragment newInstance() {
        PupvHomeFragment fragment = new PupvHomeFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pupv_home, container, false);

        CardView button1 = (CardView) view.findViewById(R.id.create_new_product);

        FirebaseUser cUser = mAuth.getCurrentUser();

        button1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ProductCreationFragment createProductFragment = new ProductCreationFragment();
                FragmentTransaction transaction = requireActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_container, createProductFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        CardView button2 = (CardView) view.findViewById(R.id.create_new_package);

        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PackageCreationFragment createPackageFragment = new PackageCreationFragment();
                FragmentTransaction transaction = requireActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_container, createPackageFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        CardView button3 = (CardView) view.findViewById(R.id.create_new_service);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ServiceCreationFragment createPackageFragment = new ServiceCreationFragment();
                FragmentTransaction transaction = requireActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_container, createPackageFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        CardView button4 = (CardView) view.findViewById(R.id.display_all_services);

        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ServicesListFragment allServicesFragment = new ServicesListFragment();
                FragmentTransaction transaction = requireActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_container, allServicesFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });


        CardView button5 = (CardView) view.findViewById(R.id.display_all_products);

        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProductListFragment allServicesFragment = new ProductListFragment();
                FragmentTransaction transaction = requireActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_container, allServicesFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        CardView button6 = (CardView) view.findViewById(R.id.display_all_packages);

        button6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PackageListFragment allServicesFragment = new PackageListFragment();
                FragmentTransaction transaction = requireActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_container, allServicesFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        CardView button7 = (CardView) view.findViewById(R.id.pupvNotificationButton);
        button7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NotificationFragment notificationFragment = new NotificationFragment();
                FragmentTransaction transaction = requireActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_container, notificationFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        CardView button8 = (CardView) view.findViewById(R.id.pricelistButton);
        button8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PricelistFragment pricelistFragment = new PricelistFragment();
                FragmentTransaction transaction = requireActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_container, pricelistFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        CardView button10 = (CardView) view.findViewById(R.id.service_reservations);
        button10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToAllServiceReservations();
            }
        });

        CardView button11 = (CardView) view.findViewById(R.id.package_reservations);
        button11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToAllPackageReservations();
            }
        });

        CardView button9 = (CardView) view.findViewById(R.id.logout);
        button9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToLogOut();
            }
        });

        return view;
    }

    private void navigateToLogOut() {
        FirebaseAuth.getInstance().signOut();

        SharedPreferences sharedPreferences = requireActivity().getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove("userToken");
        editor.apply();

        LoginFragment homePageFragment = new LoginFragment();
        FragmentTransaction transaction = requireActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, homePageFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void navigateToAllServiceReservations(){
       /* AllServiceReservationsFragment allServiceReservationsFragment = new AllServiceReservationsFragment();
        FragmentTransaction transaction = requireActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, allServiceReservationsFragment);
        transaction.addToBackStack(null);
        transaction.commit();*/
    }

    private void navigateToAllPackageReservations(){
      /*  AllPackageReservationsFragment allPackageReservationsFragment = new AllPackageReservationsFragment();
        FragmentTransaction transaction = requireActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, allPackageReservationsFragment);
        transaction.addToBackStack(null);
        transaction.commit();*/
    }
}