package rs.ftn.ma.mplanner.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.adapters.CategoryAdapter;
import rs.ftn.ma.mplanner.interfaces.CategoryCallback;
import rs.ftn.ma.mplanner.interfaces.CategoryDeleteCallback;
import rs.ftn.ma.mplanner.interfaces.OnDeleteButtonClickListener;
import rs.ftn.ma.mplanner.model.Category;
import rs.ftn.ma.mplanner.repo.CategoryRepository;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CategoryListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CategoryListFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private CategoryRepository repo;

    public CategoryListFragment() {
        // Required empty public constructor
        repo = new CategoryRepository();
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CategoryListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CategoryListFragment newInstance(String param1, String param2) {
        CategoryListFragment fragment = new CategoryListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_category_list, container, false);

        repo.select(new CategoryCallback() {
            @Override
            public void onCategoryReceived(Category[] categories) {
                ListView lw = view.findViewById(R.id.category_list_listview);
                ArrayList<Category> cat = new ArrayList<>(Arrays.asList(categories));
                CategoryAdapter adapter = new CategoryAdapter(requireContext(), cat);
                adapter.setOnDeleteButtonClickListener(new OnDeleteButtonClickListener() {
                    @Override
                    public void onDeleteClicked(int position) {
                        //currentType.removeService(position);
                        Category temp = cat.get(position);
                        repo.delete(temp.getId(), new CategoryDeleteCallback() {
                            @Override
                            public void onCategoryDeleted() {

                            }

                            @Override
                            public void onDeleteError(Exception e) {

                            }
                        });
                        cat.remove(position);
                        adapter.notifyDataSetChanged();
                    }
                });
                lw.setOnItemClickListener((parent, view1, position, id) -> {
                    Category temp = categories[position];
                    Bundle bundle = new Bundle();
                    bundle.putString("mode", "update");
                    bundle.putString("id", temp.getId());
                    Navigation.findNavController(view1).navigate(R.id.changeToCategoryCreation2,bundle);
                });
                lw.setAdapter(adapter);
            }

            @Override
            public void onError(Exception e) {

            }
        });
        return view;
    }
}