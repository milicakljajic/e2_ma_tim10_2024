package rs.ftn.ma.mplanner.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Date;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.interfaces.EventOrganizerInsertCallback;
import rs.ftn.ma.mplanner.interfaces.EventProviderInsertCallback;
import rs.ftn.ma.mplanner.interfaces.NotificationInsertCallback;
import rs.ftn.ma.mplanner.model.EventOrganizer;
import rs.ftn.ma.mplanner.model.EventProvider;
import rs.ftn.ma.mplanner.model.Notification;
import rs.ftn.ma.mplanner.repo.EventOrganizerRepo;
import rs.ftn.ma.mplanner.repo.EventProviderRepository;
import rs.ftn.ma.mplanner.repo.NotificationRepository;

public class RegistrationPhotoFragment extends Fragment {

    private static final String ARG_PARAM1 = "EventOrganizer";
    private static final String ARG_PARAM2 = "EventProvider";
    private EventOrganizer newEventOrganizer;

    private EventProvider newEventProvider;
    private final FirebaseAuth mAuth;
    private final EventOrganizerRepo repo;
    private final EventProviderRepository eprepo;

    private final NotificationRepository nrepo;

    public RegistrationPhotoFragment() {
        // Required empty public constructor
        mAuth = FirebaseAuth.getInstance();
        repo = new EventOrganizerRepo();
        eprepo = new EventProviderRepository();
        nrepo = new NotificationRepository();
    }


    public static RegistrationPhotoFragment newInstance(EventOrganizer newEventOrganizer, EventProvider newEventProvider) {
        RegistrationPhotoFragment fragment = new RegistrationPhotoFragment();
        Bundle args = new Bundle();
        args.putParcelable("EventOrganizer", newEventOrganizer);
        args.putParcelable("EventProvider", newEventProvider);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if(getArguments().getParcelable(ARG_PARAM1) != null)
                newEventOrganizer = getArguments().getParcelable(ARG_PARAM1);
            if(getArguments().getParcelable(ARG_PARAM2) != null)
                newEventProvider = getArguments().getParcelable(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragment_registration_photo, container, false);

        //Button button = (Button) view.findViewById(R.id.login_registration);
        Button button = (Button) view.findViewById(R.id.registration_photo_finish);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //EVENT ORGANIZER
                if(newEventOrganizer != null) {
                    //FIXME LATER - Slika da bude upload
                    if(true){
                        newEventOrganizer.setPhotoUrl("TODO");
                        newEventOrganizer.setIsEnabled(false);
                        //TODO HASH LOZINKU
                        mAuth.createUserWithEmailAndPassword(newEventOrganizer.getEmail(), newEventOrganizer.getPassword())
                                .addOnSuccessListener(requireActivity(), new OnSuccessListener<AuthResult>() {
                                    @Override
                                    public void onSuccess(AuthResult authResult) {
                                        // Sign in success, update UI with the signed-in user's information
                                        FirebaseUser user = mAuth.getCurrentUser();
                                        if(user != null) {
                                            newEventOrganizer.setFirebaseUserId(user.getUid());
                                            newEventOrganizer.setIsEnabled(false);
                                            repo.insert(newEventOrganizer, new EventOrganizerInsertCallback() {
                                                @Override
                                                public void onEventOrganizerInserted(String eoId) {
                                                    Toast.makeText(getActivity(), "Successfully registered", Toast.LENGTH_SHORT).show();
                                                    user.sendEmailVerification();
                                                    Navigation.findNavController(view).navigate(R.id.changeToLogin2);
                                                }

                                                @Override
                                                public void onInsertError(Exception e) {

                                                }
                                            });

                                        }
                                    }
                                })
                                .addOnFailureListener(requireActivity(), new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        // If sign in fails, display a message to the user.
                                        // updateUI(null);
                                    }
                                });
                        //Navigation.findNavController(view).navigate(R.id.changeToLogin2);
                    }
                }

                //EVENT PROVIDER (PUP-V)
                if(newEventProvider != null) {
                    //FIXME LATER - Slika da bude upload
                    if(true){
                        newEventProvider.setPhotoUrl("TODO");
                        newEventProvider.setIsEnabled(false);
                        //TODO HASH LOZINKU
                        mAuth.createUserWithEmailAndPassword(newEventProvider.getEmail(), newEventProvider.getPassword())
                                .addOnSuccessListener(requireActivity(), new OnSuccessListener<AuthResult>() {
                                    @Override
                                    public void onSuccess(AuthResult authResult) {
                                        // Sign in success, update UI with the signed-in user's information
                                        FirebaseUser user = mAuth.getCurrentUser();
                                        if(user != null) {
                                            newEventProvider.setFirebaseUserId(user.getUid());
                                            newEventProvider.setIsEnabled(false);
                                            newEventProvider.setLastModified(new Date());
                                            newEventProvider.setApproved(false);
                                            eprepo.insert(newEventProvider, new EventProviderInsertCallback() {
                                                @Override
                                                public void onEventProviderInserted(String eoId) {
                                                    Toast.makeText(getActivity(), "Successfully registered", Toast.LENGTH_SHORT).show();
                                                    user.sendEmailVerification();

                                                    //Zakucano za jednog jedinog admina
                                                    nrepo.insert(new Notification("GkoDer6HeafPJ4sRSZjirj4VPdk2", "New event provider registered", newEventProvider.getEmail(), false), new NotificationInsertCallback() {
                                                        @Override
                                                        public void onNotificationInserted(String notificationId) {
                                                            Toast.makeText(getActivity(), "Successfully registered", Toast.LENGTH_SHORT).show();
                                                            Navigation.findNavController(view).navigate(R.id.changeToLogin2);
                                                        }

                                                        @Override
                                                        public void onInsertError(Exception e) {

                                                        }
                                                    });

                                                }

                                                @Override
                                                public void onInsertError(Exception e) {

                                                }
                                            });

                                        }
                                    }
                                })
                                .addOnFailureListener(requireActivity(), new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        // If sign in fails, display a message to the user.
                                        // updateUI(null);
                                    }
                                });
                    }
                }
            }
        });

        button = (Button) view.findViewById(R.id.registration_photo_skip);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Navigation.findNavController(view).navigate(R.id.changeToMainActivity1);
            }
        });
        return view;
    }
}