package rs.ftn.ma.mplanner.interfaces;

import rs.ftn.ma.mplanner.model.Report;

public interface ReportCallback {
    void onReportRecieved(Report[] reports);
    void onError(Exception e);
}
