package rs.ftn.ma.mplanner.interfaces;

import rs.ftn.ma.mplanner.model.Activity;

public interface ActivityCallback {
    void onActivityReceived(Activity[] activities);
    void onError(Exception e);
}