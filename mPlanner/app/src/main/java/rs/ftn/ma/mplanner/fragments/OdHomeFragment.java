package rs.ftn.ma.mplanner.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.activities.AuthActivity;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link OdHomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OdHomeFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    FirebaseAuth mAuth;
    public OdHomeFragment() {
        // Required empty public constructor
        mAuth = FirebaseAuth.getInstance();
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment OdHomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static OdHomeFragment newInstance(String param1, String param2) {
        OdHomeFragment fragment = new OdHomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_od_home, container, false);

        CardView button = (CardView) view.findViewById(R.id.create_event_button);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Navigation.findNavController(view).navigate(R.id.changeToCreationEventFragment);
            }
        });

        CardView button2 = (CardView) view.findViewById(R.id.product_service_suggestion_button);

        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Navigation.findNavController(view).navigate(R.id.changeToSuggestionFragment2);
            }
        });

        CardView button3 = (CardView) view.findViewById(R.id.plan_budget_button);

        button3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Navigation.findNavController(view).navigate(R.id.changeToBudgetPlanFragment3);
            }
        });

        CardView button4 = (CardView) view.findViewById(R.id.product_service_search_button);

        button4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Navigation.findNavController(view).navigate(R.id.achangeToProductServiceSearchFragment);
            }
        });

        CardView button5 = (CardView) view.findViewById(R.id.eo_logout);

        button5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Navigation.findNavController(view).navigate(R.id.achangeToProductServiceSearchFragment);
                mAuth.signOut();
                Intent myIntent = new Intent(requireContext(), AuthActivity.class);
                startActivity(myIntent);
            }
        });


        CardView button22 = (CardView) view.findViewById(R.id.od_profile_button);

        button22.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Navigation.findNavController(view).navigate(R.id.changeToOdProfileFragment);
            }
        });


        CardView button23 = (CardView) view.findViewById(R.id.eo_company);

        button23.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("mode", "create");
                Navigation.findNavController(view).navigate(R.id.changeToCompanyDisplay1, bundle);
            }
        });

        CardView button24 = (CardView) view.findViewById(R.id.eo_company_read);

        button24.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("mode", "read");
                Navigation.findNavController(view).navigate(R.id.changeToCompanyDisplay1, bundle);
            }
        });


        CardView button25 = (CardView) view.findViewById(R.id.create_guest_list_button);

        button25.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Navigation.findNavController(view).navigate(R.id.changeToCreationGuestListFragment);
            }
        });

        CardView button26 = (CardView) view.findViewById(R.id.create_event_agenda_button);

        button26.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Navigation.findNavController(view).navigate(R.id.changeToEventAgendaFragment);
            }
        });


        CardView button8 = (CardView) view.findViewById(R.id.eo_product);
        button8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(view).navigate(R.id.eoScheduleProduct);
            }
        });

        CardView button9 = (CardView) view.findViewById(R.id.eo_notifications);
        button9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(view).navigate(R.id.changeToOdNotifications);
            }
        });


        CardView button10 = (CardView) view.findViewById(R.id.eo_service);
        button10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(view).navigate(R.id.eoScheduleService);
            }
        });
        return view;
    }
}