package rs.ftn.ma.mplanner.interfaces;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import rs.ftn.ma.mplanner.notifications.PushNotification;

public interface NotificationApi {
    String CONTENT_TYPE = "application/json";
    String API_KEY = "AAAAo:A91bFOoY9cMecyYpHCtNeSCXLluRuS8q4ySOOT1GeK9d5jUXmEKuJL5m9qZG0LeCPy-V43zm-Gr3l94WjdqdvzkEIiux45pFrpEFDlBgZYNGPiDkO7ZO82K88_NADMoz4_U7";

    @Headers({"Authorization: key=" + API_KEY, "Content-Type: " + CONTENT_TYPE})
    @POST("fcm/send")
    Call<ResponseBody> postNotification(
            @Body PushNotification notification
    );
}
