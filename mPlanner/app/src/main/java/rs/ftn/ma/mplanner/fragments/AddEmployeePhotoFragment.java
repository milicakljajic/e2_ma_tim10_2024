package rs.ftn.ma.mplanner.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.interfaces.EmployeeInsertCallback;
import rs.ftn.ma.mplanner.interfaces.EventProviderCallback;
import rs.ftn.ma.mplanner.model.Employee;
import rs.ftn.ma.mplanner.model.EventProvider;
import rs.ftn.ma.mplanner.repo.EmployeeRepository;
import rs.ftn.ma.mplanner.repo.EventProviderRepository;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AddEmployeePhotoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddEmployeePhotoFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "Employee";

    FirebaseAuth mAuth;
    EmployeeRepository repo;
    EventProviderRepository prepo;
    private Employee newEmployee;
    private EventProvider provider;

    public AddEmployeePhotoFragment() {
        // Required empty public constructor
        mAuth = FirebaseAuth.getInstance();
        repo = new EmployeeRepository();
        prepo = new EventProviderRepository();
    }

    // TODO: Rename and change types and number of parameters
    public static AddEmployeePhotoFragment newInstance(Employee newEmployee) {
        AddEmployeePhotoFragment fragment = new AddEmployeePhotoFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, newEmployee);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            newEmployee = getArguments().getParcelable(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_employee_photo, container, false);


        Button button = (Button) view.findViewById(R.id.ae_photo_skip);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Navigation.findNavController(view).navigate(R.id.changeToPupvHomeFragment4);
            }
        });
        FirebaseUser cUser= mAuth.getCurrentUser();
        prepo.selectByFirebaseId(new EventProviderCallback() {
            @Override
            public void onEventProviderReceived(EventProvider[] providers) {
                provider = providers[0];
            }

            @Override
            public void onError(Exception e) {

            }
        }, cUser.getUid());


        button = (Button) view.findViewById(R.id.ae_photo_finish);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (newEmployee != null) {
                    //FIXME LATER - Slika da bude upload
                    if (true) {
                        newEmployee.setPhotoUrl("TODO");
                        newEmployee.setIsEnabled(false);
                        newEmployee.setCompany(provider.getCompany());
                        //TODO HASH LOZINKU
                        mAuth.createUserWithEmailAndPassword(newEmployee.getEmail(), newEmployee.getPassword())
                                .addOnSuccessListener(requireActivity(), new OnSuccessListener<AuthResult>() {
                                    @Override
                                    public void onSuccess(AuthResult authResult) {
                                        // Sign in success, update UI with the signed-in user's information
                                        FirebaseUser user = mAuth.getCurrentUser();
                                        if (user != null) {
                                            newEmployee.setFirebaseUserId(user.getUid());
                                            newEmployee.setIsEnabled(false);
                                            repo.insert(newEmployee, new EmployeeInsertCallback() {
                                                @Override
                                                public void onEmployeeInserted(String eoId) {
                                                    Toast.makeText(getActivity(), "Successfully created employee", Toast.LENGTH_SHORT).show();
                                                    user.sendEmailVerification();
                                                }

                                                @Override
                                                public void onInsertError(Exception e) {

                                                }
                                            });

                                        }
                                    }
                                })
                                .addOnFailureListener(requireActivity(), new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        // If sign in fails, display a message to the user.
                                        // updateUI(null);
                                    }
                                });
                        //Navigation.findNavController(view).navigate(R.id.changeToPupvHomeFragment4);
                    }
                }
            }
        });

        return view;
    }
}