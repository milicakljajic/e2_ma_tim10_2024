package rs.ftn.ma.mplanner.interfaces;

public interface EventProviderUpdateCallback {
    void onEventProviderUpdated();
    void onUpdateError(Exception e);
}
