package rs.ftn.ma.mplanner.fragments;

import static androidx.constraintlayout.helper.widget.MotionEffect.TAG;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.adapters.EventTypeRemoveAdapter;
import rs.ftn.ma.mplanner.adapters.ProductAdapter;
import rs.ftn.ma.mplanner.adapters.SubcategoryAdapter;
import rs.ftn.ma.mplanner.interfaces.SubcategoryCallback;
import rs.ftn.ma.mplanner.model.EventType;
import rs.ftn.ma.mplanner.model.Product;
import rs.ftn.ma.mplanner.model.Subcategory;
import rs.ftn.ma.mplanner.repo.SubcategoryRepository;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EditProductFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EditProductFragment extends Fragment {

    private Spinner subcategorySpinner;
    private Product product;
    private ProductAdapter adapter;
    private List<Product> productList;
    private Subcategory selectedSubcategory;
    private List<EventType> eventTypesProduct;
    private RecyclerView eventTypesAddRecyclerView;
    private RecyclerView eventTypesRecyclerView;

    private SubcategoryRepository subcategoryRepository;
    public EditProductFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        eventTypesProduct = new ArrayList<>();
        productList = new ArrayList<Product>();
        selectedSubcategory = new Subcategory();
        subcategoryRepository = new SubcategoryRepository();
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("products")
                .whereEqualTo("deleted", false)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Product product = document.toObject(Product.class);
                                productList.add(product);
                            }
                            adapter.notifyDataSetChanged();
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });

    }
    public static EditProductFragment newInstance(Product product) {
        EditProductFragment fragment = new EditProductFragment();
        Bundle args = new Bundle();
        args.putParcelable("product", product);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_edit_product, container, false);
        subcategorySpinner = view.findViewById(R.id.spinner2);

        adapter = new ProductAdapter(getContext(), (ArrayList<Product>) productList);
        Bundle args = getArguments();
        if (args != null) {
            product = args.getParcelable("product");

            if (product != null) {
                subcategoryRepository.select(new SubcategoryCallback() {
                    @Override
                    public void onSubcategoryReceived(Subcategory[] subcategories) {
                        ArrayList<Subcategory> subs = new ArrayList<Subcategory>(Arrays.asList(subcategories));
                        SubcategoryAdapter adapter = new SubcategoryAdapter(requireContext(), subs);
                        subcategorySpinner.setAdapter(adapter);

                        int initialSelection = -1;
                        for (int i = 0; i < subs.size(); i++) {
                            if (subs.get(i).getId().toString().equals(product.getSubcategoryId())) {
                                initialSelection = i;
                                break;
                            }
                        }

                        if (initialSelection != -1) {
                            subcategorySpinner.setSelection(initialSelection);
                        }
                    }

                    @Override
                    public void onError(Exception e) {

                    }
                });

                subcategorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        selectedSubcategory = (Subcategory) parent.getItemAtPosition(position);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }

        }

        eventTypesRecyclerView = view.findViewById(R.id.eventTypesRecyclerView);
        EventTypeRemoveAdapter adapter = new EventTypeRemoveAdapter(eventTypesProduct, this);
        eventTypesRecyclerView.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        eventTypesRecyclerView.setLayoutManager(layoutManager);
        adapter.notifyDataSetChanged();



        if (args != null) {
            product = args.getParcelable("product");
            if (product != null) {
                EditText enterNameEditText = view.findViewById(R.id.enterNameEditText);
                EditText enterDescriptionEditText = view.findViewById(R.id.enterDescriptionEditText);
                EditText enterPriceEditText = view.findViewById(R.id.enterPriceEditText);
                EditText enterDiscountEditText = view.findViewById(R.id.enterDiscountEditText);
                enterNameEditText.setText(product.getName());
                enterDescriptionEditText.setText(product.getDescription());
                enterPriceEditText.setText(String.valueOf(product.getPrice()));
                enterDiscountEditText.setText(String.valueOf(product.getDescription()));
                RadioGroup availableRadioGroup = view.findViewById(R.id.availableRadioGroup);
                if (product.getAvailable()) {
                    availableRadioGroup.check(R.id.yesRadioButton);
                } else {
                    availableRadioGroup.check(R.id.noRadioButton);
                }
                RadioGroup visibleRadioGroup = view.findViewById(R.id.visibleRadioGroup);
                if (product.getAvailable()) {
                    visibleRadioGroup.check(R.id.visibleYesRadioButton);
                } else {
                    visibleRadioGroup.check(R.id.visibleNoRadioButton);
                }
            }
        }

        Button editButton = view.findViewById(R.id.editButton);
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText enterNameEditText = view.findViewById(R.id.enterNameEditText);
                EditText enterDescriptionEditText = view.findViewById(R.id.enterDescriptionEditText);
                EditText enterPriceEditText = view.findViewById(R.id.enterPriceEditText);
                EditText enterDiscountEditText = view.findViewById(R.id.enterDiscountEditText);
                RadioGroup availableRadioGroup = view.findViewById(R.id.availableRadioGroup);
                RadioGroup visibleRadioGroup = view.findViewById(R.id.visibleRadioGroup);
                String title = enterNameEditText.getText().toString();
                String description = enterDescriptionEditText.getText().toString();
                double price = Double.parseDouble(enterPriceEditText.getText().toString());
                int discount = Integer.parseInt(enterDiscountEditText.getText().toString());
                boolean available = availableRadioGroup.getCheckedRadioButtonId() == R.id.yesRadioButton;
                boolean visible = visibleRadioGroup.getCheckedRadioButtonId() == R.id.visibleYesRadioButton;

                product.setName(title);
                product.setDescription(description);
                product.setPrice(price);
                product.setDiscountPercentage(discount);
                product.setAvailable(available);
                product.setAvailable(visible);
                //product.sete(eventTypesProduct);

                updateProduct(product);

            }
        });

        return view;
    }

    private void updateProduct(Product product) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        CollectionReference productsRef = db.collection("products");

        productsRef.whereEqualTo("id", product.getId())
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        for (DocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                            // Pronađen je dokument sa zadatim id-em, ažuriramo ga
                            String documentId = documentSnapshot.getId();
                            DocumentReference productRef = db.collection("products").document(documentId);

                            productRef.update(
                                            "title", product.getName(),
                                            "description", product.getDescription(),
                                            "price", product.getPrice(),
                                            "discount", product.getDiscountPercentage(),
                                            "available", product.getAvailable(),
                                            "visible", product.getAvailable(),
                                            "subcategory", product.getSubcategoryId())
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            adapter.notifyDataSetChanged();

                                            Log.d(TAG, "DocumentSnapshot successfully updated!");
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {                                        @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Log.w(TAG, "Error updating document", e);
                                    }
                                    });
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error querying documents", e);
                    }
                });
    }

    private void loadEventTypesToAdd() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        CollectionReference eventTypesRef = db.collection("eventTypes");

        eventTypesRef.get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                List<EventType> allEventTypes = new ArrayList<>();
                for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                    EventType eventType = documentSnapshot.toObject(EventType.class);
                    allEventTypes.add(eventType);
                }

                List<EventType> eventTypesToAdd = new ArrayList<>();
                for (EventType eventType : allEventTypes) {
                    boolean found = false;
                    for (EventType pp : eventTypesProduct) {
                        if (pp.getId().equals(eventType.getId())) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        eventTypesToAdd.add(eventType);
                    }
                }

            }
        });
    }
}