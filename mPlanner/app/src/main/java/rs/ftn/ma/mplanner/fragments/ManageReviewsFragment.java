package rs.ftn.ma.mplanner.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.interfaces.ReportCallback;
import rs.ftn.ma.mplanner.model.EventProvider;
import rs.ftn.ma.mplanner.model.Report;
import rs.ftn.ma.mplanner.repo.ReportRepository;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ManageReviewsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ManageReviewsFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "EventProvider";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private EventProvider mParam1;
    private String mParam2;

    public ManageReviewsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ManageReviewsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ManageReviewsFragment newInstance(EventProvider param1, String param2) {
        ManageReviewsFragment fragment = new ManageReviewsFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getParcelable(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_manage_reviews, container, false);

        ListView n = v.findViewById(R.id.commentsNestedListView);
        ReportRepository rrepo = new ReportRepository();
        rrepo.select(new ReportCallback() {
            @Override
            public void onReportRecieved(Report[] reports) {
               // ReportAdapter ad = new ReportAdapter(v.getContext(), reports);
               // n.setAdapter(ad);
            }

            @Override
            public void onError(Exception e) {

            }
        });
        return v;
    }
}