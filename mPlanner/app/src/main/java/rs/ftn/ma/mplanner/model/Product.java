package rs.ftn.ma.mplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Product implements Parcelable {

    private Integer Id;
    private String Name;
    private String Description;
    private Double Price;


    private String CategoryId;

    private String SubcategoryId;

    private Integer DiscountPercentage;
    private Boolean IsAvailable;

    private Boolean IsSelected;

    public Product(){}

    public Product(Integer id, String name, String description, Double price, String category, String subcategory, Integer discountPercentage, Boolean isAvailable) {
        Id = id;
        Name = name;
        Description = description;
        Price = price;
        CategoryId = category;
        SubcategoryId = subcategory;
        DiscountPercentage = discountPercentage;
        IsAvailable = isAvailable;
    }

    public Product(Integer id, String name, String description, Double price, String category, String subcategory, Integer discountPercentage, Boolean isAvailable, Boolean isSelected) {
        Id = id;
        Name = name;
        Description = description;
        Price = price;
        CategoryId = category;
        SubcategoryId = subcategory;
        DiscountPercentage = discountPercentage;
        IsAvailable = isAvailable;
        IsSelected = isSelected;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public Double getPrice() {
        return Price;
    }

    public void setPrice(Double price) {
        Price = price;
    }

    public String getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(String categoryId) {
        CategoryId = categoryId;
    }

    public String getSubcategoryId() {
        return SubcategoryId;
    }

    public void setSubcategoryId(String subcategoryId) {
        SubcategoryId = subcategoryId;
    }

    public Integer getDiscountPercentage() {
        return DiscountPercentage;
    }

    public void setDiscountPercentage(Integer discountPercentage) {
        DiscountPercentage = discountPercentage;
    }

    public Boolean getAvailable() {
        return IsAvailable;
    }

    public void setAvailable(Boolean available) {
        IsAvailable = available;
    }

    public Boolean getSelected() {
        return IsSelected;
    }

    public void setSelected(Boolean selected) {
        IsSelected = selected;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.Id);
        dest.writeString(this.Name);
        dest.writeString(this.Description);
        dest.writeValue(this.Price);
        //dest.writeString(this.Category);
        //dest.writeString(this.Subcategory);
        dest.writeString(this.CategoryId);
        dest.writeString(this.SubcategoryId);
        dest.writeValue(this.DiscountPercentage);
        dest.writeValue(this.IsAvailable);
    }

    public void readFromParcel(Parcel source) {
        this.Id = (Integer) source.readValue(Integer.class.getClassLoader());
        this.Name = source.readString();
        this.Description = source.readString();
        this.Price = (Double) source.readValue(Double.class.getClassLoader());
        //this.Category = source.readString();
        //this.Subcategory = source.readString();
        this.CategoryId = source.readString();
        this.SubcategoryId = source.readString();
        this.DiscountPercentage = (Integer) source.readValue(Integer.class.getClassLoader());
        this.IsAvailable = (Boolean) source.readValue(Boolean.class.getClassLoader());
    }

    protected Product(Parcel in) {
        this.Id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.Name = in.readString();
        this.Description = in.readString();
        this.Price = (Double) in.readValue(Double.class.getClassLoader());
        //this.Category = in.readString();
        //this.Subcategory = in.readString();
        this.CategoryId = in.readString();
        this.SubcategoryId = in.readString();
        this.DiscountPercentage = (Integer) in.readValue(Integer.class.getClassLoader());
        this.IsAvailable = (Boolean) in.readValue(Boolean.class.getClassLoader());
    }

    public static final Parcelable.Creator<Product> CREATOR = new Parcelable.Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel source) {
            return new Product(source);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    @Override
    public String toString() {
        return Name;
    }
}
