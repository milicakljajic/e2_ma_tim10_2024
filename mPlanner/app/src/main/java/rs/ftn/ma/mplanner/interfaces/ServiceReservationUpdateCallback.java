package rs.ftn.ma.mplanner.interfaces;

public interface ServiceReservationUpdateCallback {
    void onServiceReservationUpdated();
    void onUpdateError(Exception e);
    void onServiceReservationDeleted();
    void onDeleteError(Exception e);
}

