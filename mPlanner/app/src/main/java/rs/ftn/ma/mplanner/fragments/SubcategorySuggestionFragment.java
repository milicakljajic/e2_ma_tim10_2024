package rs.ftn.ma.mplanner.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.adapters.SuggestionAdapter;
import rs.ftn.ma.mplanner.model.Company;
import rs.ftn.ma.mplanner.model.EventProvider;
import rs.ftn.ma.mplanner.model.Subcategory;
import rs.ftn.ma.mplanner.model.Suggestion;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SubcategorySuggestionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SubcategorySuggestionFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public SubcategorySuggestionFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SubcategorySuggestionFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SubcategorySuggestionFragment newInstance(String param1, String param2) {
        SubcategorySuggestionFragment fragment = new SubcategorySuggestionFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_subcategory_suggestion, container, false);
        Subcategory subcategory = new Subcategory();
        Subcategory subcategory2 = new Subcategory();
        subcategory.setName("Nesto da se vidi");
        subcategory2.setId("cwfUk9WzY969AXvDYzeZ");
        EventProvider eventProvider = new EventProvider(null, "vDoSgrTd1vOGDCPuLLcnJrsSVX43", "fira1000@gmail.com", "sifrasifra", "Provajder1000", "Provajder1000", "Adresa", "312321321", false, "TODO", new Company(), new ArrayList<>(), new ArrayList<>(),null, new Date());

        Suggestion[] suggestions = new Suggestion[]{
                new Suggestion("", "Suggestion1", eventProvider, subcategory),
                new Suggestion("", "Suggestion2", eventProvider, subcategory2),
        };

        ListView lw = view.findViewById(R.id.subcategory_suggesions_list);
        ArrayList<Suggestion> sugs = new ArrayList<>(Arrays.asList(suggestions));
        SuggestionAdapter adapter = new SuggestionAdapter(requireContext(), sugs);
        lw.setAdapter(adapter);

        return view;
    }
}