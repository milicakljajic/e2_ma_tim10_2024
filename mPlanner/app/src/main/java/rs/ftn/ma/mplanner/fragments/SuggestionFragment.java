package rs.ftn.ma.mplanner.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.adapters.ProductAdapter;
import rs.ftn.ma.mplanner.interfaces.EventCallback;
import rs.ftn.ma.mplanner.model.Event;
import rs.ftn.ma.mplanner.model.Product;
import rs.ftn.ma.mplanner.repo.EventRepository;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SuggestionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SuggestionFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private EventRepository repo;
    public SuggestionFragment() {
        repo = new EventRepository();
    }


    public static SuggestionFragment newInstance(String param1, String param2) {
        SuggestionFragment fragment = new SuggestionFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_suggestion, container, false);

        repo.select(new EventCallback() {
            @Override
            public void onEventReceived(Event[] events) {
                Spinner spinner = (Spinner) view.findViewById(R.id.suggestion_fragment_spinner);
                ArrayAdapter<Event> adapter = new ArrayAdapter<Event>(
                        requireContext(),
                        android.R.layout.simple_spinner_item,
                        events
                );
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(adapter);
            }

            @Override
            public void onError(Exception e) {

            }
        }, false);


        Button button = (Button) view.findViewById(R.id.plan_budget_button);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Navigation.findNavController(view).navigate(R.id.changeToBudgetPlanFragment);
            }
        });

        Product[] products = new Product[]{new Product(1, "Photo book", "Photo book with your photos", 5000.0, "Photographs and albums", "Photo and video", 0, true),
                new Product(2, "Album with 20 photographs", "Album with 20 created photographs", 1000.0, "Photographs and albums", "Photo and video", 0, true),
                new Product(3, "Album with 50 photographs", "Album with 50 created photographs", 2000.0, "Photographs and albums", "Photo and video", 0, true)};

        //Product[] products = new Product[]{};

        ListView lw = view.findViewById(R.id.product_service_suggestion_list);
        ProductAdapter adapter2 = new ProductAdapter(requireContext(), products,1);
        lw.setAdapter(adapter2);

        return view;
    }
}