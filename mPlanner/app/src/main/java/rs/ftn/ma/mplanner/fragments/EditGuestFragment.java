package rs.ftn.ma.mplanner.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import javax.annotation.Nonnull;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.interfaces.EventCallback;
import rs.ftn.ma.mplanner.interfaces.EventUpdateCallback;
import rs.ftn.ma.mplanner.model.Event;
import rs.ftn.ma.mplanner.model.Guest;
import rs.ftn.ma.mplanner.repo.EventRepository;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EditGuestFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EditGuestFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private EditText guestNameEditText;
    private EditText guestLastNameEditText;
    private Spinner ageSpinner;
    private Switch invitedSwitch;
    private Switch acceptedSwitch;
    private Switch veganSwitch;
    private Switch vegetarianSwitch;
    private Button editGuestButton;

    private String eventId;
    private String guestName;

    private EventRepository eventRepository;
    private Event currentEvent;
    private Guest currentGuest;

    public EditGuestFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment EditGuestFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EditGuestFragment newInstance(String param1, String param2) {
        EditGuestFragment fragment = new EditGuestFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            guestName = getArguments().getString("guestName");
            eventId = getArguments().getString("eventId");
        }
    }

    @Override
    public View onCreateView(@Nonnull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit_guest, container, false);

        guestNameEditText = view.findViewById(R.id.guest_name);
        guestLastNameEditText = view.findViewById(R.id.guest_last_name);
        ageSpinner = view.findViewById(R.id.age_spinner);
        invitedSwitch = view.findViewById(R.id.invited_switch);
        acceptedSwitch = view.findViewById(R.id.accepted_switch);
        veganSwitch = view.findViewById(R.id.vegan_switch);
        vegetarianSwitch = view.findViewById(R.id.vegetarian_switch);
        editGuestButton = view.findViewById(R.id.edit_guest_button);

        eventRepository = new EventRepository();

        if (getArguments() != null) {
            eventId = getArguments().getString("eventId");
            guestName = getArguments().getString("guestName");

            loadEventAndGuest();
        }

        editGuestButton.setOnClickListener(v -> updateGuest());

        return view;
    }

    private void loadEventAndGuest() {
        eventRepository.selectById(new EventCallback() {
            @Override
            public void onEventReceived(Event[] events) {
                if (events.length > 0) {
                    currentEvent = events[0];
                    currentEvent.setId(eventId);

                    for (Guest guest : currentEvent.getGuestList()) {
                        if (guest.getName().equals(guestName)) {
                            currentGuest = guest;
                            populateGuestDetails();
                            break;
                        }
                    }
                }
            }

            @Override
            public void onError(Exception e) {
                Toast.makeText(getContext(), "Error loading event", Toast.LENGTH_SHORT).show();
            }
        }, eventId);
    }

    private void populateGuestDetails() {
        if (currentGuest != null) {
            guestNameEditText.setText(currentGuest.getName());
            guestLastNameEditText.setText(currentGuest.getLastName());
            ageSpinner.setSelection(currentGuest.getAgeCategory());
            invitedSwitch.setChecked(currentGuest.isInvited());
            acceptedSwitch.setChecked(currentGuest.hasAccepted());
            //veganSwitch.setChecked(currentGuest.isVegan());
            //vegetarianSwitch.setChecked(currentGuest.isVegetarian());
        }
    }

    private void updateGuest() {
        if (currentGuest != null) {
            currentGuest.setName(guestNameEditText.getText().toString());
            currentGuest.setLastName(guestLastNameEditText.getText().toString());
            currentGuest.setAgeCategory(ageSpinner.getSelectedItemPosition());
            currentGuest.setInvited(invitedSwitch.isChecked());
            currentGuest.setAccepted(acceptedSwitch.isChecked());
            //currentGuest.setVegan(veganSwitch.isChecked());
            //currentGuest.setVegetarian(vegetarianSwitch.isChecked());

            eventRepository.updateGuestList(eventId, currentEvent.getGuestList(), new EventUpdateCallback() {
                @Override
                public void onEventUpdated() {
                    Toast.makeText(getContext(), "Guest updated successfully", Toast.LENGTH_SHORT).show();
                    // Navigate back to guest list or another appropriate action
                }

                @Override
                public void onUpdateError(Exception e) {
                    Toast.makeText(getContext(), "Error updating guest", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}