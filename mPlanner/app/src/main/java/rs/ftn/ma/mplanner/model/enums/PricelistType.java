package rs.ftn.ma.mplanner.model.enums;

public enum PricelistType {
    PRODUCT,
    SERVICE,
    PACKAGE
}
