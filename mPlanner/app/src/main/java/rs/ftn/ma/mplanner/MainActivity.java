package rs.ftn.ma.mplanner;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;
import android.os.Bundle;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;

import java.util.HashSet;
import java.util.Set;

import rs.ftn.ma.mplanner.model.Admin;
import rs.ftn.ma.mplanner.model.Employee;
import rs.ftn.ma.mplanner.model.EventOrganizer;
import rs.ftn.ma.mplanner.model.EventProvider;

public class MainActivity extends AppCompatActivity {

    private static final String ARG_PARAM1 = "EventOrganizer";
    private static final String ARG_PARAM2 = "Employee";
    private static final String ARG_PARAM3 = "Admin";

    private static final String ARG_PARAM4 = "EventProvider";
    private EventOrganizer eo;
    private EventProvider ep;
    private AppBarConfiguration mAppBarConfiguration;
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private NavController navController;
    private Toolbar toolbar;
    private ActionBar actionBar;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private Set<Integer> topLevelDestinations = new HashSet<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);

        createNotificationChannel();


        EventOrganizer eo;
        try {
            eo = getIntent().getExtras().getParcelable("EventOrganizer");
        }
        catch (NullPointerException ex) {
            eo = null;
        }

        Employee e;
        try {
            e = getIntent().getExtras().getParcelable("Employee");
        }
        catch (NullPointerException ex) {
            e = null;
        }

        Admin a;
        try {
            a = getIntent().getExtras().getParcelable("Admin");
        }
        catch (NullPointerException ex) {
            a = null;
        }

        EventProvider ep;
        try {
            ep = getIntent().getExtras().getParcelable("EventProvider");
        }
        catch (NullPointerException ex) {
            ep = null;
        }

        // Inicijalizacija elemenata UI-a

        NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentContainerView3);
        navController = navHostFragment.getNavController();

        // Postavljamo toolbar kao glavnu traku za ovu aktivnost
        setSupportActionBar(toolbar);

        toolbar.setTitle("");

        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(false); // Isključujemo prikaz strelice ka nazad
            actionBar.setHomeAsUpIndicator(R.drawable.menu_icon); // Postavljamo ikonicu za meni
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }

        actionBarDrawerToggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mAppBarConfiguration = new AppBarConfiguration
                .Builder(R.id.nav_home, R.id.nav_profile, R.id.nav_settings)
                .setOpenableLayout(drawer)
                .build();


        NavigationUI.setupWithNavController(navigationView, navController);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        //TODO ZA OSTALE, UVEK POSLATI SVE OBJ VAMO NA PROVERU
        if(eo != null) {
            navController.navigate(R.id.changeToOdHome);
            //navController.popBackStack();
        }
        else if(e != null) {
            navController.navigate(R.id.changeToPupzNav1);
        }
        else if(a != null) {
            navController.navigate(R.id.changeToAdminHomeFragment1);
        }
        else if(ep != null) {
            navController.navigate(R.id.changeToPupvHome);
        }
        setTitle("");
    }


    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is not in the Support Library.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //admin
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_desc);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("unique-channel-id-1", name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this.
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);

            //employee
            name = getString(R.string.channel_name);
            description = getString(R.string.channel_desc);
            importance = NotificationManager.IMPORTANCE_DEFAULT;
            channel = new NotificationChannel("unique-channel-id-2", name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this.
            notificationManager.createNotificationChannel(channel);

            //provider
            name = getString(R.string.channel_name);
            description = getString(R.string.channel_desc);
            importance = NotificationManager.IMPORTANCE_DEFAULT;
            channel = new NotificationChannel("unique-channel-id-3", name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this.
            notificationManager.createNotificationChannel(channel);
        }
    }

}