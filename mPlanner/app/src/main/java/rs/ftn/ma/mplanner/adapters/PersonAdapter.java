package rs.ftn.ma.mplanner.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import rs.ftn.ma.mplanner.R;
import rs.ftn.ma.mplanner.model.Person;

public class PersonAdapter extends BaseAdapter {
    Context context;
    Person[] employees;
    LayoutInflater inflter;

    public PersonAdapter(Context applicationContext, Person[] employees) {
        this.context = context;
        this.employees = employees;
        inflter = (LayoutInflater.from(applicationContext));
    }



    @Override
    public int getCount() {
        return employees.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.fragment_employees_list_item, null);
        TextView firstname = (TextView) view.findViewById(R.id.employee_item_firstname);
        TextView lastname = (TextView) view.findViewById(R.id.employee_item_lastname);
        TextView email = (TextView) view.findViewById(R.id.employee_item_email);

        firstname.setText(employees[i].firstname);
        lastname.setText(employees[i].lastname);
        email.setText(employees[i].email);

        return view;
    }
}
